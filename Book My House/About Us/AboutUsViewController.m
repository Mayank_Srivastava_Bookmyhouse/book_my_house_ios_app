//
//  AboutUsViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "AboutUsViewController.h"
#import "MenuView.h"


static NSString * const strAbout_Us = @"http://bookmyhouse.com/api/about_us.php";
static NSString * const strNoConnectivity = @"No internet connectivity";

@interface AboutUsViewController () {
    MenuView *viewMenu;
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}

- (IBAction)actionListeners:(id)sender;

@end

@implementation AboutUsViewController


#pragma mark - View Controller mathods
- (void)loadView {
    
    [super loadView];
    
    [self settingAPI];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view.
    
    [self screenSettings];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)screenSettings {
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //Setting Background view
    viewMenu = [MenuView createView];
    //    viewMenu.frame = CGRectMake(- 2 * CGRectGetWidth(self.view.frame) / 3,  0, 2 * CGRectGetWidth(self.view.frame) / 3, CGRectGetHeight(self.view.frame));
    viewMenu.frame = CGRectMake(- 1 * CGRectGetWidth(self.view.frame),  0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    viewMenu.viewController = self;
    [self.view addSubview:viewMenu];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [viewMenu closeMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}


#pragma mark - Action Listeners
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnMenu) {
        if (viewMenu.isOpen) {
            
            [viewMenu closeMenu];
        }
        else {
            
            [viewMenu openMenu];
        }
    }
    else {
        
    }
}

#pragma mark - Touch delegates
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [viewMenu closeMenu];
}


#pragma mark - API Settings 
- (void)settingAPI {
    

    [[AppDelegate share] disableUserInteractionwithLoader:YES];
    [ServiceAPI getAboutUsWithCallBack:^(BOOL isSuccess, id data) {
        [[AppDelegate share] enableUserInteraction];
        if (isSuccess) {
            [webView loadHTMLString:[data objectForKey:@"content"] baseURL:nil];
        }
        else {
            [self.navigationController.view makeToast:strNoConnectivity duration:duration position:CSToastPositionCenter];
        }
    }];
}
@end
