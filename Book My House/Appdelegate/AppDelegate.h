//
//  AppDelegate.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <FBSDKCoreKit.h>
#import <FBSDKLoginKit.h>

#import "Header.h"


typedef void(^FacebookInfo)(BOOL isSuccess, NSDictionary *dict);
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSArray *arrSuggestiveInfo;
@property (nonatomic, copy) void(^backgroundTransferCompletionHandler)();

+ (AppDelegate *)share;
//+ (void)trackScreen:(NSString *)strScreen;
- (void)disableUserInteractionwithLoader:(BOOL)yesOrNo;
- (void)enableUserInteraction;
//+ (void)eventTracking:(NSString *)strScreen;

+ (void)leftShiftInContainer:(UICollectionView *)colView;
+ (void)rightShiftInContainer:(UICollectionView *)colView;

- (void)signInWithFacebook:(UIViewController *)controller withCallBack:(FacebookInfo)info;
- (void)signOutFromFacebook;

- (void)setInterScreenMessages:(NSString *)strMessage;

- (void)enableNoInternetAlert:(SEL)action fromClass:(UIViewController *)controller;
- (void)disableNoInternetAlert;
@end

