//
//  AppDelegate.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//


#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "UIImage+animatedGIF.h"
#import "CustomView.h"
#import "AppDelegate.h"

#import "CustomTabBarController.h"
#import "PhotoVC.h"
#import "PhotoFullScreenVC.h"
#import "NSString+FontAwesome.h"
#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)


@import GoogleMaps;
static NSString * const strGooglePlus = @"googleusercontent";


@interface AppDelegate () {
    UIView *view;
    BOOL isRegularInternetFailiure;
    FBSDKLoginManager *login;
    NoInternetView *viewNoInternet;
}

@end

@implementation AppDelegate

#pragma mark - ApplDelegates object
+ (AppDelegate *)share {
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return appDel;
}


#pragma mark - App Delegates
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
//    NSError* configureError;
//    [[GGLContext sharedInstance] configureWithError: &configureError];
//    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);

    [GMSServices provideAPIKey:strGoogleMapKey];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [Crashlytics startWithAPIKey:@"821ff1e887af55314cb3c2dce90b4aec7e7df391"];
    [Fabric with:@[[Crashlytics class]]];
    
    
    //Internet Access
    
//    [Mixpanel sharedInstanceWithToken:strMixPanelAPISecret];
//    Mixpanel *panel = [Mixpanel sharedInstance];
//    [panel track:@"App opened"];
    
    
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    [self respondToInternetDisruption];
    return YES;
}

-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    
    self.backgroundTransferCompletionHandler = completionHandler;
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    
    NSString *strUrl = [url absoluteString];
    if ([strUrl containsString:strGooglePlus]) {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    else {
            return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:[options objectForKey:UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:[options objectForKey:UIApplicationOpenURLOptionsOpenInPlaceKey]];
    }
}


#pragma mark - App Loader Mathods
- (void)disableUserInteractionwithLoader:(BOOL)yesOrNo {
    NSLog(@"%s", __FUNCTION__);
    
    if (![NSThread isMainThread]) {
        
        [self disableUserInteractionwithLoader:yesOrNo];
        return;
    }
    else {
        if (!view) {
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.window.frame), CGRectGetHeight(self.window.frame))];
            view.backgroundColor = [UIColor clearColor];
            
            if (!UIAccessibilityIsReduceTransparencyEnabled()) {
                
                UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
                UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                blurEffectView.frame = view.frame;
                blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                
                [view addSubview:blurEffectView];
            } else {
                view.backgroundColor = [UIColor blackColor];
            }
            [self.window addSubview:view];
            
            
            if (yesOrNo) {
                
                UILabel *lbl = [[UILabel alloc] initWithFrame:view.frame];
                lbl.textAlignment = NSTextAlignmentCenter;
                
                id spinner  = [NSString fontAwesomeIconStringForEnum:FACog];
                lbl.font = [UIFont fontWithName:kFontAwesomeFamilyName size:80.0f];
                lbl.textColor = [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
                lbl.text = [NSString stringWithFormat:@"%@", spinner];
                [view addSubview:lbl];
                
                CABasicAnimation *rotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
                rotate.fromValue = [NSNumber numberWithFloat:0];
                rotate.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(360)];
                rotate.duration = 5;
                rotate.repeatCount = MAXFLOAT;
                [lbl.layer addAnimation:rotate forKey:@"10"];
                
            }
            else {
            }
        }
        else {
            
        }
    }
}

- (void)enableUserInteraction {
    
    if (![NSThread isMainThread]) {
        [self enableUserInteraction];
        return;
    }
    else {
        [view removeFromSuperview];
        view = nil;
    }
}

#pragma mark - Application Reachability Test
- (void)respondToInternetDisruption {
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
            
            if (viewNoInternet) {
                [self autoResignNoInternetAlert];
            }
        }
        
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

//#pragma mark - Event Tracking
//+ (void)eventTracking:(NSString *)strScreen {
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:strScreen];
//    
//    
//}

#pragma mark - Screen Tracking
+ (void)trackScreen:(NSString *)strScreen {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:strScreen];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Arrow scrolling
+ (void)leftShiftInContainer:(UICollectionView *)colView {
    
    NSArray *visibleItems = [colView indexPathsForVisibleItems];
    NSIndexPath *currentItem = [visibleItems firstObject];
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
    [colView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    
}

+ (void)rightShiftInContainer:(UICollectionView *)colView {
    
    NSArray *visibleItems = [colView indexPathsForVisibleItems];
    NSIndexPath *currentItem = [visibleItems lastObject];
    NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
    [colView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
}

- (void)signInWithFacebook:(UIViewController *)controller withCallBack:(FacebookInfo)info{
    
    login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login logInWithReadPermissions:FACEBOOK_PERMISSION
                 fromViewController:controller
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                
                                [[AppDelegate share] disableUserInteractionwithLoader:YES];
                                if (error) {
                                    NSLog(@"Error %@", error.localizedDescription);
                                    [[AppDelegate share] enableUserInteraction];
                                }
                                else {
                                    
                                    if (result.token) {
                                        
                                        FBSDKGraphRequest *fbRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:FACEBOOK_PARAMETERS];
                                        
                                        FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
                                        [connection addRequest:fbRequest completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                            
                                            if (error) {
                                                NSLog(@"Error %@", error.localizedDescription);
                                                [[AppDelegate share] enableUserInteraction];
                                                
                                            }
                                            else {
                                                
                                                NSDictionary *dictReq = (NSDictionary *)result;
                                                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                                                if (![[dictReq objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                                                    [dict setObject:[dictReq objectForKey:@"name"] forKey:@"username"];
                                                }
                                                if (![[dictReq objectForKey:@"email"] isKindOfClass:[NSNull class]]) {
                                                    
                                                    if ([dictReq objectForKey:@"email"] != nil) {
                                                        [dict setObject:[NSString stringWithFormat:@"%@", [dictReq objectForKey:@"email"]] forKey:@"email"];
                                                    }
                                                    else {
                                                        NSString *str = [dictReq objectForKey:@"id"];
                                                        str = [str stringByAppendingString:@"@facebook.com"];
                                                        [dict setObject:str forKey:@"email"];
                                                    }
                                                    
                                                }
                                                
                                                if (![[dictReq objectForKey:@"id"] isKindOfClass:[NSNull class]]) {
                                                    
                                                    [dict setObject:[NSString stringWithFormat:@"%@", [dictReq objectForKey:@"id"]] forKey:@"fb_id"];
                                                }
                                                
                                                info(YES, dict);
                                                                                            }
                                            
                                        }];
                                        
                                        [connection start];
                                    }
                                    else {
                                        [[AppDelegate share] enableUserInteraction];
                                        // [FBSDKAccessToken currentAccessToken]
                                        
                                        info(NO, nil);
                                    }
                                }
                                
                            }];
}

- (void)signOutFromFacebook {
    
    [login logOut];
    if ([FBSDKAccessToken currentAccessToken]) {
        [FBSDKAccessToken setCurrentAccessToken:nil];
    }
    
}

#pragma mark - Messages
- (void)setInterScreenMessages:(NSString *)strMessage {
    
    [self.window makeToast:strMessage duration:2 * duration position:CSToastPositionCenter];
}



#pragma mark - No Internet Alerts
- (void)enableNoInternetAlert:(SEL)action fromClass:(UIViewController *)controller{
    
    if ([NSThread mainThread]) {
        if (!viewNoInternet) {
            viewNoInternet = [NoInternetView createView];
            viewNoInternet.lblMessage.text = @"You are offline please check your internet connection";
            viewNoInternet.action = action;
            viewNoInternet.parentClass = controller;
            
            viewNoInternet.frame = CGRectMake(-1 * CGRectGetWidth(self.window.frame), 0, CGRectGetWidth(self.window.frame), CGRectGetHeight(self.window.frame));
            
            [UIView animateWithDuration:0.5 animations:^{
                viewNoInternet.frame = self.window.frame;
            }];
            
            [viewNoInternet.btnTryAgain addTarget:controller action:action forControlEvents:UIControlEventTouchUpInside];
            [self.window addSubview:viewNoInternet];
        }
        else {
            viewNoInternet.lblMessage.text = @"Internet connections could not be established";
        }
    }
    else {
        [self enableNoInternetAlert:action fromClass:controller];
    }
}

- (void)disableNoInternetAlert {
    
    if ([NSThread isMainThread]) {
        
        [UIView animateWithDuration:0.5 animations:^{
            viewNoInternet.frame = CGRectMake(2 * CGRectGetWidth(self.window.frame), 0, CGRectGetWidth(self.window.frame), CGRectGetHeight(self.window.frame));
        } completion:^(BOOL finished) {
            
            [viewNoInternet removeFromSuperview];
            viewNoInternet = nil;
        }];
        
    }
    else {
        [self disableNoInternetAlert];
    }
}

- (void)autoResignNoInternetAlert {
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        viewNoInternet.frame = CGRectMake(2 * CGRectGetWidth(self.window.frame), 0, CGRectGetWidth(self.window.frame), CGRectGetHeight(self.window.frame));
        
    } completion:^(BOOL finished) {
        [viewNoInternet.btnTryAgain sendActionsForControlEvents:UIControlEventTouchUpInside];
        [viewNoInternet removeFromSuperview];
        viewNoInternet = nil;
    }];
    
}

#pragma mark - Orientation
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window  {
    
    UIViewController *controller = [self topViewController];
    if ([controller isKindOfClass:[PhotoFullScreenVC class]]) {
        PhotoFullScreenVC *obj = (PhotoFullScreenVC *)controller;
            if (obj.beingDismissed) {
                return UIInterfaceOrientationMaskPortrait;
            }
            else if (obj.isBeingPresented){
                
                return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
            }
            else if (obj.isMovingToParentViewController) {
                return UIInterfaceOrientationMaskPortrait;
            }
            else {
                return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
            }
    }
//    else if ([controller isKindOfClass:[UINavigationController class]]) {
//        UINavigationController *nav = (UINavigationController *)controller;
//        if ( [[nav.viewControllers lastObject] isKindOfClass:[CustomTabBarController class]]) {
//            CustomTabBarController *obj = (CustomTabBarController *)[nav.viewControllers lastObject];
//            if (obj.beingDismissed) {
//                return UIInterfaceOrientationMaskPortrait;
//            }
//            else if (obj.isBeingPresented){
//                
//                return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
//            }
//            else if (obj.isMovingToParentViewController) {
//                return UIInterfaceOrientationMaskPortrait;
//            }
//            else if (obj.preferredInterfaceOrientationForPresentation == UIInterfaceOrientationLandscapeLeft || obj.preferredInterfaceOrientationForPresentation == UIInterfaceOrientationLandscapeRight ) {
//                
//               return UIInterfaceOrientationMaskPortrait;
//            }
//            else {
//                return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
//            }
//        }
//    }
    
    return UIInterfaceOrientationMaskPortrait;
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

@end
