//
//  AuxillaryMathods.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuxillaryMathods : NSObject
+ (BOOL)isValidEmail:(NSString *)strEmail;
+ (BOOL)isValidPassword:(NSString *)strPassword;
+ (BOOL)isUserName:(NSString *)strName;
+ (BOOL)isCorrectLocation:(NSString *)strLocation;
+ (BOOL)isValidMobileNumber:(NSString *)strMobileNumber;
+ (BOOL)validatePanCardNumber:(NSString *)cardNumber;
+ (BOOL)isValidPIN:(NSString *)strPIN;
@end
