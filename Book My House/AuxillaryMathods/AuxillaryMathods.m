//
//  AuxillaryMathods.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "AuxillaryMathods.h"

@implementation AuxillaryMathods


+ (BOOL)isValidEmail:(NSString *)strEmail {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:strEmail];
}

+ (BOOL)isValidPassword:(NSString *)strPassword {
    
    if (strPassword.length >= 6) {
        return YES;
    }
    else {
        return NO;
    }
    
}


+ (BOOL)isUserName:(NSString *)strName {
    
    NSRange r = [strName rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_ "] invertedSet]];
    if (r.location != NSNotFound) {
        return NO;
    }
    else {
        return YES;
    }
    
}

+ (BOOL)isCorrectLocation:(NSString *)strLocation {
    
    NSRange r = [strLocation rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "] invertedSet]];
    if (r.location != NSNotFound) {
        return NO;
    }
    else {
        return YES;
    }
}

+ (BOOL)isValidMobileNumber:(NSString *)strMobileNumber {
    NSString *phoneRegex = @"[789][0-9]{9}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:strMobileNumber];
}

+ (BOOL)validatePanCardNumber:(NSString *)cardNumber {
    
    NSString *emailRegex = @"^[A-Z]{5}[0-9]{4}[A-Z]$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [cardTest evaluateWithObject:cardNumber];
}

+ (BOOL)isValidPIN:(NSString *)strPIN {
    NSRange r = [strPIN rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]];
    if (r.location != NSNotFound) {
        return NO;
    }
    else {
        return YES;
    }
}
@end
