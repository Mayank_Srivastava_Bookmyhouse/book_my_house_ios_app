//
//  LocationHandler.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/11/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <CoreLocation/CoreLocation.h>
//#import <CoreLocation/CoreLocation.h>

@import CoreLocation;

@protocol LocationHandlerDelegate <NSObject>

- (void)getCurrentLocation:(CLLocation *)location;

@end
@interface LocationHandler : NSObject
@property (weak, nonatomic) id<LocationHandlerDelegate> delegate;
+ (id)sharedManager;
- (void)startUpdatingGPSLocation;
- (void)stopUpdatingGPSLocation;
@end
