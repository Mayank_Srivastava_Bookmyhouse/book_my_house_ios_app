//
//  LocationHandler.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/11/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "LocationHandler.h"


@interface LocationHandler ()<CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@end
@implementation LocationHandler
@synthesize delegate;

+ (id)sharedManager {
    
    static LocationHandler *obj;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        obj = [[self alloc] init];
    });
    
    return obj;
}


- (id)init {
    self = [super init];
    
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.distanceFilter = 1000;
        self.locationManager.pausesLocationUpdatesAutomatically = YES;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    
    return self;
}


- (void)startUpdatingGPSLocation {
    [_locationManager startMonitoringSignificantLocationChanges];
    [_locationManager startUpdatingLocation];
    
}

- (void)stopUpdatingGPSLocation {
    [_locationManager stopUpdatingLocation];
    [_locationManager stopMonitoringSignificantLocationChanges];

}


#pragma mark - Corelocation delegates
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [delegate getCurrentLocation:[locations lastObject]];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"Error %@", error.localizedDescription);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    NSLog(@"Authorisation status %d", status);
}
@end
