//
//  CCAvenueVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/23/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentModal.h"
#import "Header.h"
#import "SearchViewController.h"

@interface CCAvenueVC : UIViewController<UIWebViewDelegate>
@end
