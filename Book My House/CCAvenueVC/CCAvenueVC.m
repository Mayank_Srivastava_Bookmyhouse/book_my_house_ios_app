//
//  CCAvenueVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/23/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CCAvenueVC.h"



@interface CCAvenueVC () {
    
    __weak IBOutlet UIWebView *web;
    __weak IBOutlet UIActivityIndicatorView *indicator;
    __weak IBOutlet UIButton *btnClose;
    
    PaymentModal *manager;
    NSDictionary *dictRequest;
}

- (IBAction)actionListener:(id)sender;
@end

@implementation CCAvenueVC


#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void)loadView {
    
    manager = [PaymentModal sharedManager];
    dictRequest = manager.dictActionForm;
    [super loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    manager = [PaymentModal sharedManager];
     dictRequest = manager.dictActionForm;
    [self loadAPI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)updateScreenSettings {
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        
    }
}

#pragma mark - API Action

- (void)loadAPI {

    NSString *strContent1 = [NSString stringWithFormat:@"%@&order_id=%@&encRequest=%@&access_code=%@", [dictRequest objectForKey:@"post_url"], [dictRequest objectForKey:@"order_id"], [dictRequest objectForKey:@"encRequest"], [dictRequest objectForKey:@"access_code"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strContent1]];
    [web loadRequest:request];
    
}


#pragma mark - Web View delegates
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if (navigationType == UIWebViewNavigationTypeFormSubmitted) {
        
//        for (UIViewController *viewController in self.navigationController.viewControllers) {
//            
//            if ([viewController isKindOfClass:[SearchViewController class]]) {
//                SearchViewController *obj = (SearchViewController *)viewController;
//                [self.navigationController popToViewController:obj animated:YES];
//            }
//        }
    }
    else {
        
    }
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [indicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [indicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
}



@end
