//
//  CommentCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/2/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
