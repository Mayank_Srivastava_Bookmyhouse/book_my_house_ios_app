//
//  CommentsVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/2/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentCell.h"
#import "Header.h"

typedef NS_ENUM(NSUInteger, CommentsVCAccessOptions) {
    CommentsVCAccessOptionsFeaturedProjects,
    CommentsVCAccessOptionsUnitDetails,
    CommentsVCAccessOptionsMisc,
};

@interface CommentsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSDictionary *dictSourseInfo;
@property (assign, nonatomic) CommentsVCAccessOptions options;
@end
