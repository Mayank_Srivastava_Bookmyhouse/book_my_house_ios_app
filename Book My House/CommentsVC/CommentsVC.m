//
//  CommentsVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/2/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CommentsVC.h"

@interface CommentsVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UITableView *tblView;
    
    NSArray *arrComments;
}

- (IBAction)actionListener:(id)sender;
@end

@implementation CommentsVC


#pragma mark - View Controller Mathods

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
    [self loadCommentList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    
    tblView.rowHeight = 63;
    tblView.estimatedRowHeight = UITableViewAutomaticDimension;
}


#pragma mark - API Actions
- (void)loadCommentList {
    if (_dictSourseInfo != nil) {
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getAllReviewWith:_dictSourseInfo  withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                arrComments = (NSArray *)data;
                [tblView reloadData];
            }
            else {
                [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
            }
        }];
    }
    else {
        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}



#pragma mark - Action Listener
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Table View delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrComments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Comments"];
    NSDictionary *dict = [arrComments objectAtIndex:indexPath.row];

    if (![[dict objectForKey:@"comment_user_name"] isKindOfClass:[NSNull class]]) {
        cell.lblName.text = [dict objectForKey:@"comment_user_name"];
    }
    else {
        cell.lblName.text = @"--";
    }
    
    if (![[dict objectForKey:@"comment_title"] isKindOfClass:[NSNull class]]) {
        cell.lblTitle.text = [dict objectForKey:@"comment_title"];
    }
    else {
        cell.lblTitle.text = @"--";
    }
    
    if (![[dict objectForKey:@"comment_description"] isKindOfClass:[NSNull class]]) {
        cell.lblDescription.text = [dict objectForKey:@"comment_description"];
    }
    else {
        cell.lblDescription.text = @"";
    }
    
    if (![[dict objectForKey:@"rating"] isKindOfClass:[NSNull class]]) {
        cell.starRating.rating = [[dict objectForKey:@"rating"] floatValue];
        
        cell.starRating.starImage = [UIImage imageNamed:@"star-template"];
        cell.starRating.starHighlightedImage = [UIImage imageNamed:@"star-highlighted-template"];
        cell.starRating.maxRating = 5.0;
        cell.starRating.horizontalMargin = 12;
        cell.starRating.editable = NO;
        cell.starRating.displayMode = EDStarRatingDisplayHalf;
        cell.starRating.tag = indexPath.row;
        
    }
    else {
        
    }
    
    
    
    return cell;
}
@end
