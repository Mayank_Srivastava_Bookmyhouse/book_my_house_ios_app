//
//  ContactCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/19/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceholderTextView.h"

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIPlaceholderTextView *txtMessage;
+ (ContactCell *)createTitleCell;
+ (ContactCell *)createMessageCell;
+ (ContactCell *)createButtonCell;
@end

@interface ContactCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCaption;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

+ (ContactCell1 *)createCell;
@end
