//
//  ContactCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/19/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell


#pragma mark - Cell Producers
+ (ContactCell *)createTitleCell {
    
    ContactCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactCell" owner:self options:nil] firstObject];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(cell.frame))];
    view.backgroundColor = [UIColor clearColor];
    
    cell.txtTitle.leftView = view;
    cell.txtTitle.leftViewMode = UITextFieldViewModeAlways;
    
    return cell;
}


+ (ContactCell *)createMessageCell {
    ContactCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactCell" owner:self options:nil] objectAtIndex:2];
    
    return cell;
}


+ (ContactCell *)createButtonCell {
    ContactCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactCell" owner:self options:nil] objectAtIndex:1];
    return cell;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

@implementation ContactCell1

+ (ContactCell1 *)createCell {
    ContactCell1 *cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactCell" owner:self options:nil] objectAtIndex:3];
    
    return cell;
}


@end
