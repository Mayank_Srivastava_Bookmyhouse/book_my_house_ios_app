//
//  ContactUsViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MessageUI/MessageUI.h>
#import "ContactCell.h"
#import "Header.h"

@interface ContactUsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@end
