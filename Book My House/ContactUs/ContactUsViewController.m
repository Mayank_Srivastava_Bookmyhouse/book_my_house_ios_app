//
//  ContactUsViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ContactUsViewController.h"
#import "MenuView.h"
#import "AuxillaryMathods.h"


static NSString * const strFillFirstName             = @"Please fill your first name";
static NSString * const strFillValidFirstName        = @"Please fill your valid first name";

static NSString * const strFillLastName             = @"Please fill your last name";
static NSString * const strFillValidLastName        = @"Please fill your valid last name";

static NSString * const strFillEmail                 = @"Please fill your email id";
static NSString * const strValidEmail                = @"Please fill your valid email id";

static NSString * const strFillMobileNo              = @"Please give your mobile number";
static NSString * const strFillMessage               = @"Please give your message";
static NSString * const strValidNumber               = @"Please enter valid number";
static NSString * const strRequestSuccess            = @"Request successfully sent";
static NSString * const strRequestFailed             = @"Request could not be sent";

@interface ContactUsViewController () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UITableView *tblView;
    
    MenuView *viewMenu;
    int direction;
    int shakes;
    
    NSMutableDictionary *dict;
    NSArray *arrHeaders, *arrCaptions, *arrImages, *arrBMHInfo;
    NSMutableArray *arrInformation;
}
- (IBAction)actionListeners:(id)sender;

@end

@implementation ContactUsViewController


#pragma mark - View Controller Mathods
- (void)loadView {
    
    arrHeaders = @[@"Get in touch", @"Contact Form"];
    arrCaptions = @[@"Drop on in", @"Give us a call", @"Connect online"];//BMH_Location, BMH_Contact, BMH_Connect
    arrImages = @[@"BMH_Location", @"BMH_Contact", @"BMH_Connect"];
    arrInformation = [NSMutableArray array];
    [super loadView];
    [self loadContactInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view.
    [self screenSettings];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    viewMenu = [MenuView createView];
    viewMenu.frame = CGRectMake(- 1 * CGRectGetWidth(self.view.frame),  0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    viewMenu.viewController = self;
    [self.view addSubview:viewMenu];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [viewMenu closeMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - API Action
- (void)loadContactInfo {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getBMHContactInfo:^(BOOL isSucess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSucess) {
                
                if ([data isKindOfClass:[NSDictionary class]]) {
                    
                    NSString *str;
                    [arrInformation addObject:[data objectForKey:@"address"]];
                    str = [NSString stringWithFormat:@"Office: %@", [data objectForKey:@"phone"]];
                    [arrInformation addObject:str];
                    str = [NSString stringWithFormat:@"Email: %@\n%@",[data objectForKey:@"email1"], [data objectForKey:@"email2"]];
                    [arrInformation addObject:str];
                    
                    [tblView reloadData];
                    
                }
                else {
                    
                }
            }
            NSLog(@"Data %@", data);
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadContactInfo) fromClass:self];
    }
   
    
    
}

#pragma mark - Action Listener
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnMenu) {
        if (viewMenu.isOpen) {
            
            [viewMenu closeMenu];
        }
        else {
            
            [viewMenu openMenu];
        }
    }
    else {
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
            [[AppDelegate share] disableNoInternetAlert];
            
            direction = 1;
            shakes = 0;
            
            NSString *str, *strEmail, *strContact, *strMessage, *strFirstName, *strLastName;
            ContactCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
            
            str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (str.length == 0) {
                [self.view makeToast:strFillFirstName duration:duration position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                
                return;
            }
            else if (![AuxillaryMathods isUserName:str]) {
                [self.view makeToast:strFillValidFirstName duration:duration position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                return;
            }
            else {
                strFirstName = str;
            }
            
            
            cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:1]];
            
            str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (str.length == 0) {
                [self.view makeToast:strFillLastName duration:duration position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                
                return;
            }
            else if (![AuxillaryMathods isUserName:str]) {
                [self.view makeToast:strFillValidLastName duration:duration position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                return;
            }
            else {
                strLastName = str;
            }
            
            
            cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:1]];
            str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (str.length == 0) {
                [self.view makeToast:strFillEmail
                                                 duration:duration
                                                 position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                return;
            }
            else if (![AuxillaryMathods isValidEmail:str]) {
                [self.view makeToast:strValidEmail
                                                 duration:duration
                                                 position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                return;
            }
            else {
                strEmail = str;
            }
            
            cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:1]];
            str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (str.length == 0) {
                [self.view makeToast:strFillMobileNo
                                                 duration:duration
                                                 position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                return;
            }
            else if (![AuxillaryMathods isValidMobileNumber:str]) {
                [self.view makeToast:strValidNumber
                                                 duration:duration
                                                 position:CSToastPositionCenter];
                [self shake:cell.txtTitle];
                return;
            }
            else {
                strContact = str;
            }
            
            cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:4 inSection:1]];
            str = [cell.txtMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (str.length == 0) {
                [self.view makeToast:strFillMessage
                                                 duration:duration
                                                 position:CSToastPositionCenter];
                [self shakeView:cell.txtMessage];
                return;
            }
            else {
                strMessage = str;
            }
            
            NSMutableDictionary *dictReq = [NSMutableDictionary dictionary];
            [dictReq setObject:strFirstName forKey:@"firstname"];
            [dictReq setObject:strLastName forKey:@"lastname"];
            [dictReq setObject:strEmail forKey:@"email"];
            [dictReq setObject:strMessage forKey:@"message"];
            [dictReq setObject:strContact forKey:@"contactno"];
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI contactUs:dictReq andCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    
                    for (UIViewController *obj in self.navigationController.viewControllers) {
                        
                        if ([obj isKindOfClass:[SearchViewController class]]) {
                            SearchViewController *searchObj = (SearchViewController *)obj;
                            [self.navigationController popToViewController:searchObj animated:NO];
                            [[AppDelegate share] setInterScreenMessages:strRequestSuccess];
                        }
                    }
                    
                    
                }
                else {
                    [self.view makeToast:strRequestFailed duration:0.7 * duration position:CSToastPositionCenter];
                }
                
            }];
        }
        else {
            [[AppDelegate share] enableNoInternetAlert:@selector(actionListeners:) fromClass:self];
        }
        
    }
         
         
}

#pragma mark - Screen Settings
- (void)screenSettings {
    dict = [NSMutableDictionary dictionary];
}

#pragma mark - Touch delegates
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [viewMenu closeMenu];
}


#pragma mark - TableView Delegates 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrHeaders.count;
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return arrInformation.count;
    }
    else {
        return 6;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.00f;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        if (arrInformation.count) {
            return 45;
        }
        else {
            return 0;
        }
    }
    else
        return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 45)];
    view.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame))];
    lbl.font = [UIFont fontWithName:@"PT Sans" size:20.0];
    lbl.text = [arrHeaders objectAtIndex:section];
    [view addSubview:lbl];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return 220;
    }
    else {
        return UITableViewAutomaticDimension;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
    }
    else {
        if (indexPath.row < 4) {
            return 50;
        }
        else if (indexPath.row == 4) {
            return 92;
        }
        else {
            return 40;
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        
        ContactCell1 *cell = [tableView  dequeueReusableCellWithIdentifier:@"cell4"];
        
        if (!cell) {
            cell = [ContactCell1 createCell];
            cell.lblDescription.tag = indexPath.row;
            cell.lblCaption.text = [arrCaptions objectAtIndex:indexPath.row];
            cell.imgCaption.image = [UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
            cell.lblDescription.text = [arrInformation objectAtIndex:indexPath.row];
        }
        return cell;
    }
    else if (indexPath.section == 1) {
        if (indexPath.row < 4) {
            ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
            
            if (!cell) {
                cell = [ContactCell createTitleCell];
            }
            cell.txtTitle.delegate = self;
            
            if (indexPath.row == 0) {
                cell.txtTitle.placeholder = @"First Name";
            }
            else if (indexPath.row == 1) {
                cell.txtTitle.placeholder = @"Last Name";
            }
            else if (indexPath.row == 2) {
                cell.txtTitle.placeholder = @"Email";
                cell.txtTitle.keyboardType = UIKeyboardTypeEmailAddress;
            }
            else if (indexPath.row == 3) {
                cell.txtTitle.placeholder = @"Mobile Number";
                cell.txtTitle.keyboardType = UIKeyboardTypeNumberPad;
            }
            
            return cell;
            
        }
        else if (indexPath.row == 4) {
            ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell"];
            
            if (!cell) {
                cell = [ContactCell createMessageCell];
                cell.txtMessage.placeholder = @"Message";
                cell.txtMessage.placeholderColor = [UIColor lightGrayColor];
                cell.txtMessage.delegate = self;
            }
            
            
            return cell;
        }
        
        else {
            
            ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell"];
            
            
            if (!cell) {
                cell = [ContactCell createButtonCell];
                cell.btnSend.layer.cornerRadius = 5.0;
                [cell.btnSend addTarget:self action:@selector(actionListeners:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            return cell;
        }
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
        }
        else if (indexPath.row == 1) {
            NSString *str = [arrInformation objectAtIndex:indexPath.row];
            str = [str substringFromIndex:8];
            NSString *phoneNumber = [@"tel://" stringByAppendingString:str];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
        }
        else if (indexPath.row == 2) {
            
        }
    }
}


#pragma mark - Shake text Field
-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

- (void)shakeView:(UITextView *)theOneYouWannaShake {
    
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shakeView:theOneYouWannaShake];
     }];

}


#pragma mark - TextField Delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    ContactCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
    
    if (textField == cell.txtTitle) {
        return newLength <= 30;
    }
    
    cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:1]];
    
    if (textField == cell.txtTitle) {
        return newLength <= 30;
    }
    cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:1]];
    
    if (textField == cell.txtTitle) {
        return newLength <= 40;
    }
    
    cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:1]];
    
    if (textField == cell.txtTitle) {
        return newLength <= 10;
    }
    return 0;
}

#pragma mark - Text View Delegates


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if (range.length + range.location > textView.text.length) {
        
        return NO;
    }
    
    NSUInteger newLength = textView.text.length + text.length - range.length;
    return newLength <= 500;
}



@end
