//
//  CustomVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 20/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomVC : UIViewController
@property (strong, nonatomic) NSString *strTitle;
@property (strong, nonatomic) NSString *strHTMLContent;
@end
