//
//  CustomVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 20/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CustomVC.h"

@interface CustomVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIWebView *web;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation CustomVC

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self screenSettings];
}





#pragma mark - Screen Settings
- (void)screenSettings {
    lblTitle.text = _strTitle;
    [web loadHTMLString:_strHTMLContent baseURL:nil];
}


#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
