//
//  CustomTableCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomTableCellDelegate <NSObject>

- (void)signUpButtonClicked;

@end



@interface CustomTableCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPolicy;
@property (weak, nonatomic) IBOutlet UIButton *btnsignUp;
@property (weak, nonatomic) id<CustomTableCellDelegate> delegate;


+ (CustomTableCell *)createCellWithDelegate:(id)delegate;
+ (CustomTableCell *)createInstructionCell;
+ (CustomTableCell *)createSignUpCellWithdelegate:(id<CustomTableCellDelegate>)delegate;


- (IBAction)actionListeners:(id)sender;
@end
