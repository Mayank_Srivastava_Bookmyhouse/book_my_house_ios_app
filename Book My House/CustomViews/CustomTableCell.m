//
//  CustomTableCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CustomTableCell.h"

@implementation CustomTableCell


#pragma mark - Top Cells
+ (CustomTableCell *)createCellWithDelegate:(id)delegate {
    
    CustomTableCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"CustomTableCell" owner:self options:nil] firstObject];
    cell.txtTitle.delegate = delegate;
    cell.txtTitle.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(cell.txtTitle.frame))];
    cell.txtTitle.leftViewMode = UITextFieldViewModeAlways;
    
    return cell;
}



#pragma mark - Instruction Cell
+ (CustomTableCell *)createInstructionCell {
    CustomTableCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"CustomTableCell" owner:self options:nil] objectAtIndex:1];
    
    return cell;
    
}


#pragma mark - Sign Up Cell
+ (CustomTableCell *)createSignUpCellWithdelegate:(id<CustomTableCellDelegate>)delegate {
    CustomTableCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"CustomTableCell" owner:self options:nil] objectAtIndex:2];
    cell.delegate = delegate;
    return cell;
}


#pragma mark - Action Listeners
- (IBAction)actionListeners:(id)sender {
    [_delegate signUpButtonClicked];
}


- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
