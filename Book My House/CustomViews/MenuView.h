//
//  MenuView.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Header.h"
#import "SearchViewController.h"
#import "HomeViewController.h"
#import "AboutUsViewController.h"
#import "ContactUsViewController.h"
#import "FavouriteVC.h"

@interface MenuView : UIView<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnGooglePlus;
@property (weak, nonatomic) IBOutlet UILabel *lblBookMark;

@property (strong, nonatomic) UIViewController *viewController;
@property (assign, nonatomic) BOOL isOpen;
- (IBAction)actionListener:(id)sender;
+ (MenuView *)createView;
- (void)openMenu;
- (void)closeMenu;
@end
