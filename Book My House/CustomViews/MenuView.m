//
//  MenuView.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "MenuView.h"
#import "SigninViewController.h"

static NSString * const strLoginForFav = @"Please Login to get your favorite list";
@implementation MenuView

#pragma mark - View Creation
+ (MenuView *)createView {
    
    MenuView *aView = [[[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil] firstObject];
     aView.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    aView.lblBookMark.text = [NSString stringWithFormat:@"\u00A9 Bookmyhouse (version %@)",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    return aView;
}


#pragma mark - Handle menu appearence and closure
- (void)closeMenu {
    [UIView animateWithDuration:0.5 animations:^{
        self.transform = CGAffineTransformIdentity;
        self.isOpen = NO;
    }];
}

- (void)openMenu {
    
    [UIView animateWithDuration:0.5 animations:^{
        self.transform = CGAffineTransformMakeTranslation(CGRectGetWidth(self.frame), 0);
        self.isOpen = YES;
    }];
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnFacebook) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/355356557838717"]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strFacebook]];
        }
    }
    else if (sender == _btnGooglePlus) {
        //
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/355356557838717"]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strGoogle_Plus]];
        }
        
    }
    else {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/355356557838717"]];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strTwitter]];
        }
    }
}



#pragma mark - Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
    
    if (strUserId == nil) {
        return 0.0;
    }
    else {
        return 100.0;
    }
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
    
    if (strUserId) {
        UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 100.0)];

        aView.backgroundColor = [UIColor whiteColor];
        UILabel *lbl = [[UILabel alloc] initWithFrame:aView.frame];
        lbl.numberOfLines = 0;
        lbl.lineBreakMode = NSLineBreakByWordWrapping;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.textColor = [UIColor grayColor];
        [aView addSubview:lbl];
        
        NSString *strEmail = [[NSUserDefaults standardUserDefaults] objectForKey:strSavedName];
        if (strEmail) {
            lbl.text = strEmail;
            
        }
        else {
            lbl.text = @"No name";
        }
        
        return aView;
        
    }
    else {
        return nil;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *strId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:17.0];
        
    }
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Home";
    }
    else if (indexPath.row == 1) {
        NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        
        if (strUserId == nil) {
            cell.textLabel.text = @"Login";
        }
        else {
            cell.textLabel.text = @"Logout";
        }
    }
    else if (indexPath.row == 2) {
        cell.textLabel.text = @"About Us";
    }
    else if (indexPath.row == 3) {
        cell.textLabel.text = @"Contact Us";
    }
    else {
        cell.textLabel.text = @"Favorites";
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL isViewAvailable = NO;
    UIViewController *target = nil;
    
    if (indexPath.row == 0) {
        
        if ([_viewController isKindOfClass:[SearchViewController class]]) {
            [self closeMenu];
        }
        else {
            
            for (UIViewController *obj in _viewController.navigationController.viewControllers) {
                
                if ([obj isKindOfClass:[SearchViewController class]]) {
                    isViewAvailable = YES;
                    target = obj;
                    break;
                    
                }
                else {
                    isViewAvailable = NO;
                    target = nil;
                }
            }
            
            if (isViewAvailable) {
                SearchViewController *searchObj = (SearchViewController *)target;
                [_viewController.navigationController popToViewController:searchObj animated:NO];
                
            }
            else {
                SearchViewController *obj = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
                [_viewController.navigationController pushViewController:obj animated:NO];
            }
            
        }
    }
    else if (indexPath.row == 1) {
        for (UIViewController *obj in _viewController.navigationController.viewControllers) {
            
            if ([obj isKindOfClass:[HomeViewController class]]) {
                
                NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
                NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
                
                if (strUserId != nil) {
                    
                    [pref removeObjectForKey:strSaveUserId];
                    [pref removeObjectForKey:strSaveUserLogin];
                    [pref removeObjectForKey:strSavedName];
                    [pref removeObjectForKey:strSaveUserEmail];
                    [pref removeObjectForKey:strSaveUserType];
                    [pref removeObjectForKey:strSaveUserIsTokenPaid];
                    [pref removeObjectForKey:strSaveUserId];
                    
                    if ([[pref objectForKey:test_mode] isEqualToString:test_mode_facebook]) {
                        [[AppDelegate share] signOutFromFacebook];
                        [pref removeObjectForKey:test_mode];
                    }
                    
                    if ([[pref objectForKey:test_mode] isEqualToString:test_mode_google]) {
                        [[GIDSignIn sharedInstance] signOut];
                        [[GIDSignIn sharedInstance] disconnect];
                        [pref removeObjectForKey:test_mode];
                    }
                    
                    [pref removeObjectForKey:strSaveCityInfo];
                    
                    [self removeFromSuperview];
                    HomeViewController *objHome = (HomeViewController *)obj;
                    [_viewController.navigationController popToViewController:objHome animated:NO];
                    break;
                }
                else {
                    [self removeFromSuperview];
                    SigninViewController *objSignIn = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
                    objSignIn.option = SigninAccessOptionHomePage;
                    [_viewController.navigationController pushViewController:objSignIn animated:NO];
                    break;
                }
                
            }
        }
    }
    else if (indexPath.row == 2) {
        if ([_viewController isKindOfClass:[AboutUsViewController class]]) {
            [self closeMenu];
        }
        else {
            
            for (UIViewController *obj in _viewController.navigationController.viewControllers) {
                
                if ([obj isKindOfClass:[AboutUsViewController class]]) {
                    isViewAvailable = YES;
                    target = obj;
                    break;
                }
                else {
                    isViewAvailable = NO;
                    target = nil;
                }
            }
            
            if (isViewAvailable) {
                AboutUsViewController *obj = (AboutUsViewController *)target;
                [_viewController.navigationController popToViewController:obj animated:NO];
            }
            else {
                AboutUsViewController *obj = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
                [_viewController.navigationController pushViewController:obj animated:NO];
            }
        }
    }
    else if (indexPath.row == 3) {
        
        
        
        
        if ([_viewController isKindOfClass:[ContactUsViewController class]]) {
            [self closeMenu];
        }
        else {
            
            for (UIViewController *obj  in _viewController.navigationController.viewControllers) {
                
                if ([obj isKindOfClass:[ContactUsViewController class]]) {
                    isViewAvailable = YES;
                    target = obj;
                    break;
                }
                else {
                    isViewAvailable = NO;
                    target = nil;
                }
                
            }
            
            if (isViewAvailable) {
                ContactUsViewController *obj = (ContactUsViewController *)target;
                [_viewController.navigationController popToViewController:obj animated:NO];
            }
            else {
                ContactUsViewController *obj = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
                [_viewController.navigationController pushViewController:obj animated:NO];
            }
            
        }
    }
    else {
        
        NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        
        if (strUserId != nil) {
            if ([_viewController isKindOfClass:[FavouriteVC class]]) {
                [self closeMenu];
            }
            else {
                
                for (UIViewController *obj in _viewController.navigationController.viewControllers) {
                    
                    if ([obj isKindOfClass:[FavouriteVC class]]) {
                        isViewAvailable = YES;
                        target = obj;
                        break;
                    }
                    else {
                        isViewAvailable = NO;
                        target = nil;
                    }
                }
                
                if (isViewAvailable) {
                    FavouriteVC *obj = (FavouriteVC *)target;
                    [_viewController.navigationController popToViewController:obj animated:NO];
                }
                else {
                    
                    FavouriteVC *obj = [_viewController.storyboard instantiateViewControllerWithIdentifier:@"FavouriteVC"];
                    [_viewController.navigationController pushViewController:obj animated:NO];
                }
            }
        }
        else {
            //[_viewController.navigationController.view makeToast:strLoginForFav duration:duration position:CSToastPositionCenter];
            
            [[[AppDelegate share] window] makeToast:strLoginForFav duration:2 * duration position:CSToastPositionCenter];
            //[_viewController.navigationController popToRootViewControllerAnimated:NO];
            
            if ([_viewController isKindOfClass:[SearchViewController class]]) {
                
                SearchViewController *controller = (SearchViewController *)_viewController;
                SigninViewController *obj = [controller.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
                

                obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                obj.modalPresentationStyle = UIModalPresentationFormSheet;
                obj.option = SigninAccessOptionFavorite;
                
                UINavigationController *navigationcontroller = [[UINavigationController alloc] initWithRootViewController:obj];
                [controller presentViewController:navigationcontroller animated:YES completion:nil];
            }
            else if ([_viewController isKindOfClass:[AboutUsViewController class]]) {
                AboutUsViewController *controller = (AboutUsViewController *)_viewController;
                SigninViewController *obj = [controller.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
                obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                obj.modalPresentationStyle = UIModalPresentationFormSheet;
                obj.option = SigninAccessOptionFavorite;
                UINavigationController *navigationcontroller = [[UINavigationController alloc] initWithRootViewController:obj];
                [controller presentViewController:navigationcontroller animated:YES completion:nil];
            }
            else if ([_viewController isKindOfClass:[ContactUsViewController class]] ) {
                ContactUsViewController *controller = (ContactUsViewController *)_viewController;
                SigninViewController *obj = [controller.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
                obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                obj.modalPresentationStyle = UIModalPresentationFormSheet;
                obj.option = SigninAccessOptionFavorite;
                UINavigationController *navigationcontroller = [[UINavigationController alloc] initWithRootViewController:obj];
                [controller presentViewController:navigationcontroller animated:YES completion:nil];
            }
            else if ([_viewController isKindOfClass:[FavouriteVC class]] ) {
                FavouriteVC *controller = (FavouriteVC *)_viewController;
                SigninViewController *obj = [controller.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
                obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                obj.modalPresentationStyle = UIModalPresentationFormSheet;
                obj.option = SigninAccessOptionFavorite;
                UINavigationController *navigationcontroller = [[UINavigationController alloc] initWithRootViewController:obj];
                [controller presentViewController:navigationcontroller animated:YES completion:nil];
            }
            
        }
    }
}
@end
