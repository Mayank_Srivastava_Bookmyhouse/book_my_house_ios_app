//
//  UIPlaceholderTextView.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/19/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "UIPlaceholderTextView.h"

@interface UIPlaceholderTextView ()
@property (nonatomic, retain) UILabel *placeHolderLabel;
@end

@implementation UIPlaceholderTextView

CGFloat const UI_PLACEHOLDER_TEXT_CHANGED_ANIMATION_DURATION = 0.25;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if (!self.placeholder) {
        self.placeholder = @"";
    }
    
    if (!self.placeholderColor) {
        self.placeholderColor = [UIColor lightGrayColor];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
}


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.placeholder = @"";
        self.placeholderColor = [UIColor lightGrayColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    
    
    return self;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)setText:(NSString *)text {
    
    [super setText:text];
    [self textChanged:nil];
}


- (void)textChanged:(NSNotification *)notification {
    
    if(self.placeholder.length == 0)
    {
        return;
    }
    
    [UIView animateWithDuration:UI_PLACEHOLDER_TEXT_CHANGED_ANIMATION_DURATION animations:^{
        if([[self text] length] == 0)
        {
            [[self viewWithTag:999] setAlpha:1];
        }
        else
        {
            [[self viewWithTag:999] setAlpha:0];
        }
    }];
}


- (void)drawRect:(CGRect)rect {
    
    if (self.placeholder.length > 0) {
        
        if (_placeHolderLabel == nil) {

            _placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(8,8,self.bounds.size.width - 16,0)];
            _placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _placeHolderLabel.numberOfLines = 0;
            _placeHolderLabel.font = [UIFont fontWithName:@"PT Sans" size:16.0];
            _placeHolderLabel.backgroundColor = [UIColor clearColor];
            _placeHolderLabel.textColor = _placeholderColor;
            _placeHolderLabel.alpha = 0;
            _placeHolderLabel.tag = 999;
            
            [self addSubview:_placeHolderLabel];
        }
        
        _placeHolderLabel.text = _placeholder;
        [_placeHolderLabel sizeToFit];
        [self sendSubviewToBack:_placeHolderLabel];
    }
    
    
    if (self.text.length == 0 && self.placeholder.length > 0) {
        [[self viewWithTag:999] setAlpha:1.0];
    }
    
    [super drawRect:rect];
}


@end
