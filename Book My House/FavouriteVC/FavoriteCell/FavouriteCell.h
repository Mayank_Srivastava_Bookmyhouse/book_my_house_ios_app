//
//  FavouriteCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/4/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblBuildingType;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;


//Additionals
@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeeds;
@property (weak, nonatomic) IBOutlet UILabel *lblLifeStyle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlView;
@property (weak, nonatomic) IBOutlet UIView *viewBg;

@end

@interface FavoriteProperty : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UIButton *btnFav;
@property (weak, nonatomic) IBOutlet UILabel *lblAppriciation;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UILabel *lblPessessionDate;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblBuildingType;
@property (weak, nonatomic) IBOutlet UILabel *lblConstructionStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPricePSF;


@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeeds;
@property (weak, nonatomic) IBOutlet UILabel *lblLifestyle;

@end