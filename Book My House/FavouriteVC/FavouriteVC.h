//
//  FavouriteVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/4/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "FavouriteCell.h"
#import "MenuView.h"
#import "ProjectDetailVC.h"
#import "UnitDescriptionVC.h"

@interface FavouriteVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
- (void)loadFavouriteList;
@end
