//
//  FavouriteVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/4/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "FavouriteVC.h"



static NSString * const strID1 = @"cell1";
static NSString * const strID2 = @"cell1";
static NSString * const strNoData = @"No Data available";

@interface FavouriteVC () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UITableView *tblView;
    
    
    MenuView *viewMenu;
    NSArray *arrResponse;
    BOOL isAPILoaded;
    CGPoint aPoint;
    UIRefreshControl *refreshControl;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation FavouriteVC


#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
    
}


- (void)viewWillAppear:(BOOL)animated {
    [self loadFavouriteList];
    [super viewWillAppear:animated];
    
    viewMenu = [MenuView createView];
    viewMenu.frame = CGRectMake(- 1 * CGRectGetWidth(self.view.frame),  0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    viewMenu.viewController = self;
    [self.view addSubview:viewMenu];
 
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:loading];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    
    [tblView addSubview:refreshControl];
    
 
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [viewMenu closeMenu];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

- (void)refresh {
    
    [refreshControl beginRefreshing];
    
    [self loadFavouriteList];
}

#pragma mark - Screen settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    tblView.contentInset = UIEdgeInsetsMake(-36.0, 0, 0, 0);
    tblView.rowHeight = 408.0;
    tblView.estimatedRowHeight = UITableViewAutomaticDimension;
}


#pragma mark - API Actions
- (void)loadFavouriteList {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:strUserId forKey:@"user_id"];
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI fetchFavouriteList:dict withCallback:^(BOOL isSuccess, id data) {
            
            if ([refreshControl isRefreshing]) {
                [refreshControl endRefreshing];
            }
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                arrResponse = data;
                if (arrResponse.count) {
                    tblView.rowHeight = 289.0;
                    tblView.estimatedRowHeight = UITableViewAutomaticDimension;
                    [tblView reloadData];
                }
                else {
                    [self resignToParentView];
                }
                
                
            }
            else {
                [self resignToParentView];
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadFavouriteList) fromClass:self];
    }
}


#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnMenu) {
        if (viewMenu.isOpen) {
            
            [viewMenu closeMenu];
        }
        else {
            
            [viewMenu openMenu];
        }
    }
    
}

- (void)didSelectedUnfavorite:(UIButton *)sender {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSDictionary *dictReq = [arrResponse objectAtIndex:sender.tag];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        [dict setObject:strUserId forKey:@"user_id"];
        [dict setObject:[dictReq objectForKey:@"property_type"] forKey:@"type"];
        
        if ([[dictReq objectForKey:@"property_type"] isEqualToString:@"Unit"]) {
            [dict setObject:[dictReq objectForKey:@"unit_id"] forKey:@"id"];
        }
        else {
           [dict setObject:[dictReq objectForKey:@"ID"] forKey:@"id"];
        }

        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI markAsFavorite:dict withCallback:^(BOOL isSuccess, id data) {
            
            [[AppDelegate share] enableUserInteraction];
            
            if (isSuccess) {
                NSMutableArray *arr = [arrResponse mutableCopy];
                
                if ([arr containsObject:dictReq]) {
                    [arr removeObject:dictReq];
                    arrResponse = [arr copy];
                    
                    if (arrResponse.count) {
                        [tblView reloadData];
                    }
                    else {
                        [self resignToParentView];
                    }
                }
            }
            else {
                
                
            }
            
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didSelectedUnfavorite:) fromClass:self];
    }
    
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

#pragma mark - Table view delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrResponse.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 408.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *dict = [arrResponse objectAtIndex:indexPath.section];
    
    if ([[dict objectForKey:@"property_type"] isEqualToString:@"Unit"]) {
        
        FavoriteProperty *cell = [tableView dequeueReusableCellWithIdentifier:strID1];
        
        cell.lblLifestyle.layer.cornerRadius = cell.lblInfra.layer.cornerRadius = cell.lblNeeds.layer.cornerRadius = 5.0;
        cell.lblLifestyle.clipsToBounds = cell.lblInfra.clipsToBounds = cell.lblNeeds.clipsToBounds = YES;

        
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=189.0&img=%@", baseImageUrl, extension, CGRectGetWidth(tableView.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"unit_image"]]];
        
        [cell.imgBg setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        
        if (![[dict objectForKey:@"project_name"] isKindOfClass:[NSNull class]]) {
            
            UIFont *font = cell.lblProjectName.font;
            NSAttributedString * attrStr1 = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"project_name"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                             options:@{
                                                                                       NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                       }
                                                                  documentAttributes:nil
                                                                               error:nil];
            
            
            cell.lblProjectName.attributedText = attrStr1;
            cell.lblProjectName.font = font;
            
            //cell.lblProjectName.text = [dict objectForKey:@"project_name"];
        }
        else {
            cell.lblProjectName.text = @"--";
        }
        
        if (![[dict objectForKey:@"unit_no"] isKindOfClass:[NSNull class]]) {
            
            cell.lblBuilderName.text = [NSString stringWithFormat:@"Unit No: %@", [dict objectForKey:@"unit_no"]];
        }
        else {
            cell.lblBuilderName.text = @"--";
        }
        
        if (![[dict objectForKey:@"display_name"] isKindOfClass:[NSNull class]] && ![[dict objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
            cell.lblBuildingType.text = [NSString stringWithFormat:@"%@ (%@ sqft)", [dict objectForKey:@"display_name"], [dict objectForKey:@"size"]];
        }
        else {
            cell.lblBuildingType.text = @"--";
        }
        
        if (![[dict objectForKey:@"UnitFloor"] isKindOfClass:[NSNull class]]) {
            
            cell.lblAddress.text = [NSString stringWithFormat:@"Floor: %@", [dict objectForKey:@"UnitFloor"]];
        }
        else {
            cell.lblAddress.text = @"--";
        }
        
        if (![[dict objectForKey:@"price_one_year"] isKindOfClass:[NSNull class]]) {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Increase"];
            attachment.bounds = CGRectMake(0, -5, 20, CGRectGetHeight(cell.lblAppriciation.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [dict objectForKey:@"price_one_year"]]]];
            
            cell.lblAppriciation.attributedText = strProgress;
        }
        
        if (![[dict objectForKey:@"price"] isKindOfClass:[NSNull class]]) {
            cell.lblPrice.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"price"]];
        }
        
        if (![[dict objectForKey:@"prop_price_persq"] isKindOfClass:[NSNull class]]) {
            cell.lblPricePSF.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"prop_price_persq"]];
        }
        
        
        if (![[dict objectForKey:@"ratings_average"] isKindOfClass:[NSNull class]] && [dict objectForKey:@"ratings_average"] != nil) {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Star-Rating"];
            attachment.bounds = CGRectMake(0, -5, 20, CGRectGetHeight(cell.lblRating.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@/5", [dict objectForKey:@"ratings_average"]]]];
            
            cell.lblRating.attributedText = strProgress;
            
            //        cell.lblRating.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"ratings_average"]];
        }
        else {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Star-Rating"];
            attachment.bounds = CGRectMake(0, -5, 20, CGRectGetHeight(cell.lblRating.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"3/5"]];
            
            cell.lblRating.attributedText = strProgress;
        }
        
        if (![[dict objectForKey:@"possession_date"] isKindOfClass:[NSNull class]]) {
            if ([dict objectForKey:@"possession_date"] != nil) {
                cell.lblPessessionDate.text = [NSString stringWithFormat:@"Possession Date: %@", [dict objectForKey:@"possession_date"]];
            }
            else {
                cell.lblPessessionDate.text = @"";
            }
            
        }
        
        if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
            cell.lblInfra.text = [NSString stringWithFormat:@"Infra\n%@", [dict objectForKey:@"infra"]];
        }
        
        if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
            cell.lblNeeds.text = [NSString stringWithFormat:@"Needs\n%@", [dict objectForKey:@"needs"]];
        }
        
        if (![[dict objectForKey:@"life_style"] isKindOfClass:[NSNull class]]) {
            cell.lblLifestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@", [dict objectForKey:@"life_style"]];
            
        }
        
        cell.btnFav.tag = indexPath.section;
        
        [cell.btnFav addTarget:self action:@selector(didSelectedUnfavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;

    }
    else {
        
        FavoriteProperty *cell = [tableView dequeueReusableCellWithIdentifier:strID2];
        
        cell.lblLifestyle.layer.cornerRadius = cell.lblInfra.layer.cornerRadius = cell.lblNeeds.layer.cornerRadius = 5.0;
        cell.lblLifestyle.clipsToBounds = cell.lblInfra.clipsToBounds = cell.lblNeeds.clipsToBounds = YES;
        

        NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=189.0&img=%@", baseImageUrl, extension, CGRectGetWidth(tableView.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"banner_url"]]];
        
        [cell.imgBg setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        if (![[dict objectForKey:@"project_name"] isKindOfClass:[NSNull class]]) {
            cell.lblProjectName.text = [dict objectForKey:@"project_name"];
        }
        else {
            cell.lblProjectName.text = @"--";
        }
        
        if (![[dict objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
            cell.lblBuilderName.text = [dict objectForKey:@"builder_name"];
        }
        else {
            cell.lblBuilderName.text = @"--";
        }
        
        if (![[dict objectForKey:@"show_text"] isKindOfClass:[NSNull class]]) {
            cell.lblBuildingType.text = [[dict objectForKey:@"show_text"] uppercaseString];
        }
        else {
            cell.lblBuildingType.text = @"--";
        }
        
        if (![[dict objectForKey:@"exactlocation"] isKindOfClass:[NSNull class]]) {
            cell.lblAddress.text = [dict objectForKey:@"exactlocation"];
        }
        else {
            cell.lblAddress.text = @"--";
        }
        
        if (![[dict objectForKey:@"ratings_average"] isKindOfClass:[NSNull class]] && [dict objectForKey:@"ratings_average"] != nil) {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Star-Rating"];
            attachment.bounds = CGRectMake(0, -5, 20, CGRectGetHeight(cell.lblRating.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@/5", [dict objectForKey:@"ratings_average"]]]];
            
            cell.lblRating.attributedText = strProgress;
            
        }
        else {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Star-Rating"];
            attachment.bounds = CGRectMake(0, -5, 20, CGRectGetHeight(cell.lblRating.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"3/5"]];
            
            cell.lblRating.attributedText = strProgress;
            
        }
        
        if (![[dict objectForKey:@"price_one_year"] isKindOfClass:[NSNull class]]) {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Increase"];
            attachment.bounds = CGRectMake(0, -5, 20, CGRectGetHeight(cell.lblAppriciation.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [dict objectForKey:@"price_one_year"]]]];
            
            cell.lblAppriciation.attributedText = strProgress;
        }
        
        if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
            cell.lblInfra.text = [NSString stringWithFormat:@"Infra\n%@", [dict objectForKey:@"infra"]];
        }
        
        if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
            cell.lblNeeds.text = [NSString stringWithFormat:@"Needs\n%@", [dict objectForKey:@"needs"]];
        }
        
        if (![[dict objectForKey:@"life_style"] isKindOfClass:[NSNull class]]) {
            cell.lblLifestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@", [dict objectForKey:@"life_style"]];

        }
        
        if (![[dict objectForKey:@"price"] isKindOfClass:[NSNull class]]) {
            cell.lblPrice.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"price"]];
        }
        
        if (![[dict objectForKey:@"prop_price_persq"] isKindOfClass:[NSNull class]]) {
            cell.lblPricePSF.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"prop_price_persq"]];
        }//possession_dt
        
        if (![[dict objectForKey:@"possession_date"] isKindOfClass:[NSNull class]]) {
            if ([dict objectForKey:@"possession_date"] != nil) {
                cell.lblPessessionDate.text = [NSString stringWithFormat:@"Possession Date: %@", [dict objectForKey:@"possession_date"]];
            }
            else {
                cell.lblPessessionDate.text = @"";
            }
            
        }
        
        cell.btnFav.tag = indexPath.section;
        
        [cell.btnFav addTarget:self action:@selector(didSelectedUnfavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    NSArray *arr = [[arrResponse objectAtIndex:section] objectForKey:@"unit_specifications"];
    
    if (arr.count < 3) {
        return arr.count * 31 + 1;
    }
    else {
        return 94;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    
    NSArray *arr = [[arrResponse objectAtIndex:section] objectForKey:@"unit_specifications"];
    CGFloat footer_hieght;
    if (arr.count < 3) {
        footer_hieght = arr.count * 31 + 1;
    }
    else {
        footer_hieght = 94;
    }
    
    UIView *viewTemplate = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), footer_hieght)];
        //viewTemplate.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0];
    viewTemplate.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0];
    
    UIScrollView *scrlView = [[UIScrollView alloc] initWithFrame:CGRectMake(3, 0,  CGRectGetWidth(viewTemplate.frame) - 6, CGRectGetHeight(viewTemplate.frame))];
    scrlView.backgroundColor = [UIColor clearColor];
    CGFloat hieght = 30;
    CGFloat width = (CGRectGetWidth(scrlView.frame) - 4)/3;
    
    
    int i = 0;
    for (NSDictionary *temp in arr) {
        
        UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(1, (hieght + 1) * i + 1, width, hieght)];
        lbl1.backgroundColor = [UIColor whiteColor];
        lbl1.textAlignment = NSTextAlignmentCenter;
        lbl1.font = [UIFont fontWithName:@"PT Sans" size:14.0];
        lbl1.textColor = [UIColor lightGrayColor];
        lbl1.text = [temp objectForKey:@"wpcf_flat_typology"];
        [scrlView addSubview:lbl1];
        
        UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(width + 2, (hieght + 1) * i + 1, width, hieght)];
        lbl2.backgroundColor = [UIColor whiteColor];
        lbl2.textAlignment = NSTextAlignmentCenter;
        lbl2.font = [UIFont fontWithName:@"PT Sans" size:14.0];
        lbl2.text = [NSString stringWithFormat:@"%@ sqft", [temp objectForKey:@"area_range"]];//1bhkarea_range
        [scrlView addSubview:lbl2];
        
        
        UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(2 * width + 3, (hieght + 1) * i + 1, width, hieght)];
        lbl3.backgroundColor = [UIColor whiteColor];
        lbl3.textAlignment = NSTextAlignmentCenter;
        lbl3.font = [UIFont fontWithName:@"PT Sans" size:14.0];
        lbl3.text = [NSString stringWithFormat:@"\u20B9 %@", [temp objectForKey:@"price_range"]];//1bhk_price
        [scrlView addSubview:lbl3];
        
        
        scrlView.contentSize = CGSizeMake(CGRectGetMaxX(lbl3.frame), CGRectGetMaxY(lbl3.frame));
        i ++;
   
    }
        [viewTemplate addSubview:scrlView];
        
        return viewTemplate;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [arrResponse objectAtIndex:indexPath.section];
    
    if ([[dict objectForKey:@"property_type"] isEqualToString:@"Unit"]){//
        
        NSMutableDictionary *dict1 = [NSMutableDictionary dictionary];
        [dict1 setObject:[dict objectForKey:@"unit_id"] forKey:@"unit_id"];
        [dict1 setObject:@"Buy" forKey:@"wpcf_flat_unit_type"];
        UnitDescriptionVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"UnitDescriptionVC"];
        obj.dictSourse = dict1;
        obj.strPropertyCatagory = [NSString stringWithFormat:@"%@ (%@ sqft)", [dict objectForKey:@"display_name"], [dict objectForKey:@"size"]];
        [self.navigationController pushViewController:obj animated:YES];
        
    }
    else {
        NSMutableDictionary *dict1 = [NSMutableDictionary dictionary];
        [dict1 setObject:[dict objectForKey:@"ID"] forKey:@"project_id"];
        
        ProjectDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailVC"];
        obj.option = FeaturedProjectAccessOptionFavorite;
        obj.dictSourse = dict1;
        [self.navigationController pushViewController:obj animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




#pragma mark - Touch delegates
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [viewMenu closeMenu];
}


#pragma mark - Auxillary Mathods
- (void)setScrollWithBHK:(NSString *)strBHK
                    area:(NSString *)strArea
                   price:(NSString *)strPrice
              startPoint:(CGPoint)startPoint
                 andSize:(CGSize)size
            isScrollView:(UIScrollView *)scrl {
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(startPoint.x, startPoint.y, size.width, size.height)];
    lbl.numberOfLines = 3;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    [scrl addSubview:lbl];
    
    NSString *str = [NSString stringWithFormat:@" %@\n", strBHK];
    NSMutableAttributedString *str1 =[[NSMutableAttributedString alloc] initWithString:str
                                                                            attributes:@{
                                                                                         NSForegroundColorAttributeName : [UIColor grayColor],
                                                                                         NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:12.0]
                                                                                         
                                                                                         }];
    str = [NSString  stringWithFormat:@" %@\n", strArea];
    NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:str
                                                               attributes:@{
                                                                            
                                                                            NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:12.0]
                                                                            }];
    
    str = [NSString stringWithFormat:@" %@", strPrice];
    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:str
                                                               attributes:@{
                                                                            NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:12.0]
                                                                            }];
    
    
    [str1 appendAttributedString:str2];
    [str1 appendAttributedString:str3];
    lbl.attributedText = str1;
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbl.frame), 0, 2.0, CGRectGetHeight(lbl.frame))];
    aView.backgroundColor = [UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0];
    [scrl addSubview:aView];
    
    scrl.contentSize = CGSizeMake(CGRectGetMaxX(aView.frame), CGRectGetHeight(scrl.frame));
    aPoint = CGPointMake(CGRectGetMaxX(aView.frame), 0);
    
}

#pragma mark - Auxillary Mathods

- (void)updateBHKInfo:(NSArray *)arrBHK inView:(UIView *)aView {
    
}

- (void)resignToParentView {
   
    for (UIViewController *obj in self.navigationController.viewControllers) {
        
        if ([obj isKindOfClass:[SearchViewController class]]) {
            SearchViewController *searchObj = (SearchViewController *)obj;
            [self.navigationController popToViewController:searchObj animated:NO];
            [[AppDelegate share] setInterScreenMessages:@"No favorites available"];
            [searchObj getInfoFromApi];
        }
    }
    
}
@end
