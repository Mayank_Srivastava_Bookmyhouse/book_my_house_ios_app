//
//  Header.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/2/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//




#ifndef Header_h
#define Header_h

#import <GoogleSignIn/GoogleSignIn.h>
#import <Google/Analytics.h>

#import "AuxillaryMathods.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "UIView+Toast.h"
#import "ServiceAPI.h"
#import "LocationHandler.h"
#import "IQKeyboardManager.h"
#import "UIImageView+AFNetworking.h"
#import "YTPlayerView.h"


//Facebook App Id
#define FACEBOOK_PERMISSION @[@"public_profile", @"email"]
//#define FACEBOOK_PERMISSION @[@"public_profile"]
#define FACEBOOK_PARAMETERS @{@"fields": @"id, email, name"}




// URL for Development - Bundle id com.bookmyhouse.development
//Social URLS
static NSString * const strFacebook                             = @"https://www.facebook.com/bookmyhouse/";
static NSString * const strGoogle_Plus                          = @"https://plus.google.com/+BookMyHouseComIndia";
static NSString * const strTwitter                              = @"https://twitter.com/bookmyhouseind";//
//static NSString * const strGoogleMapKey                         = @"AIzaSyB3kw8a2z-nDpUKQ3ChR6UT9VpE15n6RYA";
static NSString * const strGooglePlaceAPIUrl                    = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
static NSString * const strGoogleDirectionUrl                   = @"https://maps.googleapis.com/maps/api/directions/json";
//static NSString * const strGooglePlaceAPIKey                    = @"AIzaSyCEL5dmQC2czbec6ey-bWNpaFaKXBuRbHo";
static NSString * const strMixPanelAPISecret                    = @"0dd2d140d79d5601007086e9e2220dda";
static NSString * const strGooglePlaceAPIKey                    = @"AIzaSyCEL5dmQC2czbec6ey-bWNpaFaKXBuRbHo";
static NSString * const strGoogleDirectionAPIKey                = @"AIzaSyDh48wbb3MqjKB7mZAhvnNtHdqR7pAfDh4";



// URL for Development - Bundle id com.bookmyhouse.development
static NSString * const strGoogleMapKey                 = @"AIzaSyAZI5l2XOxDbKU-ZX7hw6kQUqfhhQhT4pk";



//Test Server
static NSString * const baseUrl                         = @"http://52.77.73.171/apimain/api/";
static NSString * const baseIconUrl                     = @"http://52.77.73.171/";
static NSString * const extension                       = @".php";
static NSString * const loading                         = @"LOADING";
static NSString * const baseImageUrl                    = @"http://52.77.73.171/apimain/resize";




/*
//Live server   http://bookmyhouse.com
static NSString * const baseUrl                         = @"https://bookmyhouse.com/api/";
static NSString * const baseIconUrl                     = @"https://bookmyhouse.com/";
static NSString * const extension                       = @".php";
static NSString * const loading                         = @"LOADING";
static NSString * const baseImageUrl                    = @"https://bookmyhouse.com/api/resize";
*/

//Mathod name
static NSString * const urlRegister                    = @"seekersignup";
static NSString * const urlSignIn                      = @"loginseeker";
static NSString * const url_Third_Party_Login          = @"thirdPartyLogin";
static NSString * const url_Suggestive_Search          = @"search";

static NSString * const urlGetCities                   = @"getcities";
static NSString * const urlGet_All_Builders            = @"get_all_builders";
static NSString * const urlGet_All_projects            = @"get_all_projects";

static NSString * const url_Featured_Developers        = @"featured_developers";
static NSString * const url_Featured_Projects          = @"featured_projects";
static NSString * const url_Special_Projects           = @"specialprojects";
static NSString * const urlSearchProject               = @"searchProject";
static NSString * const url_get_locations              = @"get_locations";
static NSString * const url_heatmap                    = @"heatmap";


static NSString * const url_get_alerts                 = @"get_alerts";
static NSString * const url_About_us                   = @"about_us";
static NSString * const url_Project_detail             = @"project_detail";
static NSString * const url_projects_by_location       = @"projects_by_location";
static NSString * const url_unit_list_by_proj_id       = @"unit_list_byproj_id";
static NSString * const url_unit_detail                = @"unit_detail";
static NSString * const url_add_favorite               = @"add_favorite";
static NSString * const url_get_all_comments           = @"get_all_comments";;
static NSString * const url_line_chart                 = @"line_chart";
static NSString * const url_image_map                  = @"image_map";
static NSString * const url_get_user_favorites         = @"get_user_favorites";
static NSString * const url_add_comment                = @"add_comment";
static NSString * const url_Save_Info                  = @"saveuserinfo";
static NSString * const url_Show_User_Info             = @"showuserinfo";
static NSString * const url_Payment_API                = @"payment_api";

static NSString * const url_project_album              = @"project_album";
static NSString * const url_get_gallary_image          = @"get_galleryimage";
static NSString * const url_get_gallary_videos         = @"get_video";
static NSString * const url_get_gallary_const_upd      = @"get_constructionupdate";
static NSString * const url_get_gallary_VSV            = @"get_virtual_site_visit";
static NSString * const url_get_gallary_360_tour       = @"get_360_tour";
static NSString * const url_forget_password            = @"forget_password";
static NSString * const url_Enquiry                    = @"enquiry";
static NSString * const url_site_visit                 = @"site_visit";
static NSString * const url_Contact_Us                 = @"contact_us";
static NSString * const url_BMH_Contact_Info           = @"contact_data";//
//static NSString * const url_Special_Projects           = @"specialprojects";
//payment_api



//Message Strings
static NSString * const strInternetNotAvailable        = @"Internet Not available";
static NSString * const strSomeErrorOccured            = @"Some error occured";
static NSString * const strNoProject                   = @"No Projects founds for your search criteria";
static CGFloat duration                                = 3.0;
static NSString * const strLoginForAlert               = @"Please Login to get alerts";
static NSString * const strAlertNotSaved               = @"Alerts could not be saved";

static NSString * const test_mode                      = @"test_mode";
static NSString * const test_mode_facebook             = @"test_mode_facebook";
static NSString * const test_mode_google               = @"test_mode_google";
static NSString * const test_mode_Host                 = @"test_mode_Host";
static NSString * const test_mode_no_Host              = @"test_mode_no_Host";
static NSString * const city_selected_no_login         = @"city_selected_no_login";
//static NSString * const city__

//Saved Data in Shared preferences
static NSString * const strSavedName                    = @"name";
static NSString * const strSaveUserEmail                = @"user_email";
static NSString * const strSaveUserId                   = @"user_id";
static NSString * const strSaveUserLogin                = @"user_login";
static NSString * const strSaveUserType                 = @"user_type";
static NSString * const strSaveUserIsTokenPaid          = @"is_token_paid";
static NSString * const strSavePayment_Info             = @"payment_info";
static NSString * const strSaveCityInfo                 = @"strSavedInfo";
static NSString * const strSaveKeyword                  = @"keyword";
static NSString * const strSaveSuggestion               = @"SaveSuggestion";
static NSString * const strSaveSortLocalityIndex        = @"strSaveSortLocalityIndex";
static NSString * const strSaveMultiFilter              = @"strSaveMultiFilter";
static NSString * const strSaveUnitFilter               = @"strSaveUnitFilter";
static NSString * const strSaveBuilderList              = @"strSaveBuilderList";

//Notifications
static NSString * const kResignKeyboard                 = @"ResignKeyboard";
static NSString * const kApplyFilters                   = @"kApplyFilters";
static NSString * const kSelectCity                     = @"kSelectCity";
static NSString * const kUnitFilter                     = @"kUnitFilters";
static NSString * const kSelectBuilder                  = @"kSelectBuilder";
static NSString * const kSectorDetail                   = @"kSectorDetail";
static NSString * const kProjectListMultifilter         = @"kProjectListMultifilter";
static NSString * const kProjectMapMultifilter          = @"kProjectMapMultifilter";
static NSString * const kSuggestion                     = @"kSuggestion";
static NSString * const kGetAlertOnTabularScreen        = @"kGetAlertOnTabularScreen";
static NSString * const kGetAlertOnFeaturedDeveloper    = @"kGetAlertOnFeaturedDeveloper";
static NSString * const kFavoriteProjectDetail          = @"kFavoriteProjectDetail";
static NSString * const kBookSiteVisit                  = @"kBookSiteVisit";
static NSString * const kFavoriteUnit                   = @"kFavoriteUnit";
static NSString * const kPostCommentOnProjectDetails    = @"kPostCommentOnProjectDetails";
static NSString * const kPostCommentOnUnitDetails       = @"kPostCommentOnUnitDetails";
static NSString * const kFavoriteUnitFromUnitSelect     = @"kFavoriteUnitFromUnitSelect";
static NSString * const kFavoriteUnitFromUnitSelectTabular     = @"kFavoriteUnitFromUnitSelectTabular";
static NSString * const kFavoriteProjectCarousel        = @"kFavoriteProjectCarousel";
static NSString * const kProjectCarousel                = @"kProjectCarousel";
static NSString * const kFeaturedProjectFavorite        = @"kFeaturedProjectFavorite";
static NSString * const kFeaturedDeveloperDetailsFav    = @"kFeaturedDeveloperDetailsFav";
static NSString * const kPrefillEmail                   = @"kPrefillEmail";
#endif /* Header_h */
