//
//  HomeViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//


#import <UIKit/UIKit.h>
//#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import <FBSDKCoreKit.h>
#import <FBSDKLoginKit.h>

#import "Header.h"
#import "SearchViewController.h"





typedef NS_ENUM(NSUInteger, HomeViewControllerAccessOption) {
    HomeViewControllerAccessOptionNormal,
    HomeViewControllerAccessOptionMisc,
};


@interface HomeViewController : UIViewController<UIScrollViewDelegate>
@property (assign, nonatomic) HomeViewControllerAccessOption option;

@end
