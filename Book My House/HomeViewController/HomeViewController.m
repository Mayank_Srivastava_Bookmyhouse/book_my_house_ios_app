//
//  HomeViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "HomeViewController.h"
#import "PrivacyViewController.h"


#define TAG_FACEBOOK 1
#define TAG_GOOGLE_PLUS 2
#define TAG_SIGN_IN 3
#define TAG_REGISTER 4
#define TAG_PRIVACY_POLICY 5
#define TAG_TERMS_OF_USE 6
#define TAG_SKIP_THIS_FOR_NOW 7

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HIEGHT [[UIScreen mainScreen] bounds].size.height

@interface HomeViewController () {
    
    __weak IBOutlet UIScrollView *scrlView;
    __weak IBOutlet UIButton *btnFacebook;
    __weak IBOutlet GIDSignInButton *btnGooglePlus;
    __weak IBOutlet UIButton *btnSignIn;
    __weak IBOutlet UIButton *btnRegister;
    __weak IBOutlet UIButton *btnSkipThisForNow;
    __weak IBOutlet UIPageControl *pageControl;
    
    
    NSArray *arrImages;
    NSUserDefaults *pref;
    NSTimer *timer;
    BOOL doScrollRight;
}
- (IBAction)actionListeners:(id)sender;

@end

@implementation HomeViewController


#pragma mark - View Controller Mathods

- (void)loadView {
    [super loadView];
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
    if (strUserId != nil) {
        SearchViewController *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        search.option = SearchViewControllerAccessOptionSignIn;
        [self.navigationController pushViewController:search animated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pref = [NSUserDefaults standardUserDefaults];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    // Uncomment to automatically sign in the user.
   
    if (SCREEN_WIDTH == 320 && SCREEN_HIEGHT == 480) {//iPhone 4s
        arrImages = @[@"walkthrough(iPhone)1-320x440", @"walkthrough(iPhone)2-320x440", @"walkthrough(iPhone)3-320x440"];
    }
    else if (SCREEN_WIDTH == 320 && SCREEN_HIEGHT == 568) {//iPhone 5, 5s
        arrImages = @[@"walkthrough(iPhone)1-320x528", @"walkthrough(iPhone)2-320x528", @"walkthrough(iPhone)3-320x528"];
    }
    else if (SCREEN_WIDTH == 375 && SCREEN_HIEGHT == 667) {//iPhone 6, 6s
        arrImages = @[@"walkthrough(iPhone)1-375x627", @"walkthrough(iPhone)2-375x627", @"walkthrough(iPhone)3-375x627"];
    }
    else if (SCREEN_WIDTH == 414 && SCREEN_HIEGHT == 736) {//iPhone 6 Plus, 6s plus
        arrImages = @[@"walkthrough(iPhone)1-414x696", @"walkthrough(iPhone)2-414x696", @"walkthrough(iPhone)3-414x696"];
    }
    else if (SCREEN_WIDTH == 768 && SCREEN_HIEGHT == 1024) {//iPad 2, iPad Air, iPad Air 2,
        arrImages = @[@"walkthrough(iPhone)1-768x984", @"walkthrough(iPhone)2-768x984", @"walkthrough(iPhone)3-768x984"];
    }
    else {//iPad pro
        arrImages = @[@"walkthrough(iPhone)1-1024x1326", @"walkthrough(iPhone)2-1024x1326", @"walkthrough(iPhone)3-1024x1326"];
    }
    
    if (_option == HomeViewControllerAccessOptionNormal) {
        [self screenSettings];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_option == HomeViewControllerAccessOptionNormal) {
        [self settingPagingScreens];
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [timer invalidate];
}


#pragma mark - Screen Settings
- (void)screenSettings {
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    //Setting Background color
    UIImageView *img = [[UIImageView alloc] initWithFrame:self.view.frame];
    img.image = [UIImage imageNamed:[arrImages firstObject]];
    img.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:img];
    [self.view sendSubviewToBack:img];
    
    btnRegister.layer.borderWidth = 1.5;
    btnRegister.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    
    btnSignIn.layer.borderWidth = 1.5;
    btnSignIn.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    pageControl.currentPage = 0;
    pageControl.numberOfPages = arrImages.count;
}


- (void)settingPagingScreens {
    
    UIImageView *imgView;
    
    for (int i = 0; i < arrImages.count; i++) {
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(i * CGRectGetWidth(scrlView.frame), 0, CGRectGetWidth(scrlView.frame), CGRectGetHeight(scrlView.frame))];
        imgView.image = [UIImage imageNamed:[arrImages objectAtIndex:i]];
        imgView.contentMode = UIViewContentModeScaleToFill;
        [scrlView addSubview:imgView];
    }
    
    scrlView.contentSize = CGSizeMake(arrImages.count * CGRectGetWidth(scrlView.frame), CGRectGetHeight(scrlView.frame));
    
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"privacySegue"]) {
        PrivacyViewController *obj = segue.destinationViewController;
        obj.option = PrivacyViewControllerAccessOptionPrivacy;
        
    }
    else if ([segue.identifier isEqualToString:@"termsSegue"]) {
        PrivacyViewController *obj = segue.destinationViewController;
        obj.option = PrivacyViewControllerAccessOptionTermsOfUse;

    }
}



#pragma mark - Button Actions

- (IBAction)actionListeners:(id)sender {
    
    if ([sender isKindOfClass:[UIButton class]]) {
        
        UIButton *btn = (UIButton *)sender;
        if (btn.tag == TAG_FACEBOOK) {
          //  [self signInWithFacebook];
        }
        else if (btn.tag == TAG_GOOGLE_PLUS) {
            //[self signInWithGooglePlus];
        }
        else if (btn.tag == TAG_SIGN_IN) {
            
        }
        else if (btn.tag == TAG_REGISTER) {
            
        }
        else if (btn.tag == TAG_PRIVACY_POLICY) {
            
        }
        else if (btn.tag == TAG_TERMS_OF_USE) {
            
        }
        else if (btn.tag == TAG_SKIP_THIS_FOR_NOW) {
            
        }
        else{
            
        }
        
        
    }
    else {
        
    }
}


#pragma mark - Scrollview Delegates
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (scrollView == scrlView) {
        CGFloat pageWidth = CGRectGetWidth(scrlView.frame);
        CGFloat fractionalPage = scrlView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageControl.currentPage = page;
    }
    else {
    }
}

- (void)onTimer {
    
    if (pageControl.currentPage == pageControl.numberOfPages - 1 || pageControl.currentPage == 0) {
        doScrollRight = !doScrollRight;
    }
    
    if (doScrollRight && pageControl.currentPage < pageControl.numberOfPages - 1) {
        
        CGFloat currentOffset = scrlView.contentOffset.x;
        CGFloat newOffset = currentOffset + CGRectGetWidth(scrlView.frame);
        [scrlView setContentOffset:CGPointMake(newOffset,0.0) animated:YES];
        
        
    }
    else if (!doScrollRight && pageControl.currentPage > 0) {
        CGFloat currentOffset = scrlView.contentOffset.x;
        CGFloat newOffset = currentOffset - 2 * CGRectGetWidth(scrlView.frame);
        [UIView animateWithDuration:1.0 animations:^{
            [scrlView setContentOffset:CGPointMake(newOffset, 0.0) animated:NO];
        }];
        
        
    }
}

@end
