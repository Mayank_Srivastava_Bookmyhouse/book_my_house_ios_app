//
//  MakePaymentCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitProfile : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProject;
@property (weak, nonatomic) IBOutlet UILabel *lblPossesssionMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilder;
@property (weak, nonatomic) IBOutlet UIImageView *imgUnit;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitType;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitArea;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@end

@interface PayCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img;
@end

@interface PaymentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnDebitCard;
@property (weak, nonatomic) IBOutlet UIButton *btnCreditCard;
@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtCVVNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtNameOnCard;
@property (weak, nonatomic) IBOutlet UITextField *txtExpiryDate;
@end

@interface MakePaymentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnMakePayment;
@end

