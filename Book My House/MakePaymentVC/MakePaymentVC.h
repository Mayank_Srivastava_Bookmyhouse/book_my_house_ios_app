//
//  MakePaymentVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentModal.h"
#import "Header.h"
#import "MakePaymentCell.h"
#import "CCAvenueVC.h"

@interface MakePaymentVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) NSDictionary *dictInfo;
@end
