//
//  MakePaymentVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "MakePaymentVC.h"


static NSString * const strID1                                    = @"cell1";
static NSString * const strID2                                    = @"cell2";
static NSString * const strID3                                    = @"cell3";
static NSString * const strID4                                    = @"cell4";

static NSString * const strNoPhoneNumber                          = @"Phone number not available";

static NSString * const strCardNumber                             = @"Please enter card number";
static NSString * const strCCVNumber                              = @"Please enter CCV number";
static NSString * const strNameOnCard                             = @"Name on card";
static NSString * const strExpiryDate                             = @"Please enter expiry date";


@interface MakePaymentVC () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnClose;
    
    PaymentModal *manager;
    NSDictionary *dictSourse;
    NSMutableArray *arrCardInfo;
    NSArray *arrMonth, *arrYear;
    PaymentCell *cellPayment;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation MakePaymentVC

#pragma mark - View Controller Mathods
- (void)loadView {
    
    
    arrMonth = @[@"JAN", @"FEB", @"MAR", @"APR", @"MAY", @"JUN", @"JUL", @"AUG", @"SEP", @"OCT", @"NOV", @"DEC"];
    arrYear = @[@"2016", @"2017", @"2018", @"2019", @"2020",@"2021", @"2022", @"2023", @"2024" ,@"2025",@"2026", @"2027"];
    
    manager = [PaymentModal sharedManager];
    dictSourse = manager.dict;
    [super loadView];
    
    arrCardInfo = [NSMutableArray array];
    
    for (int i = 0; i < 4; i ++) {
        [arrCardInfo addObject:@""];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    
    NSString *str = [NSString stringWithFormat:@"Helpline: %@", [dictSourse objectForKey:@"builder_contactno"]];
    [btnHelp setTitle:str forState:UIControlStateNormal];

}

- (void)updateScreenSettings {
    
}


#pragma mark - API Actions
- (void)loadPersonalInfo {
    
}

#pragma mark - Action Listener
- (IBAction)actionListener:(id)sender {
    if (sender == btnMenu) {
        
    }
    else if (sender == btnHelp) {
        if (![[dictSourse objectForKey:@"builder_contactno"] isKindOfClass:[NSNull class]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [dictSourse objectForKey:@"builder_contactno"]]]];
        }
        else {
            [self.view makeToast:strNoPhoneNumber duration:duration position:CSToastPositionCenter];
        }
    }
    else if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didSelectMakePayment:(UIButton *)btn {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *str = [arrCardInfo objectAtIndex:0];
        str = [str stringByAppendingString:str];
        if (str.length == 0) {
            [self.view makeToast:strCardNumber duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"card_number"];
        }
        
        str = [arrCardInfo objectAtIndex:1];
        str = [str stringByAppendingString:str];
        if (str.length == 0) {
            [self.view makeToast:strCCVNumber duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"ccv_number"];
        }
        
        str = [arrCardInfo objectAtIndex:2];
        str = [str stringByAppendingString:str];
        if (str.length == 0) {
            [self.view makeToast:strNameOnCard duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"name_on_card"];
        }
        
        
        str = [arrCardInfo objectAtIndex:3];
        str = [str stringByAppendingString:str];
        if (str.length == 0) {
            [self.view makeToast:strExpiryDate duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"expiry_date"];
        }
        NSLog(@"Dictionary %@", dict);
        
        manager.dictCardInfo = dict;
        
        
        NSMutableDictionary *dictReq = [NSMutableDictionary dictionary];
        
        NSString *strUserID = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        [dictReq setObject:strUserID forKey:@"user_id"];
        
        NSLog(@"Dictionary %@", [manager.dict objectForKey:@"project_id"]);
        [dictReq setObject:[manager.dict objectForKey:@"project_id"] forKey:@"unit_id"];
        
        
        [ServiceAPI postPaymentForm:dictReq withCallback:^(BOOL isSuccess, id data) {
            if (isSuccess) {
                manager.dictActionForm = data;
                CCAvenueVC *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"CCAvenueVC"];
                [self.navigationController pushViewController:obj animated:YES];
                
            }
            else {
                [self.view makeToast:@"Some error occured" duration:duration position:CSToastPositionCenter];
            }
        }];
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didSelectMakePayment:) fromClass:self];
    }
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 241.0;
    }
    else if (indexPath.row == 1) {
        return 64.0;
    }
    else if (indexPath.row == 2) {
        return 142.0;
    }
    else if (indexPath.row == 3) {
        return 110.0;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.row == 0) {
        
        UnitProfile *cell = [tableView dequeueReusableCellWithIdentifier:strID1];
        
        if (![[dictSourse objectForKey:@"project_name"] isKindOfClass:[NSNull class]]) {
            cell.lblProject.text = [dictSourse objectForKey:@"project_name"];
        }
        else {
            cell.lblProject.text = @"--";;
        }
        
        
        if (![[dictSourse objectForKey:@"project_name"] isKindOfClass:[NSNull class]]) {
            cell.lblProject.text = [dictSourse objectForKey:@"project_name"];
        }
        else {
            cell.lblProject.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
            cell.lblBuilder.text = [dictSourse objectForKey:@"builder_name"];
        }
        else {
            cell.lblBuilder.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"display_name"] isKindOfClass:[NSNull class]]) {
            cell.lblUnitType.text = [dictSourse objectForKey:@"display_name"];
        }
        else {
            cell.lblUnitType.text = @"--";
        }
        
        if (![[dictSourse objectForKey:@"max_area_range"] isKindOfClass:[NSNull class]]) {
            
            cell.lblUnitArea.text = [NSString stringWithFormat:@"%@ sq ft", [dictSourse objectForKey:@"max_area_range"]];
        }
        else {
            cell.lblUnitArea.text = @"--";
        }
        
        if (![[dictSourse objectForKey:@"bsp"] isKindOfClass:[NSNull class]] && ![[dictSourse objectForKey:@"currency"] isKindOfClass:[NSNull class]]) {
            
            cell.lblTotalPrice.text = [NSString stringWithFormat:@"\u20B9 %@", [self setPriceRange:[NSString stringWithFormat:@"%@", [dictSourse objectForKey:@"bsp"]]]];
        }
        else {
            
        }
        
        if (![[dictSourse objectForKey:@"unit_no"] isKindOfClass:[NSNull class]]) {
            cell.lblUnitNumber.text = [dictSourse objectForKey:@"unit_no"];
        }
        else {
            cell.lblUnitNumber.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"UnitFloor"] isKindOfClass:[NSNull class]]) {
            cell.lblFloor.text = [dictSourse objectForKey:@"UnitFloor"];
        }
        else {
            cell.lblFloor.text = @"--";
        }
        
         [cell.imgUnit setImageWithURL:[NSURL URLWithString:[dictSourse objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        PayCell *cell = [tableView dequeueReusableCellWithIdentifier:strID2];
        return cell;
    }
    else if (indexPath.row == 2) {
        PaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:strID3];
        
        [self setTextField:cell.txtCardNumber withPlaceholder:@"CARD NUMBER"];
        [self setTextField:cell.txtCVVNumber withPlaceholder:@"CCV NUMBER"];
        [self setTextField:cell.txtNameOnCard withPlaceholder:@"NAME ON CARD"];
        [self setTextField:cell.txtExpiryDate withPlaceholder:@"EXPIRY CARD: MM/YY"];
        
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        picker.delegate = self;
        cell.txtExpiryDate.inputView = picker;
        
        cell.txtCardNumber.text = [arrCardInfo objectAtIndex:cell.txtCardNumber.tag];
        cell.txtCVVNumber.text = [arrCardInfo objectAtIndex:cell.txtCVVNumber.tag];
        cell.txtNameOnCard.text = [arrCardInfo objectAtIndex:cell.txtNameOnCard.tag];
        cell.txtExpiryDate.text = [arrCardInfo objectAtIndex:cell.txtExpiryDate.tag];
        
        cell.txtCardNumber.delegate = self;
        cell.txtCVVNumber.delegate = self;
        cell.txtNameOnCard.delegate = self;
        cell.txtExpiryDate.delegate = self;
        
        cellPayment = cell;
        
        return cell;
    }
    else if (indexPath.row == 3) {
        MakePaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:strID4];
        
        [cell.btnMakePayment addTarget:self action:@selector(didSelectMakePayment:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else {
        return nil;
    }
}


#pragma mark - Auxillary Mathods
- (NSString *)setPriceRange:(NSString *)str {
    
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs+", result];
    }
    
    return strNum;
}

- (NSString *)setExactPriceRange:(NSString *)str {
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs", result];
    }
    return strNum;
}

- (void)setTextField:(UITextField *)txtField withPlaceholder:(NSString *)str {
    
    txtField.layer.borderColor = [UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0].CGColor;
    txtField.layer.borderWidth = 1.0;
    
    txtField.placeholder = str;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtField.frame))];
    txtField.leftView = view;
    txtField.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark - UITextField Delegates
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if (textField == cellPayment.txtCardNumber) {
        return newLength <= 16 || returnKey;
    }
    else if (textField == cellPayment.txtCVVNumber) {
        return newLength <= 3 || returnKey;
    }
    else if (textField == cellPayment.txtNameOnCard) {
        return newLength <= 25 || returnKey;
    }
    else if (textField == cellPayment.txtExpiryDate) {
        return newLength <= 7 || returnKey;
    }
    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [arrCardInfo replaceObjectAtIndex:textField.tag withObject:textField.text];
}




#pragma mark - Picker View

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (component == 0) {
        return arrMonth.count;
    }
    else {
        return arrYear.count;
    }
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (component == 0) {
        return [arrMonth objectAtIndex:row];
    }
    else if (component == 1){
        return [arrYear objectAtIndex:row];
    }
    else {
        return @"";
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString *selectedYear = [arrYear objectAtIndex:[pickerView selectedRowInComponent:1]];
    NSString *selectedMonth = [arrMonth objectAtIndex:[pickerView selectedRowInComponent:0]];
    
    
    if ([selectedMonth isEqualToString:@"JAN"]) {
        selectedMonth = @"1";
    }
    else if ([selectedMonth isEqualToString:@"FEB"]) {
        selectedMonth = @"2";
    }
    else if ([selectedMonth isEqualToString:@"MAR"]) {
        selectedMonth = @"3";
    }
    else if ([selectedMonth isEqualToString:@"APR"]) {
        selectedMonth = @"4";
    }
    else if ([selectedMonth isEqualToString:@"MAY"]) {
        selectedMonth = @"5";
    }
    else if ([selectedMonth isEqualToString:@"JUN"]) {
        selectedMonth = @"6";
    }
    else if ([selectedMonth isEqualToString:@"JUL"]) {
        selectedMonth = @"7";
    }
    else if ([selectedMonth isEqualToString:@"AUG"]) {
        selectedMonth = @"8";
    }
    else if ([selectedMonth isEqualToString:@"SEP"]) {
        selectedMonth = @"9";
    }
    else if ([selectedMonth isEqualToString:@"OCT"]) {
        selectedMonth = @"10";
    }
    else if ([selectedMonth isEqualToString:@"NOV"]) {
        selectedMonth = @"11";
    }
    else if ([selectedMonth isEqualToString:@"DEC"]) {
        selectedMonth = @"12";
    }
    
    NSString *str = [NSString stringWithFormat:@"%@ / %@", selectedMonth, selectedYear];
    cellPayment.txtExpiryDate.text = str;
}
@end
