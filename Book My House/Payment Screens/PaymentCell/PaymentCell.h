//
//  PaymentCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/7/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProject;
@property (weak, nonatomic) IBOutlet UILabel *lblPossesssionMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilder;
@property (weak, nonatomic) IBOutlet UIImageView *imgUnit;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitType;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitArea;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@end


@interface ParkingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UITextField *lblNumerOfParking;
@end

@interface AppartmentSpecification : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValues;
@end

@interface GrandTotal : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValues;
@end

@interface Offer_Discount : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@end

@interface I_Accept : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnIAccept;
@end