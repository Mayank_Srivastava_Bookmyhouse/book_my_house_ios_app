//
//  PaymentModal.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/9/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentModal : NSObject
@property (strong, nonatomic) NSDictionary *dict;
@property (strong, nonatomic) NSDictionary *dictCouponInfo;
@property (strong, nonatomic) NSArray *arr;
@property (strong, nonatomic) NSDictionary *dictCardInfo;
@property (strong, nonatomic) NSDictionary *dictActionForm;
+ (id)sharedManager;
@end
