//
//  PaymentModal.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/9/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PaymentModal.h"

@implementation PaymentModal
+ (id)sharedManager {
    
    static dispatch_once_t token;
    
    static PaymentModal *obj = nil;
    
    dispatch_once(&token, ^{
        obj = [[PaymentModal alloc] init];
    });
    
    return obj;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        self.dict = [[NSDictionary alloc] init];
        self.arr = [[NSArray alloc] init];
    }
    
    return self;
}
@end
