//
//  PaymentPlanVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/11/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "PaymentModal.h"
#import "PaymentTermsAndConditionsVC.h"

@interface PaymentPlanVC : UIViewController<UIWebViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@end
