//
//  PaymentPlanVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/11/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PaymentPlanVC.h"


static NSString * const strNoPhoneNumber             = @"Phone number not available";
//static NSString * const strNoTermsConditions         = @"Sorry, TERMS & CONDITIONS are not available";
static NSString * const strDownPaymentPlan           = @"Down payment plan not available.";
static NSString * const strPossessionPlan            = @"Possession plan is not available";
@interface PaymentPlanVC () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UIImageView *imgTop;
    __weak IBOutlet UIWebView *web;
    __weak IBOutlet UILabel *lblOffer;
    __weak IBOutlet UILabel *lblDiscount;
    __weak IBOutlet UIButton *btnAccept;
    __weak IBOutlet UIActivityIndicatorView *indicator;
    __weak IBOutlet UITextField *txtPaymentPlan;
    
    PaymentModal *manager;
    
    
    
    NSArray *arrPayment;
    NSDictionary *dictSourse;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation PaymentPlanVC

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)loadView {
    manager = [PaymentModal sharedManager];
    [super loadView];
    [self managePaymentPlan];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self displayPaymentPlans:txtPaymentPlan.text];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    NSString *str = [NSString stringWithFormat:@"Helpline: %@", [dictSourse objectForKey:@"builder_contactno"]];
    [btnHelp setTitle:str forState:UIControlStateNormal];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, CGRectGetHeight(txtPaymentPlan.frame))];
    leftView.backgroundColor = [UIColor whiteColor];
    txtPaymentPlan.leftView = leftView;
    txtPaymentPlan.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, CGRectGetHeight(txtPaymentPlan.frame))];
    imgView.image = [UIImage imageNamed:@"Sort_Down"];
    txtPaymentPlan.rightView = imgView;
    txtPaymentPlan.rightViewMode = UITextFieldViewModeAlways;
    
    
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    picker.delegate = self;
    txtPaymentPlan.inputView = picker;
    
    txtPaymentPlan.layer.borderColor = [UIColor lightGrayColor].CGColor;
    txtPaymentPlan.layer.borderWidth = 2.0;
    
    
    if (arrPayment.count) {
        txtPaymentPlan.text = [NSString stringWithFormat:@"%@", [arrPayment firstObject]];
    }
    else {
        txtPaymentPlan.text = @"";
    }
    
    
    [self formatLabel:lblOffer WithPrefix:@" OFFER " andDescription:@"Home Loan offer 4.99% for the first 5 years"];
    [self formatLabel:lblDiscount WithPrefix:@" DISCOUNT " andDescription:@" 5% ON BSP\t\t\t\t\t\t\t\t\t\t\t\t\t"];
}

- (void)updatedScreenSettings {
    
}

- (void)displayPaymentPlans:(NSString *)strPlans {
    
    if ([strPlans isEqualToString:@"Down Payment"]) {
        if (![[dictSourse objectForKey:@"down_payment_plan"] isKindOfClass:[NSNull class]]) {
            NSString *str = [dictSourse objectForKey:@"down_payment_plan"];
            
            if (![str isEqualToString:@""]) {
                [web loadHTMLString:str baseURL:nil];
            }
            else {
               [self.view makeToast:strDownPaymentPlan duration:duration position:CSToastPositionCenter];
            }
            
            
        }
        else {
            [self.view makeToast:strDownPaymentPlan duration:duration position:CSToastPositionCenter];
        }
    }
    else {
        if (![[dictSourse objectForKey:@"possession_plan"] isKindOfClass:[NSNull class]]) {
            NSString *str = [dictSourse objectForKey:@"possession_plan"];
            if (![str isEqualToString:@""]) {
                [web loadHTMLString:str baseURL:nil];
            }
            else {
                [self.view makeToast:strPossessionPlan duration:duration position:CSToastPositionCenter];

            }
            
            
        }
        else {
            [self.view makeToast:strPossessionPlan duration:duration position:CSToastPositionCenter];
        }
    }
    
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnHelp) {
        if (![[dictSourse objectForKey:@"builder_contactno"] isKindOfClass:[NSNull class]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [dictSourse objectForKey:@"builder_contactno"]]]];
        }
        else {
            [self.view makeToast:strNoPhoneNumber duration:duration position:CSToastPositionCenter];
        }
    }
    else if (sender == btnMenu) {
        
    }
    else if (sender == btnAccept) {
        
        PaymentTermsAndConditionsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentTermsAndConditionsVC"];
        [self.navigationController pushViewController:obj animated:YES];
    }
}
#pragma mark - API Actions

#pragma mark - Auxillary Actions
- (void)managePaymentPlan {
    
    dictSourse = manager.dict;
    arrPayment = (NSArray *)[manager.dict objectForKey:@"payment_plans"];
    NSLog(@"Dictionary %@", dictSourse);
    
}

- (void)formatLabel:(UILabel *)lbl WithPrefix:(NSString *)strPreFix andDescription:(NSString *)strDesc {
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:strPreFix attributes:@{
                                                                                                              NSFontAttributeName :
                                                                                          [UIFont fontWithName:@"PT Sans" size:17.0],                    NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                              NSBackgroundColorAttributeName : [UIColor colorWithRed:235.0/255.0 green:137.0/255.0 blue:30.0/255.0 alpha:1.0]
                                                                                                              }];
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:strDesc attributes:@{
                                                                                               NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:15.0],
                                                                                               NSForegroundColorAttributeName : [UIColor blackColor],
                                                                                               NSBackgroundColorAttributeName : [UIColor whiteColor]
                                                                                               }];
    
    [str appendAttributedString:str1];
    lbl.attributedText = str;
    
    lbl.layer.cornerRadius = 5.0;
    lbl.clipsToBounds = YES;
}

#pragma mark - Webview Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [indicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [indicator stopAnimating];
    [webView sizeThatFits:webView.frame.size];
}


#pragma mark - Picker View Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrPayment.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arrPayment objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    txtPaymentPlan.text = [arrPayment objectAtIndex:row];
    [self displayPaymentPlans:txtPaymentPlan.text];
}

@end
