//
//  PaymentTermsAndConditionsVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentModal.h"
#import "Header.h"
#import "PersonalInformation.h"

@interface PaymentTermsAndConditionsVC : UIViewController
@end
