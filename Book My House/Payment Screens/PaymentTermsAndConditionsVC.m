//
//  PaymentTermsAndConditionsVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PaymentTermsAndConditionsVC.h"

static NSString * const strNoPhoneNumber             = @"Phone number not available";
static NSString * const strNoTermsConditions         = @"Sorry, TERMS & Conditions not avalable";
static NSString * const strTermsOfUseUrl             = @"http://52.77.73.171/apimain/api/term_condition_ios.php";


@interface PaymentTermsAndConditionsVC () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UILabel *lblCheckOut;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnclose;
    __weak IBOutlet UIButton *btnAccept;
    __weak IBOutlet UIImageView *imgTop;
    __weak IBOutlet UIWebView *web;
    __weak IBOutlet UILabel *lblOffer;
    __weak IBOutlet UILabel *lblDiscount;
    __weak IBOutlet UIActivityIndicatorView *indicator;
    
    NSDictionary *dictSourse;
    
    PaymentModal *manager;
    
}
- (IBAction)actionListener:(id)sender;

@end

@implementation PaymentTermsAndConditionsVC


#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)loadView {
    manager = [PaymentModal sharedManager];
    [super loadView];
    [self managePaymentPlan];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [web loadHTMLString:[manager.dict objectForKey:@"term_condition"] baseURL:nil];
//    [ServiceAPI getPrivacyPolicyWithUrl:strTermsOfUseUrl withCallBach:^(BOOL isSuccess, id data) {
//        [[AppDelegate share] enableUserInteraction];
//        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strTermsOfUseUrl]]];
//        NSString *str = (NSString *)data;
//        [web loadHTMLString:str baseURL:nil];
//    }];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    
    NSString *str = [NSString stringWithFormat:@"Helpline: %@", [dictSourse objectForKey:@"builder_contactno"]];
    [btnHelp setTitle:str forState:UIControlStateNormal];
    
}

- (void)updatedScreenSettings {
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnMenu) {
        
    }
    else if (sender == btnHelp) {
        if (![[dictSourse objectForKey:@"builder_contactno"] isKindOfClass:[NSNull class]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [dictSourse objectForKey:@"builder_contactno"]]]];
        }
        else {
            [self.view makeToast:strNoPhoneNumber duration:duration position:CSToastPositionCenter];
        }
    }
    else if (sender == btnclose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnAccept) {
        PersonalInformation *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInformation"];
        [self.navigationController pushViewController:obj animated:YES];
    }
}

#pragma mark - Auxillary Mathods
- (void)managePaymentPlan {
    
    dictSourse = manager.dict;    
}


#pragma mark - Webview Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [indicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [indicator stopAnimating];
}

@end
