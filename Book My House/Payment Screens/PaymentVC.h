//
//  PaymentVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/7/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentCell.h"
#import "PaymentModal.h"
#import "Header.h"
#import "PaymentPlanVC.h"
#import "SigninViewController.h"

@interface PaymentVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) NSString *imgUnit;
@end
