//
//  PaymentVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/7/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PaymentVC.h"

static NSString * const strNoPhoneNumber = @"Phone number not available";

@interface PaymentVC () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIButton *btnIAccept;
    
    NSDictionary *dictSourse;
    int maxParking, selectedParking;
    ParkingCell *cellParking;
    NSArray *arrList;
    NSMutableArray *arrSubList;
    
    PaymentModal *manager;
    
    float total_parking_charges, tax_On_Parking, grand_total;
    
}
- (IBAction)actionListener:(id)sender;


@end

@implementation PaymentVC


#pragma mark - View Controller Mathods
- (void)loadView {
    
    
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        if ([viewController isKindOfClass:[SigninViewController class]]) {
            
            [allViewControllers removeObjectIdenticalTo:viewController];
            self.navigationController.viewControllers = allViewControllers;
        }
        
    }
    
    manager = [PaymentModal sharedManager];
    selectedParking = 1;
    
    total_parking_charges = [[manager.dict objectForKey:@"parking_charges"] floatValue];
    tax_On_Parking = [[manager.dict objectForKey:@"parking_service_tax"] floatValue];
    grand_total = [[manager.dict objectForKey:@"grand_total_int"] floatValue];
    
    maxParking = (int)[[manager.dict objectForKey:@"max_parking"] integerValue];
    [super loadView];
    [self manageUnitInfoForParkings];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    [btnIAccept addTarget:self action:@selector(didTappedIAcccept:) forControlEvents:UIControlEventTouchUpInside];
    
    tblView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    NSString *str = [NSString stringWithFormat:@"Helpline: %@", [dictSourse objectForKey:@"builder_contactno"]];
    [btnHelp setTitle:str forState:UIControlStateNormal];
}
#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnMenu) {
        
    }
    else if (sender == btnHelp) {
        if (![[dictSourse objectForKey:@"builder_contactno"] isKindOfClass:[NSNull class]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [dictSourse objectForKey:@"builder_contactno"]]]];
        }
        else {
            [self.view makeToast:strNoPhoneNumber duration:duration position:CSToastPositionCenter];
        }
        
    }
    else if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)didTappedIAcccept:(UIButton *)btn {
    
    PaymentPlanVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentPlanVC"];
    [self.navigationController pushViewController:obj animated:YES];
}
#pragma mark - API Actions
- (void)loadScreen {
    
}

#pragma mark - Tablew View Delegates
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 290.0;
    }
    else if (indexPath.row == 1) {
        return 48.0;
    }
    else if (indexPath.row > 1 && indexPath.row < 16) {
        return UITableViewAutomaticDimension;
    }
    else if (indexPath.row == 16) {
        return 56.0;
    }
    else if (indexPath.row == 17){
        return 48.0;
    }
    
    return 0.0;
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return 290.0;
    }
    else if (indexPath.row == 1) {
        return 48.0;
    }
    else if (indexPath.row > 1 && indexPath.row < 16) {
        return 48.0;
    }
    else if (indexPath.row == 16) {
        return 56.0;
    }
    else if (indexPath.row == 17){
        return 48.0;
    }
    
    return 0.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3 + arrList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    if (indexPath.row == 0) {
        
        static NSString * const strID1 = @"ProfileCell";
        ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:strID1];
        
        if (![[dictSourse objectForKey:@"project_name"] isKindOfClass:[NSNull class]]) {
            UIFont *font = cell.lblProject.font;
            
            NSString *str = [dictSourse objectForKey:@"project_name"];
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding]
                                                                            options:@{
                                                                                      NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                      }
                                                                 documentAttributes:nil
                                                                              error:nil];
            
            
            cell.lblProject.attributedText = attrStr;
            cell.lblProject.font = font;
        }
        else {
            cell.lblProject.text = @"--";;
        }
        
        
        if (![[dictSourse objectForKey:@"project_name"] isKindOfClass:[NSNull class]]) {
            cell.lblProject.text = [dictSourse objectForKey:@"project_name"];
        }
        else {
            cell.lblProject.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
            cell.lblBuilder.text = [dictSourse objectForKey:@"builder_name"];
        }
        else {
            cell.lblBuilder.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"display_name"] isKindOfClass:[NSNull class]]) {
            cell.lblUnitType.text = [dictSourse objectForKey:@"display_name"];
        }
        else {
            cell.lblUnitType.text = @"--";
        }
        
        if (![[dictSourse objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
            
            cell.lblUnitArea.text = [NSString stringWithFormat:@"%@ sq ft", [dictSourse objectForKey:@"size"]];
        }
        else {
            cell.lblUnitArea.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"grand_total_int"] isKindOfClass:[NSNull class]] && ![[dictSourse objectForKey:@"currency"] isKindOfClass:[NSNull class]]) {

            cell.lblTotalPrice.text = [NSString stringWithFormat:@"\u20B9 %@", [self setPriceRange:[NSString stringWithFormat:@"%@", [dictSourse objectForKey:@"grand_total_int"]]]];
        }
        else {
            cell.lblTotalPrice.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"booking_fees"] isKindOfClass:[NSNull class]]) {
            cell.lblBookingAmount.text = [NSString stringWithFormat:@"\u20B9 %@", [dictSourse objectForKey:@"booking_fees"]];
        }
        else {
            cell.lblBookingAmount.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"unit_no"] isKindOfClass:[NSNull class]]) {
            
            cell.lblUnitNumber.text = [NSString stringWithFormat:@"Unit No.: %@", [dictSourse objectForKey:@"unit_no"]];;
        }
        else {
            cell.lblUnitNumber.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"UnitFloor"] isKindOfClass:[NSNull class]]) {
            
            cell.lblFloor.text = [NSString stringWithFormat:@"Floor: %@", [dictSourse objectForKey:@"UnitFloor"]];
        }
        else {
            cell.lblFloor.text = @"--";
        }
        
        
        if (![[dictSourse objectForKey:@"possession_dt"] isKindOfClass:[NSNull class]]) {//lblPossesssionMonth
            cell.lblPossesssionMonth.text = [NSString stringWithFormat:@"Possession by %@", [dictSourse objectForKey:@"possession_dt"]];
        }
        else {
            cell.lblPossesssionMonth.text = @"--";
        }
        
        
        if ([[dictSourse objectForKey:@"images"] count]) {
            NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [[[dictSourse objectForKey:@"images"] firstObject] objectForKey:@"url"]];
            
            NSString *str1 = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(cell.imgUnit.frame), CGRectGetHeight(cell.imgUnit.frame), str];
            [cell.imgUnit setImageWithURL:[NSURL URLWithString:str1] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        }
        
        return cell;
    }
    else if (indexPath.row == 1) {
       
        static NSString * const strID2 = @"ParkingCell";
        ParkingCell *cell = [tableView dequeueReusableCellWithIdentifier:strID2];
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, CGRectGetHeight(cell.lblNumerOfParking.frame))];
        leftView.backgroundColor = [UIColor whiteColor];
        cell.lblNumerOfParking.leftView = leftView;
        cell.lblNumerOfParking.leftViewMode = UITextFieldViewModeAlways;
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, CGRectGetHeight(cell.lblNumerOfParking.frame))];
        imgView.image = [UIImage imageNamed:@"Sort_Down"];
        cell.lblNumerOfParking.rightView = imgView;
        cell.lblNumerOfParking.rightViewMode = UITextFieldViewModeAlways;
        
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        picker.delegate = self;
        
        cell.lblNumerOfParking.inputView = picker;
                                                                     
        cell.lblNumerOfParking.text = [NSString stringWithFormat:@"%d", selectedParking];
        [picker selectRow:selectedParking inComponent:0 animated:YES];
        
        cellParking = cell;
        return cell;
    }
    else if (indexPath.row > 1 && indexPath.row < 16) {
        
        static NSString * const strID3 = @"AppartmentSpecification";
        AppartmentSpecification *cell = [tableView dequeueReusableCellWithIdentifier:strID3];
        cell.lblTitle.text = [arrList objectAtIndex:indexPath.row - 2];
        cell.lblValues.text = [arrSubList objectAtIndex:indexPath.row - 2];
        
        if (indexPath.row % 2 == 0) {
            cell.viewBg.backgroundColor = [UIColor whiteColor];
        }
        else {
            cell.viewBg.backgroundColor = [UIColor lightGrayColor];
        }
        
        
        return cell;
    }
    else if (indexPath.row == 16) {
        static NSString * const strID4 = @"GrandTotal";
        GrandTotal *cell = [tableView dequeueReusableCellWithIdentifier:strID4];
        cell.lblTitle.text = @"Grand Total";
        
        if (![[dictSourse objectForKey:@"grand_total_int"] isKindOfClass:[NSNull class]]) {
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setUsesGroupingSeparator:YES];
            [formatter setGroupingSeparator: [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator]];
            //[formatter setGroupingSeparator:@","];
            //[formatter setGroupingSize:3];
            
            CGFloat num = grand_total + (selectedParking - 1) * total_parking_charges * (1 + tax_On_Parking / 100);
            
            NSString *str = [formatter stringFromNumber:[NSNumber numberWithFloat:num]];
            cell.lblValues.text = [NSString stringWithFormat:@"\u20B9 %@", str];
        }
        else {
            cell.lblValues.text = @"--";
        }
        
        return cell;
    }
//    else if (indexPath.row == 17) {
//        
//        static NSString * const strID5 = @"Offer_Discount";
//        Offer_Discount *cell = [tableView dequeueReusableCellWithIdentifier:strID5];
//        
//        [self formatLabel:cell.lblOffer WithPrefix:@" OFFER " andDescription:@"Home Loan offer 4.99% for the first 5 years"];
//        [self formatLabel:cell.lblDiscount WithPrefix:@" DISCOUNT " andDescription:@" 5% ON BSP\t\t\t\t\t\t\t\t\t\t\t\t\t"];
//        return cell;
//    }
    else if (indexPath.row == 17) {
        static NSString * const strID6 = @"I_Accept";
        I_Accept *cell = [tableView dequeueReusableCellWithIdentifier:strID6];
        [cell.btnIAccept addTarget:self action:@selector(didTappedIAcccept:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    return nil;
}

#pragma mark - Picker Delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return maxParking + 1;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (row == 0) {
        return [NSString stringWithFormat:@"Select Parking"];
    }
    else
    return [NSString stringWithFormat:@"%d", (int)row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (row == 0) {
        selectedParking = 1;
    }
    else {
        selectedParking = (int)row;
    }
    [self manageUnitInfoForParkings];
}

#pragma mark - Auxillary Mathods
- (NSString *)setPriceRange:(NSString *)str {
    
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs+", result];
    }
    
    return strNum;
}

- (NSString *)setExactPriceRange:(NSString *)str {
    
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs", result];
    }
    
    return strNum;
}

- (void)formatLabel:(UILabel *)lbl WithPrefix:(NSString *)strPreFix andDescription:(NSString *)strDesc {
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:strPreFix attributes:@{
                                                                                                               NSFontAttributeName :
                                                                                          [UIFont fontWithName:@"PT Sans" size:17.0],                     NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                               NSBackgroundColorAttributeName : [UIColor colorWithRed:235.0/255.0 green:137.0/255.0 blue:30.0/255.0 alpha:1.0]
                                                                                                               }];
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:strDesc attributes:@{
                                                                                                                                      NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:15.0],
                                                                                                                                      NSForegroundColorAttributeName : [UIColor blackColor],
                                                                                                                                      NSBackgroundColorAttributeName : [UIColor whiteColor]
                                                                                                                                      }];
    
    [str appendAttributedString:str1];
    lbl.attributedText = str;
    
    lbl.layer.cornerRadius = 5.0;
    lbl.clipsToBounds = YES;
}

- (void)manageUnitInfoForParkings {
    dictSourse = manager.dict;
    
    
    
    arrList = @[@"Appartment", @"Size", @"Price", @"BSP", @"PLC:", @"EDC/IDC:", @"Club Membership:", @"Parking Charges:",@"IBMS:", @"IFMS:", @"Service Tax of Club Mem + PLC (15%)", @"Service Tax of BSP (4.5%)",@"Service Tax of Car Parking (3.09%)", @"Booking Fee:"];
    
    arrSubList = [NSMutableArray array];
    
    /*
     ============================================================================================================================================================
     WARNING:::::::::     Please Don't change order of Array ---> "arrSubList"
     ============================================================================================================================================================
     */
    
    if (![[manager.dict objectForKey:@"display_name"] isKindOfClass:[NSNull class]]) {
        [arrSubList addObject:[manager.dict objectForKey:@"display_name"]];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"%@ sq. ft.", [manager.dict objectForKey:@"size"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"-- sq. ft."];
    }
    
    if (![[manager.dict objectForKey:@"price_SqFt"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@ PSF", [manager.dict objectForKey:@"price_SqFt"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"-- PSF"];
    }
    
    
    if (![[manager.dict objectForKey:@"bsp"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"bsp"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"total_plc"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"total_plc"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"total_edc_idc"] isKindOfClass:[NSNull class]]) {//
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"total_edc_idc"]];
        
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"club_charges"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"club_charges"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"parking_charges"] isKindOfClass:[NSNull class]]) {
        
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [self setExactPriceRange:[NSString stringWithFormat:@"%0.0f", total_parking_charges * selectedParking]]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"total_ibms"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"total_ibms"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"total_ifms"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"total_ifms"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    
    if (![[manager.dict objectForKey:@"ser_tax_club_plc"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"ser_tax_club_plc"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    
    if (![[manager.dict objectForKey:@"total_bsp_tax"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"total_bsp_tax"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    if (![[manager.dict objectForKey:@"parking_service_tax"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %0.0f", tax_On_Parking * selectedParking  * total_parking_charges / 100];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    
    
    if (![[manager.dict objectForKey:@"booking_fees"] isKindOfClass:[NSNull class]]) {
        NSString *str = [NSString stringWithFormat:@"\u20B9 %@", [manager.dict objectForKey:@"booking_fees"]];
        [arrSubList addObject:str];
    }
    else {
        [arrSubList addObject:@"--"];
    }
    [tblView reloadData];
    
}
@end
