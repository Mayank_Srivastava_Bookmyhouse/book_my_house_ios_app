//
//  AgentVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/16/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "AgentVC.h"

static NSString * const strNoPhoneNumber = @"Phone number not available";
static NSString * const strAgentName     = @"Please fill Agent Name";
static NSString * const strCorrectName   = @"Please fill correct Agent Name";
static NSString * const strCuponcode     = @"Please enter coupon code";

@interface AgentVC () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIButton *btnPayment;
    __weak IBOutlet UITextField *txtCoupon;
    __weak IBOutlet UITextField *txtAgentName;
    __weak IBOutlet UIButton *btnCallUs;
    
    int direction;
    int shakes;
    
    NSDictionary *dictSourse;
    PaymentModal *manager;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation AgentVC

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void)loadView {
    manager = [PaymentModal sharedManager];
    
    [super loadView];
    [self manageUnitInfoForParkings];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self screenSettings];
    
    UIView *upperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtAgentName.frame))];
    UIView *lowerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtCoupon.frame))];

    upperView.backgroundColor = [UIColor whiteColor];
    
    txtAgentName.leftView = upperView;
    txtAgentName.leftViewMode = UITextFieldViewModeAlways;
    
    txtCoupon.leftView = lowerView;
    txtCoupon.leftViewMode = UITextFieldViewModeAlways;
    
    txtCoupon.layer.borderColor = txtAgentName.layer.borderColor = [UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0].CGColor;
    
    txtCoupon.layer.borderWidth = txtAgentName.layer.borderWidth = 1.0;
    
    txtAgentName.placeholder = @"Agent Name";
    txtCoupon.placeholder = @"Coupon code";
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    [self setNeedsStatusBarAppearanceUpdate];
    NSString *str = [NSString stringWithFormat:@"Helpline: %@", [dictSourse objectForKey:@"builder_contactno"]];
    [btnHelp setTitle:str forState:UIControlStateNormal];
    
    NSString *str1 = [NSString stringWithFormat:@"Call us on %@ to become a BMH agent", [dictSourse objectForKey:@"builder_contactno"]];
    
    [btnCallUs setTitle:str1 forState:UIControlStateNormal];

}

- (void)updateScreenSettings {
    
}

#pragma mark - Button Action
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnMenu) {
        
    }
    else if (sender == btnHelp || sender == btnCallUs) {
        if (![[dictSourse objectForKey:@"builder_contactno"] isKindOfClass:[NSNull class]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [dictSourse objectForKey:@"builder_contactno"]]]];
        }
        else {
            [self.navigationController.view makeToast:strNoPhoneNumber duration:duration position:CSToastPositionCenter];
        }
    }
    else if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnPayment) {
        
        direction = 1;
        shakes = 0;
        
        if (txtAgentName.text.length == 0) {
            [self.navigationController.view makeToast:strAgentName duration:duration position:CSToastPositionCenter];
            [self shake:txtAgentName];
            return;
        }
        
        if (![AuxillaryMathods isUserName:txtAgentName.text]) {
            [self.navigationController.view makeToast:strCorrectName duration:duration position:CSToastPositionCenter];
            [self shake:txtAgentName];
            return;
        }
        
        if (txtCoupon.text.length == 0) {
            [self.navigationController.view makeToast:strCuponcode duration:duration position:CSToastPositionCenter];
            [self shake:txtCoupon];
            return;

        }
    }
}

#pragma mark - API Actions

#pragma mark - Auxillary Mathods
- (void)manageUnitInfoForParkings {
    dictSourse = manager.dict;
}

#pragma mark - Shake text Field
-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}
@end
