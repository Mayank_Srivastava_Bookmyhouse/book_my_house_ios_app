//
//  PersonalCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/13/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomTextField : UITextField
@property (assign, nonatomic) NSInteger section;
@property (assign, nonatomic) NSInteger row;
@end

@interface CaptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCaption;
+ (CaptionCell *)createCell;
@end

@interface InfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption;
@property (weak, nonatomic) IBOutlet CustomTextField *txtField;
+ (InfoCell *)createCell;
@end

@interface AgentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnBook;
+ (AgentCell *)createCell;
@end





@interface SubInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CustomTextField *txtInfo;
+ (SubInfoCell *)createCell;
@end


