//
//  PersonalCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/13/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PersonalCell.h"

@implementation CaptionCell

+ (CaptionCell *)createCell {
    CaptionCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"PersonalCell" owner:self options:nil] firstObject];
    
    return cell;
}
@end


@implementation InfoCell

+ (InfoCell *)createCell {
    InfoCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"PersonalCell" owner:self options:nil] objectAtIndex:1];
    
    return cell;
}

@end

@implementation AgentCell

+ (AgentCell *)createCell {
    AgentCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"PersonalCell" owner:self options:nil] objectAtIndex:2];
    
    return cell;
}

@end

@implementation CustomTextField

@end

@implementation SubInfoCell
+ (SubInfoCell *)createCell {
    SubInfoCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"PersonalCell" owner:self options:nil] objectAtIndex:3];
    
    return cell;
}

@end


