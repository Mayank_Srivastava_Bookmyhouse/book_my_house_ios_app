//
//  PersonalInformation.h
//  Book My House
//
//  Created by Mayank Srivastava on 5/13/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalCell.h"
#import "PaymentModal.h"
#import "AgentVC.h"
#import "Header.h"
#import "MakePaymentVC.h"

@interface PersonalInformation : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end
