//
//  PersonalInformation.m
//  Book My House
//
//  Created by Mayank Srivastava on 5/13/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PersonalInformation.h"


static NSString * const strID1                                    = @"cell1";
static NSString * const strID2                                    = @"cell2";
static NSString * const strID3                                    = @"cell3";
static NSString * const strID4                                    = @"cell4";

static NSString * const strNoPhoneNumber                          = @"Phone number not available";
static NSString * const strFillFirstName                          = @"Please fill your first name";
static NSString * const strValidFirstName                         = @"Please fill your valid first name";
static NSString * const strFillSecondName                         = @"Please fill your second name";
static NSString * const strValidSecondName                        = @"Please fill your valid second name";
static NSString * const strFillEmail                              = @"Please fill your email id";
static NSString * const strValidEmail                             = @"Please fill your valid email id";

static NSString * const strFillPhone                              = @"Please enter your phone number";
static NSString * const strValidPhone                             = @"Please enter valid phone number";
static NSString * const strValidDoB                               = @"Please enter correct Date of birth";
static NSString * const strFillCoApp                              = @"Please enter co-applicants name";
static NSString * const strValidCoApp                             = @"Please fill correct co-applicants name";

static NSString * const strFillPAN                                = @"Please enter your PAN number";

static NSString * const strValidPAN                               = @"Please enter PAN number";

static NSString * const strFillAddress                            = @"Please enter your address";
static NSString * const strFillCity                               = @"Please enter city name";
static NSString * const strCorrectCity                            = @"Please enter city name correctly";
static NSString * const strFillState                              = @"Please enter state name";
static NSString * const strCorrectState                           = @"Please enter state name correctly";
static NSString * const strFillPIN                                = @"Please enter PIN code";
static NSString * const strCorrectPin                             = @"Please enter PIN correctly";
static NSString * const strFillNationality                        = @"Please enter nationality";
static NSString * const strFillCorrectNationality                 = @"Please enter correct nationality";

@interface PersonalInformation () {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIButton *btnPayment;
    __weak IBOutlet UITableView *tblView;
    
    NSArray *arrPlaceholder, *arrHeader;
    NSMutableArray *answers;
    int direction;
    int shakes;
    PaymentModal *manager;
    NSDictionary *dictSourse;
    NSMutableDictionary *dict;
    BOOL isDataFetched;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation PersonalInformation

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)loadView {
    
    dict = [NSMutableDictionary dictionary];
    
    manager = [PaymentModal sharedManager];
    answers = [NSMutableArray array];
    
    
    arrHeader = @[@" Applicant Information",@" Additional Information"];
    
    [answers addObject:@[@"", @"", @"", @"", @"", @"", @""]];
    [answers addObject:@[@"", @"", @"", @"", @""]];
    
    
    arrPlaceholder = @[@[@"First Name", @"Last name", @"Email", @"Phone", @"Date Of Birth", @"Co-applicant", @"Pan"], @[@"Address", @"City", @"State", @"Pin Code", @"Nationality"]];
    
    [super loadView];
    
    [self manageUnitInfoForParkings];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
    [self loadAPI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    NSString *str = [NSString stringWithFormat:@"Helpline: %@", [dictSourse objectForKey:@"builder_contactno"]];
    [btnHelp setTitle:str forState:UIControlStateNormal];
    
}

- (void)updatedScreenSettings {
    
}
#pragma mark - API Actions

- (void)loadAPI {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSString *strUserID = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        [dict setObject:strUserID forKey:@"user_id"];
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI showUserInfo:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                NSMutableArray *arr = [NSMutableArray array];
                [answers removeAllObjects];
                if (![[data objectForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"first_name"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"last_name"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"last_name"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"email"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"email"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"mobile"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"mobile"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                [arr addObject:@""];
                [arr addObject:@""];
                [arr addObject:@""];
                
                [answers addObject:[arr copy]];
                [arr removeAllObjects];
                
                
                if (![[data objectForKey:@"address"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"address"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"city"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"city"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"state"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"state"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"zip"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"zip"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                if (![[data objectForKey:@"nationality"] isKindOfClass:[NSNull class]]) {
                    [arr addObject:[data objectForKey:@"nationality"]];
                }
                else {
                    [arr addObject:@""];
                }
                
                [answers addObject:[arr copy]];
                isDataFetched = YES;
                [tblView reloadData];
            }
            else {
                
            }
        }];
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadAPI) fromClass:self];
    }
    
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnMenu) {
        
    }
    else if (sender == btnHelp) {
        if (![[dictSourse objectForKey:@"builder_contactno"] isKindOfClass:[NSNull class]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [dictSourse objectForKey:@"builder_contactno"]]]];
        }
        else {
            [self.view makeToast:strNoPhoneNumber duration:duration position:CSToastPositionCenter];
        }
    }
    else if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnPayment) {
        
        
        direction = 1;
        shakes = 0;
        
        
        NSString *strUserID = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        [dict setObject:strUserID forKey:@"user_id"];
        NSString *str = [[answers objectAtIndex:0] objectAtIndex:0];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            
            [self.view makeToast:strFillFirstName
                                             duration:duration
                                             position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isUserName:str]) {
            
            [self.view makeToast:strValidFirstName
                                             duration:duration
                                             position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"first_name"];
        }
        
        
        str = [[answers objectAtIndex:0] objectAtIndex:1];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//        if (str.length == 0) {
//            [self.navigationController.view makeToast:strFillSecondName
//                                             duration:duration
//                                             position:CSToastPositionCenter];
//            return;
//        }
        if (![AuxillaryMathods isUserName:str]) {
            [self.view makeToast:strValidSecondName
                                             duration:duration
                                             position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"second_name"];
        }
        
        
        str = [[answers objectAtIndex:0] objectAtIndex:2];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillEmail
                                             duration:duration
                                             position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isValidEmail:str]) {
            [self.view makeToast:strValidEmail
                                             duration:duration
                                             position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"email"];
        }
        
        
        str = [[answers objectAtIndex:0] objectAtIndex:3];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillPhone duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isValidMobileNumber:str]) {
            [self.view makeToast:strValidPhone duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"mobile"];
        }
        
        str = [[answers objectAtIndex:0] objectAtIndex:4];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strValidDoB duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"dob"];
        }
       
        str = [[answers objectAtIndex:0] objectAtIndex:5];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//        if (str.length == 0) {
//            [self.navigationController.view makeToast:strFillCoApp duration:duration position:CSToastPositionCenter];
//            return;
//        }
        if (![AuxillaryMathods isUserName:str]) {
            [self.view makeToast:strValidCoApp duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"co_applicant"];
        }
        
        
        str = [[answers objectAtIndex:0] objectAtIndex:6];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillPAN duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (str.length  != 10) {
            [self.view makeToast:strValidPAN duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"pan"];
        }
        
        str = [[answers objectAtIndex:1] objectAtIndex:0];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillAddress duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"address"];
        }
        
        str = [[answers objectAtIndex:1] objectAtIndex:1];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillCity duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isCorrectLocation:str]) {
            //strCorrectCity
            [self.view makeToast:strCorrectCity duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"city"];
        }
        
        str = [[answers objectAtIndex:1] objectAtIndex:2];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillState duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isCorrectLocation:str]) {
            //strCorrectCity
            [self.view makeToast:strCorrectState duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"state"];
        }
        
        str = [[answers objectAtIndex:1] objectAtIndex:3];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillPIN duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (str.length != 6) {
            [self.view makeToast:strCorrectPin duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isValidPIN:str]) {
            [self.view makeToast:strCorrectPin duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"zip"];
        }
        
        str = [[answers objectAtIndex:1] objectAtIndex:4];
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length == 0) {
            [self.view makeToast:strFillNationality duration:duration position:CSToastPositionCenter];
            return;
        }
        else if (![AuxillaryMathods isCorrectLocation:str]) {
            [self.view makeToast:strFillCorrectNationality duration:duration position:CSToastPositionCenter];
            return;
        }
        else {
            [dict setObject:str forKey:@"nationality"];
        }
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI setPersonalInformation:dict withCallback:^(BOOL isSuccess, id data) {
            if (isSuccess) {
                NSMutableDictionary *dictReq = [NSMutableDictionary dictionary];
                NSString *strUserID = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
                [dictReq setObject:strUserID forKey:@"user_id"];
                [dictReq setObject:[manager.dict objectForKey:@"project_id"] forKey:@"unit_id"];
                
                [ServiceAPI postPaymentForm:dictReq withCallback:^(BOOL isSuccess, id data) {
                    [[AppDelegate share] enableUserInteraction];
                    if (isSuccess) {
                        manager.dictActionForm = data;
                        
                        CCAvenueVC *obj= [self.storyboard instantiateViewControllerWithIdentifier:@"CCAvenueVC"];
                        [self.navigationController pushViewController:obj animated:YES];
                        
                    }
                    else {
                        [self.view makeToast:@"Some error occured" duration:duration position:CSToastPositionCenter];
                    }
                }];
            }
            else {
                [[AppDelegate share] enableUserInteraction];
            }
        }];
    }
}

- (void)didSelectPayment:(UIButton *)btn {
    
    AgentVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"AgentVC"];
    [self.navigationController pushViewController:obj animated:YES];
}


- (void)didSelectDateOfBirth:(UIDatePicker *)picker {
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd-MM-YYYY"];
    
    NSString *str = [format stringFromDate:picker.date];
    InfoCell *cell =[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    cell.txtField.text = str;
}

#pragma mark - Table View Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (isDataFetched) {
        return arrPlaceholder.count;
    }
    else {
        return 0;
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [[arrPlaceholder objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CaptionCell *cell = [tableView dequeueReusableCellWithIdentifier:strID1];
    
    if (cell == nil) {
        cell = [CaptionCell createCell];
        cell.lblCaption.text = [arrHeader objectAtIndex:section];
    }
    
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        if (indexPath.row != 4) {
            
            return [self confugureTableView:tableView withCellForRowAtIndexPath:indexPath];
        }
        else {
            return [self configurePickerBasedTableView:tableView withCellForRowAtIndexPath:indexPath];
        }
    }
    else {
        return [self confugureTableView:tableView withCellForRowAtIndexPath:indexPath];
    }
}


- (UITableViewCell *)confugureTableView:(UITableView *)tableView withCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SubInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strID4];
    
    if (cell == nil) {
        cell = [SubInfoCell createCell];
        cell.txtInfo.section = indexPath.section;
        cell.txtInfo.row = indexPath.row;
        cell.txtInfo.delegate = self;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(cell.txtInfo.frame))];
        cell.txtInfo.leftView = view;
        cell.txtInfo.leftViewMode = UITextFieldViewModeAlways;
        
        
        if (indexPath.section == 0) {
            
            if (indexPath.row == 0 && indexPath.row == 1 && indexPath.row == 5 && indexPath.row == 6) {
                cell.txtInfo.keyboardType = UIKeyboardTypeDefault;
            }
            else if (indexPath.row == 2) {
                cell.txtInfo.keyboardType = UIKeyboardTypeEmailAddress;
            }
            else if (indexPath.row == 3) {
                cell.txtInfo.keyboardType = UIKeyboardTypePhonePad;
            }
        }
        else {
            
            if (indexPath.row == 0 && indexPath.row == 1 && indexPath.row == 2 && indexPath.row == 4) {
                cell.txtInfo.keyboardType = UIKeyboardTypeDefault;
            }
            else if (indexPath.row == 3) {
                cell.txtInfo.keyboardType = UIKeyboardTypeNumberPad;
            }
        }
    }
    
    cell.txtInfo.placeholder = [[arrPlaceholder objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cell.txtInfo.text = [[answers objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    return cell;
    
}

- (UITableViewCell *)configurePickerBasedTableView:(UITableView *)tableView withCellForRowAtIndexPath:(NSIndexPath *)indexPath {
        InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strID2];

    if (cell == nil) {
        cell = [InfoCell createCell];
        cell.txtField.section = indexPath.section;
        cell.txtField.row = indexPath.row;
        cell.txtField.delegate = self;
        
        
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.maximumDate = [NSDate date];
        cell.txtField.inputView = datePicker;

        [datePicker addTarget:self action:@selector(didSelectDateOfBirth:) forControlEvents:UIControlEventValueChanged];

        
    }

    cell.lblCaption.text = [[arrPlaceholder objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(cell.txtField.frame))];
    cell.txtField.leftView = view;
    cell.txtField.leftViewMode = UITextFieldViewModeAlways;

    cell.txtField.text = [[answers objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    return cell;
    
}




- (void)manageUnitInfoForParkings {
    dictSourse = manager.dict;
}


#pragma mark - Shake text Field
-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

#pragma mark - Text field delegates
- (void)textFieldDidEndEditing:(CustomTextField *)textField {
    NSMutableArray *arr = [[answers objectAtIndex:textField.section] mutableCopy];
    [arr replaceObjectAtIndex:textField.row withObject:textField.text];
    [answers replaceObjectAtIndex:textField.section withObject:arr];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength <= 10;
        
    }
    else {
        return YES;
    }
    
    
}
@end
