//
//  PrivacyViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

typedef NS_ENUM(NSUInteger, PrivacyViewControllerAccessOption) {
    PrivacyViewControllerAccessOptionPrivacy,
    PrivacyViewControllerAccessOptionTermsOfUse,
    PrivacyViewControllerAccessOptionTermsAndConditions,
    PrivacyViewControllerAccessOptionMisc,
};
@interface PrivacyViewController : UIViewController<UIWebViewDelegate>
@property (assign, nonatomic) PrivacyViewControllerAccessOption option;

@end
