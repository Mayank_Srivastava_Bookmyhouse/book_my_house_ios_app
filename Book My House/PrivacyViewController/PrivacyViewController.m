//
//  PrivacyViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PrivacyViewController.h"



static NSString * const strNoConnectivity = @"No internet connectivity";

static NSString * const strTitlePrivacy = @"Privacy Policy";//http://bookmyhouse.com/
static NSString * const stPrivacyPolicyUrl = @"http://bookmyhouse.com/api/privacy_policy_ios.php";
//static NSString * const stPrivacyPolicyUrl = @"http://52.77.73.171/apimain/api/privacy_policy_ios.php";
static NSString * const strTitleTermsOfUse = @"Terms of use";
static NSString * const strTermsOfUseUrl = @"http://bookmyhouse.com/api/term_condition_ios.php";
//static NSString * const strTermsOfUseUrl = @"http://52.77.73.171/apimain/api/term_condition_ios.php";
static NSString * const strTitleTermsAndConditions = @"Terms And Conditions";







@interface PrivacyViewController () {
    
    __weak IBOutlet UILabel *lblHeading;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}
- (IBAction)actionListeners:(id)sender;

@end

@implementation PrivacyViewController


#pragma mark - View Controller Mathods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self screenSettings];
}

- (BOOL)prefersStatusBarHidden {
    
    return YES;
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if (_option == PrivacyViewControllerAccessOptionPrivacy) {
            lblHeading.text =strTitlePrivacy;
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI getPrivacyPolicyWithUrl:stPrivacyPolicyUrl withCallBach:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                //[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:stPrivacyPolicyUrl]]];
                NSString *str = (NSString *)data;
                [webView loadHTMLString:str baseURL:nil];
            }];
            
        }
        else if (_option == PrivacyViewControllerAccessOptionTermsOfUse) {
            lblHeading.text = strTitleTermsOfUse;
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI getPrivacyPolicyWithUrl:strTermsOfUseUrl withCallBach:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                //[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strTermsOfUseUrl]]];
                NSString *str = (NSString *)data;
                [webView loadHTMLString:str baseURL:nil];
            }];
            //[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strTermsOfUseUrl]]];
        }
        else if (_option == PrivacyViewControllerAccessOptionTermsAndConditions){
            lblHeading.text = strTitleTermsAndConditions;
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI getPrivacyPolicyWithUrl:strTermsOfUseUrl withCallBach:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                //[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strTermsOfUseUrl]]];
                NSString *str = (NSString *)data;
                [webView loadHTMLString:str baseURL:nil];
            }];
        }
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(screenSettings) fromClass:self];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)actionListeners:(id)sender {
    
    if ([sender isKindOfClass:[UIButton class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Webview delegates
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
//        NSLog(@"Scheme %@", request.URL.absoluteString);
//        
//        if ([request.URL.scheme isEqualToString:@"http"]) {
//            
//        }
//        else if ([request.URL.scheme isEqualToString:@"tel"]) {
//            NSString *str = @"tel:\\";
//            str = [str stringByAppendingString:request.URL.absoluteString];
//            [[UIApplication sharedApplication] openURL:[request URL]];
//        }
//        else {
//            
//        }
        
        
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
        [activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.navigationController.view makeToast:strNoConnectivity
                                     duration:duration
                                     position:CSToastPositionCenter];
}

@end
