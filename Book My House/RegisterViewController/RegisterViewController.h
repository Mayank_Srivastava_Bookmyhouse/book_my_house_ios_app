//
//  RegisterViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "CustomTableCell.h"
#import "SearchViewController.h"

typedef NS_ENUM(NSUInteger, RegisterOption) {
    RegisterOptionNormal,
    RegisterOptionPayment,
    
    RegisterOptionFavorite,
    RegisterOptionMisc,
};

@interface RegisterViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, CustomTableCellDelegate>
@property(assign, nonatomic) RegisterOption option;
@end
