//
//  RegisterViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "RegisterViewController.h"
#import "PrivacyViewController.h"



static NSString * const strFillName = @"Please fill your name";
static NSString * const strValidName = @"Please fill your valid name";

static NSString * const strFillEmail = @"Please fill your email id";
static NSString * const strValidEmail = @"Please fill your valid email id";


static NSString * const strFillPassword = @"Please fill your password";
static NSString * const strValidPassword = @"Password should be of 6 or more characters";

static NSString * const strFillMobileNumber = @"Please fill your mobile number";
static NSString * const strValidMobileNumber = @"Please fill your valid mobile number";

@interface RegisterViewController () {
    int direction;
    int shakes;
    NSMutableDictionary *dict;
    
    UITapGestureRecognizer *tap;
}

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
- (IBAction)actionListener:(id)sender;

@end

@implementation RegisterViewController


#pragma mark - UIViewController Mathods
- (void)viewDidLoad {
    
    dict = [NSMutableDictionary dictionary];
    
    [super viewDidLoad];
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarByPosition];
    [self setNeedsStatusBarAppearanceUpdate];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self screenSettings];
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Screen settings
- (void)screenSettings {
    
    _tblView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
    [_tblView addGestureRecognizer:tap];
    
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Already a member? " attributes:@{
                                                                                                                      
                                                                                                                      NSForegroundColorAttributeName : [UIColor lightGrayColor],
                                                                                          NSFontAttributeName :[UIFont fontWithName:@"PT Sans" size:16.0]
                                                                                                                      }];
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Login Here" attributes:@{
                                                                                                         
                                                                                                         NSForegroundColorAttributeName : [UIColor colorWithRed:0 green:148.0/255.0 blue:122.0/255.0 alpha:1.0],
                                                                                          NSFontAttributeName :[UIFont fontWithName:@"PT Sans" size:16.0]                                                                                                  }];
    
    [str appendAttributedString:str1];
    
    [_btnSignIn setAttributedTitle:str forState:UIControlStateNormal];
    _btnSignIn.superview.layer.borderWidth = 0.5;
    _btnSignIn.superview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    

}

- (void)resignKeyboard {

    for (int i = 0; i <= 3; i++) {
        CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:i  inSection:0]];
        [cell.txtTitle resignFirstResponder];

    }
}


#pragma mark - Table View Delegates and Datasourse
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tblView.frame), 20)];
    aView.backgroundColor = [UIColor clearColor];
    return aView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row >= 0 && indexPath.row <= 3) {
        return 50;
    }
    else if (indexPath.row == 4) {
        return 50;
    }
    else {
        return 45;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row >= 0 && indexPath.row <= 3) {
        
        static NSString *reuseIdentifier = @"CustomTableCell1";
        
        CustomTableCell *cell = (CustomTableCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (cell == nil) {
            cell = [CustomTableCell createCellWithDelegate:self];
            cell.txtTitle.backgroundColor = [UIColor clearColor];
            CALayer *layer = [CALayer layer];
            layer.frame = CGRectMake(0, CGRectGetHeight(cell.txtTitle.frame) - 2, CGRectGetWidth(cell.txtTitle.frame), 2);
            layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
            [cell.txtTitle.layer addSublayer:layer];
            
            
            if (indexPath.row == 3) {
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, CGRectGetHeight(cell.txtTitle.frame))];
                view.backgroundColor = [UIColor clearColor];
                UIButton *btn = [[UIButton alloc] initWithFrame:view.frame];
                btn.titleLabel.font = [UIFont fontWithName:@"PT Sans" size:16.0];
                [btn setTitleColor:[UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                [btn setTitle:@"Show" forState:UIControlStateSelected];//
                [btn setTitle:@"Hide" forState:UIControlStateNormal];
                btn.selected = YES;
                [btn addTarget:self action:@selector(hideShowPassword:) forControlEvents:UIControlEventTouchUpInside];
                [view addSubview:btn];
                
                cell.txtTitle.rightView = view;
                cell.txtTitle.rightViewMode = UITextFieldViewModeWhileEditing;
            }
            
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.txtTitle.tag = indexPath.row;
        if (indexPath.row == 0) {
            cell.txtTitle.placeholder = @"Name";
            cell.txtTitle.keyboardType = UIKeyboardTypeDefault;
            cell.txtTitle.secureTextEntry = NO;
            cell.txtTitle.returnKeyType = UIReturnKeyNext;
            
        }
        else if (indexPath.row == 1) {
            cell.txtTitle.placeholder = @"Email";
            cell.txtTitle.keyboardType = UIKeyboardTypeEmailAddress;
            cell.txtTitle.secureTextEntry = NO;
            cell.txtTitle.returnKeyType = UIReturnKeyNext;
        }
        else if (indexPath.row == 2) {
            cell.txtTitle.placeholder = @"Mobile No.";
            cell.txtTitle.keyboardType = UIKeyboardTypePhonePad;
            cell.txtTitle.secureTextEntry = NO;
            cell.txtTitle.returnKeyType = UIReturnKeyGo;
        }
        else if (indexPath.row == 3) {
            cell.txtTitle.placeholder = @"Password";
            cell.txtTitle.keyboardType = UIKeyboardTypeDefault;
            cell.txtTitle.secureTextEntry = YES;
            cell.txtTitle.returnKeyType = UIReturnKeyNext;
        }
        return cell;
    
    }
    else if (indexPath.row == 4) {
        static NSString *reuseIdentifier = @"CustomTableCell3";
        CustomTableCell *cell = (CustomTableCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (cell == nil) {
            cell = [CustomTableCell createSignUpCellWithdelegate:self];
            cell.btnsignUp.layer.borderWidth = 1.5;
            cell.btnsignUp.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
        }
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    else if (indexPath.row == 5) {
       
        static NSString *reuseIdentifier = @"CustomTableCell2";
        CustomTableCell *cell = (CustomTableCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (cell == nil) {
            cell = [CustomTableCell createInstructionCell];
            cell.backgroundColor = [UIColor clearColor];
            
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"By creating an account, you agree to our " attributes:@{
                                                                                                                                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]
                                                                                                                                                         }];
            
            NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Terms and Conditions" attributes:@{
                                                                                                                       NSForegroundColorAttributeName: [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0],
                                                                                                                       NSUnderlineStyleAttributeName: @"1"
                                                                                                                       
                                                                                                                       }];
            
            [str appendAttributedString:str1];
            cell.lblPolicy.attributedText = str;
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame))];
            btn.backgroundColor = [UIColor clearColor];
            [cell addSubview:btn];
            [btn addTarget:self action:@selector(termsAndConditions:) forControlEvents:UIControlEventTouchUpInside];
        
        }
        
        return cell;
    }
    else {
        UITableViewCell *cell;
        
        return cell;
    }
}



#pragma mark - Button Actions

- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == _btnSignIn) {
        
        
        
        
         BOOL isSignInViewControllerExist = NO;
         for (SigninViewController *viewController in self.navigationController.viewControllers) {
         
         if ([viewController isKindOfClass:[SigninViewController class]]) {
             isSignInViewControllerExist = YES;
             break;
         }
         else {
         
         }
         }
         
            if (isSignInViewControllerExist) {
                for (SigninViewController *viewController in self.navigationController.viewControllers) {
         
                    if ([viewController isKindOfClass:[SigninViewController class]]) {
                        SigninViewController *obj = (SigninViewController *)viewController;
                        [self.navigationController popToViewController:obj animated:YES];
                        break;
                    }
                    else {
         
                    }
                }
         }
         else {
             SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
             [self.navigationController pushViewController:obj animated:YES];
         }
    }
    
}

- (void)termsAndConditions:(UITapGestureRecognizer *)gesture {
    
    PrivacyViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyViewController"];
    obj.option = PrivacyViewControllerAccessOptionTermsAndConditions;
    [self.navigationController pushViewController:obj animated:YES];

}

- (void)hideShowPassword:(UIButton *)sender {
    
    
    sender.selected = !sender.selected;
    
    CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0]];
    cell.txtTitle.secureTextEntry = sender.selected;
    [ cell.txtTitle becomeFirstResponder];
}


#pragma mark - Touch delegates
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[_tblView indexPathForSelectedRow]];
    [cell.txtTitle resignFirstResponder];
    
}


#pragma mark - TextField Delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return newLength <= 10;
    }
    else
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.tag <= 3) {
        CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:textField.tag + 1 inSection:0]];
        [cell.txtTitle becomeFirstResponder];
    }
    else if (textField.tag == 3) {
        
    }
    
    return YES;
}


#pragma mark - Custom Table Cell Delegate
- (void)signUpButtonClicked {
    
    direction = 1;
    shakes = 0;
    
    
    NSString *str;
    CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (str.length == 0) {
        
        [self.view makeToast:strFillName
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
    }
    else if (![AuxillaryMathods isUserName:str]) {
        
        [self.view makeToast:strValidName
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
    }
    else {
        [dict setObject:cell.txtTitle.text forKey:@"first_name"];
    }
    
    cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
    str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (str.length == 0) {
        [self.view makeToast:strFillEmail
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
        
    }
    else if (![AuxillaryMathods isValidEmail:str]) {
        
        [self.view makeToast:strValidEmail
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
        
    }
    else {
        [dict setObject:cell.txtTitle.text forKey:@"email"];
    }
    
    cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0]];
    str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (str.length == 0) {
        
        [self.view makeToast:strFillPassword
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
    }
    else if (![AuxillaryMathods isValidPassword:str]) {
        [self shake:cell.txtTitle];
        [self.view makeToast:strValidPassword
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
    }
    else {
        [dict setObject:cell.txtTitle.text forKey:@"password"];
    }

    cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0]];
    str = [cell.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (str.length == 0) {
        
        [self.view makeToast:strFillMobileNumber
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
    }
    else if (![AuxillaryMathods isValidMobileNumber:str]) {
        
        [self.view makeToast:strValidMobileNumber
                                         duration:duration
                                         position:CSToastPositionCenter];
        [self shake:cell.txtTitle];
        return;
    }
    else {
        //first_name,email,mobile,password,
        [dict setObject:cell.txtTitle.text forKey:@"mobile"];
    }
 [[AFNetworkReachabilityManager sharedManager] startMonitoring];
#if (TARGET_IPHONE_SIMULATOR)

    [[AppDelegate share] disableUserInteractionwithLoader:YES];
    [ServiceAPI signUpWithParams:dict
                      mathodName:urlRegister
                fromCallingClass:self
                     andCallback:@selector(didReceiveRegistrationResponse:)];
    
    
#else
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI signUpWithParams:dict
                          mathodName:urlRegister
                    fromCallingClass:self
                         andCallback:@selector(didReceiveRegistrationResponse:)];
    }
    else {
        [self.navigationController.view makeToast:strInternetNotAvailable
                                         duration:duration
                                         position:CSToastPositionCenter];
    }
#endif
    
}




#pragma mark - Shake text Field
-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}


#pragma mark - API Actions
- (void)didReceiveRegistrationResponse:(id)response {

    [[AppDelegate share] enableUserInteraction];
    
    if (![response isKindOfClass:[NSNull class]] && [response isKindOfClass:[NSDictionary class]]) {
        
        if ([[response objectForKey:@"success"] boolValue]) {
            //[self.navigationController popViewControllerAnimated:YES];
            
            BOOL isSignInScreenAvailable = NO;
            for (UIViewController *controller in self.navigationController.viewControllers) {
                
                if ([controller isKindOfClass:[SigninViewController class]]) {
                    
                    isSignInScreenAvailable = YES;
                    break;
                }
                
            }
            
            if (isSignInScreenAvailable) {
                for (UIViewController *controller in self.navigationController.viewControllers) {
                    
                    if ([controller isKindOfClass:[SigninViewController class]]) {
                        
                        SigninViewController *obj = (SigninViewController *)controller;
                        [self.navigationController popToViewController:obj animated:YES];
                        
                    }
                    
                }
            }
            else {
                SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
                [self.navigationController pushViewController:obj animated:YES];
            }
            
            [self.view makeToast:@"You have successfully registered. Please login to continue" duration:2 * duration position:CSToastPositionCenter];
        }
        else {
            if (![[response objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                
                for (int i = 0; i <= 3; i++) {
                    CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:i  inSection:0]];
                    cell.txtTitle.text = @"";
                    //[cell.txtTitle resignFirstResponder];
                    
                }
                
                [self.view makeToast:[response objectForKey:@"message"]
                                                 duration:duration
                                                 position:CSToastPositionCenter];
            }
        }
        
        
    }
    else {
        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
    }
}



#pragma mark - Auxillary Mathods
- (void)cancelNumberPad {
    CustomTableCell *cell = [_tblView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0]];
    [cell.txtTitle resignFirstResponder];
}

- (void)doneWithNumberPad {
    [self cancelNumberPad];
    [self signUpButtonClicked];
}
@end
