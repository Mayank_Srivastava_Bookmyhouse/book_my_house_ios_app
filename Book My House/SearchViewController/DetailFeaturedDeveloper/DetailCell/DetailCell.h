//
//  DetailCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/19/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgTop;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblFeatures;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceRange;
@property (weak, nonatomic) IBOutlet UILabel *lblPricePSF;
@property (weak, nonatomic) IBOutlet UILabel *lblPossessionDate;

@property (weak, nonatomic) IBOutlet UILabel *lblAppriciation;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;


@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeeds;
@property (weak, nonatomic) IBOutlet UILabel *lblLifestyle;
@end
