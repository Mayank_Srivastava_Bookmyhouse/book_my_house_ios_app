//
//  ProjectListVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCell.h"
#import "Header.h"
#import "ProjectDetailVC.h"
#import "NotificationAlert.h"
#import "FilterVC.h"


typedef NS_ENUM(NSUInteger, DetailFeaturedDeveloperAccessOption) {
    DetailFeaturedDeveloperAccessOptionSeeAllProjects,
    DetailFeaturedDeveloperAccessOptionSearchProperty,
    DetailFeaturedDeveloperAccessOptionSearchScreen,
    DetailFeaturedDeveloperAccessOptionSectors,
    DetailFeaturedDeveloperAccessOptionSpecialProject,
    DetailFeaturedDeveloperAccessOptionMisc,
};
@interface ProjectListVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UITextFieldDelegate >
@property (nonatomic, copy) NSDictionary *dictSourse;
@property (nonatomic, copy) NSDictionary *dictInfo;
@property (nonatomic, copy) NSArray *arrSourse;
@property (nonatomic, assign) DetailFeaturedDeveloperAccessOption option;
@property (nonatomic, strong) UIViewController *owner;
@property (nonatomic, strong) NSString *strCityId;
@property (nonatomic, strong) NSString *strTitle;
- (void)callingAPI;
- (void)addFilters:(NSNotification *)info;
@end
