 //
//  ProjectListVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ProjectListVC.h"
#import "CustomView.h"
#import "PrivacyViewController.h"
#import "MultiFilterVC.h"
#import "ProjectMapVC.h"


static NSString * const strNoMobilenumber                    = @"Please fill your mobile number";
static NSString * const strValidMobileNumber                 = @"Please give your correct mobile number";
static NSString * const strNoName                            = @"Please enter your name";
static NSString * const strInvalidName                       = @"Please enter invalid name";
static NSString * const strNoEmail                           = @"Please give your email id";
static NSString * const strInvalidEmail                      = @"Please fill valid email id";
static NSString * const strPleaseCheckMarkT_C                = @"Please check mark T&C";
static NSString * const strEnquirySuccess                    = @"Alert successfully sent";
static NSString * const strEnquiryFailed                     = @"Alert could not be sent";
//static NSString * const strNoProject                         = @"No Project found in this criteria";

@interface ProjectListVC () {
    
    __weak IBOutlet UIButton *btnback;
    __weak IBOutlet UIButton *btnFilter;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIView *viewFooter;
    __weak IBOutlet UIButton *btnMaps;
    
    
    UIRefreshControl *refreshControl;
    NSMutableArray *arrInfo;
    
    NSUserDefaults *pref;
    NotificationAlert *notifAlert;
    
    CGPoint aPoint;
    int direction, shakes;
    
    GetAlertView *viewAlert;
    NSInteger indexSelected;
    NSMutableDictionary *dictFavoriteReq;
    BOOL isCommercial, isResidential;
    NSArray *arrLegends, *arrSegmentControl;
    NSMutableArray *arrCordinates;
    GMSPath *pathCity;
}

- (IBAction)actionListeners:(id)sender;
@end

@implementation ProjectListVC


#pragma mark - View Controller Mathods
- (void)viewDidLoad {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callingAPI) name:kProjectListMultifilter object:nil];
    pref = [NSUserDefaults standardUserDefaults];
    arrInfo = [NSMutableArray array];
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
    [self callingAPI];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //Setting Background view
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
    [pref removeObjectForKey:strSaveMultiFilter];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Screen Settings
- (void)screenSettings {
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor darkGrayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:loading];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    tblView.contentInset = UIEdgeInsetsMake(-36.0, 0, 0, 0);
    lblTitle.text = [NSString stringWithFormat:@"%@", _strTitle];
    
    
    NSArray *arrInfra = @[@"1 - 2", @"3 - 4", @"5 - 6", @"7 - 8", @"9 - 10"];
    
    arrSegmentControl = @[arrInfra, arrInfra, arrInfra, arrInfra, arrInfra];
    
}

- (void)refresh {
    [refreshControl beginRefreshing];
    [self callingAPI];
}

- (void)callingAPI {

    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        if ([_dictSourse objectForKey:@"builder_id"]) {
            [dict setObject: [_dictSourse objectForKey:@"builder_id"] forKey:@"builder_id"];
        }
        
        if ([_dictSourse objectForKey:@"city_id"]) {
            [dict setObject: [_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
        }
        
        if ([_dictSourse objectForKey:@"type"]) {
            [dict setObject:[_dictSourse objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictSourse objectForKey:@"p_type"]) {
            [dict setObject:[_dictSourse objectForKey:@"p_type"] forKey:@"p_type"];
        }

        if ([_dictSourse  objectForKey:@"location_id"]) {
            [dict setObject:[_dictSourse  objectForKey:@"location_id"] forKey:@"location"];
        }
        
        if ([_dictInfo objectForKey:@"type"]) {
            [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictInfo objectForKey:@"p_type"]) {
            [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictInfo objectForKey:@"city_id"]) {
            [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
        }
        
        
        if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
            [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sublocation_id"];
            
        }
    
        if ([_dictSourse objectForKey:@"sector_id"]) {
            [dict setObject:[_dictSourse objectForKey:@"sector_id"] forKey:@"sublocation_id"];
        }
        
        if ([_dictSourse objectForKey:@"special_category"]) {
            [dict setObject:[_dictSourse objectForKey:@"special_category"] forKey:@"special_category"];
        }
        
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
        }
        
        NSDictionary *dictReq = [pref objectForKey:strSaveMultiFilter];
        if (dictReq) {
            [dict setObject:dictReq forKey:@"filter"];
        }
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
            
            [[AppDelegate share] enableUserInteraction];
            if ([refreshControl isRefreshing]) {
                [refreshControl endRefreshing];
            }
            
            if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects || _option == DetailFeaturedDeveloperAccessOptionSearchProperty || _option == DetailFeaturedDeveloperAccessOptionSearchScreen || _option == DetailFeaturedDeveloperAccessOptionSectors || _option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
                
                if (isSuccess) {
                    if ([legands isKindOfClass:[NSArray class]]) {
                        
                        NSMutableArray *arr = [arrSegmentControl mutableCopy];
                        [arr replaceObjectAtIndex:2 withObject:legands];
                        arrSegmentControl = [arr copy];
                    }
                    
                    if ([locationCordinates isKindOfClass:[NSString class]]) {
                        NSString *str = [NSString stringWithFormat:@"%@", locationCordinates];
                        
                        str = [str stringByReplacingOccurrencesOfString:@"[" withString:@""];
                        str = [str stringByReplacingOccurrencesOfString:@"]" withString:@""];
                        NSArray *arr = [str componentsSeparatedByString:@","];
                        GMSMutablePath *path =[GMSMutablePath path];
                        
                        for (int  i = 0; i < arr.count; i += 2) {
                            [path addLatitude:[[arr objectAtIndex:i] doubleValue] longitude:[[arr objectAtIndex:i+1] doubleValue]];
                        }
                        pathCity = path;
                    }
                    
                    if ([data isKindOfClass:[NSArray class]]) {
                        
                        [arrInfo removeAllObjects];
                        arrInfo = [data mutableCopy];
                        
                        if (arrInfo.count < 2) {
                            btnFilter.hidden = YES;
                            lblTitle.text = [NSString stringWithFormat:@"%@", _strTitle];
                        }
                        
                        [self setprojectCatagories];
                        [tblView reloadData];
                    }
                    else {
                        [arrInfo removeAllObjects];
                        [tblView reloadData];
                        //[self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    [arrInfo removeAllObjects];
                    [tblView reloadData];
                    //[self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
                
            }

        }];

        
        /*
        if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects || _option == DetailFeaturedDeveloperAccessOptionSearchProperty ) {
            
            
            [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                [[AppDelegate share] enableUserInteraction];
                if ([refreshControl isRefreshing]) {
                    [refreshControl endRefreshing];
                }
                
                if (isSuccess) {
                    if (data && [data isKindOfClass:[NSArray class]]) {
                        NSArray *arr = (NSArray *)data;
                        if ([arr count]) {
                            
                            if (arr.count < 2) {
                                btnFilter.hidden = YES;
                                lblTitle.text = [NSString stringWithFormat:@"%@", _strTitle];
                            }
                            
                            arrInfo = [NSMutableArray array];
                            [arrInfo addObjectsFromArray:arr];
                            tblView.backgroundView = nil;
                        }
                        else {
                            tblView.backgroundView = [self setBackground];
                        }
                        [self setprojectCatagories];
                        [tblView reloadData];
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
                
            }];
            
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSearchScreen) {
            
            
            
            
            [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                
                [[AppDelegate share] enableUserInteraction];
                if ([refreshControl isRefreshing]) {
                    [refreshControl endRefreshing];
                }
                if (isSuccess) {
                    
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }];
        }//
        else if (_option == DetailFeaturedDeveloperAccessOptionSectors) {
            
            [ServiceAPI getProjectList:dict
                          withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                              
                              
                              [[AppDelegate share] enableUserInteraction];
                              if ([refreshControl isRefreshing]) {
                                  [refreshControl endRefreshing];
                              }
                              if (isSuccess) {
                                  if ([data isKindOfClass:[NSArray class]]) {
                                      
                                      //[arrInfo removeAllObjects];
                                      arrInfo = [data mutableCopy];
                                      
                                      if (arrInfo.count < 2) {
                                          btnFilter.hidden = YES;
                                          lblTitle.text = [NSString stringWithFormat:@"%@", _strTitle];
                                      }
                                      
                                      [self setprojectCatagories];
                                      [tblView reloadData];
                                  }
                                  else {
                                      [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                                  }
                              }
                              else {
                                  [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                              }
                              
                          }];
            
            
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
            
            [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                
                [[AppDelegate share] enableUserInteraction];
                if ([refreshControl isRefreshing]) {
                    [refreshControl endRefreshing];
                }
                
                
            }];
            
        }
        */
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(callingAPI) fromClass:self];
    }
    
}

- (void)loadLocalityInfo {
    
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"segueDetails"]) {
        
        UIButton *btn = (UIButton *)sender;
        ProjectListVC *obj = segue.destinationViewController;
        obj.dictSourse = [arrInfo objectAtIndex:btn.tag];
        
    }
    else if ([segue.identifier isEqualToString:@"segueMultiFilter"]) {
        
        MultiFilterVC *obj = segue.destinationViewController;
        
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dictTemp in arrInfo) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[dictTemp objectForKey:@"builder_name"] forKey:@"name"];
            [dict setObject:[dictTemp objectForKey:@"builder_id"] forKey:@"id"];
            [arr addObject:dict];
        }
        
        obj.arrBuilderList = [[[NSOrderedSet alloc] initWithArray:arr] array];
        
        if (isResidential && !isCommercial) {
            obj.option = PropertyTypeResidentialOnly;
        }
        else if (!isResidential && isCommercial) {
            obj.option = PropertyTypeCommenrcialOnly;
        }
        else if (isResidential && isCommercial) {
            obj.option = PropertyTypeResidentialAndCommercial;
        }
    }
}



#pragma mark - Table View Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (arrInfo.count) {
        return arrInfo.count + 1;
    }
    else {
        return 1;
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (arrInfo.count) {
        if (section < arrInfo.count) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
    
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrInfo.count) {
        return 372;
    }
    else {
        //return 114.0;
        return CGRectGetHeight(self.view.frame) - 184;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrInfo.count) {
        
        if (indexPath.section < arrInfo.count) {
            
            DetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell"];
            cell.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0];
            
            cell.btnFavorite.tag = indexPath.section;
            [cell.btnFavorite addTarget:self action:@selector(didTappedFavorite:) forControlEvents:UIControlEventTouchUpInside];
            
            NSDictionary *dict = [arrInfo objectAtIndex:indexPath.section];
            
            cell.btnFavorite.selected = [[dict objectForKey:@"favorite"] boolValue];
            
            [cell.imgTop setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"banner_img"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
            
            if (![[dict objectForKey:@"display_name"] isKindOfClass:[NSNull class]]) {
                cell.lblProjectName.text = [[dict objectForKey:@"display_name"] capitalizedString];          
            }
            
            if (![[dict objectForKey:@"project_status"] isKindOfClass:[NSNull class]]) {
                cell.lblProjectStatus.text = [NSString stringWithFormat:@"(%@)", [dict objectForKey:@"project_status"]];
            }
            
            if (![[dict objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
                cell.lblBuilderName.text = [[dict objectForKey:@"builder_name"] uppercaseString];
            }
            
            if (![[dict objectForKey:@"exactlocation"] isKindOfClass:[NSNull class]]) {
                cell.lblBuilderAddress.text = [dict objectForKey:@"exactlocation"];
            }
            
            if (![[dict objectForKey:@"unit_type"] isKindOfClass:[NSNull class]]) {
                cell.lblFeatures.text = [dict objectForKey:@"unit_type"];
            }
            
            cell.lblInfra.layer.cornerRadius = cell.lblNeeds.layer.cornerRadius = cell.lblLifestyle.layer.cornerRadius = 5.0;
            cell.lblInfra.clipsToBounds = cell.lblNeeds.clipsToBounds = cell.lblLifestyle.clipsToBounds = YES;
            
            if (![[dict objectForKey:@"infraval"] isKindOfClass:[NSNull class]]) {
                cell.lblInfra.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"infra"]];
            }
            
            if (![[dict objectForKey:@"needsval"] isKindOfClass:[NSNull class]]) {
                cell.lblNeeds.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"needs"]];
            }
            
            if (![[dict objectForKey:@"life_styleval"] isKindOfClass:[NSNull class]]) {
                cell.lblLifestyle.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"life_style"]];
            }
            
            if (![[dict objectForKey:@"appriciation"] isKindOfClass:[NSNull class]]) {
                cell.lblAppriciation.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"appriciation"]];
            }
            
            if (![[dict objectForKey:@"ratings_average"] isKindOfClass:[NSNull class]]) {
                
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"Rating"];
                attachment.bounds = CGRectMake(0, -8, 28, 28);
                
                NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
                
                [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@/5", [dict objectForKey:@"ratings_average"]]]];
                
                cell.lblRating.attributedText = strProgress;
                
                //cell.lblRating.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"ratings_average"]];
            }
            
            if (![[dict objectForKey:@"userFavorite"] isKindOfClass:[NSNull class]]) {
                
                if ([[dict objectForKey:@"userFavorite"] boolValue]) {
                    //cell.imgFav.image = [UIImage imageNamed:@""];
                }
                else {
                    //cell.imgFav.image = [UIImage imageNamed:@""];
                }
            }
            
            if (![[dict objectForKey:@"price_one_year"] isKindOfClass:[NSNull class]]) {
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"Increase"];
                
                attachment.bounds = CGRectMake(0, -3, 18, 18);
                
                NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
                
                [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [dict objectForKey:@"price_one_year"]]]];
                
                cell.lblAppriciation.attributedText = strProgress;
            }
            
            if (![[dict objectForKey:@"psf"] isKindOfClass:[NSNull class]]) {
                cell.lblPricePSF.text = [NSString stringWithFormat:@"\u20B9 %@ Avg Price/sqft", [dict objectForKey:@"psf"]];
            }
            
            //project_price_range
            if (![[dict objectForKey:@"project_price_range"] isKindOfClass:[NSNull class]]) {
                cell.lblPriceRange.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"project_price_range"]];
            }
            
            if (![[dict objectForKey:@"possession_date"] isKindOfClass:[NSNull class]]) {
                
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Possession:  " attributes:@{
                                                                                                                                 NSForegroundColorAttributeName : [UIColor lightGrayColor]
                                                                                                                                 }];
                
                [str appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [dict objectForKey:@"possession_date"]] attributes:@{
                                                                                                                                                                                     
                                                                                                                                                                                     NSForegroundColorAttributeName : [UIColor blackColor]                                                                                                                                              }]];
                
                cell.lblPossessionDate.attributedText = str;
            }
            
            
            
            return cell;
            
        }
        else {
            
            
            return nil;
        }
        
    }
    else {
        //
       UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"No Data"];
        return cell;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (arrInfo.count) {
        if (section < arrInfo.count) {
            
            NSArray *arr = [[arrInfo objectAtIndex:section] objectForKey:@"unit_specifications"];
            if (arr.count < 3) {
                return arr.count * 31 + 1;
            }
            else {
                return 94;
            }
        }
        else {
            return 120;
        }
    }
    else {
        return 120;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (arrInfo.count) {
        if (section < arrInfo.count) {
            
            NSArray *arr = [[arrInfo objectAtIndex:section] objectForKey:@"unit_specifications"];
            
            CGFloat footer_hieght;
            if (arr.count < 3) {
                footer_hieght = arr.count * 31 + 1;
            }
            else {
                footer_hieght = 94;
            }
            
            UIView *viewTemplate = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), footer_hieght)];
            viewTemplate.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0];
            
            
            UIScrollView *scrlView = [[UIScrollView alloc] initWithFrame:CGRectMake(3, 0,  CGRectGetWidth(viewTemplate.frame) - 6, CGRectGetHeight(viewTemplate.frame))];
            scrlView.backgroundColor = [UIColor clearColor];
            CGFloat hieght = 30;
            CGFloat width = (CGRectGetWidth(scrlView.frame) - 4)/3;
        
            int i = 0;
            for (NSDictionary *temp in arr) {
                
                UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(1, (hieght + 1) * i + 1, width, hieght)];
                lbl1.backgroundColor = [UIColor whiteColor];
                lbl1.textAlignment = NSTextAlignmentCenter;
                lbl1.font = [UIFont fontWithName:@"PT Sans" size:14.0];
                lbl1.textColor = [UIColor lightGrayColor];
                lbl1.text = [temp objectForKey:@"wpcf_flat_typology"];
                [scrlView addSubview:lbl1];
                
                UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(width + 2, (hieght + 1) * i + 1, width, hieght)];
                lbl2.backgroundColor = [UIColor whiteColor];
                lbl2.textAlignment = NSTextAlignmentCenter;
                lbl2.font = [UIFont fontWithName:@"PT Sans" size:14.0];
                lbl2.text = [NSString stringWithFormat:@"%@ sqft", [temp objectForKey:@"area_range"]];//1bhkarea_range
                [scrlView addSubview:lbl2];
                
                
                UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(2 * width + 3, (hieght + 1) * i + 1, width, hieght)];
                lbl3.backgroundColor = [UIColor whiteColor];
                lbl3.textAlignment = NSTextAlignmentCenter;
                lbl3.font = [UIFont fontWithName:@"PT Sans" size:14.0];
                lbl3.text = [NSString stringWithFormat:@"\u20B9 %@", [temp objectForKey:@"price_range"]];//1bhk_price
                [scrlView addSubview:lbl3];
                
                scrlView.contentSize = CGSizeMake(CGRectGetMaxX(lbl3.frame), CGRectGetMaxY(lbl3.frame));
                
                i ++;
                
            }
            
            [viewTemplate addSubview:scrlView];
            
            return viewTemplate;
        }
        else {
            
            viewFooter.frame = CGRectMake(0, 0, CGRectGetWidth(tblView.frame), CGRectGetHeight(viewFooter.frame));
            
            for (UIView *aView in viewFooter.subviews.firstObject.subviews) {
                
                if ([aView isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)aView;
                    btn.layer.borderWidth = 2.0;
                    btn.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
                    [btn addTarget:self action:@selector(didClickedGetAlerts) forControlEvents:UIControlEventTouchUpInside];
                }
                
            }
            return viewFooter;
        }
    }
    else {
        viewFooter.frame = CGRectMake(0, 0, CGRectGetWidth(tblView.frame), CGRectGetHeight(viewFooter.frame));
        
        for (UIView *aView in viewFooter.subviews.firstObject.subviews) {
            
            if ([aView isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)aView;
                btn.layer.borderWidth = 2.0;
                btn.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
                [btn addTarget:self action:@selector(didClickedGetAlerts) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
        return viewFooter;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrInfo.count) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailVC"];
        obj.dictSourse = [arrInfo objectAtIndex:indexPath.section];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        
    }
}

#pragma mark - API Action
- (void)transitToProjectDetail:(id)sender {
    NSLog(@"%s", __FUNCTION__);
}

#pragma mark - Auxillary Mathods 
- (UILabel *)setBackground {
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tblView.frame), CGRectGetHeight(tblView.frame))];
    lbl.textAlignment = NSTextAlignmentCenter;
    
    return lbl;
}


#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnback) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnMaps) {
        
        if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects || _option == DetailFeaturedDeveloperAccessOptionSearchProperty || _option == DetailFeaturedDeveloperAccessOptionSearchScreen || _option == DetailFeaturedDeveloperAccessOptionSectors || _option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
            
            ProjectMapVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectMapVC"];
            /*
             DetailFeaturedDeveloperAccessOptionSeeAllProjects,
             DetailFeaturedDeveloperAccessOptionSearchProperty,
             DetailFeaturedDeveloperAccessOptionSearchScreen,
             DetailFeaturedDeveloperAccessOptionSectors,
             DetailFeaturedDeveloperAccessOptionSpecialProject,
             */
            if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects) {
                obj.option = SectorDetailsAccessOptionCatagory;
            }
            else if (_option == DetailFeaturedDeveloperAccessOptionSearchProperty) {
                obj.option = SectorDetailsAccessOptionCatagory;
            }
            else if (_option == DetailFeaturedDeveloperAccessOptionSearchScreen) {
                obj.option = SectorDetailsAccessOptionCatagory;
            }
            else if (_option == DetailFeaturedDeveloperAccessOptionSectors) {
                obj.option = SectorDetailsAccessOptionCatagory;
            }
            else if (_option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
                obj.option = SectorDetailsAccessOptionCatagory;
            }
            
            obj.path = pathCity;
            obj.dictSourse = _dictSourse;
            obj.arrLegand = arrSegmentControl;
            obj.strTitle = _strTitle;//
            
            NSDictionary *dictReq = [pref objectForKey:strSaveMultiFilter];
            if (dictReq) {
                obj.dictFilter = dictReq;
            }
            
            [self.navigationController pushViewController:obj animated:YES];
            
        }
    
    }
}



- (void)didClickedGetAlerts {
    
    if (viewAlert == nil) {
        //btnEnquiry.selected = !btnEnquiry;
        
        viewAlert = [GetAlertView createView];
        viewAlert.frame = CGRectMake( -1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        //viewAlert.owner = self;
        [self setTextField:viewAlert.txtName withPlaceholder:@"Name"];
        [self setTextField:viewAlert.txtEmail withPlaceholder:@"Email"];
        [self setTextField:viewAlert.txtMobileNumber withPlaceholder:@"Mobile Number"];
        [viewAlert.btnCancel addTarget:self action:@selector(cancelEnqiryForm) forControlEvents:UIControlEventTouchUpInside];
        [viewAlert.btnSend addTarget:self action:@selector(sendEnquiry) forControlEvents:UIControlEventTouchUpInside];
        [viewAlert.btnTermsAndConditions addTarget:self action:@selector(didTappedTermsAndConditions) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:viewAlert];
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             viewAlert.frame = self.view.frame;
                         } completion:^(BOOL finished) {
                                                          
                         }];
        
    }
    
}

- (void)cancelEnqiryForm {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewAlert.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [viewAlert removeFromSuperview];
                         viewAlert = nil;
                     }];
    
}

- (void)sendEnquiry {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if ([viewAlert.btnSend isEnabled]) {
            viewAlert.btnSend.enabled = NO;
        }
        
        direction = 1;
        shakes = 0;
        
        if (viewAlert.txtName.text.length == 0) {
            [self.view makeToast:strNoName duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtName];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else if (![AuxillaryMathods isUserName:viewAlert.txtName.text]) {
            [self.view makeToast:strInvalidName duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtName];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else {
            
        }
        
        
        if (viewAlert.txtEmail.text.length == 0) {
            [self.view makeToast:strNoEmail duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtEmail];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else if (![AuxillaryMathods isValidEmail:viewAlert.txtEmail.text]) {
            [self.view makeToast:strInvalidEmail duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtEmail];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else {
            
        }
        
        
        if (viewAlert.txtMobileNumber.text.length == 0) {
            [self.view makeToast:strNoMobilenumber duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtMobileNumber];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else if (![AuxillaryMathods isValidMobileNumber:viewAlert.txtMobileNumber.text]) {
            [self.view makeToast:strValidMobileNumber duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtMobileNumber];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else {
            
        }
        
        
        if (!viewAlert.btnCheck.selected) {
            [self.view makeToast:strPleaseCheckMarkT_C duration:duration position:CSToastPositionCenter];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];// projectName,name,contactno,email,enquiry
        [dict setObject:viewAlert.txtName.text forKey:@"name"];
        [dict setObject:viewAlert.txtMobileNumber.text forKey:@"contactno"];
        [dict setObject:viewAlert.txtEmail.text forKey:@"email"];
        
        if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects) {
            
            NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
            [subDict setObject:[_dictSourse objectForKey:@"builder_id"] forKey:@"builder_id"];
            [subDict setObject:[_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
            [dict setObject:subDict forKey:@"search"];
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSearchProperty) {
            NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
            [subDict setObject:[_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
            [subDict setObject:[_dictSourse objectForKey:@"builder_id"] forKey:@"builder_id"];
            [subDict setObject:[_dictSourse objectForKey:@"p_type"] forKey:@"p_type"];
            [subDict setObject:[_dictSourse objectForKey:@"type"] forKey:@"type"];
        
            [dict setObject:subDict forKey:@"search"];
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSearchScreen) {
            
            NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
            [subDict setObject:[_dictInfo  objectForKey:@"city_id"] forKey:@"city_id"];
            [subDict setObject:[_dictInfo  objectForKey:@"p_type"] forKey:@"p_type"];
            [subDict setObject:[_dictInfo  objectForKey:@"type"] forKey:@"type"];
            
            if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Location"] && [[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"]) {
                [subDict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"location_id"];
            }
            
            if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Location"] && [[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"]) {
                [subDict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"location_id"];
            }
            
            
            [dict setObject:subDict forKey:@"search"];
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSectors) {
            
            NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
            [subDict setObject:[_dictInfo  objectForKey:@"city_id"] forKey:@"city_id"];
            [subDict setObject:[_dictInfo  objectForKey:@"p_type"] forKey:@"p_type"];
            [subDict setObject:[_dictInfo  objectForKey:@"type"] forKey:@"type"];
            
            if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Sublocation"] && [[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"]) {
                [subDict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sublocation_id"];
            }
            [dict setObject:subDict forKey:@"search"];
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
            NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
            [subDict setObject:[_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
            [subDict setObject:[_dictSourse objectForKey:@"special_category"] forKey:@"catagory_type"];
            [dict setObject:subDict forKey:@"search"];
        }
        
        
        [ServiceAPI postAlert:dict andCalback:^(BOOL isSuccess, id data) {
            if (isSuccess) {
                [self cancelEnqiryForm];
                
                notifAlert = [NotificationAlert createView];
                notifAlert.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                [notifAlert.btnClose addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:notifAlert];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButton)];
                [notifAlert addGestureRecognizer:tap];
                
                [UIView animateWithDuration:0.25
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     notifAlert.frame = self.view.frame;
                                 } completion:^(BOOL finished) {
                                     
                                     
                                     
                                 }];
            }
            else {
                [self.view makeToast:strEnquiryFailed duration:duration position:CSToastPositionCenter];
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(sendEnquiry) fromClass:self];
    }
}

- (void)didTappedTermsAndConditions {
    
    PrivacyViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyViewController"];
    obj.option = PrivacyViewControllerAccessOptionTermsAndConditions;
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)closeButton {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         notifAlert.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [notifAlert removeFromSuperview];
                         notifAlert = nil;
                     }];
    
}


- (void)didTappedFavorite:(UIButton *)sender {
    indexSelected = sender.tag;
    dictFavoriteReq = [NSMutableDictionary dictionary];
    [dictFavoriteReq setObject:@"project" forKey:@"type"];
    [dictFavoriteReq setObject:[[arrInfo objectAtIndex:indexSelected] objectForKey:@"ID"] forKey:@"id"];
    
    [self favoriteFeaturedProjects];

}

- (void)favoriteFeaturedProjects {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId != nil) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kFeaturedDeveloperDetailsFav object:nil];
            
            [dictFavoriteReq setObject:strUserId forKey:@"user_id"];
            
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI markAsFavorite:dictFavoriteReq withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    if ([[data objectForKey:@"success"] boolValue]) {
                        
                        NSMutableDictionary *dict = [[arrInfo objectAtIndex:indexSelected] mutableCopy];
                        
                        if ([[dict objectForKey:@"favorite"] boolValue]) {
                            [dict setObject:[NSString stringWithFormat:@"0"] forKey:@"favorite"];
                        }
                        else {
                            [dict setObject:[NSString stringWithFormat:@"1"] forKey:@"favorite"];
                        }
                        
                        NSMutableArray *arr = [arrInfo mutableCopy];
                        [arr replaceObjectAtIndex:indexSelected withObject:dict];
                        arrInfo = [arr copy];
                        [self setprojectCatagories];
                        [tblView reloadData];
                        
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }];
        }
        else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteFeaturedProjects) name:kFeaturedDeveloperDetailsFav object:nil];
            
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionFavoriteDetailFeaturedDeveloper;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(favoriteFeaturedProjects) fromClass:self];
    }
}

#pragma mark - Notification Center Access

/*
- (void)addFilters:(NSNotification *)info {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        NSDictionary *dictReq = [pref objectForKey:strSaveMultiFilter];
        [dict setObject:dictReq forKey:@"filter"];
        
        if ([_dictSourse objectForKey:@"builder_id"]) {
            [dict setObject: [_dictSourse objectForKey:@"builder_id"] forKey:@"builder_id"];
        }
        
        if ([_dictSourse objectForKey:@"city_id"]) {
            [dict setObject: [_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
        }
        
        if ([_dictSourse objectForKey:@"type"]) {
            [dict setObject:[_dictSourse objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictSourse objectForKey:@"p_type"]) {
            [dict setObject:[_dictSourse objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictSourse  objectForKey:@"location_id"]) {
            [dict setObject:[_dictSourse  objectForKey:@"location_id"] forKey:@"location"];
        }
        
        if ([_dictInfo objectForKey:@"type"]) {
            [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictInfo objectForKey:@"p_type"]) {
            [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictInfo objectForKey:@"city_id"]) {
            [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
        }
        
        
        if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
            [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sublocation_id"];
            
        }
        
        if ([_dictSourse objectForKey:@"sector_id"]) {
            [dict setObject:[_dictSourse objectForKey:@"sector_id"] forKey:@"sublocation_id"];
        }
        
        if ([_dictSourse objectForKey:@"special_category"]) {
            [dict setObject:[_dictSourse objectForKey:@"special_category"] forKey:@"special_category"];
        }
        
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
        }
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
            
            [[AppDelegate share] enableUserInteraction];
            if ([refreshControl isRefreshing]) {
                [refreshControl endRefreshing];
            }
            
            if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects || _option == DetailFeaturedDeveloperAccessOptionSearchProperty || _option == DetailFeaturedDeveloperAccessOptionSearchScreen || _option == DetailFeaturedDeveloperAccessOptionSectors) {
                
                if (isSuccess) {
                    if (data && [data isKindOfClass:[NSArray class]]) {
                        
                        //[arrInfo removeAllObjects];
                        arrInfo = [data mutableCopy];
                        
                        if (arrInfo.count < 2) {
                            btnFilter.hidden = YES;
                            lblTitle.text = [NSString stringWithFormat:@"%@", _strTitle];
                        }
                        
                        [self setprojectCatagories];
                        [tblView reloadData];
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
                
            }
            else if (_option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
                if (isSuccess) {
                    
                    if ([legands isKindOfClass:[NSArray class]]) {
                        
                        NSMutableArray *arr = [arrSegmentControl mutableCopy];
                        [arr replaceObjectAtIndex:2 withObject:legands];
                        arrSegmentControl = [arr copy];
                    }
                    
                    if ([locationCordinates isKindOfClass:[NSString class]]) {
                        NSString *str = [NSString stringWithFormat:@"%@", locationCordinates];
                        
                        str = [str stringByReplacingOccurrencesOfString:@"[" withString:@""];
                        str = [str stringByReplacingOccurrencesOfString:@"]" withString:@""];
                        NSArray *arr = [str componentsSeparatedByString:@","];
                        GMSMutablePath *path =[GMSMutablePath path];
                        
                        for (int  i = 0; i < arr.count; i += 2) {
                            [path addLatitude:[[arr objectAtIndex:i] doubleValue] longitude:[[arr objectAtIndex:i+1] doubleValue]];
                        }
                        pathCity = path;
                    }
                    
                    if ([data isKindOfClass:[NSArray class]]) {
                        
                        //[arrInfo removeAllObjects];
                        arrInfo = [data mutableCopy];
                        
                        if (arrInfo.count < 2) {
                            btnFilter.hidden = YES;
                            lblTitle.text = [NSString stringWithFormat:@"%@", _strTitle];
                        }
                        
                        [self setprojectCatagories];
                        [tblView reloadData];
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                    
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
                
            }
            
        }];
        
        
        
        
        
        if (_option == DetailFeaturedDeveloperAccessOptionSeeAllProjects || _option == DetailFeaturedDeveloperAccessOptionSearchProperty) {
            
            if (![[_dictSourse objectForKey:@"builder_id"] isKindOfClass:[NSNull class]]) {
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                              [dict setObject:[_dictSourse objectForKey:@"builder_id"] forKey:@"builder_id"];
                [dict setObject:[_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
                
                NSString *strUserId = [pref objectForKey:strSaveUserId];
                if (strUserId) {
                    [dict setObject:strUserId forKey:@"user_id"];
                }
                
                [dict setObject:dictReq forKey:@"filter"];
                [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                    
                    [[AppDelegate share] enableUserInteraction];
                    if ([refreshControl isRefreshing]) {
                        [refreshControl endRefreshing];
                    }
                    
                    if (isSuccess) {
                        if (data && [data isKindOfClass:[NSArray class]]) {
                            NSArray *arr = (NSArray *)data;
                            if ([arr count]) {
                                arrInfo = [NSMutableArray array];
                                [arrInfo addObjectsFromArray:arr];
                            }
                            else {
                                arrInfo = [NSMutableArray array];
                            }
                            [self setprojectCatagories];
                            
                        }
                        else {
                            [arrInfo removeAllObjects];
                        }
                        
                        [tblView setContentOffset:CGPointZero animated:NO];
                        [tblView reloadData];
                        [pref setObject:[dictReq copy] forKey:strSaveMultiFilter];
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                    
                }];
            }
            
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSearchScreen) {
            
            
            NSString *strUserId = [pref objectForKey:strSaveUserId];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[_dictSourse  objectForKey:@"location_id"] forKey:@"location"];
            [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
            [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
            [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
            [dict setObject:dictReq forKey:@"filter"];
            
            if (strUserId) {
                [dict setObject:strUserId forKey:@"user_id"];
            }
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            
            [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    if (data && [data isKindOfClass:[NSArray class]]) {
                        NSArray *arr = (NSArray *)data;
                        if ([arr count]) {
                            arrInfo = [NSMutableArray array];
                            [arrInfo addObjectsFromArray:arr];
                            [self setprojectCatagories];
                            
                        }else {
                            arrInfo = [NSMutableArray array];
                        }
                        [self setprojectCatagories];
                        
                    }
                    else {
                        [arrInfo removeAllObjects];
                    }
                    
                    [tblView setContentOffset:CGPointZero animated:NO];
                    [tblView reloadData];
                    [pref setObject:[dictReq copy] forKey:strSaveMultiFilter];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }];
            
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSectors) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            NSString *strUserId = [pref objectForKey:strSaveUserId];
            
            [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
            [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
            [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
            [dict setObject:[_dictInfo objectForKey:@"sector_id"] forKey:@"sublocation_id"];
            [dict setObject:dictReq forKey:@"filter"];
            
            
            if (strUserId) {
                [dict setObject:strUserId forKey:@"user_id"];
            }
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI getProjectList:dict
                          withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                              [[AppDelegate share] enableUserInteraction];
                              if (isSuccess) {
                                  if (data && [data isKindOfClass:[NSArray class]]) {
                                      NSArray *arr = (NSArray *)data;
                                      if ([arr count]) {
                                          arrInfo = [NSMutableArray array];
                                          [arrInfo addObjectsFromArray:arr];
                                          [self setprojectCatagories];
                                          
                                      }else {
                                          arrInfo = [NSMutableArray array];
                                      }
                                      [self setprojectCatagories];
                                      
                                  }
                                  else {
                                      [arrInfo removeAllObjects];
                                  }
                                  
                                  [tblView setContentOffset:CGPointZero animated:NO];
                                  [tblView reloadData];
                                  [pref setObject:[dictReq copy] forKey:strSaveMultiFilter];
                              }
                              else {
                                  [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                              }
                          }];
        }
        else if (_option == DetailFeaturedDeveloperAccessOptionSpecialProject) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[_dictSourse objectForKey:@"special_category"] forKey:@"special_category"];
            [dict setObject:[_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
            [dict setObject:dictReq forKey:@"filter"];
            
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    if (data && [data isKindOfClass:[NSArray class]]) {
                        NSArray *arr = (NSArray *)data;
                        if ([arr count]) {
                            arrInfo = [NSMutableArray array];
                            [arrInfo addObjectsFromArray:arr];
                            [self setprojectCatagories];
                            
                        }else {
                            arrInfo = [NSMutableArray array];
                        }
                        [self setprojectCatagories];
                        
                    }
                    else {
                        [arrInfo removeAllObjects];
                    }
                    
                    [tblView setContentOffset:CGPointZero animated:NO];
                    [tblView reloadData];
                    [pref setObject:[dictReq copy] forKey:strSaveMultiFilter];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }];
        }

    }
    
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(addFilters:) fromClass:self];
    }
}
*/
#pragma mark - Auxillary View

- (void)setprojectCatagories  {
    
    for (NSDictionary *dict in arrInfo) {
        
        if ([[dict objectForKey:@"is_commercial"] boolValue] == 0) {
            isResidential = YES;
            break;
        }
    }
    
    for (NSDictionary *dict in arrInfo) {
        
        if ([[dict objectForKey:@"is_commercial"] boolValue] == 1) {
            isCommercial = YES;
            break;
        }
    }
    
}

- (void)setTextField:(UITextField *)txtField withPlaceholder:(NSString *)placeholder {
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtField.frame))];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer1.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [aView.layer addSublayer:layer1];
    
    aView.backgroundColor = [UIColor whiteColor];
    txtField.leftView = aView;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    
    txtField.placeholder = placeholder;
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtField.layer addSublayer:layer];
    
    txtField.delegate = self;
    
}

-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

#pragma mark - UITextField Delegates
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength <= 10;
    }
    else {
        return YES;
    }
}


@end
