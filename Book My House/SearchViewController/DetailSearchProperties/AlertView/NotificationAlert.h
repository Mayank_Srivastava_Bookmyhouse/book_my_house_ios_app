//
//  NotificationAlert.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/29/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationAlert : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) UIViewController *owner;
+ (NotificationAlert *)createView;
@end
