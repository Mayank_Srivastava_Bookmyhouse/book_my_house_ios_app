//
//  NotificationAlert.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/29/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "NotificationAlert.h"

@implementation NotificationAlert

+ (NotificationAlert *)createView {
    
    NotificationAlert *view = [[[NSBundle mainBundle] loadNibNamed:@"AlertView" owner:self options:nil] firstObject];
    
    
    return view;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
