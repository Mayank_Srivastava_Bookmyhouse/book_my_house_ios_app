//
//  CrouselCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/15/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CrouselCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgSide;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAvg;
@property (weak, nonatomic) IBOutlet UILabel *lblProjects;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCompare;
@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeeds;
@property (weak, nonatomic) IBOutlet UILabel *lblLefestyle;
@property (weak, nonatomic) IBOutlet UILabel *lblAppriciation;

@end
