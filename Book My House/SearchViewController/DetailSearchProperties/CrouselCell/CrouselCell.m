//
//  CrouselCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/15/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CrouselCell.h"

@implementation CrouselCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    _lblInfra.layer.cornerRadius = _lblNeeds.layer.cornerRadius = _lblLefestyle.layer.cornerRadius = 3.0;
    _btnAddToCompare.layer.cornerRadius = 5.0;
    
}

@end
