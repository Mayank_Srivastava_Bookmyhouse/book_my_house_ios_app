//
//  LocalityMapVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "CrouselCell.h"
#import "FooterCrouselCell.h"
#import "ProjectMapVC.h"
#import "LocalityListVC.h"
#import "NotificationAlert.h"
@import GoogleMaps;
typedef NS_ENUM(NSUInteger, LocalityMapVCAccessOption) {
    LocalityMapVCAccessOptionLocality,
    LocalityMapVCAccessOptionSector,
    LocalityMapVCAccessOptionMisc,
};

@interface LocalityMapVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, GMSMapViewDelegate>

@property (assign, nonatomic) LocalityMapVCAccessOption option;
@property (copy, nonatomic) NSDictionary *dictInfo;
@end
