//
//  LocalityMapVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "LocalityMapVC.h"
#import "DWBubbleMenuButton.h"
#import "SectorCarousel.h"

@interface LocalityMapVC () {
    NSMutableArray *arrLocality, *arrCityCordinates, *arrCurrectSectorPolygons, *arrSectorMarkers;
    GMSPolygon *polygonCurrentMicroMarket, *polygonCurrentSector;
    GMSMarker *markerCurrentMicroMarket;
    
    NSMutableDictionary *dictMapInfo;
    GMSCameraPosition *camera;
    NSUInteger zoom;
    GMSCameraUpdate *updatedCamera;
    NSArray *arrSegmentControl, *arrColorCode;
    DWBubbleMenuButton *downMenuButton;
    
    NSUserDefaults *pref;
    NotificationAlert *notifAlert;
    
    
}

@property (weak, nonatomic) IBOutlet UIButton *btnCross;
@property (weak, nonatomic) IBOutlet UIButton *btnTabular;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UIButton *btnZoomOut;
@property (weak, nonatomic) IBOutlet UIButton *btnZoomIn;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;


- (IBAction)actionListeners:(id)sender;
@end

@implementation LocalityMapVC


#pragma mark - View Controller Mathods
- (void)loadView {
    
    arrLocality = [NSMutableArray array];
    arrCityCordinates = [NSMutableArray array];
    
    pref = [NSUserDefaults standardUserDefaults];
    
    NSArray *arrInfra = @[@"1 - 2", @"3 - 4", @"5 - 6", @"7 - 8", @"9 - 10"];
    
    arrSegmentControl = @[arrInfra, arrInfra, arrInfra, arrInfra, arrInfra];
    
    
    arrColorCode = @[[UIColor colorWithRed:100.0/255.0 green:198.0/255.0 blue:125.0/255.0 alpha:1.0], [UIColor colorWithRed:144.0/255.0 green:201.0/255.0 blue:122.0/255.0 alpha:1.0], [UIColor colorWithRed:254.0/255.0 green:229.0/255.0 blue:129.0/255.0 alpha:1.0], [UIColor colorWithRed:250.0/255.0 green:185.0/255.0 blue:127.0/255.0 alpha:1.0], [UIColor colorWithRed:239.0/255.0 green:103.0/255.0 blue:107.0/255.0 alpha:1.0]];
    
    zoom = 12;
    [super loadView];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSetting];
    [self loadsearchedPropertyInformation];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [downMenuButton removeFromSuperview];
    downMenuButton = nil;
    [self updateScreenSettings];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [downMenuButton removeFromSuperview];
    downMenuButton = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kApplyFilters object:nil];
}

#pragma mark - Screen Settings
- (void)screenSetting {
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    if (_option == LocalityMapVCAccessOptionLocality) {
        [_collectionView registerClass:[CrouselCell class] forCellWithReuseIdentifier:@"CrouselCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"CrouselCell" bundle:nil] forCellWithReuseIdentifier:@"CrouselCell"];
        _lblTitle.text = @"Search Locality";
    }
    else {
        [_collectionView registerClass:[SectorCarousel class] forCellWithReuseIdentifier:@"SectorCarousel"];
        [_collectionView registerNib:[UINib nibWithNibName:@"SectorCarousel" bundle:nil] forCellWithReuseIdentifier:@"SectorCarousel"];
        _lblTitle.text = @"Search Sectors";
    }

    [self.view setNeedsDisplay];
    
    
}




- (void)updateScreenSettings {
    
    UIView *view = [self createHomeButtonView];
    
    CGRect frame = CGRectMake(CGRectGetMaxX(self.view.frame) - CGRectGetWidth(view.frame) - 10, CGRectGetMaxY(_segmentControl.frame) + 10, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
    
    downMenuButton = [[DWBubbleMenuButton alloc] initWithFrame:frame
                                            expansionDirection:DirectionDown];
    
    downMenuButton.homeButtonView = view;
    downMenuButton.buttonSpacing = 0;
    [downMenuButton addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:downMenuButton];
    [self.view bringSubviewToFront:downMenuButton];
    
    [downMenuButton showButtons];
    
}

- (UIView *)createHomeButtonView {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 30.0)];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    img.image = [UIImage imageNamed:@"Down-indicator"];
    
    [view addSubview:img];
    img.center = view.center;
    
    
    return view;
}

- (NSArray *)createDemoButtonArray {
    
    NSMutableArray *buttonsMutable = [NSMutableArray array];
    
    int i = 0;
    for (NSString *title in [arrSegmentControl objectAtIndex:_segmentControl.selectedSegmentIndex]) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.frame = CGRectMake(0.f, 0.f, 60.f, 30.f);
        [button addSubview:[self addLabelWithText:title andFrame:button.frame]];
        button.backgroundColor = [arrColorCode objectAtIndex:i];
        button.clipsToBounds = YES;
        button.tag = i++;
        [buttonsMutable addObject:button];
    }
    
    
    return [buttonsMutable copy];
}

- (UILabel *)addLabelWithText:(NSString *)str andFrame:(CGRect)frame{
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.font = [UIFont fontWithName:@"PT Sans" size:10.0];
    titleLabel.text = str;
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    return titleLabel;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"TabularSearch"]) {
        LocalityListVC *obj = segue.destinationViewController;
        obj.dictInfo = [_dictInfo copy];
    }
    else if ([segue.identifier isEqualToString:@"SortLocality"]) {
        
    }
}


#pragma mark - API Actions
- (void)loadsearchedPropertyInformation {
    
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]){
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
        [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        [dict setObject:[[_dictInfo objectForKey:@"p_type"] lowercaseString] forKey:@"p_type"];
        
        if ([_dictInfo objectForKey:@"keyword"] != nil) {
            
            NSDictionary *dict1 = [_dictInfo objectForKey:@"keyword"];
            if ([[dict1 objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
                [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sub_location_id"];
            }
            else
                [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"location_id"];
        }
        
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getLocalityInfoForMap:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            
            if (![data isKindOfClass:[NSNull class]]) {
                
                if ([[data objectForKey:@"success"] boolValue]) {
                    
                    if (_option == LocalityMapVCAccessOptionLocality) {
                        [arrLocality addObjectsFromArray:[data objectForKey:@"location_list"]];
                        [self locateCityOnMap:[data objectForKey:@"city_location"]];
                    }
                    else {//Sectors
                        [arrLocality addObjectsFromArray:[data objectForKey:@"sector_lists"]];
                        [self locateMicroMarketOnMap:[data objectForKey:@"micromarket_location"]];
                    }
                    
                    
                    
                    if (arrLocality.count == 1) {
                        _btnLeft.hidden = _btnRight.hidden = YES;
                    }
                    else {
                        _btnLeft.hidden = YES;
                        _btnRight.hidden = NO;
                    }
                    
                    
                    //arrSegmentControl
                    NSArray *arr = [data objectForKey:@"legend"];
                    NSMutableArray *arr1 = [arrSegmentControl mutableCopy];
                    [arr1 replaceObjectAtIndex:2 withObject:arr];
                    arrSegmentControl = [arr1 copy];
                    
                    [downMenuButton removeFromSuperview];
                    downMenuButton = nil;
                    [self updateScreenSettings];
                    
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
                
            }
            else {
                [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadsearchedPropertyInformation) fromClass:self];

    }
}

#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if (sender == _btnCross) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == _btnFilter) {
        
    }
    else if (sender == _btnTabular) {
        
    }
    else if (sender == _segmentControl) {
        
        [downMenuButton removeFromSuperview];
        downMenuButton = nil;
        [self updateScreenSettings];
        
        if (_option == TabularSearchOptionNormal) {
            NSIndexPath *indexPath = [[_collectionView indexPathsForVisibleItems] firstObject];
            NSDictionary *dict = [[dictMapInfo objectForKey:@"location_paths"] objectAtIndex:indexPath.row];
            [self manageSectorPolygonsForLocality:indexPath andInfo:dict];
        }
        else {
            NSIndexPath *indexPath = [[_collectionView indexPathsForVisibleItems] firstObject];
            GMSPath *path = [[dictMapInfo objectForKey:@"sector_path"] objectAtIndex:indexPath.row];
            [self manageSectorPolygonsForSectors:indexPath andInfo:path];
        }
        
        
        
        
    }
    else if (sender == _btnZoomIn || sender == _btnZoomOut) {
        
        if (zoom > 20) {
            zoom = 20;
            return;
        }
        
        if (zoom < 8) {
            zoom = 8;
            return;
        }
        
        if (sender == _btnZoomIn) {
            [_mapView animateToZoom:--zoom];
        }
        else if (sender == _btnZoomOut) {
            [_mapView animateToZoom:++zoom];
        }
        
    }
    else if (sender == _btnLeft) {
        
        NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        
        if (prevItem.item >= 0) {
            [_collectionView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == _btnRight) {
        NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_collectionView numberOfItemsInSection:0];
        
        if (num > nextItem.item) {
            [_collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }

    }
}

- (void)didClickedGetAlerts {
    
    NSString *strEmail = [pref objectForKey:strSaveUserEmail];
    NSLog(@"Email id %@", strEmail);
    
    if (strEmail == nil) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.view.window makeToast:strLoginForAlert duration:duration position:CSToastPositionCenter];
    }
    else {
        //Set up the alert signal
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:strEmail forKey:@"email"];
        [dict setObject:[[_dictInfo objectForKey:@"city"] objectForKey:@"city_id"] forKey:@"city"];
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI setAlertForCurrentLocation:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                
                notifAlert = [NotificationAlert createView];
                notifAlert.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                [notifAlert.btnClose addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:notifAlert];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButton)];
                [notifAlert addGestureRecognizer:tap];
                
                [UIView animateWithDuration:0.25
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     notifAlert.frame = self.view.frame;
                                 } completion:^(BOOL finished) {
                                     
                                     
                                     
                                 }];
            }
            else {
                [self.view makeToast:strAlertNotSaved duration:duration position:CSToastPositionCenter];
            }
        }];
    }
}

- (void)closeButton {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         notifAlert.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [notifAlert removeFromSuperview];
                         notifAlert = nil;
                     }];
    
}

- (void)didAddToCompare:(UIButton *)btn {
}


#pragma mark - Collection View Delegates
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return (CGRectGetWidth(self.view.frame) - 300);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, (CGRectGetWidth(self.view.frame) - 300)/2, 0, (CGRectGetWidth(self.view.frame) - 300)/2);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(300, 150);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return arrLocality.count;
    }
    else if (section == 1) {
        return 1;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        NSDictionary *dict = [arrLocality objectAtIndex:indexPath.row];
        
        if (_option == LocalityMapVCAccessOptionLocality) {
            
            static NSString *str1 = @"CrouselCell";
            CrouselCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:str1 forIndexPath:indexPath];
            
            
            cell.lblInfra.layer.cornerRadius = cell.lblNeeds.layer.cornerRadius = cell.lblLefestyle.layer.cornerRadius = 5.0;
            cell.lblInfra.clipsToBounds = cell.lblNeeds.clipsToBounds = cell.lblLefestyle.clipsToBounds = YES;
            
            [cell.imgSide setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"location_img"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
            
            if (![[dict objectForKey:@"location_name"] isKindOfClass:[NSNull class]]) {
                cell.lblTitle.text = [dict objectForKey:@"location_name"];
            }
            else {
                cell.lblTitle.text = @"";
            }
            
            
            if (![[dict objectForKey:@"avgPsfLocation"] isKindOfClass:[NSNull class]]) {
                
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
                [formatter setGroupingSeparator:groupingSeparator];
                [formatter setGroupingSize:3];
                [formatter setAlwaysShowsDecimalSeparator:NO];
                [formatter setUsesGroupingSeparator:YES];
                
                
                NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[[dict objectForKey:@"avgPsfLocation"] doubleValue]]];
                
                cell.lblAvg.text = [NSString stringWithFormat:@"\u20B9 %@ Avg Price/sq.ft.", str];
            }
            else {
                cell.lblAvg.text = @"Avg:";
            }
            
            if (![[dict objectForKey:@"total_projects"] isKindOfClass:[NSNull class]]) {
                cell.lblProjects.text = [NSString stringWithFormat:@"No. of Projects: %@", [dict objectForKey:@"total_projects"]];
            }
            else {
                cell.lblProjects.text = @"";
            }
            
            if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
                cell.lblInfra.text = [NSString stringWithFormat:@"Infra\n%@",[dict objectForKey:@"infra"]];
            }
            else {
                cell.lblInfra.text = @"";
            }
            
            if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
                cell.lblNeeds.text = [NSString stringWithFormat:@"Needs\n%@",[dict objectForKey:@"needs"]];
            }
            else {
                cell.lblNeeds.text = @"";
            }
            
            if (![[dict objectForKey:@"lifeStyle"] isKindOfClass:[NSNull class]]) {
                cell.lblLefestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@",[dict objectForKey:@"lifeStyle"]];
            }
            else {
                cell.lblLefestyle.text = @"";
            }
            
            if (![[dict objectForKey:@"avg_rating"] isKindOfClass:[NSNull class]] && [dict objectForKey:@"avg_rating"]) {
                cell.lblRating.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"avg_rating"]];
            }
            else {
                cell.lblRating.text = @"0";
            }
            if (![[dict objectForKey:@"returns"] isKindOfClass:[NSNull class]]) {
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"Increase"];
                
                attachment.bounds = CGRectMake(0, -1, 20, 0.8 * CGRectGetHeight(cell.lblAppriciation.frame));
                
                NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
                
                [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [dict objectForKey:@"returns"]]]];
                
                cell.lblAppriciation.attributedText = strProgress;
            }
            else {
                cell.lblAppriciation.text = @"";
            }
            
            return cell;
        }
        else {
            
            static NSString *str2 = @"SectorCarousel";
            SectorCarousel *cell = [collectionView dequeueReusableCellWithReuseIdentifier:str2 forIndexPath:indexPath];
            
            
            cell.lblInfra.layer.cornerRadius = cell.lblNeeds.layer.cornerRadius = cell.lblLefestyle.layer.cornerRadius = 5.0;
            cell.lblInfra.clipsToBounds = cell.lblNeeds.clipsToBounds = cell.lblLefestyle.clipsToBounds = YES;
            
            [cell.imgSide setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"sector_image"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
            
            if (![[dict objectForKey:@"sector_name"] isKindOfClass:[NSNull class]]) {
                cell.lblTitle.text = [dict objectForKey:@"sector_name"];
            }
            else {
                cell.lblTitle.text = @"";
            }
            
            if ([[_dictInfo objectForKey:@"keyword"] objectForKey:@"location_name"]) {
                cell.lblLocality.text = [[_dictInfo objectForKey:@"keyword"] objectForKey:@"location_name"];
            }
            else {
                cell.lblLocality.text = @"";
            }
            
            if (![[dict objectForKey:@"avg_PSF_Sector"] isKindOfClass:[NSNull class]]) {
                
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
                [formatter setGroupingSeparator:groupingSeparator];
                [formatter setGroupingSize:3];
                [formatter setAlwaysShowsDecimalSeparator:NO];
                [formatter setUsesGroupingSeparator:YES];
                
                
                NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[[dict objectForKey:@"avg_PSF_Sector"] doubleValue]]];
                
                cell.lblAvg.text = [NSString stringWithFormat:@"\u20B9 %@ Avg Price/sq.ft.", str];
            }
            else {
                cell.lblAvg.text = @"Avg:";
            }
            
            if (![[dict objectForKey:@"total_Projects"] isKindOfClass:[NSNull class]]) {
                cell.lblProjects.text = [NSString stringWithFormat:@"No. of Projects: %@", [dict objectForKey:@"total_Projects"]];
            }
            else {
                cell.lblProjects.text = @"";
            }
            
            if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
                cell.lblInfra.text = [NSString stringWithFormat:@"Infra\n%@",[dict objectForKey:@"infra"]];
            }
            else {
                cell.lblInfra.text = @"";
            }
            
            if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
                cell.lblNeeds.text = [NSString stringWithFormat:@"Needs\n%@",[dict objectForKey:@"needs"]];
            }
            else {
                cell.lblNeeds.text = @"";
            }
            
            if (![[dict objectForKey:@"lifeStyle"] isKindOfClass:[NSNull class]]) {
                cell.lblLefestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@",[dict objectForKey:@"lifeStyle"]];
            }
            else {
                cell.lblLefestyle.text = @"";
            }
            
            if (![[dict objectForKey:@"avg_rating"] isKindOfClass:[NSNull class]] && [dict objectForKey:@"avg_rating"]) {
                cell.lblRating.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"avg_rating"]];
            }
            else {
                cell.lblRating.text = @"0";
            }
            if (![[dict objectForKey:@"returns"] isKindOfClass:[NSNull class]]) {
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"Increase"];
                
                attachment.bounds = CGRectMake(0, -1, 20, 0.8 * CGRectGetHeight(cell.lblAppriciation.frame));
                
                NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
                
                [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [dict objectForKey:@"returns"]]]];
                
                cell.lblAppriciation.attributedText = strProgress;
            }
            else {
                cell.lblAppriciation.text = @"";
            }
            return cell;
        }
    }
    else if (indexPath.section == 1){
        static NSString *str = @"FooterCrouselCell";
        
        FooterCrouselCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:str forIndexPath:indexPath];
        [cell.btnGetAlerts addTarget:self action:@selector(didClickedGetAlerts) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    return nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSIndexPath *indexPath = [_collectionView indexPathForCell:[[_collectionView visibleCells] firstObject]];
    
    if (arrLocality.count == 1) {
        _btnLeft.hidden = YES;
        _btnRight.hidden = YES;
    }
    else {
        if (indexPath.item == 0) {
            _btnLeft.hidden = YES;
            _btnRight.hidden = NO;
        }
        else if (indexPath.item == arrLocality.count - 1) {
            _btnRight.hidden = YES;
            _btnLeft.hidden = NO;
        }
        else if (indexPath.item > 0 && indexPath.item < arrLocality.count - 1) {
            _btnLeft.hidden = NO;
            _btnRight.hidden = NO;
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {

    NSIndexPath *indexPath = [_collectionView indexPathForCell:[[_collectionView visibleCells] firstObject]];
    
    if (arrLocality.count == 1) {
        _btnLeft.hidden = YES;
        _btnRight.hidden = YES;
    }
    else {
        if (indexPath.item == 0) {
            _btnLeft.hidden = YES;
            _btnRight.hidden = NO;
        }
        else if (indexPath.item == arrLocality.count - 1) {
            _btnRight.hidden = YES;
            _btnLeft.hidden = NO;
        }
        else if (indexPath.item > 0 && indexPath.item < arrLocality.count - 1) {
            _btnLeft.hidden = NO;
            _btnRight.hidden = NO;
        }
    }
    
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProjectMapVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectMapVC"];
    
    if (_option == TabularSearchOptionNormal) {
        obj.option = SectorDetailsAccessOptionSearchDetailsForLocality;
    }
    else {
        obj.option = SectorDetailsAccessOptionSearchDetailsForSector;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
    [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
    [dict setObject:[[_dictInfo objectForKey:@"p_type"] lowercaseString] forKey:@"p_type"];
    
    NSDictionary *dictSourse = [arrLocality objectAtIndex:indexPath.item];
    
    if (_option == TabularSearchOptionNormal) {
        [dict setObject:[dictSourse objectForKey:@"location_id"] forKey:@"location"];
        obj.strTitle = [dictSourse objectForKey:@"location_name"];
        
        NSDictionary *dict = [[dictMapInfo objectForKey:@"location_paths"] objectAtIndex:indexPath.row];
        obj.path = [dict objectForKey:@"location_path"];
        
    }
    else {
        [dict setObject:[dictSourse objectForKey:@"sector_id"] forKey:@"sub_location_id"];
        obj.strTitle = [dictSourse objectForKey:@"sector_name"];
        obj.path = [[dictMapInfo objectForKey:@"sector_path"] objectAtIndex:indexPath.row];
    }
    obj.dictSourse = dict;
    obj.arrLegand = arrSegmentControl;
    [self.navigationController pushViewController:obj animated:YES];

}



- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_option == LocalityMapVCAccessOptionLocality) {
        if (indexPath.section == 0) {
            zoom = 12;
            
            if (polygonCurrentMicroMarket.map != nil) {
                polygonCurrentMicroMarket.map = nil;
            }
            
            if (arrLocality.count == 1) {
                _lblTitle.text = [[arrLocality objectAtIndex:indexPath.row] objectForKey:@"location_name"];
            }
            
            NSDictionary *dict = [[dictMapInfo objectForKey:@"location_paths"] objectAtIndex:indexPath.row];
            [self renderMicroMarketPolygonForLocality:[dict objectForKey:@"location_path"] withIndexPath:indexPath];
            [self manageSectorPolygonsForLocality:indexPath andInfo:dict];
        }
        else {
            
        }
    }
    else if (_option == LocalityMapVCAccessOptionSector) {
        
         if (indexPath.section == 0) {
             zoom = 12;
         
             if (polygonCurrentSector.map != nil) {
                 polygonCurrentSector.map = nil;
             }
             
             _lblTitle.text = [[arrLocality objectAtIndex:indexPath.row] objectForKey:@"sector_name"];
             
            [self manageSectorPolygonsForSectors:indexPath andInfo:[[dictMapInfo objectForKey:@"sector_path"] objectAtIndex:indexPath.row]];
         }
         else {
         
         }
        
    }
    
    

}

#pragma mark -
#pragma mark - Google Map for Locality
- (void)locateCityOnMap:(NSString *)strCity {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *str = [strCity stringByReplacingOccurrencesOfString:@"[" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"]" withString:@""];
        NSArray *arr = [str componentsSeparatedByString:@","];
        
        
        for (int i = 0; i < arr.count; i = i + 2) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[arr objectAtIndex:i] forKey:@"lat"];
            [dict setObject:[arr objectAtIndex:i+1] forKey:@"long"];
            [arrCityCordinates addObject:dict];
        }
        
        dictMapInfo = [NSMutableDictionary dictionary];
        GMSMutablePath *pathCity = [GMSMutablePath path];
        
        for (NSDictionary *temp in arrCityCordinates) {
            [pathCity addLatitude:[[temp objectForKey:@"lat"] doubleValue] longitude:[[temp objectForKey:@"long"] doubleValue]];
        }
        
        [dictMapInfo setObject:[pathCity copy] forKey:@"city_path"];
        
        GMSMutablePath *pathMicroMarket = [GMSMutablePath path];
        NSMutableArray *arrMicroMarkets = [NSMutableArray array];
        NSMutableArray *arrSectors = [NSMutableArray array];
        
        for (NSDictionary *location in arrLocality) {
            
            for (NSDictionary *location_cordinate in [location objectForKey:@"location_cordinates"]) {
                
                [pathMicroMarket addLatitude:[[location_cordinate objectForKey:@"lat"] doubleValue] longitude:[[location_cordinate objectForKey:@"long"] doubleValue]];
                
            }
            
            [arrMicroMarkets addObject:[pathMicroMarket copy]];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            NSMutableArray *arr = [NSMutableArray array];
            [dict setObject:[pathMicroMarket copy] forKey:@"location_path"];
            
            GMSMutablePath *pathSectors = [GMSMutablePath path];
            for (NSDictionary *sector in [location objectForKey:@"sector_details"]) {
                
                
                for (NSDictionary *sectorCordinate in [sector objectForKey:@"sector_cordinates"]) {
                    
                    [pathSectors addLatitude:[[sectorCordinate objectForKey:@"lat"] doubleValue] longitude:[[sectorCordinate objectForKey:@"long"] doubleValue]];
                }
                [arr addObject:[pathSectors copy]];
                [pathSectors removeAllCoordinates];
            }
            
            [dict setObject:[arr copy] forKey:@"sector_paths"];
            [arrSectors addObject:dict];
            [dictMapInfo setObject:[arrSectors copy] forKey:@"location_paths"];
            [pathMicroMarket removeAllCoordinates];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            GMSPolygon *polygonCity = [GMSPolygon polygonWithPath:pathCity];
            polygonCity = [GMSPolygon polygonWithPath:pathCity];
            polygonCity.strokeColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
            polygonCity.strokeWidth = 4.0;
            polygonCity.fillColor = [UIColor clearColor];
            polygonCity.map = _mapView;
            
            _collectionView.delegate = self;
            _collectionView.dataSource = self;
            _mapView.delegate = self;
        });
    });
    
}

- (void)manageSectorPolygonsForLocality:(NSIndexPath *)indexPath andInfo:(NSDictionary *)dict {
    
    
    for (GMSPolygon *polygon in arrCurrectSectorPolygons) {
        polygon.map = nil;
    }
    for (GMSMarker *marker in arrSectorMarkers) {
        marker.map = nil;
    }
    
    arrCurrectSectorPolygons = [NSMutableArray array];
    arrSectorMarkers = [NSMutableArray array];
    int i = 0;
    for (GMSPath *path in [dict objectForKey:@"sector_paths"]) {
        
        [self renderSectorPolygonForLocality:path withSectorInfo:[[[arrLocality objectAtIndex:indexPath.row] objectForKey:@"sector_details"] objectAtIndex:i++]];
    }
    
    double latitude  = [[[[arrLocality objectAtIndex:indexPath.row] objectForKey:@"location_cordinates"] valueForKeyPath:@"@avg.lat"] doubleValue];
    double longitude = [[[[arrLocality objectAtIndex:indexPath.row] objectForKey:@"location_cordinates"] valueForKeyPath:@"@avg.long"] doubleValue];
    
    if (camera == nil) {
        
        
        
        camera = [GMSCameraPosition cameraWithLatitude:latitude
                                             longitude:longitude
                                                  zoom:zoom
                                               bearing:0
                                          viewingAngle:0];
        [_mapView setCamera:camera];
        
    }
    else {
        updatedCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(latitude, longitude)
                                              zoom:zoom];
        [_mapView animateWithCameraUpdate:updatedCamera];
    }
}


- (void)renderMicroMarketPolygonForLocality:(GMSPath *)path withIndexPath:(NSIndexPath *)indexPath {
    
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:path];
    //polygon.fillColor = [UIColor colorWithRed:240.0/255.0 green:248.0/255.0 blue:110.0/255.0 alpha:0.9];
    polygon.fillColor = [UIColor clearColor];
    polygon.strokeWidth = 2.0;
    polygon.strokeColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
    polygonCurrentMicroMarket = polygon;
    polygon.tappable = NO;
    polygon.map = _mapView;
    
    
    NSDictionary *dict = [arrLocality objectAtIndex:indexPath.row];
    
    if (markerCurrentMicroMarket.map != nil) {
        markerCurrentMicroMarket.map = nil;
    }
    markerCurrentMicroMarket = [GMSMarker markerWithPosition:[self getCenterCordinates:path]];
    markerCurrentMicroMarket.snippet = [dict objectForKey:@"location_name"];
    markerCurrentMicroMarket.iconView = [self setCustomMarker];
    markerCurrentMicroMarket.map = _mapView;
    
    //_mapView.selectedMarker = markerCurrentMicroMarket;
}




- (void)renderSectorPolygonForLocality:(GMSPath *)path withSectorInfo:(NSDictionary *)dictSector {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        UIColor *colorCode;
        if (_segmentControl.selectedSegmentIndex == 0) {
            
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"infra"] integerValue]];
        }
        else if (_segmentControl.selectedSegmentIndex == 1) {
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"needs"] integerValue]];
        }
        else if (_segmentControl.selectedSegmentIndex == 2) {
            colorCode = [self selectColorOnPSFForSector:[[dictSector objectForKey:@"psf_average"] integerValue]];
        }
        else if (_segmentControl.selectedSegmentIndex == 3) {
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"lifeStyle"] integerValue]];
        }
        else {
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"return"] integerValue]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            GMSPolygon *polygon = [GMSPolygon polygonWithPath:path];
            polygon.fillColor = colorCode;
            polygon.strokeWidth = 0.5;
            polygon.strokeColor = [UIColor blackColor];
            [arrCurrectSectorPolygons addObject:polygon];
            polygon.tappable = YES;
            polygon.map = _mapView;
 
            GMSMarker *marker = [GMSMarker markerWithPosition:[self getCenterCordinates:path]];
            marker.snippet = [dictSector objectForKey:@"sector_name"];
            marker.iconView = [self setCustomMarker];
            marker.map = _mapView;
            [arrSectorMarkers addObject:marker];
        });
    });
    
    
    
}


#pragma mark -
#pragma mark - Google Map for Sectors
- (void)locateMicroMarketOnMap:(NSArray *)arr {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dictMapInfo = [NSMutableDictionary dictionary];
        GMSMutablePath *pathMicroMarket = [GMSMutablePath path];
        
        for (NSDictionary *temp in arr) {
            [pathMicroMarket addLatitude:[[temp objectForKey:@"lat"] doubleValue] longitude:[[temp objectForKey:@"long"] doubleValue]];
        }
        
        [dictMapInfo setObject:[pathMicroMarket copy] forKey:@"micromarket_path"];
        
        NSMutableArray *arrSectors = [NSMutableArray array];
        for (NSDictionary *sector in arrLocality) {
            
            GMSMutablePath *pathSectors = [GMSMutablePath path];
            for (NSDictionary *temp in [sector objectForKey:@"sector_cord"]) {
                [pathSectors addLatitude:[[temp objectForKey:@"lat"] doubleValue] longitude:[[temp objectForKey:@"long"] doubleValue]];
            }
            [arrSectors addObject:[pathSectors copy]];
            [pathSectors removeAllCoordinates];
        }
        
        [dictMapInfo setObject:[arrSectors copy] forKey:@"sector_path"];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            GMSPolygon *polygonMicroMarket = [GMSPolygon polygonWithPath:pathMicroMarket];
            polygonMicroMarket.strokeColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
            polygonMicroMarket.strokeWidth = 4.0;
            polygonMicroMarket.fillColor = [UIColor clearColor];
            polygonMicroMarket.map = _mapView;
            
            _collectionView.delegate = self;
            _collectionView.dataSource = self;
            _mapView.delegate = self;
            
        });
        
    });
}

- (void)manageSectorPolygonsForSectors:(NSIndexPath *)indexPath andInfo:(GMSPath *)path {
    
    for (GMSPolygon *polygon in arrCurrectSectorPolygons) {
        polygon.map = nil;
    }
    for (GMSMarker *marker in arrSectorMarkers) {
        marker.map = nil;
    }
    
    arrCurrectSectorPolygons = [NSMutableArray array];
    arrSectorMarkers = [NSMutableArray array];
    
    [self renderSectorPolygonForSector:path withSectorInfo:[arrLocality objectAtIndex:indexPath.row]];
    
    
}

- (void)renderSectorPolygonForSector:(GMSPath *)path withSectorInfo:(NSDictionary *)dictSector {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        UIColor *colorCode;
        if (_segmentControl.selectedSegmentIndex == 0) {
            
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"infra_code"] integerValue]];
        }
        else if (_segmentControl.selectedSegmentIndex == 1) {
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"needs_code"] integerValue]];
        }
        else if (_segmentControl.selectedSegmentIndex == 2) {
            colorCode = [self selectColorOnPSFForSector:[[dictSector objectForKey:@"avg_pricePSF_code"] integerValue]];
            //colorCode = [self selectColorOnPSFForSector:0];
        }
        else if (_segmentControl.selectedSegmentIndex == 3) {
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"lifeStyle_code"] integerValue]];
        }
        else {
            colorCode = [self selectColorOnSector:[[dictSector objectForKey:@"return_code"] integerValue]];
        }
        
        double latitude  = [[[dictSector objectForKey:@"sector_cord"] valueForKeyPath:@"@avg.lat"] doubleValue];
        double longitude = [[[dictSector objectForKey:@"sector_cord"] valueForKeyPath:@"@avg.long"] doubleValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            if (camera == nil) {
                camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                     longitude:longitude
                                                          zoom:zoom
                                                       bearing:0
                                                  viewingAngle:0];
                [_mapView setCamera:camera];
                
            }
            else {
                updatedCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(latitude, longitude)
                                                      zoom:zoom];
                [_mapView animateWithCameraUpdate:updatedCamera];
            }
            
            GMSPolygon *polygon = [GMSPolygon polygonWithPath:path];
            polygon.fillColor = colorCode;
            polygon.strokeWidth = 0.5;
            polygon.strokeColor = [UIColor blackColor];
            polygon.tappable = YES;
            polygon.map = _mapView;
            [arrCurrectSectorPolygons addObject:polygon];
            
            
            GMSMarker *marker = [GMSMarker markerWithPosition:[self getCenterCordinates:path]];
            marker.snippet = [dictSector objectForKey:@"sector_name"];
            marker.iconView = [self setCustomMarker];
            marker.map = _mapView;
            [arrSectorMarkers addObject:marker];
        });
    });
    
    
    
}




#pragma mark - Reference for Color Codes
- (UIColor *)selectColorOnSector:(CGFloat)value {
    return [arrColorCode objectAtIndex:(value - 1) < 0?1:(value - 1)];
}

- (UIColor *)selectColorOnPSFForSector:(CGFloat)value {
    return [arrColorCode objectAtIndex:(value - 1) < 0?1:(value - 1)];
}

#pragma mark - GMSMapView Delegate
- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay {
    
    
    if (_option == LocalityMapVCAccessOptionLocality) {
        GMSPolygon *polygon = (GMSPolygon *)overlay;
        
        if (polygon == polygonCurrentMicroMarket) {
            mapView.selectedMarker = markerCurrentMicroMarket;
        }
        else {
            
            mapView.selectedMarker = [arrSectorMarkers objectAtIndex:[arrCurrectSectorPolygons indexOfObject:polygon]];
        }
    }
    else {
        mapView.selectedMarker = [arrSectorMarkers firstObject];
    }
    
}

#pragma mark - Auxillary Mathods
- (CLLocationCoordinate2D)getCenterCordinates:(GMSPath *)path {
    
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < path.count; i ++) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        CLLocationCoordinate2D cordinates = [path coordinateAtIndex:i];
        [dict setObject:[NSString stringWithFormat:@"%f", cordinates.latitude] forKey:@"lat"];
        [dict setObject:[NSString stringWithFormat:@"%f", cordinates.longitude] forKey:@"long"];
        [arr addObject:dict];
    }
    CGFloat latitude = [[arr valueForKeyPath:@"@avg.lat"] doubleValue];
    CGFloat longitude = [[arr valueForKeyPath:@"@avg.long"] doubleValue];
    return CLLocationCoordinate2DMake(latitude, longitude);
}

- (UIView *)setCustomMarker {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

@end
