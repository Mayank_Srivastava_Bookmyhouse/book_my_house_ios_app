//
//  FooterCrouselCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/15/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterCrouselCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnGetAlerts;

@end
