//
//  SectorCarousel.h
//  Book My House
//
//  Created by Mayank Srivastava on 06/01/17.
//  Copyright © 2017 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectorCarousel : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgSide;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLocality;
@property (weak, nonatomic) IBOutlet UILabel *lblAvg;
@property (weak, nonatomic) IBOutlet UILabel *lblProjects;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCompare;
@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeeds;
@property (weak, nonatomic) IBOutlet UILabel *lblLefestyle;
@property (weak, nonatomic) IBOutlet UILabel *lblAppriciation;
@end
