//
//  BuilderFilterVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/14/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "BuilderFilterVC.h"

@interface BuilderFilterVC () {
    
    __weak IBOutlet UIButton *btnClearAll;
    __weak IBOutlet UIButton *btnDone;
    __weak IBOutlet UISearchBar *btnSearcBar;
    __weak IBOutlet UITableView *tblView;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation BuilderFilterVC

#pragma mark - Viewcontroller Mathods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self screenSettings];
}


#pragma mardk - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    
}

- (void)updateScreenSettings {
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    NSLog(@"%s", __FUNCTION__);
    if (sender == btnClearAll) {
        
    }
    else if (sender == btnDone) {
        
    }
}



@end
