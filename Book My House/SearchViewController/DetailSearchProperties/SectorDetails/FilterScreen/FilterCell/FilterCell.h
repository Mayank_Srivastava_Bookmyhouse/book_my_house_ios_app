//
//  FilterCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/31/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MARKRangeSlider.h"

@interface FilterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnNewLaunch;
@property (weak, nonatomic) IBOutlet UIButton *btnUnderConstruction;
@property (weak, nonatomic) IBOutlet UIButton *btnReadyToMoveIn;
@property (strong, nonatomic) NSMutableDictionary *dictStatus;
@property (strong, nonatomic) NSString *strFilter;


+ (FilterCell *)createCell;
- (IBAction)actionListeners:(id)sender;
- (void)setButtonUnselected:(UIButton *)btn;
- (void)setButtonSelected:(UIButton *)btn;
@end



@interface SortedBy : UITableViewCell<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnRelevence;
@property (weak, nonatomic) IBOutlet UIButton *btnNewest;
@property (weak, nonatomic) IBOutlet UIButton *btnPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnPricePSF;
@property (weak, nonatomic) IBOutlet UIButton *btnRating;
@property (weak, nonatomic) IBOutlet UIButton *btnPossession;
@property (weak, nonatomic) IBOutlet UIButton *btnInfra;
@property (weak, nonatomic) IBOutlet UIButton *btnNeeds;
@property (weak, nonatomic) IBOutlet UIButton *btnLifestyle;
@property (weak, nonatomic) IBOutlet UIButton *btnReturns;
@property (weak, nonatomic) NSString *strSotedBy;
@property (strong, nonatomic) UITableView *tblPrice;
@property (strong, nonatomic) UITableView *tblPricePSF;
@property (strong, nonatomic) UITableView *tblPossession;

+ (SortedBy *)createCell;
- (IBAction)actionListeners:(id)sender;

- (void)setButtonUnselected:(UIButton *)btn;
- (void)setButtonSelected:(UIButton *)btn;
@end


@interface Discount_Offers : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (assign, nonatomic) BOOL needsDiscountAndOffers;
- (IBAction)actionListener:(id)sender;
+ (Discount_Offers *)createCell;
@end

@interface PossessionIn : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn6m;
@property (weak, nonatomic) IBOutlet UIButton *btn1yr;
@property (weak, nonatomic) IBOutlet UIButton *btn2yr;
@property (weak, nonatomic) IBOutlet UIButton *btn3yr;
@property (weak, nonatomic) IBOutlet UIButton *btn4yr;
@property (weak, nonatomic) IBOutlet UIButton *btn5yr;
@property (strong, nonatomic) NSString *strPossessionIn;
- (IBAction)actionListeners:(id)sender;
+ (PossessionIn *)createCell;
- (void)setButtonSelected:(UIButton *)btn;
- (void)setButtonUnselected:(UIButton *)btn;

@end


@interface SelectBuilder : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAll;
+ (SelectBuilder *)createCell;
@end

@interface SelectPropertyType : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnResidential;
@property (weak, nonatomic) IBOutlet UIButton *btnCommercial;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
+ (SelectPropertyType *)createCell;
@end


@interface BHK : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn1BHK;
@property (weak, nonatomic) IBOutlet UIButton *btn2BHK;
@property (weak, nonatomic) IBOutlet UIButton *btn3BHK;
@property (weak, nonatomic) IBOutlet UIButton *btn4BHK;
@property (weak, nonatomic) IBOutlet UIButton *btn5BHK;
@property (weak, nonatomic) IBOutlet UIButton *btn5PlusBHK;

@property (weak, nonatomic) IBOutlet UIImageView *img1BHK;
@property (weak, nonatomic) IBOutlet UIImageView *img2BHK;
@property (weak, nonatomic) IBOutlet UIImageView *img3BHK;
@property (weak, nonatomic) IBOutlet UIImageView *img4BHK;
@property (weak, nonatomic) IBOutlet UIImageView *img5BHK;
@property (weak, nonatomic) IBOutlet UIImageView *img5PlusBHK;

@property (weak, nonatomic) IBOutlet UILabel *lbl1BHK;
@property (weak, nonatomic) IBOutlet UILabel *lbl2BHK;
@property (weak, nonatomic) IBOutlet UILabel *lbl3BHK;
@property (weak, nonatomic) IBOutlet UILabel *lbl4BHK;
@property (weak, nonatomic) IBOutlet UILabel *lbl5BHK;
@property (weak, nonatomic) IBOutlet UILabel *lbl5PlusBHK;


@property (strong, nonatomic) NSMutableDictionary *dictBHK;
@property (strong, nonatomic) NSString *strBhk;
- (IBAction)actionListener:(id)sender;

+ (BHK *)createCell;
@end


@interface PriceRange : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLowerBound;
@property (weak, nonatomic) IBOutlet UILabel *lblUpperBound;
@property (weak, nonatomic) IBOutlet MARKRangeSlider *slider;
@property (strong, nonatomic) NSString *strUpprerLimit;
@property (strong, nonatomic) NSString *strLowerLimit;

+ (PriceRange *)createCell;
@end

@interface Aminities : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnGas;
@property (weak, nonatomic) IBOutlet UIButton *btnLift;
@property (weak, nonatomic) IBOutlet UIButton *btnParking;
@property (weak, nonatomic) IBOutlet UIButton *btnGym;
@property (weak, nonatomic) IBOutlet UIButton *btnSecurity;
@property (weak, nonatomic) IBOutlet UIButton *btnWardrobe;
@property (weak, nonatomic) IBOutlet UIButton *btnWifi;
@property (weak, nonatomic) IBOutlet UIButton *btnFurnished;
@property (weak, nonatomic) IBOutlet UIButton *btnAC;
@property (weak, nonatomic) IBOutlet UIButton *btnHomeAutomation;
@property (weak, nonatomic) IBOutlet UIButton *btnPowerBackup;
@property (weak, nonatomic) IBOutlet UIButton *btnSwimmingPool;

@property (weak, nonatomic) IBOutlet UIImageView *imgGas;
@property (weak, nonatomic) IBOutlet UIImageView *imgLift;
@property (weak, nonatomic) IBOutlet UIImageView *imgParking;
@property (weak, nonatomic) IBOutlet UIImageView *imgGym;
@property (weak, nonatomic) IBOutlet UIImageView *imgSecurity;
@property (weak, nonatomic) IBOutlet UIImageView *imgWardrobe;
@property (weak, nonatomic) IBOutlet UIImageView *imgWifi;
@property (weak, nonatomic) IBOutlet UIImageView *imgFurnished;
@property (weak, nonatomic) IBOutlet UIImageView *imgAC;
@property (weak, nonatomic) IBOutlet UIImageView *imgswimmingPool;
@property (weak, nonatomic) IBOutlet UIImageView *imgHomeAutomation;
@property (weak, nonatomic) IBOutlet UIImageView *imgPowerBackup;
@property (strong, nonatomic) NSMutableDictionary *dictAminities;

- (IBAction)actionListeners:(id)sender;

+ (Aminities *)createCell;
@end