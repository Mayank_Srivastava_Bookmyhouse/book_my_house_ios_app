//
//  FilterCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/31/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "FilterCell.h"
#import "Header.h"
#import "PropertyCell.h"

@implementation FilterCell

- (void)awakeFromNib {
    // Initialization code
    
    _btnNewLaunch.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnNewLaunch.titleLabel.textAlignment = NSTextAlignmentCenter;
    [super awakeFromNib];
}

+ (FilterCell *)createCell {
    FilterCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] firstObject];
    cell.btnNewLaunch.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnNewLaunch.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    cell.btnReadyToMoveIn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnReadyToMoveIn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    cell.btnUnderConstruction.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnUnderConstruction.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    cell.dictStatus = [NSMutableDictionary dictionary];
    [cell.dictStatus setObject:@NO forKey:@"New Launch"];
    [cell.dictStatus setObject:@NO forKey:@"Ready To Move"];
    [cell.dictStatus setObject:@NO forKey:@"Under Construction"];
    cell.strFilter = @"";
    return cell;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionListeners:(id)sender {
    
    if (sender == _btnNewLaunch) {
        _btnNewLaunch.selected = !_btnNewLaunch.selected;
        if (_btnNewLaunch.selected) {
            [self setButtonSelected:_btnNewLaunch];
            [_dictStatus setObject:@YES forKey:@"New Launch"];
        }
        else {
            [self setButtonUnselected:_btnNewLaunch];
            [_dictStatus setObject:@NO forKey:@"New Launch"];
        }
    }
    else if (sender == _btnReadyToMoveIn) {
        _btnReadyToMoveIn.selected = !_btnReadyToMoveIn.selected;
        if (_btnReadyToMoveIn.selected) {
            [self setButtonSelected:_btnReadyToMoveIn];
            [_dictStatus setObject:@YES forKey:@"Ready To Move"];
        }
        else {
            [self setButtonUnselected:_btnReadyToMoveIn];
            [_dictStatus setObject:@NO forKey:@"Ready To Move"];
        }
    }
    else if (sender == _btnUnderConstruction) {
        _btnUnderConstruction.selected = !_btnUnderConstruction.selected;
        if (_btnUnderConstruction.selected) {
            [self setButtonSelected:_btnUnderConstruction];
            [_dictStatus setObject:@YES forKey:@"Under Construction"];
        }
        else {
            [self setButtonUnselected:_btnUnderConstruction];
            [_dictStatus setObject:@NO forKey:@"Under Construction"];
        }
    }
    
    
    NSLog(@"Status %@", _dictStatus);
    _strFilter = @"";
    NSMutableArray *arr = [NSMutableArray array];
    for (NSString *keys in [_dictStatus allKeys]) {
        
        if ([[_dictStatus objectForKey:keys] boolValue]) {
            [arr addObject:keys];
        }
    }
    
    if (arr.count) {
        _strFilter = [arr componentsJoinedByString:@","];
    }
    else {
        _strFilter = @"";
    }

}

- (void)setButtonUnselected:(UIButton *)btn {
    
    btn.selected = NO;
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.titleLabel.textColor = [UIColor
                                blackColor];
}

- (void)setButtonSelected:(UIButton *)btn {
    
    btn.selected = YES;
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor
                             colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
    
    btn.titleLabel.textColor = [UIColor
                                colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
}

@end

@implementation SortedBy

+ (SortedBy *)createCell {
    SortedBy *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:1];
    
    cell.btnRelevence.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnRelevence.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnRelevence.titleLabel.numberOfLines = 2;
    
    cell.btnNewest.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnNewest.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnNewest.titleLabel.numberOfLines = 2;
    
    cell.btnPrice.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnPrice.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnPrice.titleLabel.numberOfLines = 2;
    
    cell.btnPricePSF.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnPricePSF.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnPricePSF.titleLabel.numberOfLines = 2;
    
    cell.btnRating.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnRating.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnRating.titleLabel.numberOfLines = 2;
    
    cell.btnPossession.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnPossession.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnPossession.titleLabel.numberOfLines = 2;
    
    
    cell.btnInfra.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnInfra.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnInfra.titleLabel.numberOfLines = 2;
    
    cell.btnNeeds.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnNeeds.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnNeeds.titleLabel.numberOfLines = 2;
    
    cell.btnLifestyle.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnLifestyle.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnLifestyle.titleLabel.numberOfLines = 2;
    
    cell.btnReturns.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.btnReturns.titleLabel.textAlignment = NSTextAlignmentCenter;
    cell.btnReturns.titleLabel.numberOfLines = 2;
    
    return cell;
}


- (IBAction)actionListeners:(id)sender {
    
    [_btnPrice setTitle:@"Price" forState:UIControlStateNormal];
    [_btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
    [_btnPricePSF setTitle:@"Price PSF" forState:UIControlStateNormal];
    
    if (sender == _btnRelevence) {
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        _strSotedBy = @"Relevence";
        
        [self setButtonSelected:_btnRelevence];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnNewest) {
        
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        _strSotedBy = @"newest";
        [self setButtonSelected:_btnNewest];
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnPrice) {
        
        _strSotedBy = @"Price";
        if (_tblPrice == nil) {
            _tblPrice = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_btnPrice.frame), CGRectGetMaxY(_btnPrice.frame), CGRectGetWidth(_btnPrice.frame), 2 * CGRectGetHeight(_btnPrice.frame))];
            _tblPrice.separatorColor = [UIColor clearColor];
            _tblPrice.delegate = self;
            _tblPrice.dataSource = self;
            [self addSubview:_tblPrice];
        }
        else {
            
        }
        
        _tblPrice.hidden = NO;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnPricePSF) {
        _strSotedBy = @"PricePSF";
        if (_tblPricePSF == nil) {
            _tblPricePSF = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_btnPricePSF.frame), CGRectGetMaxY(_btnPricePSF.frame), CGRectGetWidth(_btnPricePSF.frame), 2 * CGRectGetHeight(_btnPricePSF.frame))];
            _tblPricePSF.separatorColor = [UIColor clearColor];
            _tblPricePSF.delegate = self;
            _tblPricePSF.dataSource = self;
            [self addSubview:_tblPricePSF];
            
        }
        else {
            
        }
        
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = NO;
        _tblPossession.hidden = YES;
        
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnRating) {
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        _strSotedBy = @"rating";
        [self setButtonSelected:_btnRating];
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnPossession) {
        
        _strSotedBy = @"Possession";
        if (_tblPossession == nil) {
            _tblPossession = [[UITableView alloc] initWithFrame:CGRectMake(4 * CGRectGetMinX(_btnPossession.frame) / 5, CGRectGetMaxY(_btnPossession.frame), 1.5 * CGRectGetWidth(_btnPossession.frame), 2 * CGRectGetHeight(_btnPossession.frame))];
            _tblPossession.separatorColor = [UIColor clearColor];
            _tblPossession.delegate = self;
            _tblPossession.dataSource = self;
            [self addSubview:_tblPossession];
            
        }
        else {
            
        }
        
        _tblPricePSF.hidden = YES;
        _tblPrice.hidden = YES;
        _tblPossession.hidden = NO;
        
        
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnInfra) {
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        _strSotedBy = @"infra";
        [self setButtonSelected:_btnInfra];
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnNeeds) {
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        _strSotedBy = @"needs";
        [self setButtonSelected:_btnNeeds];
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnLifestyle) {
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
        _strSotedBy = @"lifestyle";
        [self setButtonSelected:_btnLifestyle];
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnNeeds];
        [self setButtonUnselected:_btnReturns];
    }
    else if (sender == _btnReturns) {
        _tblPrice.hidden = YES;
        _tblPricePSF.hidden = YES;
        _tblPossession.hidden = YES;
         _strSotedBy = @"returnsval";
        [self setButtonSelected:_btnReturns];
        [self setButtonUnselected:_btnRelevence];
        [self setButtonUnselected:_btnPrice];
        [self setButtonUnselected:_btnPricePSF];
        [self setButtonUnselected:_btnNewest];
        [self setButtonUnselected:_btnPossession];
        [self setButtonUnselected:_btnInfra];
        [self setButtonUnselected:_btnRating];
        [self setButtonUnselected:_btnLifestyle];
        [self setButtonUnselected:_btnNeeds];
    }
    
    NSLog(@"Sort by %@", _strSotedBy);
}


- (void)setButtonUnselected:(UIButton *)btn {
    
    btn.selected = NO;
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.titleLabel.textColor = [UIColor
                                blackColor];
}

- (void)setButtonSelected:(UIButton *)btn {
    
    btn.selected = YES;
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
    
    btn.titleLabel.textColor = [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
}

#pragma mark - Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 25;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *str = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:15.0];
        
    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    if (tableView == _tblPrice) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Low to high";
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = @"High to low";
        }
        else if (indexPath.row == 2) {
            cell.textLabel.text = @"Cancel";
        }
    }
    else if (tableView == _tblPricePSF) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Low to high";
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = @"High to low";
        }
        else if (indexPath.row == 2) {
            cell.textLabel.text = @"Cancel";
        }
    }
    else if (tableView == _tblPossession) {
        if (indexPath.row == 0) {
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            
            cell.textLabel.text = @" Earliest to latest";
            [cell.textLabel sizeToFit];
        }
        else if (indexPath.row == 1) {
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.textLabel.text = @" Latest to earliest";
            [cell.textLabel sizeToFit];

        }
        else if (indexPath.row == 2) {
            cell.textLabel.text = @"Cancel";
        }
    }
    
    cell.textLabel.layer.borderWidth = 1.0;
    cell.textLabel.layer.borderColor = [UIColor grayColor].CGColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == _tblPrice) {
        if (indexPath.row == 0) {
            _strSotedBy = @"priceltoh";
            [_btnPrice setTitle:@"Low to high" forState:UIControlStateNormal];
            [self setButtonSelected:_btnPrice];
            [_btnPrice sizeToFit];
        }
        else if (indexPath.row == 1) {
            _strSotedBy = @"pricehtol";
            [_btnPrice setTitle:@"High to low" forState:UIControlStateNormal];
            [self setButtonSelected:_btnPrice];
            [_btnPrice sizeToFit];
        }
        else if (indexPath.row == 2) {
            _strSotedBy = @"";
            [_btnPrice setTitle:@"Price" forState:UIControlStateNormal];
            [self setButtonUnselected:_btnPrice];
        }

        _tblPrice.hidden = YES;
    }
    else if (tableView == _tblPossession) {
        if (indexPath.row == 0) {
            _strSotedBy = @"possessionltoh";
            [_btnPossession setTitle:@"Earliest to latest" forState:UIControlStateNormal];
            [self setButtonSelected:_btnPossession];
            [_btnPossession sizeToFit];
        }
        else if (indexPath.row == 1) {
            _strSotedBy = @"possessionhtol";
            [_btnPossession setTitle:@"Latest to earliest" forState:UIControlStateNormal];
            [self setButtonSelected:_btnPossession];
            [_btnPossession sizeToFit];
        }
        else if (indexPath.row == 2) {
            _strSotedBy = @"";
            [_btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
            [self setButtonUnselected:_btnPossession];
        }
        _tblPossession.hidden = YES;
    }
    else if (tableView == _tblPricePSF) {
        if (indexPath.row == 0) {
            _strSotedBy = @"psfltoh";
            [_btnPricePSF setTitle:@"Low to high" forState:UIControlStateNormal];
            [self setButtonSelected:_btnPricePSF];
            [_btnPricePSF sizeToFit];
        }
        else if (indexPath.row == 1) {
             _strSotedBy = @"psfhtol";
            [_btnPricePSF setTitle:@"High to low" forState:UIControlStateNormal];
            [self setButtonSelected:_btnPricePSF];
            [_btnPricePSF sizeToFit];
        }
        else if (indexPath.row == 2) {
            _strSotedBy = @"";
            [_btnPricePSF setTitle:@"Price PSF" forState:UIControlStateNormal];
            [self setButtonUnselected:_btnPricePSF];
        }
        _tblPricePSF.hidden = YES;
    }
 
}
@end


@implementation Discount_Offers


+ (Discount_Offers *)createCell {
    Discount_Offers *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:2];
    
    return cell;
}

- (IBAction)actionListener:(id)sender {
   
   
    if (sender == _btnCheck) {
         _btnCheck.selected = !_btnCheck.selected;
        if (!_btnCheck.selected) {
            
            _imgCheck.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
        else {
            _imgCheck.image = [UIImage imageNamed:@"Checkbox_green"];
        }
    }
    _needsDiscountAndOffers = _btnCheck.selected;
    
    NSLog(@"Discounts and offers %d", _needsDiscountAndOffers);
}

@end


@implementation PossessionIn

+ (PossessionIn *)createCell {
    PossessionIn *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:3];
    cell.strPossessionIn = @"";
    return cell;
}

- (IBAction)actionListeners:(id)sender {
    
    if (sender == _btn6m) {
        _strPossessionIn = @"6 m";
        [self setButtonSelected:_btn6m];
        [self setButtonUnselected:_btn1yr];
        [self setButtonUnselected:_btn2yr];
        [self setButtonUnselected:_btn3yr];
        [self setButtonUnselected:_btn4yr];
        [self setButtonUnselected:_btn5yr];
    }
    else if (sender == _btn1yr) {
        _strPossessionIn = @"1 Yr";
        [self setButtonSelected:_btn1yr];
        [self setButtonUnselected:_btn6m];
        [self setButtonUnselected:_btn2yr];
        [self setButtonUnselected:_btn3yr];
        [self setButtonUnselected:_btn4yr];
        [self setButtonUnselected:_btn5yr];
    }
    else if (sender == _btn2yr) {
        _strPossessionIn = @"1 Yr";
        [self setButtonSelected:_btn2yr];
        [self setButtonUnselected:_btn1yr];
        [self setButtonUnselected:_btn6m];
        [self setButtonUnselected:_btn3yr];
        [self setButtonUnselected:_btn4yr];
        [self setButtonUnselected:_btn5yr];
    }
    else if (sender == _btn3yr) {
        _strPossessionIn = @"3 Yr";
        [self setButtonSelected:_btn3yr];
        [self setButtonUnselected:_btn1yr];
        [self setButtonUnselected:_btn2yr];
        [self setButtonUnselected:_btn6m];
        [self setButtonUnselected:_btn4yr];
        [self setButtonUnselected:_btn5yr];
    }
    else if (sender == _btn4yr) {
        _strPossessionIn = @"4 Yr";
        [self setButtonSelected:_btn4yr];
        [self setButtonUnselected:_btn1yr];
        [self setButtonUnselected:_btn2yr];
        [self setButtonUnselected:_btn3yr];
        [self setButtonUnselected:_btn6m];
        [self setButtonUnselected:_btn5yr];
    }
    else if (sender == _btn5yr) {
        _strPossessionIn = @"5 Yr";
        [self setButtonSelected:_btn5yr];
        [self setButtonUnselected:_btn1yr];
        [self setButtonUnselected:_btn2yr];
        [self setButtonUnselected:_btn3yr];
        [self setButtonUnselected:_btn4yr];
        [self setButtonUnselected:_btn6m];
    }
    
    NSLog(@"Possession In %@", _strPossessionIn);
}

- (void)setButtonUnselected:(UIButton *)btn {
    
    btn.selected = NO;
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.titleLabel.textColor = [UIColor
                                blackColor];
}

- (void)setButtonSelected:(UIButton *)btn {
    btn.selected = YES;
    btn.layer.borderWidth = 1.0;
    btn.layer.borderColor = [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
    
    btn.titleLabel.textColor = [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
}

@end

@implementation SelectBuilder
+ (SelectBuilder *)createCell {
    SelectBuilder *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:4];
    return cell;
}


@end



@implementation SelectPropertyType

#pragma mark - Cell Creation
+ (SelectPropertyType *)createCell {
    SelectPropertyType *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:5];
    
    [cell.colView registerClass:[PropertyCell class] forCellWithReuseIdentifier:@"propertyCell"];
    [cell.colView registerNib:[UINib nibWithNibName:@"PropertyCell" bundle:nil] forCellWithReuseIdentifier:@"propertyCell"];
        return cell;
}




#pragma mark - Auxillary Mathods

- (void)setButtonUnselected:(UIButton *)btn {
    
    btn.selected = NO;
    btn.titleLabel.textColor = [UIColor
                                blackColor];
}

- (void)setButtonSelected:(UIButton *)btn {
    btn.selected = YES;
    btn.titleLabel.textColor = [UIColor
                                colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
}
@end


@implementation BHK

+ (BHK *)createCell {
    BHK *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:6];
    cell.dictBHK = [NSMutableDictionary dictionary];
    [cell.dictBHK setValue:@NO forKey:@"1bhk"];
    [cell.dictBHK setValue:@NO forKey:@"2bhk"];
    [cell.dictBHK setValue:@NO forKey:@"3bhk"];
    [cell.dictBHK setValue:@NO forKey:@"4bhk"];
    [cell.dictBHK setValue:@NO forKey:@"5bhk"];
    [cell.dictBHK setValue:@NO forKey:@"5plusbhk"];
    return cell;
}

- (IBAction)actionListener:(id)sender {
    
    if (sender == _btn1BHK) {
        _btn1BHK.selected = !_btn1BHK.selected;
        if (_btn1BHK.selected) {
            _img1BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictBHK setValue:@YES forKey:@"1bhk"];
        }
        else {
            _img1BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictBHK setValue:@NO forKey:@"1bhk"];
        }
        
    }
    else if (sender == _btn2BHK) {
        _btn2BHK.selected = !_btn2BHK.selected;
        if (_btn2BHK.selected) {
            _img2BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictBHK setValue:@YES forKey:@"2bhk"];
        }
        else {
            _img2BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictBHK setValue:@NO forKey:@"2bhk"];
        }
        
    }
    else if (sender == _btn3BHK) {
        _btn3BHK.selected = ! _btn3BHK.selected;
        if (_btn3BHK.selected) {
            _img3BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictBHK setValue:@YES forKey:@"3bhk"];
        }
        else {
            _img3BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictBHK setValue:@NO forKey:@"3bhk"];
        }
    }
    else if (sender == _btn4BHK) {
        _btn4BHK.selected = !_btn4BHK.selected;
        if (_btn4BHK.selected) {
            _img4BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictBHK setValue:@YES forKey:@"4bhk"];
        }
        else {
            _img4BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictBHK setValue:@NO forKey:@"4bhk"];
        }
    }
    else if (sender == _btn5BHK) {
        
        _btn5BHK.selected = !_btn5BHK.selected;
        if (_btn5BHK.selected) {
            _img5BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictBHK setValue:@YES forKey:@"5bhk"];
        }
        else {
            _img5BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictBHK setValue:@NO forKey:@"5bhk"];
        }
    }
    else if (sender == _btn5PlusBHK) {
        _btn5PlusBHK.selected = !_btn5PlusBHK.selected;
        if (_btn5PlusBHK.selected) {
            _img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictBHK setValue:@YES forKey:@"5plusbhk"];
        }
        else {
            _img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictBHK setValue:@NO forKey:@"5plusbhk"];
        }
    }
    
    NSLog(@"BHK %@", _dictBHK);
    
    
    NSMutableArray *arr = [NSMutableArray array];
    for (NSString *keys in [_dictBHK allKeys]) {
        
        if ([[_dictBHK objectForKey:keys] boolValue]) {
            [arr addObject:keys];
        }
    }
    
    if (arr.count) {
        _strBhk = [arr componentsJoinedByString:@","];
    }
    else {
       _strBhk = @"";
    }
    
}




@end

@implementation PriceRange

+ (PriceRange *)createCell {
    PriceRange *cell = [[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:7];
    return cell;
}
@end


@implementation Aminities


+ (Aminities *)createCell {
    Aminities *cell =[[[NSBundle mainBundle] loadNibNamed:@"FilterCell" owner:self options:nil] objectAtIndex:8];
    cell.dictAminities = [NSMutableDictionary dictionary];
    [cell.dictAminities setValue:@NO forKey:@"Piped_Gas"];
    [cell.dictAminities setValue:@NO forKey:@"Lift"];
    [cell.dictAminities setValue:@NO forKey:@"Podium_Parking"];
    [cell.dictAminities setValue:@NO forKey:@"Gymnasium"];
    [cell.dictAminities setValue:@NO forKey:@"24*7_Security"];
    [cell.dictAminities setValue:@NO forKey:@"Treated_Water_Supply"];////ame_wardrobe
    [cell.dictAminities setValue:@NO forKey:@"Central_Wi-fi"];
    [cell.dictAminities setValue:@NO forKey:@"Party_Hall"];// //ame_furnished
    [cell.dictAminities setValue:@NO forKey:@"AC_in_Lobby"];
    [cell.dictAminities setValue:@NO forKey:@"Swimming"];
    [cell.dictAminities setValue:@NO forKey:@"Home_Automation"];
    [cell.dictAminities setValue:@NO forKey:@"Power_Backup"];
    return cell;
}


- (IBAction)actionListeners:(id)sender {
    
    if (sender == _btnGas) {
        _btnGas.selected = !_btnGas.selected;
        if (_btnGas.selected) {
            [_dictAminities setValue:@YES forKey:@"Piped_Gas"];
            _imgGas.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            _imgGas.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Piped_Gas"];
        }
    }
    else if (sender == _btnLift) {
        _btnLift.selected = !_btnLift.selected;
        if (_btnLift.selected) {
            _imgLift.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Lift"];
        }
        else {
            _imgLift.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Lift"];
        }
    }
    else if (sender == _btnParking) {
        _btnParking.selected = !_btnParking.selected;
        if (_btnParking.selected) {
            _imgParking.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Podium_Parking"];
        }
        else {
            _imgParking.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Podium_Parking"];
        }
    }
    else if (sender == _btnGym) {
        _btnGym.selected = ! _btnGym.selected;
        if (_btnGym.selected) {
            _imgGym.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Gymnasium"];
        }
        else {
            _imgGym.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Gymnasium"];
        }
    }
    else if (sender == _btnSecurity) {
        _btnSecurity.selected = !_btnSecurity.selected;
        if (_btnSecurity.selected) {
            _imgSecurity.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"24*7_Security"];
        }
        else {
            _imgSecurity.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"24*7_Security"];
        }
    }
    else if (sender == _btnWardrobe) {
        _btnWardrobe.selected = !_btnWardrobe.selected;
        if (_btnWardrobe.selected) {
            _imgWardrobe.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Treated_Water_Supply"];
        }
        else {
            _imgWardrobe.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Treated_Water_Supply"];
        }
    }
    else if (sender == _btnWifi) {
        _btnWifi.selected = !_btnWifi.selected;
        if (_btnWifi.selected) {
            _imgWifi.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Central_Wi-fi"];
        }
        else {
            _imgWifi.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Central_Wi-fi"];
        }
    }
    else if (sender == _btnFurnished) {
        _btnFurnished.selected = !_btnFurnished.selected;
        if (_btnFurnished.selected) {
            _imgFurnished.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Party_Hall"];
        }
        else {
            _imgFurnished.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Party_Hall"];
        }
        
    }
    else if (sender == _btnAC) {
        _btnAC.selected = !_btnAC.selected;
        if (_btnAC.selected) {
            _imgAC.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"AC_in_Lobby"];
        }
        else {
            _imgAC.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"AC_in_Lobby"];
        }
    }
    else if (sender == _btnSwimmingPool) {
        _btnSwimmingPool.selected = !_btnSwimmingPool.selected;
        if (_btnSwimmingPool.selected) {
            _imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Swimming"];
        }
        else {
            _imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Swimming"];
        }
    }
    else if (sender == _btnHomeAutomation) {
        _btnHomeAutomation.selected = ! _btnHomeAutomation.selected;
        if (_btnHomeAutomation.selected) {
            _imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Home_Automation"];
        }
        else {
            _imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Home_Automation"];
        }
    }
    else if (sender == _btnPowerBackup) {
        _btnPowerBackup.selected = !_btnPowerBackup.selected;
        if (_btnPowerBackup.selected) {
            _imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_green"];
            [_dictAminities setValue:@YES forKey:@"Power_Backup"];
        }
        else {
            _imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_gray"];
            [_dictAminities setValue:@NO forKey:@"Power_Backup"];
        }
    }
    
//    NSArray *arr = [_dictAminities allKeys];
//    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    for (NSString *keys in arr) {
//        
//        if ([[_dictAminities objectForKey:keys] boolValue]) {
//            [dict setObject:[_dictAminities objectForKey:keys] forKey:keys];
//        }
//    }
//    
//    [_dictAminities removeAllObjects];
//    _dictAminities = [dict mutableCopy];
    
    NSLog(@"Aminities %@", _dictAminities);
}

@end
