//
//  PropertyCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 10/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PropertyCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
