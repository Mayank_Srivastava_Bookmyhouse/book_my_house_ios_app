//
//  FilterVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/30/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterCell.h"
#import "SelectBuilderVC.h"
typedef NS_ENUM(NSUInteger, FilterAccessOptions) {
    FilterAccessOptionsSectorDetail,
    FilterAccessOptionsFeaturedProjects,
    FilterAccessOptionsMisc,
};

@interface FilterVC : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (assign, nonatomic) FilterAccessOptions options;
@property (assign,nonatomic) BOOL isCommercial;
@property (assign,nonatomic) BOOL isResidential;
@end
