//
//  FilterVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/30/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "FilterVC.h"
#import "ProjectListVC.h"

@interface FilterVC () {
    
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UIButton *btnReset;
    __weak IBOutlet UIButton *btnApplyFilters;
    __weak IBOutlet UITableView *tblView;
    
    
    PriceRange *cellPriceRange, *cellBuiltUp, *cellPricePSF;
//    BOOL isCommercial;
    NSString *strBuilderList;
    
    FilterCell *cellFilter;
    SortedBy *cellSortedBy;
    Discount_Offers *cellDiscount_Offers;
    PossessionIn *cellPossessionIn;
    SelectBuilder *cellSelectBuilder;
    SelectPropertyType *cellSelectPropertyType;
    BHK *cellBHK;
    Aminities *cellAminities;
    BOOL isBuilderSelected;
    NSUserDefaults *pref;
    NSDictionary *dictMultiFilter;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation FilterVC



#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)loadView {
    
    pref = [NSUserDefaults standardUserDefaults];
    strBuilderList = @"All";
    [super loadView];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSelectedBuilder:) name:kSelectBuilder object:nil];
}

- (void)viewDidLoad {
    
    dictMultiFilter = [pref objectForKey:strSaveMultiFilter];
    [super viewDidLoad];
    [self screensettings];
}


- (void)dealloc {
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:kSelectBuilder object:nil];
}




#pragma mark - Screen Settings
- (void)screensettings {
    [self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnCancel) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender == btnReset) {

        btnReset.selected = ! btnReset.selected;
        isBuilderSelected = NO;
        [tblView reloadData];
        [tblView setContentOffset:CGPointZero animated:YES];
        [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(manageResetButton) userInfo:nil repeats:NO];
        
        UINavigationController *nav = (UINavigationController *)self.presentingViewController;
        for (UIViewController *controller in nav.viewControllers) {
            
            if ([controller isKindOfClass:[ProjectListVC class]]) {
                ProjectListVC *obj = (ProjectListVC *)controller;
                [obj callingAPI];
                [pref removeObjectForKey:strSaveMultiFilter];
                break;
            }
            
        }
        
    }
    else if (sender == btnApplyFilters) {
        
        
        NSMutableDictionary *dict = [[pref objectForKey:strSaveMultiFilter] mutableCopy];
        if (!dict) {
            dict = [NSMutableDictionary dictionary];
        }
        
        if (cellFilter && cellFilter.strFilter.length && ![cellFilter.strFilter isKindOfClass:[NSNull class]]) {
            [dict setObject:[cellFilter.strFilter copy] forKey:@"project_status"];
        }
        else {
            if ([dict objectForKey:@"project_status"]) {
                [dict removeObjectForKey:@"project_status"];
            }
            else {
                
            }
            
        }
                
        if (cellSortedBy && cellSortedBy.strSotedBy.length && ![cellSortedBy.strSotedBy isKindOfClass:[NSNull class]]) {
            [dict setObject:[[cellSortedBy.strSotedBy copy] lowercaseString] forKey:[[cellSortedBy.strSotedBy copy] lowercaseString]];
        }
        else {
            
            if ([dict objectForKey:[[cellSortedBy.strSotedBy copy] lowercaseString]]) {
                [dict removeObjectForKey:[[cellSortedBy.strSotedBy copy] lowercaseString]];
            }
            else {
                
            }
        }
        
        if (cellDiscount_Offers && cellDiscount_Offers.needsDiscountAndOffers) {
            [dict setObject:[NSNumber numberWithBool:cellDiscount_Offers.needsDiscountAndOffers] forKey:@"discout_offers"];
        }
        else {
            if ([dict objectForKey:@"discout_offers"]) {
                [dict removeObjectForKey:@"discout_offers"];
            }
        }
        
        if (cellPossessionIn && cellPossessionIn.strPossessionIn.length && ![cellPossessionIn.strPossessionIn isKindOfClass:[NSNull class]]) {
            
            NSString *str = @"";
            
            if ([cellPossessionIn.strPossessionIn isEqualToString:@"6 m"]) {
                str = @"6m";
            }
            else if ([cellPossessionIn.strPossessionIn isEqualToString:@"1 Yr"]) {
                str = @"1y";
            }
            else if ([cellPossessionIn.strPossessionIn isEqualToString:@"2 Yr"]) {
                str = @"2y";
            }
            else if ([cellPossessionIn.strPossessionIn isEqualToString:@"3 Yr"]) {
                str = @"3y";
            }
            else if ([cellPossessionIn.strPossessionIn isEqualToString:@"4 Yr"]) {
                str = @"4y";
            }
            else if ([cellPossessionIn.strPossessionIn isEqualToString:@"5+ Yr"]) {
                str = @"5y";
            }
            [dict setObject:str forKey:@"posession"];
        }
        else {
            if ([dict objectForKey:@"posession"]) {
                [dict removeObjectForKey:@"posession"];
            }
        }
        
        if (cellSelectBuilder && cellSelectBuilder.lblAll.text.length && ![cellSelectBuilder.lblAll.text isKindOfClass:[NSNull class]] && isBuilderSelected) {
            
            
            [dict setObject:[cellSelectBuilder.lblAll.text copy] forKey:@"builders"];
        }
        else {
            if ([dict objectForKey:@"builders"]) {
                [dict removeObjectForKey:@"builders"];
            }
            else {
                
            }
        }
        
//        if (cellSelectPropertyType && ![cellSelectPropertyType.dictPropertyType isKindOfClass:[NSNull class]] && [[cellSelectPropertyType.dictPropertyType objectForKey:@"options"] count]) {
//            
//            NSString *str = [[cellSelectPropertyType.dictPropertyType objectForKey:@"options"] componentsJoinedByString:@","];
//            
//            if (str != nil) {
//                [dict setObject:str forKey:@"projectType"];
//            }
//        }
//        else {
////            if ([dict objectForKey:@"projectType"]) {
////                [dict removeObjectForKey:@"projectType"];
////            }
//        }
        
        
//        if (cellSelectPropertyType) {
//            
//            NSString *str = [[cellSelectPropertyType.dictPropertyType objectForKey:@"options"] componentsJoinedByString:@","];
//            
//            if (str != nil) {
//                [dict setObject:str forKey:@"projectType"];
//            }
//        }
//        else {
//            //            if ([dict objectForKey:@"projectType"]) {
//            //                [dict removeObjectForKey:@"projectType"];
//            //            }
//        }
        
        if (cellBHK && cellBHK.strBhk.length) {
            [dict setObject:[cellBHK.strBhk copy] forKey:@"bhk"];
        }
        else {
            if ([dict objectForKey:@"bhk"]) {
                [dict removeObjectForKey:@"bhk"];
            }
        }
        
        
        if (cellPriceRange && !(cellPriceRange.slider.leftValue == cellPriceRange.slider.minimumValue && cellPriceRange.slider.rightValue == cellPriceRange.slider.maximumValue)) {
            [dict setObject:[cellPriceRange.strUpprerLimit copy] forKey:@"max-p"];
            [dict setObject:[cellPriceRange.strLowerLimit copy] forKey:@"min-p"];
        }
        else {
            if ([dict objectForKey:@"max-p"] && [dict objectForKey:@"min-p"]) {
                [dict removeObjectForKey:@"max-p"];
                [dict removeObjectForKey:@"min-p"];
            }
        }
        
        if (cellBuiltUp && !(cellBuiltUp.slider.leftValue == cellBuiltUp.slider.minimumValue && cellBuiltUp.slider.rightValue == cellBuiltUp.slider.maximumValue)) {
            
            [dict setObject:[cellBuiltUp.strUpprerLimit copy] forKey:@"max_area_range"];
            [dict setObject:[cellBuiltUp.strLowerLimit copy] forKey:@"min_area_range"];
        }
        else {
            if ([dict objectForKey:@"max_area_range"] && [dict objectForKey:@"min_area_range"]) {
                [dict removeObjectForKey:@"max_area_range"];
                [dict removeObjectForKey:@"min_area_range"];
            }
            else {
                
            }
        }
        
        if (cellPricePSF && !(cellPricePSF.slider.leftValue == cellPricePSF.slider.minimumValue && cellPricePSF.slider.rightValue == cellPricePSF.slider.maximumValue)) {
            
            [dict setObject:[cellPricePSF.strUpprerLimit copy] forKey:@"max_psf"];
            [dict setObject:[cellPricePSF.strLowerLimit copy] forKey:@"min_psf"];
        }
        else {
            if ([dict objectForKey:@"min_psf"] &&  [dict objectForKey:@"max_psf"]) {
                [dict removeObjectForKey:@"min_psf"];
                [dict removeObjectForKey:@"max_psf"];
            }
            else {
                
            }
        }
        
        
        if (cellAminities && ![cellAminities.dictAminities isKindOfClass:[NSNull class]] && cellAminities.dictAminities.count) {
            
            NSMutableArray *arr = [NSMutableArray array];
            for (NSString *keys in [cellAminities.dictAminities allKeys]) {
                
                if ([[cellAminities.dictAminities objectForKey:keys] boolValue]) {
                    [arr addObject:keys];
                }
            }
            NSString *str = [arr componentsJoinedByString:@","];
            if (str.length) {
                [dict setObject:str forKey:@"lifestyles"];
            }
            else {
                [dict removeObjectForKey:@"lifestyles"];
            }
            
        }
        else {
            if ([dict objectForKey:@"lifestyles"]) {
                [dict removeObjectForKey:@"lifestyles"];
            }

        }
                
        if (_options == FilterAccessOptionsSectorDetail) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kSectorDetail object:dict];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kProjectListMultifilter object:dict];
        }

        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)manageResetButton {
    btnReset.selected = NO;
}


- (void)setPropertyType:(UIButton *)btn {
    
    if (btn.tag == 1) {
        _isCommercial = NO;
    }
    else if (btn.tag == 2) {
        _isCommercial = YES;
        
        BHK *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
        
        for (UIView *temp in cell.contentView.subviews) {
            
            if ([temp isKindOfClass:[UIView class]]) {
                
                for (UIView *temp1 in temp.subviews) {
                    
                    for (UIView *temp2 in temp1.subviews) {
                        
                        if ([temp2 isKindOfClass:[UIImageView class]]) {
                            UIImageView *imgView = (UIImageView *)temp2;
                            imgView.image = [UIImage imageNamed:@"Checkbox_gray"];
                        }
                        else if ([temp2 isKindOfClass:[UIButton class]]) {
                            UIButton *btn = (UIButton *)temp2;
                            if (btn.selected) {
                                btn.selected = !btn.selected;
                            }
                        }
                    }  
                }
            }
        }
        
        [cell.dictBHK setValue:@NO forKey:@"1bhk"];
        [cell.dictBHK setValue:@NO forKey:@"2bhk"];
        [cell.dictBHK setValue:@NO forKey:@"3bhk"];
        [cell.dictBHK setValue:@NO forKey:@"4Bbhk"];
        [cell.dictBHK setValue:@NO forKey:@"5bhk"];
        [cell.dictBHK setValue:@NO forKey:@"5plusbhk"];
    }
    
    [tblView reloadData];
}

#pragma mark - API Actions
- (void)priceRangeDidChange:(MARKRangeSlider *)slider {

    int num = (int)slider.leftValue;
    if (num < 100) {
        cellPriceRange.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dL", num - (num % 25)];
        cellPriceRange.strLowerLimit = [NSString stringWithFormat:@"%d", (num - (num % 25)) * 100000];
    }
    else if (num >= 100) {
        num = num / 100;        
        cellPriceRange.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", num];
        cellPriceRange.strLowerLimit = [NSString stringWithFormat:@"%d", (num) * 10000000];
    }
    
    num = (int)slider.rightValue;
    if (num < 100) {
        cellPriceRange.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dL", num - (num % 25)];
        cellPriceRange.strUpprerLimit = [NSString stringWithFormat:@"%d", (num - (num % 25)) * 100000];
    }
    else if (num >= 100) {
        num = num / 100;
        cellPriceRange.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", num];
        cellPriceRange.strUpprerLimit = [NSString stringWithFormat:@"%d", num * 10000000];
    }
    
}

- (void)builtUpAreaDidChange:(MARKRangeSlider *)slider {
    NSLog(@"%s", __FUNCTION__);
    
    int num = (int)slider.leftValue;
    
    if (num >= 250 && num < 500) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"250 sqft"];
        cellBuiltUp.strLowerLimit = @"250";
    }
    else if (num >= 500 && num < 750) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"500 sqft"];
        cellBuiltUp.strLowerLimit = @"500";
    }
    else if (num >= 750 && num < 1000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"750 sqft"];
        cellBuiltUp.strLowerLimit = @"750";
    }
    else if (num >= 1000 && num < 2000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"1000 sqft"];
        cellBuiltUp.strLowerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"2000 sqft"];
        cellBuiltUp.strLowerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"3000 sqft"];
        cellBuiltUp.strLowerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"4000 sqft"];
        cellBuiltUp.strLowerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"6000 sqft"];
        cellBuiltUp.strLowerLimit = @"6000";
    }
    else if (num >= 6000 && num < 7000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"7000 sqft"];
        cellBuiltUp.strLowerLimit = @"7000";
    }
    else if (num >= 7000 && num < 8000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"8000 sqft"];
        cellBuiltUp.strLowerLimit = @"8000";
    }else if (num == 8000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"9000 sqft"];
        cellBuiltUp.strLowerLimit = @"9000";
    }
    
    num = (int)slider.rightValue;
    if (num >= 250 && num < 500) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"250 sqft"];
        cellBuiltUp.strUpprerLimit = @"250";
    }
    else if (num >= 500 && num < 750) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"500 sqft"];
        cellBuiltUp.strUpprerLimit = @"500";
    }
    else if (num >= 750 && num < 1000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"750 sqft"];
        cellBuiltUp.strUpprerLimit = @"750";
    }
    else if (num >= 1000 && num < 2000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"1000 sqft"];
        cellBuiltUp.strUpprerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"2000 sqft"];
        cellBuiltUp.strUpprerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"3000 sqft"];
        cellBuiltUp.strUpprerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"4000 sqft"];
        cellBuiltUp.strUpprerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"6000 sqft"];
        cellBuiltUp.strUpprerLimit = @"6000";
    }
    else if (num >= 6000 && num < 7000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"7000 sqft"];
        cellBuiltUp.strUpprerLimit = @"7000";
    }
    else if (num >= 7000 && num < 8000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"8000 sqft"];
        cellBuiltUp.strUpprerLimit = @"8000";
    }else if (num == 8000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"9000 sqft"];
        cellBuiltUp.strUpprerLimit = @"9000";
    }
}

- (void)pricePSFDidChange:(MARKRangeSlider *)slider {
    NSLog(@"%s", __FUNCTION__);
    int num = (int)slider.leftValue;
    if (num >= 500 && num < 1000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 500"];
        cellPricePSF.strLowerLimit = @"500";
    }
    else if (num >= 1000 && num < 2000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 1000"];
        cellPricePSF.strLowerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 2000"];
        cellPricePSF.strLowerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 3000"];
        cellPricePSF.strLowerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 4000"];
        cellPricePSF.strLowerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 5000"];
        cellPricePSF.strLowerLimit = @"5000";
    }
    else if (num >= 6000 && num < 7000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 6000"];
        cellPricePSF.strLowerLimit = @"6000";
    }
    else if (num >= 7000 && num < 8000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 7000"];
        cellPricePSF.strLowerLimit = @"7000";
    }
    else if (num >= 8000 && num < 9000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 8000"];
        cellPricePSF.strLowerLimit = @"8000";
    }
    else if (num >= 9000 && num < 10000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 9000"];
        cellPricePSF.strLowerLimit = @"9000";
    }
    else if (num >= 10000 && num < 15000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 10000"];
        cellPricePSF.strLowerLimit = @"10000";
    }
    else if (num >= 15000 && num < 20000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 15000"];
        cellPricePSF.strLowerLimit = @"15000";
    }
    else {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
        cellPricePSF.strLowerLimit = @"20000";
    }
    
    
    num = (int)slider.rightValue;
    
    if (num >= 500 && num < 1000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 500"];
        cellPricePSF.strUpprerLimit = @"500";
    }
    else if (num >= 1000 && num < 2000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 1000"];
        cellPricePSF.strUpprerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 2000"];
        cellPricePSF.strUpprerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 3000"];
        cellPricePSF.strUpprerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 4000"];
        cellPricePSF.strUpprerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 5000"];
        cellPricePSF.strUpprerLimit = @"5000";
    }
    else if (num >= 6000 && num < 7000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 6000"];
        cellPricePSF.strUpprerLimit = @"6000";
    }
    else if (num >= 7000 && num < 8000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 7000"];
        cellPricePSF.strUpprerLimit = @"7000";
    }
    else if (num >= 8000 && num < 9000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 8000"];
        cellPricePSF.strUpprerLimit = @"8000";
    }
    else if (num >= 9000 && num < 10000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 9000"];
        cellPricePSF.strUpprerLimit = @"9000";
    }
    else if (num >= 10000 && num < 15000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 10000"];
        cellPricePSF.strUpprerLimit = @"10000";
    }
    else if (num >= 15000 && num < 20000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 15000"];
        cellPricePSF.strUpprerLimit = @"15000";
    }
    else {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
        cellPricePSF.strUpprerLimit = @"20000";
    }
    
}



#pragma mark - Table View delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 11;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 112.0;
    }
    else if (indexPath.row == 1) {
        return 176.0;
    }
    else if (indexPath.row == 2) {
        
        //return 94.0;
        return 0.0;
    }
    else if (indexPath.row == 3) {
        return 98.0;
    }
    else if (indexPath.row == 4) {
        return 94.0;
    }
    else if (indexPath.row == 5) {
        return 229;
    }
    else if (indexPath.row == 6) {
        
        if (_isCommercial) {
            return 0.0;
        }
        else {
            return 176.0;
        }
        
    }
    else if (indexPath.row >= 7 && indexPath.row <= 9) {
        return 124.0;
    }
    else if (indexPath.row == 10) {
        return 298.0;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
    
        FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
        if (btnReset.selected) {
            cell = nil;
            cell = [FilterCell createCell];
        }
        else {
            if (cell == nil) {
                cell = [FilterCell createCell];
                [self setStatusWithFilter:cell];
            }
        }
        
        cellFilter = cell;
        return cell;
    }
    else if (indexPath.row == 1) {
        
        SortedBy *cell  = [tableView dequeueReusableCellWithIdentifier:@"SortedBy"];
        if (btnReset.selected) {
            cell = nil;
            cell = [SortedBy createCell];
        }
        else {
            if (cell == nil) {
                cell = [SortedBy createCell];
                [self setSorterByFilter:cell];
            }
        }
        
        cellSortedBy = cell;
        return cell;
    }
    else if (indexPath.row == 2) {
        
        Discount_Offers *cell = [tableView dequeueReusableCellWithIdentifier:@"Discount_Offers"];
        if (btnReset.selected) {
            cell = nil;
            cell = [Discount_Offers createCell];
        }
        else {
            if (cell == nil) {
                cell = [Discount_Offers createCell];
            }
        }
        cellDiscount_Offers = cell;
        return cell;
    }
    else if (indexPath.row == 3) {
        
        PossessionIn *cell = [tableView dequeueReusableCellWithIdentifier:@"PossessionIn"];
        if (btnReset.selected) {
            cell = nil;
            cell = [PossessionIn createCell];
            
            //[self setSorterByPossessionIn:cell];
        }
        else {
            if (cell == nil) {
                cell = [PossessionIn createCell];
                [self setSorterByPossessionIn:cell];
            }
        }
        
        cellPossessionIn = cell;
        return cell;
    }
    else if (indexPath.row == 4) {
        SelectBuilder *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectBuilder"];
        if (btnReset.selected) {
            cell = nil;
            cell = [SelectBuilder createCell];
            cell.lblAll.text = strBuilderList = @"All";
//            if ([dictMultiFilter objectForKey:@"builders"]) {
//                cell.lblAll.text = strBuilderList = [dictMultiFilter objectForKey:@"builders"];
//            }
        }
        else {
            if (cell == nil) {
                cell = [SelectBuilder createCell];
                if ([dictMultiFilter objectForKey:@"builders"]) {
                    cell.lblAll.text = strBuilderList = [dictMultiFilter objectForKey:@"builders"];
                }
            }
            else
            cell.lblAll.text = strBuilderList;
        }
        
        cellSelectBuilder = cell;
        return cell;
    }
    else if (indexPath.row == 5) {
        SelectPropertyType *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectPropertyType"];
        
        if (btnReset.selected) {
            cell = nil;
            cell = [SelectPropertyType createCell];
//            cell.arrResident = [NSArray arrayWithObjects:@"Flat", @"Builder Floor", @"Plot", @"House Villa", nil];
//            cell.arrResidentUnselected = [NSArray arrayWithObjects:@"Flat_Green", @"Builder_Floor_Green", @"Plot_Green", @"House_Villa_Green", nil];
//            cell.arrResidentSelected = [NSArray arrayWithObjects:@"Flat_Gold", @"Builder_Floor_Gold", @"Plot_Gold", @"House_Villa_Gold", nil];
//            
//            
//            cell.arrCommercial = [NSArray arrayWithObjects:@"Shops", @"Office",@"Service Appartment", nil];
//            cell.arrCommercialUnSelected = [NSArray arrayWithObjects:@"Shops_Green", @"Office_Green",@"Service_Appartment_Green", nil];
//            cell.arrCommercialSelected = [NSArray arrayWithObjects:@"Shops_Gold", @"Office_Gold", @"Service_Appartment_Gold", nil];
            
            [cell.btnCommercial addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnResidential addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
            
            
            if (_isCommercial && _isResidential) {
                cell.btnCommercial.enabled = YES;
                cell.btnResidential.enabled = YES;
            }
            
            if (!_isCommercial && _isResidential) {
                cell.btnCommercial.enabled = NO;
                cell.btnResidential.enabled = YES;
                [cell.btnResidential sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            
            if (_isCommercial && !_isResidential) {
                cell.btnCommercial.enabled = YES;
                cell.btnResidential.enabled = NO;
                [cell.btnCommercial sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            
            
//            cell.arrOptions = [NSMutableArray array];
            [cell.colView reloadData];
        }
        else {
            if (cell == nil) {
                cell = [SelectPropertyType createCell];
//                cell.arrSelectedOption = [[dictMultiFilter objectForKey:@"projectType"] componentsSeparatedByString:@","];
//                cell.arrResident = [NSArray arrayWithObjects:@"Flat", @"Builder Floor", @"Plot", @"House Villa", nil];
//                cell.arrResidentUnselected = [NSArray arrayWithObjects:@"Flat_Green", @"Builder_Floor_Green", @"Plot_Green", @"House_Villa_Green", nil];
//                cell.arrResidentSelected = [NSArray arrayWithObjects:@"Flat_Gold", @"Builder_Floor_Gold", @"Plot_Gold", @"House_Villa_Gold", nil];
//                
//                
//                cell.arrCommercial = [NSArray arrayWithObjects:@"Shops", @"Office",@"Service Appartment", nil];
//                cell.arrCommercialUnSelected = [NSArray arrayWithObjects:@"Shops_Green", @"Office_Green",@"Service_Appartment_Green", nil];
//                cell.arrCommercialSelected = [NSArray arrayWithObjects:@"Shops_Gold", @"Office_Gold", @"Service_Appartment_Gold", nil];
//                
                if (_isCommercial && _isResidential) {
                    cell.btnCommercial.enabled = YES;
                    cell.btnResidential.enabled = YES;
                    [cell.btnCommercial addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnResidential addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                if (!_isCommercial && _isResidential) {
                    cell.btnCommercial.enabled = NO;
                    cell.btnResidential.enabled = YES;
                    [cell.btnCommercial addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnResidential addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnResidential sendActionsForControlEvents:UIControlEventTouchUpInside];
                }
                
                if (_isCommercial && !_isResidential) {
                    cell.btnCommercial.enabled = YES;
                    cell.btnResidential.enabled = NO;
                    [cell.btnCommercial addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnResidential addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnCommercial sendActionsForControlEvents:UIControlEventTouchUpInside];
                }
                
                if ([dictMultiFilter objectForKey:@"projectType"]) {
//                    cell.arrOptions = [[[dictMultiFilter objectForKey:@"projectType"] componentsSeparatedByString:@","] mutableCopy];
                }
                else {
//                    cell.arrOptions = [NSMutableArray array];
                }
                
                [cell.colView reloadData];
            }
        }
        
        
        cellSelectPropertyType = cell;
        return cell;
    }
    else if (indexPath.row == 6) {
        BHK *cell = [tableView dequeueReusableCellWithIdentifier:@"BHK"];
        
        if (btnReset.selected) {
            cell = nil;
            cell = [BHK createCell];
            //[self setSortByBHK:cell];
        }
        else {
            if (cell == nil) {
                cell = [BHK createCell];
                [self setSortByBHK:cell];
            }
        }
        cellBHK = cell;
        return cell;
    }
    else if (indexPath.row >= 7 && indexPath.row <= 9) {
        PriceRange *cell = [tableView dequeueReusableCellWithIdentifier:@"PriceRange"];
        
        if (btnReset.selected) {
            cell = nil;
            cell = [PriceRange createCell];
           // [self setSortPriceRange:cell withIndexPath:indexPath];
            
            if (indexPath.row == 7) {
                cell.lblTitle.text = @"Price Range";
                cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 25L"];
                cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 5Cr"];
                [cell.slider setMinValue:25.0 maxValue:500.0];
                [cell.slider setLeftValue:25.0 rightValue:500.0];
                [cell.slider addTarget:self action:@selector(priceRangeDidChange:) forControlEvents:UIControlEventValueChanged];
                cellPriceRange = cell;
            }
            else if (indexPath.row == 8) {
                cell.lblTitle.text = @"Built Up Area (in sq. ft.)";
                cell.lblLowerBound.text = [NSString stringWithFormat:@"250 sqft"];
                cell.lblUpperBound.text = [NSString stringWithFormat:@"8000 sqft"];
                [cell.slider setMinValue:250.0 maxValue:8000.0];
                [cell.slider setLeftValue:250.0 rightValue:8000.0];
                [cell.slider addTarget:self action:@selector(builtUpAreaDidChange:) forControlEvents:UIControlEventValueChanged];
                cellBuiltUp = cell;
            }
            else if (indexPath.row == 9) {
                cell.lblTitle.text = @"Price (psf)";
                cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 500"];
                cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
                [cell.slider setMinValue:500.0 maxValue:20000.0];
                [cell.slider setLeftValue:500.0 rightValue:20000.0];
                [cell.slider addTarget:self action:@selector(pricePSFDidChange:) forControlEvents:UIControlEventValueChanged];
                cellPricePSF = cell;
            }
        }
        else {
            if (cell == nil) {
                cell = [PriceRange createCell];
                [self setSortPriceRange:cell withIndexPath:indexPath];
            }
        }
        return cell;
    }
    else if (indexPath.row == 10) {
        Aminities *cell = [tableView dequeueReusableCellWithIdentifier:@"Aminities"];
        if (btnReset.selected) {
            cell = nil;
            cell = [Aminities createCell];
            //[self setSortByAminities:cell];
            
        }
        else {
            if (cell == nil) {
                cell = [Aminities createCell];
                
                [self setSortByAminities:cell];
            }
        }
        cellAminities = cell;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 4) {
        isBuilderSelected = YES;
        SelectBuilderVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectBuilderVC"];
        [self presentViewController:obj animated:YES completion:nil];
        
    }
    [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] animated:YES];
}
#pragma mark - Auxillary Mathods
- (void)getSelectedBuilder:(NSNotification *)info {
    
    NSMutableArray *arr = [NSMutableArray arrayWithArray:info.object];
    strBuilderList = [[NSMutableArray arrayWithArray:[arr valueForKeyPath:@"@distinctUnionOfObjects.name"]] componentsJoinedByString:@", "];
    [tblView reloadData];
}

- (void)setStatusWithFilter:(FilterCell *)cell {
    
    if ([dictMultiFilter objectForKey:@"project_status"]) {
        
        NSString *str = [dictMultiFilter objectForKey:@"project_status"];
        NSArray *arr = [str componentsSeparatedByString:@","];
        
        for (NSString *strTemp in arr) {
            if ([strTemp isEqualToString:@"New Launch"]) {
                
                if (!cell.btnNewLaunch.selected) {
                    cell.btnNewLaunch.selected = YES;
                    [cell setButtonSelected:cell.btnNewLaunch];
                    [cell.dictStatus setObject:@YES forKey:@"New Launch"];
                }
            }
            else if ([strTemp isEqualToString:@"Ready To Move"]) {
                
                if (!cell.btnReadyToMoveIn.selected) {
                    cell.btnReadyToMoveIn.selected = YES;
                    [cell setButtonSelected:cell.btnReadyToMoveIn];
                    [cell.dictStatus setObject:@YES forKey:@"Ready To Move"];
                }
                
            }
            else {
                if (!cell.btnUnderConstruction.selected) {
                    cell.btnUnderConstruction.selected = YES;
                    [cell setButtonSelected:cell.btnUnderConstruction];
                    [cell.dictStatus setObject:@YES forKey:@"Under Construction"];
                }
            }
            
        }
    }
}

- (void)setSorterByFilter:(SortedBy *)cell {
    
    if ([dictMultiFilter objectForKey:@"newest"] || [dictMultiFilter objectForKey:@"pricehtol"] || [dictMultiFilter objectForKey:@"priceltoh"] || [dictMultiFilter objectForKey:@"psfhtol"] || [dictMultiFilter objectForKey:@"psfltoh"] || [dictMultiFilter objectForKey:@"possessionltoh"] || [dictMultiFilter objectForKey:@"possessionhtol"] || [dictMultiFilter objectForKey:@"infra"] || [dictMultiFilter objectForKey:@"needs"] || [dictMultiFilter objectForKey:@"lifestyle"] || [dictMultiFilter objectForKey:@"returnsval"] || [dictMultiFilter objectForKey:@"rating"]) {
        
        [cell.btnPrice setTitle:@"Price" forState:UIControlStateNormal];
        [cell.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
        [cell.btnPricePSF setTitle:@"Price PSF" forState:UIControlStateNormal];
        
        if ([dictMultiFilter objectForKey:@"newest"]) {
            
            
            
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
        }
        if ([dictMultiFilter objectForKey:@"priceltoh"]) {
            [cell.btnPrice setTitle:@"Low to high" forState:UIControlStateNormal];
            cell.tblPrice.hidden = NO;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
        }
        
        if ([dictMultiFilter objectForKey:@"pricehtol"]) {
            
            [cell.btnPrice setTitle:@"High to low" forState:UIControlStateNormal];
            cell.tblPrice.hidden = NO;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            [cell setButtonSelected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
            
        }
        
        if ([dictMultiFilter objectForKey:@"psfhtol"]) {
            
            [cell.btnPricePSF setTitle:@"High to low" forState:UIControlStateNormal];
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = NO;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];

            
        }
        
        if ([dictMultiFilter objectForKey:@"psfltoh"]) {
            
            [cell.btnPricePSF setTitle:@"Low to high" forState:UIControlStateNormal];
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = NO;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];

        }
        
        if ([dictMultiFilter objectForKey:@"possessionltoh"]) {
            
            [cell.btnPossession setTitle:@"Earliest to latest" forState:UIControlStateNormal];
            cell.tblPricePSF.hidden = YES;
            cell.tblPrice.hidden = YES;
            cell.tblPossession.hidden = NO;
            
            [cell setButtonSelected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
        }
        
        if ([dictMultiFilter objectForKey:@"possessionhtol"]) {
            
            [cell.btnPossession setTitle:@"Latest to earliest" forState:UIControlStateNormal];
            
            cell.tblPricePSF.hidden = YES;
            cell.tblPrice.hidden = YES;
            cell.tblPossession.hidden = NO;
            
            [cell setButtonSelected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
        }
        
        
        if ([dictMultiFilter objectForKey:@"rating"]) {
            
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnRating];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
            
        }
        
        if ([dictMultiFilter objectForKey:@"infra"]) {
            
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            [cell setButtonSelected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
        }
        if ([dictMultiFilter objectForKey:@"needs"]) {
            
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnReturns];
            
        }
        
        if ([dictMultiFilter objectForKey:@"lifestyle"]) {
            
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            
            [cell setButtonSelected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnNeeds];
            [cell setButtonUnselected:cell.btnReturns];
            
        }
        
        if ([dictMultiFilter objectForKey:@"returnsval"]) {
            
            cell.tblPrice.hidden = YES;
            cell.tblPricePSF.hidden = YES;
            cell.tblPossession.hidden = YES;
            [cell setButtonSelected:cell.btnReturns];
            [cell setButtonUnselected:cell.btnRelevence];
            [cell setButtonUnselected:cell.btnPrice];
            [cell setButtonUnselected:cell.btnPricePSF];
            [cell setButtonUnselected:cell.btnNewest];
            [cell setButtonUnselected:cell.btnPossession];
            [cell setButtonUnselected:cell.btnInfra];
            [cell setButtonUnselected:cell.btnRating];
            [cell setButtonUnselected:cell.btnLifestyle];
            [cell setButtonUnselected:cell.btnNeeds];
        }
    }
}


- (void)setSorterByPossessionIn:(PossessionIn *)cell {
    
    if ([[dictMultiFilter objectForKey:@"posession"] isEqualToString:@"6m"]) {
        [cell setButtonSelected:cell.btn6m];
        [cell setButtonUnselected:cell.btn1yr];
        [cell setButtonUnselected:cell.btn2yr];
        [cell setButtonUnselected:cell.btn3yr];
        [cell setButtonUnselected:cell.btn4yr];
        [cell setButtonUnselected:cell.btn5yr];
    }
    else if ([[dictMultiFilter objectForKey:@"posession"] isEqualToString:@"1y"]) {
        [cell setButtonSelected:cell.btn1yr];
        [cell setButtonUnselected:cell.btn6m];
        [cell setButtonUnselected:cell.btn2yr];
        [cell setButtonUnselected:cell.btn3yr];
        [cell setButtonUnselected:cell.btn4yr];
        [cell setButtonUnselected:cell.btn5yr];
    }
    else if ([[dictMultiFilter objectForKey:@"posession"] isEqualToString:@"2y"]) {
        [cell setButtonSelected:cell.btn2yr];
        [cell setButtonUnselected:cell.btn1yr];
        [cell setButtonUnselected:cell.btn6m];
        [cell setButtonUnselected:cell.btn3yr];
        [cell setButtonUnselected:cell.btn4yr];
        [cell setButtonUnselected:cell.btn5yr];
    }
    else if ([[dictMultiFilter objectForKey:@"posession"] isEqualToString:@"3y"]) {
        [cell setButtonSelected:cell.btn3yr];
        [cell setButtonUnselected:cell.btn1yr];
        [cell setButtonUnselected:cell.btn2yr];
        [cell setButtonUnselected:cell.btn6m];
        [cell setButtonUnselected:cell.btn4yr];
        [cell setButtonUnselected:cell.btn5yr];
    }
    else if ([[dictMultiFilter objectForKey:@"posession"] isEqualToString:@"4y"]) {
        
        [cell setButtonSelected:cell.btn4yr];
        [cell setButtonUnselected:cell.btn1yr];
        [cell setButtonUnselected:cell.btn2yr];
        [cell setButtonUnselected:cell.btn3yr];
        [cell setButtonUnselected:cell.btn6m];
        [cell setButtonUnselected:cell.btn5yr];
        
    }
    else if ([[dictMultiFilter objectForKey:@"posession"] isEqualToString:@"5y"]) {
        
        [cell setButtonSelected:cell.btn5yr];
        [cell setButtonUnselected:cell.btn1yr];
        [cell setButtonUnselected:cell.btn2yr];
        [cell setButtonUnselected:cell.btn3yr];
        [cell setButtonUnselected:cell.btn4yr];
        [cell setButtonUnselected:cell.btn6m];
    }

}

- (void)setSortByBHK:(BHK *)cell {
    
    NSArray *arr = [[dictMultiFilter objectForKey:@"bhk"] componentsSeparatedByString:@","];
    
    for (NSString *strTemp in arr) {
        
        if ([strTemp isEqualToString:@"1bhk"]) {
            cell.btn1BHK.selected = YES;
            cell.img1BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [cell.dictBHK setValue:@YES forKey:@"1bhk"];
        }
        if ([strTemp isEqualToString:@"2bhk"]) {
            cell.btn2BHK.selected = YES;
            cell.img2BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [cell.dictBHK setValue:@YES forKey:@"2bhk"];
            
        }
        if ([strTemp isEqualToString:@"3bhk"]) {
            cell.btn3BHK.selected = YES;
            cell.img3BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [cell.dictBHK setValue:@YES forKey:@"3bhk"];
            
        }
        if ([strTemp isEqualToString:@"4bhk"]) {
            cell.btn4BHK.selected = YES;
            cell.img4BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [cell.dictBHK setValue:@YES forKey:@"4bhk"];
            
        }
        if ([strTemp isEqualToString:@"5bhk"]) {
            cell.btn5BHK.selected = YES;
            cell.img5BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [cell.dictBHK setValue:@YES forKey:@"5bhk"];
            
        }
        if ([strTemp isEqualToString:@"5plusbhk"]) {
            cell.btn5PlusBHK.selected = YES;
            cell.img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_green"];
            [cell.dictBHK setValue:@YES forKey:@"5plusbhk"];
            
        }
    }
}

- (void)setSortPriceRange:(PriceRange *)cell withIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 7) {
        cell.lblTitle.text = @"Price Range";
        if ([dictMultiFilter objectForKey:@"max-p"] && [dictMultiFilter objectForKey:@"min-p"]) {
            
            CGFloat num_Max = [[dictMultiFilter objectForKey:@"max-p"] floatValue];
            CGFloat num_Slide_Right_Value = num_Max / 100000.0;
            
            if (num_Slide_Right_Value < 100.0 && num_Slide_Right_Value > 25.0) {//For values in lakhs
                cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dL", (int)num_Slide_Right_Value];
                
            }
            else {
                
                cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", (int)(num_Slide_Right_Value/100)];
            }
            
            cell.strUpprerLimit = [NSString stringWithFormat:@"%@", [dictMultiFilter objectForKey:@"max-p"]];
            
            CGFloat num_Min = [[dictMultiFilter objectForKey:@"min-p"] floatValue];
            CGFloat num_Slide_Left_Value = num_Min / 100000.0;
            
            if (num_Slide_Left_Value < 100.0 && num_Slide_Left_Value > 25.0) {
                cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dL", (int)num_Slide_Left_Value];

            }
            else {
                
                cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", (int)(num_Slide_Left_Value/100)];
            }
            cell.strLowerLimit = [NSString stringWithFormat:@"%@", [dictMultiFilter objectForKey:@"min-p"]];
            
            [cell.slider setMinValue:25.0 maxValue:500.0];
            [cell.slider setLeftValue:num_Slide_Left_Value rightValue:num_Slide_Right_Value];
        }
        else {
            cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 25L"];
            cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 5Cr"];
            [cell.slider setMinValue:25.0 maxValue:500.0];
            [cell.slider setLeftValue:25.0 rightValue:500.0];
        }
        [cell.slider addTarget:self action:@selector(priceRangeDidChange:) forControlEvents:UIControlEventValueChanged];
        cellPriceRange = cell;
        
    }
    else if (indexPath.row == 8) {
        cell.lblTitle.text = @"Built Up Area (in sq. ft.)";
        if ([dictMultiFilter objectForKey:@"max_area_range"] && [dictMultiFilter objectForKey:@"min_area_range"]) {
            
            cell.lblUpperBound.text = [NSString stringWithFormat:@"%@ sqft", [dictMultiFilter objectForKey:@"max_area_range"]];
            cell.strUpprerLimit = [NSString stringWithFormat:@"%@", [dictMultiFilter objectForKey:@"max_area_range"]];
            
            cell.lblLowerBound.text = [NSString stringWithFormat:@"%@ sqft", [dictMultiFilter objectForKey:@"min_area_range"]];
            cell.strLowerLimit = [NSString stringWithFormat:@"%@", [dictMultiFilter objectForKey:@"min_area_range"]];
            
            [cell.slider setMinValue:250.0 maxValue:8000.0];
            [cell.slider setLeftValue:[[dictMultiFilter objectForKey:@"min_area_range"] floatValue] rightValue:[[dictMultiFilter objectForKey:@"max_area_range"] floatValue]];
        }
        else {
            cell.lblLowerBound.text = [NSString stringWithFormat:@"250 sqft"];
            cell.lblUpperBound.text = [NSString stringWithFormat:@"8000 sqft"];
            [cell.slider setMinValue:250.0 maxValue:8000.0];
            [cell.slider setLeftValue:250.0 rightValue:8000.0];
        }
        [cell.slider addTarget:self action:@selector(builtUpAreaDidChange:) forControlEvents:UIControlEventValueChanged];
        cellBuiltUp = cell;

    }
    else if (indexPath.row == 9) {
        
        cell.lblTitle.text = @"Price (psf)";
        if ([dictMultiFilter objectForKey:@"max_psf"] && [dictMultiFilter objectForKey:@"min_psf"]) {
            cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %@", [dictMultiFilter objectForKey:@"max_psf"]];
            cell.strUpprerLimit = [NSString stringWithFormat:@"%@", [dictMultiFilter objectForKey:@"max_psf"]];
            
            cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %@", [dictMultiFilter objectForKey:@"min_psf"]];
            cell.strLowerLimit = [NSString stringWithFormat:@"%@", [dictMultiFilter objectForKey:@"min_psf"]];
            
            [cell.slider setMinValue:500.0 maxValue:20000.0];
            [cell.slider setLeftValue:[[dictMultiFilter objectForKey:@"min_psf"] floatValue] rightValue:[[dictMultiFilter objectForKey:@"max_psf"] floatValue]];
        }
        else {
            cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 500"];
            cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
            [cell.slider setMinValue:500.0 maxValue:20000.0];
            [cell.slider setLeftValue:500.0 rightValue:20000.0];
        }
        [cell.slider addTarget:self action:@selector(pricePSFDidChange:) forControlEvents:UIControlEventValueChanged];
        cellPricePSF = cell;
    }
}



- (void)setSortByAminities:(Aminities *)cell {
    
    
    if ([dictMultiFilter objectForKey:@"lifestyles"]) {
        NSString *str = [dictMultiFilter objectForKey:@"lifestyles"];
        
        NSArray *arr = [str componentsSeparatedByString:@","];
        
        for (NSString *strTemp in arr) {
            
            if ([strTemp isEqualToString:@"AC_in_Lobby"]) {
                cell.btnAC.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"AC_in_Lobby"];
                cell.imgAC.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Party_Hall"]) {// in place of furnished
                cell.btnFurnished.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Party_Hall"];
                cell.imgFurnished.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Piped_Gas"]) {
                cell.btnGas.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Piped_Gas"];
                cell.imgGas.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Gymnasium"]) {
                cell.btnGym.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Gymnasium"];
                cell.imgGym.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Home_Automation"]) {
                cell.btnHomeAutomation.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Home_Automation"];
                cell.imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Lift"]) {
                cell.btnLift.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Lift"];
                cell.imgLift.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Podium_Parking"]) {
                cell.btnParking.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Podium_Parking"];
                cell.imgParking.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Power_Backup"]) {
                cell.btnPowerBackup.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Power_Backup"];
                cell.imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"24*7_Security"]) {
                cell.btnSecurity.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"24*7_Security"];
                cell.imgSecurity.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Swimming"]) {
                cell.btnSwimmingPool.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Swimming"];
                cell.imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Treated_Water_Supply"]) {
                cell.btnWardrobe.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Treated_Water_Supply"];
                cell.imgWardrobe.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else if ([strTemp isEqualToString:@"Central_Wi-fi"]) {
                cell.btnWifi.selected = YES;
                [cell.dictAminities setValue:@YES forKey:@"Central_Wi-fi"];
                cell.imgWifi.image = [UIImage imageNamed:@"Checkbox_green"];
            }
        }
    }
}
@end
