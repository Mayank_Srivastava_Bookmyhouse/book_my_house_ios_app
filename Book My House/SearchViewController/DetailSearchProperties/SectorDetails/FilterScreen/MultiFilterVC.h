//
//  MultiFilterVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 10/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, AccessOption) {
    AccessOptionProjectList,
    AccessOptionProjectMap,
};


typedef NS_ENUM(NSUInteger, PropertyType) {
    PropertyTypeResidentialOnly,
    PropertyTypeCommenrcialOnly,
    PropertyTypeResidentialAndCommercial,
};

@interface MultiFilterVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) NSArray *arrBuilderList;
@property (assign, nonatomic) PropertyType option;
@property (assign, nonatomic) AccessOption accessOption;
@end
