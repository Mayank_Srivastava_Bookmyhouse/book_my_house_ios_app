//
//  MultiFilterVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 10/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "MultiFilterVC.h"
#import "Header.h"
#import "FilterCell.h"
#import "ProjectListVC.h"
#import "PropertyCell.h"

@interface MultiFilterVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UIButton *btnReset;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIButton *btnApplyFilters;
    
    NSUserDefaults *pref;
    FilterCell *cellFilter;
    SortedBy *cellSortedBy;
    Discount_Offers *cellDiscount_Offers;
    PossessionIn *cellPossessionIn;
    SelectBuilder *cellSelectBuilder;
    SelectPropertyType *cellSelectPropertyType;
    BHK  *cellBHK;
    PriceRange *cellPriceRange, *cellBuiltUp, *cellPricePSF;
    Aminities *cellAminities;
    NSMutableArray *arrObservedValues;
    
    
    BOOL isNewLaunchSelected, isUnderConstructionSelected, isReadyToMoveInSelected, isNewestSelected, isPriceHighToLowSlected, isPriceLowToHighSlected,isPricePSFHighToLowSelected, isPricePSFLowToHighSelected,isRatingSelected, isPossessionLowToHighSelected, isPossessionHighToLowSelected,isInfraSelected, isNeedsSelected, isLifeStyleSelected, isReturnSelected, isPriceSelected, isPricePSFSelected, isPossessionSelected, isDiscount_OffersSelected, is6MSelected, is1YRSelected, is2YRSelected, is3YRSeleted, is4YRSelected, is5PlusYRSelected, isBuilderListSelected;
    
    BOOL isPropertyTypeSelected, isResidentialSelected, isCommercialSelected, isFlatSelected, isBuilderFloorSelected, isPlotSelected, isHouseVillaSelected, isShopSelected, isOfficeSelected, isServiceAppartmentSelected;
    
    BOOL isBHKSelected, is1BHKSelected, is2BHKSelected, is3BHKSelected, is4BHKSelected, is5BHKSelected, is5plusBHKSelected;
    
    
    BOOL isPriceRangeSelected, isBuildUpAreaSelected, isPricePSFRangeSelected;
    
    BOOL isAmenitiesSelected, isGasSelected, isLiftSelected, isParkingSelected, isGymSelected, isSecuritySelected, isWiFiSelected, isACSelected, isSwimmingPoolSelected, isHomeAutomationSelected, isPowerBackupSelected, isTreatedWaterSupplySelected, isPartyHallSelected;
    
    NSString *strBuilderList;
    
    NSArray *arrResidential, *arrCommercial, *arrResidentialIconSelected, *arrResidentialIconUnSelected, *arrCommercialIconSelected, *arrCommercialIconUnselected;
    
    CGFloat minPriceRange, maxPriceRange;
    
}
- (IBAction)actionLister:(id)sender;

@end

@implementation MultiFilterVC


#pragma mark - View controller mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)loadView {
    
    
    arrResidential = [NSArray arrayWithObjects:@"Flat", @"Builder Floor", @"Plot", @"House Villa", nil];
    arrResidentialIconUnSelected = [NSArray arrayWithObjects:@"Flat_Green", @"Builder_Floor_Green", @"Plot_Green", @"House_Villa_Green", nil];
    arrResidentialIconSelected = [NSArray arrayWithObjects:@"Flat_Gold", @"Builder_Floor_Gold", @"Plot_Gold", @"House_Villa_Gold", nil];
    
    
    arrCommercial = [NSArray arrayWithObjects:@"Shops", @"Office",@"Service Appartment", nil];
    arrCommercialIconUnselected = [NSArray arrayWithObjects:@"Shops_Green", @"Office_Green",@"Service_Appartment_Green", nil];
    arrCommercialIconSelected = [NSArray arrayWithObjects:@"Shops_Gold", @"Office_Gold", @"Service_Appartment_Gold", nil];
    
    arrObservedValues = [NSMutableArray array];
    
    pref = [NSUserDefaults standardUserDefaults];
    [super loadView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSelectedBuilder:) name:kSelectBuilder object:nil];
}


- (void)viewDidLoad {
    [self setNeedsStatusBarAppearanceUpdate];
    
    [self screenSettings];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    [pref removeObjectForKey:strSaveBuilderList];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSelectBuilder object:nil];
    
    
    
    for (id obj in arrObservedValues) {
        
        @try{
            
            if ([obj isKindOfClass:[UIButton class]]) {
                [obj removeObserver:self forKeyPath:@"selected"];
               
            }
            
        }@catch(id anException){
            //do nothing, obviously it wasn't attached because an exception was thrown
        }
        
    }
     [arrObservedValues removeAllObjects];
}




#pragma mark - Screen Settings
- (void)screenSettings {
    
    NSDictionary *dict = [pref objectForKey:strSaveMultiFilter];
    
    if (dict == nil) {
        dict = [NSMutableDictionary dictionary];
        isNewLaunchSelected = NO;
        isReadyToMoveInSelected = NO;
        isUnderConstructionSelected = NO;
        
        isNewestSelected = NO;
        isPriceSelected = NO;
        isPriceHighToLowSlected = NO;
        isPriceLowToHighSlected = NO;
        isPricePSFSelected = NO;
        isPricePSFHighToLowSelected = NO;
        isPricePSFLowToHighSelected = NO;
        isRatingSelected = NO;
        isPossessionSelected = NO;
        isPossessionLowToHighSelected = NO;
        isPossessionHighToLowSelected = NO;
        isInfraSelected = NO;
        isNeedsSelected = NO;
        isLifeStyleSelected = NO;
        isReturnSelected = NO;
        isDiscount_OffersSelected = NO;
        
        is6MSelected = NO;
        is1YRSelected = NO;
        is2YRSelected = NO;
        is3YRSeleted = NO;
        is4YRSelected = NO;
        is5PlusYRSelected = NO;
        
        isBuilderListSelected = NO;
    
        
        isPropertyTypeSelected = NO;
        isResidentialSelected = NO;
        isCommercialSelected = NO;
        isFlatSelected = NO;
        isBuilderFloorSelected = NO;
        isPlotSelected = NO;
        isHouseVillaSelected = NO;
        isShopSelected = NO;
        isOfficeSelected = NO;
        isServiceAppartmentSelected = NO;
        
        isBHKSelected = NO;
        is1BHKSelected = NO;
        is2BHKSelected = NO;
        is3BHKSelected = NO;
        is4BHKSelected = NO;
        is5BHKSelected = NO;
        is5plusBHKSelected = NO;
        
        
        
        isPriceRangeSelected = NO;
        isBuildUpAreaSelected = NO;
        isPricePSFRangeSelected = NO;
        
        
        isAmenitiesSelected = NO;
        isGasSelected = NO;
        isLiftSelected = NO;
        isParkingSelected = NO;
        isGymSelected = NO;
        isSecuritySelected = NO;
        isTreatedWaterSupplySelected = NO;
        isWiFiSelected = NO;
        isPartyHallSelected = NO;
        isACSelected = NO;
        isSwimmingPoolSelected = NO;
        isHomeAutomationSelected = NO;
        isPowerBackupSelected = NO;
        
    }
    else {
        
        if ([dict objectForKey:@"project_status"] != nil) {
            
            NSArray *arr = [[dict objectForKey:@"project_status"] componentsSeparatedByString:@","];
            
            for (NSString *strTemp in arr) {
                
                if ([strTemp isEqualToString:@"New Launch"]) {
                    isNewLaunchSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Ready To Move"]) {
                    isReadyToMoveInSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Under Construction"]) {
                    isUnderConstructionSelected = YES;
                }
            }
        }
        
        if ([dict objectForKey:@"newest"] != nil) {
            isNewestSelected = YES;
        }
        else if ([dict objectForKey:@"priceltoh"] != nil) {
            isPriceSelected = YES;
            isPriceLowToHighSlected = YES;
        }
        else if ([dict objectForKey:@"pricehtol"] != nil) {
            isPriceSelected = YES;
            isPriceHighToLowSlected = YES;
        }
        else if ([dict objectForKey:@"psfhtol"] != nil) {
            isPricePSFSelected = YES;
            isPricePSFHighToLowSelected = YES;
        }
        else if ([dict objectForKey:@"psfltoh"] != nil) {
            isPricePSFSelected = YES;
            isPricePSFLowToHighSelected = YES;
        }
        else if ([dict objectForKey:@"rating"] != nil) {
            isRatingSelected = YES;
        }
        else if ([dict objectForKey:@"possessionltoh"] != nil) {
            isPossessionSelected = YES;
            isPossessionLowToHighSelected = YES;
        }
        else if ([dict objectForKey:@"possessionhtol"] != nil) {
            isPossessionSelected = YES;
            isPossessionHighToLowSelected = YES;
        }
        else if ([dict objectForKey:@"infra"] != nil) {
            isInfraSelected = YES;
        }
        else if ([dict objectForKey:@"needs"] != nil) {
            isNeedsSelected = YES;
        }
        else if ([dict objectForKey:@"lifestyle"] != nil) {
            isLifeStyleSelected = YES;
        }
        else if ([dict objectForKey:@"returnsval"] != nil) {
            isReturnSelected = YES;
        }
        
        if ([[dict objectForKey:@"discout_offers"] boolValue]) {
            isDiscount_OffersSelected = YES;
        }
        
        
        if ([dict objectForKey:@"posession"] != nil) {
            
            NSString *str = [dict objectForKey:@"posession"];
            
            if ([str isEqualToString:@"6m"]) {
                is6MSelected = YES;
            }
            else if ([str isEqualToString:@"1y"]) {
                is1YRSelected = YES;
            }
            else if ([str isEqualToString:@"2y"]) {
                is2YRSelected = YES;
            }
            else if ([str isEqualToString:@"3y"]) {
                is3YRSeleted = YES;
            }
            else if ([str isEqualToString:@"4y"]) {
                is4YRSelected = YES;
            }
            else if ([str isEqualToString:@"5y"]) {
                is5PlusYRSelected = YES;
            }
        }
        
        if ([dict objectForKey:@"builders"] != nil) {
            isBuilderListSelected = YES;
        }
        
        if ([dict objectForKey:@"projectType"] != nil) {
            isPropertyTypeSelected = YES;
            NSArray *arr = [[dict objectForKey:@"projectType"] componentsSeparatedByString:@","];
            
            if (_option == PropertyTypeResidentialOnly) {
                
                isResidentialSelected = YES;
                isCommercialSelected = NO;
                
                for (NSString *strTemp in arr) {
                    
                    if ([strTemp isEqualToString:[arrResidential objectAtIndex:0]]) {
                        isFlatSelected = YES;
                    }
                    else if ([strTemp isEqualToString:[arrResidential objectAtIndex:1]]) {
                        isBuilderFloorSelected = YES;
                    }
                    else if ([strTemp isEqualToString:[arrResidential objectAtIndex:2]]) {
                        isPlotSelected = YES;
                    }
                    else if ([strTemp isEqualToString:[arrResidential objectAtIndex:3]]) {
                        isHouseVillaSelected = YES;
                    }
                }
                
            }
            else if (_option == PropertyTypeCommenrcialOnly) {
                
                isResidentialSelected = NO;
                isCommercialSelected = YES;
                
                for (NSString *strTemp in arr) {
                    
                    if ([strTemp isEqualToString:[arrCommercial objectAtIndex:0]]) {
                        isShopSelected = YES;
                    }
                    else if ([strTemp isEqualToString:[arrCommercial objectAtIndex:1]]) {
                        isOfficeSelected = YES;
                    }
                    else if ([strTemp isEqualToString:[arrCommercial objectAtIndex:2]]) {
                        isServiceAppartmentSelected = YES;
                    }
                }
            }
            else {
                
                if ([arrResidential containsObject:[arr firstObject]]) {
                    isResidentialSelected = YES;
                    isCommercialSelected = NO;
                    
                    for (NSString *strTemp in arr) {
                        
                        if ([strTemp isEqualToString:[arrResidential objectAtIndex:0]]) {
                            isFlatSelected = YES;
                        }
                        else if ([strTemp isEqualToString:[arrResidential objectAtIndex:1]]) {
                            isBuilderFloorSelected = YES;
                        }
                        else if ([strTemp isEqualToString:[arrResidential objectAtIndex:2]]) {
                            isPlotSelected = YES;
                        }
                        else if ([strTemp isEqualToString:[arrResidential objectAtIndex:3]]) {
                            isHouseVillaSelected = YES;
                        }
                    }
                }
                else {
                    isResidentialSelected = NO;
                    isCommercialSelected = YES;
                    
                    for (NSString *strTemp in arr) {
                        
                        if ([strTemp isEqualToString:[arrCommercial objectAtIndex:0]]) {
                            isShopSelected = YES;
                        }
                        else if ([strTemp isEqualToString:[arrCommercial objectAtIndex:1]]) {
                            isOfficeSelected = YES;
                        }
                        else if ([strTemp isEqualToString:[arrCommercial objectAtIndex:2]]) {
                            isServiceAppartmentSelected = YES;
                        }
                }
                }
            
            }
        }
        
        if ([dict objectForKey:@"bhk"] != nil) {
            isBHKSelected = YES;
            
            NSArray *arr = [[dict objectForKey:@"bhk"] componentsSeparatedByString:@","];
            
            for (NSString *strTemp in arr) {
                
                if ([strTemp isEqualToString:@"1bhk"]) {
                    is1BHKSelected = YES;
                }
                else if ([strTemp isEqualToString:@"2bhk"]) {
                    is2BHKSelected = YES;
                }
                if ([strTemp isEqualToString:@"3bhk"]) {
                    is3BHKSelected = YES;
                }
                if ([strTemp isEqualToString:@"4bhk"]) {
                    is4BHKSelected = YES;
                }
                if ([strTemp isEqualToString:@"5bhk"]) {
                    is5BHKSelected = YES;
                }
                if ([strTemp isEqualToString:@"5plusbhk"]) {
                    is5plusBHKSelected = YES;
                }
            }
        }
        else {
            isBHKSelected = NO;
        }
        
        
        
        if ([dict objectForKey:@"max-p"] != nil && [dict objectForKey:@"min-p"] != nil) {
            isPriceRangeSelected = YES;
        }
        else {
            isPriceRangeSelected = NO;
        }
        
        if ([dict objectForKey:@"max_area_range"] != nil && [dict objectForKey:@"min_area_range"] != nil) {
            isBuildUpAreaSelected = YES;
        }
        else {
            isBuildUpAreaSelected = NO;
        }
        
        if ([dict objectForKey:@"max_psf"] != nil && [dict objectForKey:@"min_psf"]) {
            isPricePSFRangeSelected = YES;
        }
        else {
            isPricePSFRangeSelected = NO;
        }
        
        
        
        if ([dict objectForKey:@"lifestyles"] != nil) {
            isAmenitiesSelected = YES;
            
            NSArray *arr = [[dict objectForKey:@"lifestyles"] componentsSeparatedByString:@","];
            
            for (NSString *strTemp in arr) {
                
                if ([strTemp isEqualToString:@"Piped_Gas"]) {
                    isGasSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Lift"]) {
                    isLiftSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Podium_Parking"]) {
                    isParkingSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Gymnasium"]) {
                    isGymSelected = YES;
                }
                else if ([strTemp isEqualToString:@"24*7_Security"]) {
                    isSecuritySelected = YES;
                }
                else if ([strTemp isEqualToString:@"Treated_Water_Supply"]) {
                    isTreatedWaterSupplySelected = YES;
                }
                else if ([strTemp isEqualToString:@"Central_Wi-fi"]) {
                    isWiFiSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Party_Hall"]) {
                    isPartyHallSelected = YES;
                }
                else if ([strTemp isEqualToString:@"AC_in_Lobby"]) {
                    isACSelected = YES;
                }
                
                else if ([strTemp isEqualToString:@"Swimming"]) {
                    isSwimmingPoolSelected = YES;
                }
                else if ([strTemp isEqualToString:@"Home_Automation"]) {
                    isHomeAutomationSelected = YES;
                }
                else {//Power_Backup
                    isPowerBackupSelected = YES;
                }
            }
        }
        else {
            isAmenitiesSelected = NO;
        }
    }
    
    tblView.delegate = self;
    tblView.dataSource = self;
}

#pragma mark - Button Actions

- (IBAction)actionLister:(id)sender {
    
    if (sender == btnBack) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender == btnReset) {
        
        [pref removeObjectForKey:strSaveMultiFilter];
        [self resetAllFlags:YES];
        
        FilterCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell = nil;
        
        [tblView reloadData];
        
        UINavigationController *nav = (UINavigationController *)self.presentingViewController;
        for (UIViewController *controller in nav.viewControllers) {
            
            
            if (_option == AccessOptionProjectList) {
                if ([controller isKindOfClass:[ProjectListVC class]] ) {
                    ProjectListVC *obj = (ProjectListVC *)controller;
                    [obj callingAPI];
                    [pref removeObjectForKey:strSaveMultiFilter];
                    break;
                }
            }
            else {
                if ([controller isKindOfClass:[ProjectListVC class]]) {
                    ProjectListVC *obj = (ProjectListVC *)controller;
                    [obj callingAPI];
                    [pref removeObjectForKey:strSaveMultiFilter];
                    break;
                }
            }
        }
        
    }
    else {
        
        NSMutableDictionary *dict = [[pref objectForKey:strSaveMultiFilter] mutableCopy];
        
        if ([[dict allKeys] count] == 0) {
             dict = [NSMutableDictionary dictionary];
        }
        
        if (isNewLaunchSelected || isReadyToMoveInSelected || isUnderConstructionSelected) {
            
            
            NSMutableArray *arr = [NSMutableArray array];
            
            if (isNewLaunchSelected) {
                [arr addObject:@"New Launch"];
            }
            
            if (isReadyToMoveInSelected) {
                [arr addObject:@"Ready To Move"];
            }
            
            if (isUnderConstructionSelected) {
                [arr addObject:@"Under Construction"];
            }
            
            NSString *str = [arr componentsJoinedByString:@","];
            [dict setObject:str forKey:@"project_status"];
            
        }
        else {
            [dict removeObjectForKey:@"project_status"];
        }
        
        
        if (isNewestSelected) {
            [dict setObject:@"newest" forKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];
        }
        else if (isPriceSelected) {
            
            [dict removeObjectForKey:@"newest"];
            if (isPriceLowToHighSlected && !isPriceHighToLowSlected) {
                [dict setObject:@"priceltoh" forKey:@"priceltoh"];
                [dict removeObjectForKey:@"pricehtol"];
            }
            
            if (!isPriceLowToHighSlected && isPriceHighToLowSlected) {
                [dict setObject:@"pricehtol" forKey:@"pricehtol"];
                [dict removeObjectForKey:@"priceltoh"];
            }
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];

        }
        else if (isPricePSFSelected) {
            
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];

            if (isPricePSFLowToHighSelected && !isPriceHighToLowSlected) {
                [dict setObject:@"psfltoh" forKey:@"psfltoh"];
                [dict removeObjectForKey:@"psfhtol"];
            }
            else {
                [dict setObject:@"psfhtol" forKey:@"psfhtol"];
                [dict removeObjectForKey:@"psfltoh"];
            }
            
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];
        }
        else if (isRatingSelected) {
            
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            
            [dict setObject:@"rating" forKey:@"rating"];
            
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];
        }
        else if (isPossessionSelected) {
            
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            
            if (isPossessionLowToHighSelected && !isPossessionHighToLowSelected) {
                [dict setObject:@"possessionltoh" forKey:@"possessionltoh"];
                [dict removeObjectForKey:@"possessionhtol"];
            }
            else {
                [dict setObject:@"possessionhtol" forKey:@"possessionhtol"];
                [dict removeObjectForKey:@"possessionltoh"];
            }
            
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];
            
        }
        else if (isInfraSelected) {
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            
            [dict setObject:@"infra" forKey:@"infra"];
            
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];
        }
        else if (isNeedsSelected) {
            
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            
            [dict setObject:@"needs" forKey:@"needs"];
            
            [dict removeObjectForKey:@"lifestyle"];
            [dict removeObjectForKey:@"returnsval"];
        }
        else if (isLifeStyleSelected) {
            
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            
            [dict setObject:@"lifestyle" forKey:@"lifestyle"];
            
            [dict removeObjectForKey:@"returnsval"];
        }
        else if (isReturnSelected) {
            
            [dict removeObjectForKey:@"newest"];
            [dict removeObjectForKey:@"priceltoh"];
            [dict removeObjectForKey:@"pricehtol"];
            [dict removeObjectForKey:@"psfltoh"];
            [dict removeObjectForKey:@"psfhtol"];
            [dict removeObjectForKey:@"rating"];
            [dict removeObjectForKey:@"possessionltoh"];
            [dict removeObjectForKey:@"possessionhtol"];
            [dict removeObjectForKey:@"infra"];
            [dict removeObjectForKey:@"needs"];
            [dict removeObjectForKey:@"lifestyle"];
            
            [dict setObject:@"returnsval" forKey:@"returnsval"];
        }
        

        if (isDiscount_OffersSelected) {
            [dict setObject:[NSNumber numberWithBool:isDiscount_OffersSelected] forKey:@"discout_offers"];
        }
        else {
            [dict removeObjectForKey:@"discout_offers"];
        }
        
        
        if (is6MSelected) {
            [dict setObject:@"6m" forKey:@"posession"];
        }
        else if (is1YRSelected) {
            [dict setObject:@"1y" forKey:@"posession"];
        }
        else if (is2YRSelected) {
            [dict setObject:@"2y" forKey:@"posession"];
        }
        else if (is3YRSeleted) {
            [dict setObject:@"3y" forKey:@"posession"];
        }
        else if (is4YRSelected) {
            [dict setObject:@"4y" forKey:@"posession"];
        }
        else if (is5PlusYRSelected) {
            [dict setObject:@"5y" forKey:@"posession"];
        }
        else {
            
        }
        
        if (isBuilderListSelected) {
            [dict setObject:strBuilderList forKey:@"builders"];
        }
        else {
            [dict removeObjectForKey:@"builders"];
        }
        
        if (isPropertyTypeSelected) {
            
            NSMutableArray *arr = [NSMutableArray array];
            if (_option == PropertyTypeResidentialOnly) {
                
                if (isFlatSelected) {
                    [arr addObject:[arrResidential objectAtIndex:0]];
                }
                if (isBuilderFloorSelected) {
                    [arr addObject:[arrResidential objectAtIndex:1]];
                }
                if (isPlotSelected) {
                    [arr addObject:[arrResidential objectAtIndex:2]];
                }
                if (isHouseVillaSelected) {
                    [arr addObject:[arrResidential objectAtIndex:3]];
                }
            }
            else if (_option == PropertyTypeCommenrcialOnly) {
                
                if (isShopSelected) {
                    [arr addObject:[arrCommercial objectAtIndex:0]];
                }
                if (isOfficeSelected) {
                    [arr addObject:[arrCommercial objectAtIndex:1]];
                }
                if (isServiceAppartmentSelected) {
                    [arr addObject:[arrCommercial objectAtIndex:2]];
                }
            }
            else {
                
                if (isResidentialSelected && !isCommercialSelected) {
                    if (isFlatSelected) {
                        [arr addObject:[arrResidential objectAtIndex:0]];
                    }
                    if (isBuilderFloorSelected) {
                        [arr addObject:[arrResidential objectAtIndex:1]];
                    }
                    if (isPlotSelected) {
                        [arr addObject:[arrResidential objectAtIndex:2]];
                    }
                    if (isHouseVillaSelected) {
                        [arr addObject:[arrResidential objectAtIndex:3]];
                    }
                }
                else {
                    if (isShopSelected) {
                        [arr addObject:[arrCommercial objectAtIndex:0]];
                    }
                    if (isOfficeSelected) {
                        [arr addObject:[arrCommercial objectAtIndex:1]];
                    }
                    if (isServiceAppartmentSelected) {
                        [arr addObject:[arrCommercial objectAtIndex:2]];
                    }
                }
            }
            
            NSString *str = [arr componentsJoinedByString:@","];
            [dict setObject:str forKey:@"projectType"];
        }
        else {
            [dict removeObjectForKey:@"projectType"];
        }
        
        if (isBHKSelected) {
            
            NSMutableArray *arr = [NSMutableArray array];
            
            if (is1BHKSelected) {
                [arr addObject:@"1bhk"];
            }
            else {
                
                if ([arr containsObject:@"1bhk"]) {
                    [arr removeObject:@"1bhk"];
                }
            }
            
            if (is2BHKSelected) {
                [arr addObject:@"2bhk"];
            }
            else {
                if ([arr containsObject:@"2bhk"]) {
                    [arr removeObject:@"2bhk"];
                }
            }
            
            if (is3BHKSelected) {
                [arr addObject:@"3bhk"];
            }
            else {
                if ([arr containsObject:@"3bhk"]) {
                    [arr removeObject:@"3bhk"];
                }
            }
            
            if (is4BHKSelected) {
                [arr addObject:@"4bhk"];
            }
            else {
                if ([arr containsObject:@"4bhk"]) {
                    [arr removeObject:@"4bhk"];
                }
            }
            
            if (is5BHKSelected) {
                [arr addObject:@"5bhk"];
            }
            else {
                if ([arr containsObject:@"5bhk"]) {
                    [arr removeObject:@"5bhk"];
                }
            }
            
            if (is5plusBHKSelected) {
                [arr addObject:@"5plusbhk"];
            }
            else {
                if ([arr containsObject:@"5plusbhk"]) {
                    [arr removeObject:@"5plusbhk"];
                }
            }
            
            
            NSString *str = [arr componentsJoinedByString:@","];
            [dict setObject:str forKey:@"bhk"];
        }
        else {
            [dict removeObjectForKey:@"bhk"];
        }
        
        
        if (isPriceRangeSelected) {
            
            [dict setObject:[cellPriceRange.strLowerLimit copy] forKey:@"min-p"];
            [dict setObject:[cellPriceRange.strUpprerLimit copy] forKey:@"max-p"];
        }
        else {
            [dict removeObjectForKey:@"min-p"];
            [dict removeObjectForKey:@"max-p"];
        }
        
        if (isBuildUpAreaSelected) {
            [dict setObject:[cellBuiltUp.strLowerLimit copy] forKey:@"min_area_range"];
            [dict setObject:[cellBuiltUp.strUpprerLimit copy] forKey:@"max_area_range"];
        }
        else {
            
            [dict removeObjectForKey:@"min_area_range"];
            [dict removeObjectForKey:@"max_area_range"];
        }
        
        if (isPricePSFRangeSelected) {
            [dict setObject:[cellPricePSF.strLowerLimit copy] forKey:@"min_psf"];
            [dict setObject:[cellPricePSF.strUpprerLimit copy] forKey:@"max_psf"];
        }
        else {
            
            [dict removeObjectForKey:@"max_psf"];
            [dict removeObjectForKey:@"min_psf"];
        }

        
        
        if (isAmenitiesSelected) {
            
            NSMutableArray *arr = [NSMutableArray array];
            if (isGasSelected) {
                [arr addObject:@"Piped_Gas"];
            }
            else {
                if ([arr containsObject:@"Piped_Gas"]) {
                    [arr removeObject:@"Piped_Gas"];
                }
            }
            
            if (isLiftSelected) {
                [arr addObject:@"Lift"];
            }
            else {
                if ([arr containsObject:@"Lift"]) {
                    [arr removeObject:@"Lift"];
                }
            }
            
            if (isParkingSelected) {
                [arr addObject:@"Podium_Parking"];
            }
            else {
                if ([arr containsObject:@"Podium_Parking"]) {
                    [arr removeObject:@"Podium_Parking"];
                }
            }
            
            if (isGymSelected) {
                [arr addObject:@"Gymnasium"];
            }
            else {
                if ([arr containsObject:@"Gymnasium"]) {
                    [arr removeObject:@"Gymnasium"];
                }
            }
            
            if (isSecuritySelected) {
                [arr addObject:@"24*7_Security"];
            }
            else {
                if ([arr containsObject:@"24*7_Security"]) {
                    [arr removeObject:@"24*7_Security"];
                }
            }
            
            if (isTreatedWaterSupplySelected) {
                [arr addObject:@"Treated_Water_Supply"];
            }
            else {
                if ([arr containsObject:@"Treated_Water_Supply"]) {
                    [arr removeObject:@"Treated_Water_Supply"];
                }
            }
            
            if (isWiFiSelected) {
                [arr addObject:@"Central_Wi-fi"];
            }
            else {
                if ([arr containsObject:@"Central_Wi-fi"]) {
                    [arr removeObject:@"Central_Wi-fi"];
                }
            }
            
            if (isPartyHallSelected) {
                [arr addObject:@"Party_Hall"];
            }
            else {
                if ([arr containsObject:@"Party_Hall"]) {
                    [arr removeObject:@"Party_Hall"];
                }
            }
            
            if (isACSelected) {
                [arr addObject:@"AC_in_Lobby"];
            }
            else {
                if ([arr containsObject:@"AC_in_Lobby"]) {
                    [arr removeObject:@"AC_in_Lobby"];
                }
            }
            
            if (isSwimmingPoolSelected) {
                [arr addObject:@"Swimming"];
            }
            else {
                if ([arr containsObject:@"Swimming"]) {
                    [arr removeObject:@"Swimming"];
                }
            }
            
            if (isHomeAutomationSelected) {
                [arr addObject:@"Home_Automation"];
            }
            else {
                if ([arr containsObject:@"Home_Automation"]) {
                    [arr removeObject:@"Home_Automation"];
                }
            }
            
            if (isPowerBackupSelected) {
                [arr addObject:@"Power_Backup"];
            }
            else {
                if ([arr containsObject:@"Power_Backup"]) {
                    [arr removeObject:@"Power_Backup"];
                }
            }
            NSString *str = [arr componentsJoinedByString:@","];
            [dict setObject:str forKey:@"lifestyles"];
        }
        else {
            [dict removeObjectForKey:@"lifestyles"];
        }
        
        [pref setObject:dict forKey:strSaveMultiFilter];
        
        if (_accessOption == AccessOptionProjectList) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kProjectListMultifilter object:nil];
        }
        else if (_accessOption == AccessOptionProjectMap) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kProjectMapMultifilter object:nil];
        }
        
        
        [self actionLister:btnBack];
    }
}

- (void)didTappedStatus:(UIButton *)sender {
    
    
    isNewLaunchSelected = NO;
    isUnderConstructionSelected = NO;
    isReadyToMoveInSelected = NO;
    
    [sender addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
    [arrObservedValues addObject:sender];
    sender.selected = YES;
    
    if (sender.tag == 1) {
        isNewLaunchSelected = YES;
    }
    
    if (sender.tag == 2) {
        isUnderConstructionSelected = YES;
    }
    if (sender.tag == 3) {
        isReadyToMoveInSelected = YES;
    }
    
    for (UIView *temp in cellFilter.contentView.subviews) {
        
        if ([temp isKindOfClass:[UIView class]]) {
            
            for (UIView *temp1 in temp.subviews) {
                
                if (temp1 != sender) {
                    
                    UIButton *btn = (UIButton *)temp1;
                    [btn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
                    [arrObservedValues addObject:btn];
                    btn.selected = NO;
                }
            }
            
        }
    }
    
    if (isReadyToMoveInSelected) {
        cellSortedBy.btnPossession.enabled = NO;
        [cellSortedBy.btnPossession setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
        [cellSortedBy.btnPossession addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
        [arrObservedValues addObject:cellSortedBy.btnPossession];
        cellSortedBy.btnPossession.selected = NO;
        isPossessionSelected = NO;
    }
    else {
        cellSortedBy.btnPossession.enabled = YES;
        if (!cellSortedBy.btnPossession.selected) {
            [cellSortedBy.btnPossession setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
        }
    }
}

- (void)didTappedSortedBy:(UIButton *)sender {
    
    isNewestSelected = NO;
    isPriceSelected = NO;
    isPriceHighToLowSlected = NO;
    isPriceLowToHighSlected = NO;
    isPricePSFSelected = NO;
    isPricePSFHighToLowSelected = NO;
    isPricePSFLowToHighSelected = NO;
    isRatingSelected = NO;
    isPossessionSelected = NO;
    isPossessionLowToHighSelected = NO;
    isPossessionHighToLowSelected = NO;
    isInfraSelected = NO;
    isNeedsSelected = NO;
    isLifeStyleSelected = NO;
    isReturnSelected = NO;
    
    [cellSortedBy.btnPrice setTitle:@"Price" forState:UIControlStateNormal];
    [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
    [cellSortedBy.btnPricePSF setTitle:@"Price PSF" forState:UIControlStateNormal];
    
    [sender addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
    sender.selected = YES;

    [arrObservedValues addObject:sender];
    if (sender.tag == 1) {
        isNewestSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
    }
    
    if (sender.tag == 2) {
        isPriceSelected = YES;
        cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
        cellSortedBy.tblPrice.hidden = NO;
        if (cellSortedBy.tblPrice == nil) {
            cellSortedBy.tblPrice = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(sender.frame), CGRectGetMaxY(sender.frame), CGRectGetWidth(sender.frame), 2 * CGRectGetHeight(sender.frame))];
            cellSortedBy.tblPrice.separatorColor = [UIColor clearColor];
            cellSortedBy.tblPrice.delegate = self;
            cellSortedBy.tblPrice.dataSource = self;
            [cellSortedBy addSubview:cellSortedBy.tblPrice];
        }
        else {
            
        }
    }
    
    if (sender.tag == 3) {
        isPricePSFSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPossession.hidden = YES;
        cellSortedBy.tblPricePSF.hidden = NO;
        if (cellSortedBy.tblPricePSF == nil) {
            cellSortedBy.tblPricePSF = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(sender.frame), CGRectGetMaxY(sender.frame), CGRectGetWidth(sender.frame), 2 * CGRectGetHeight(sender.frame))];
            cellSortedBy.tblPricePSF.separatorColor = [UIColor clearColor];
            cellSortedBy.tblPricePSF.delegate = self;
            cellSortedBy.tblPricePSF.dataSource = self;
            [cellSortedBy addSubview:cellSortedBy.tblPricePSF];
        }
        else {
            
        }
    }
    
    if (sender.tag == 4) {
        isRatingSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
    }
    
    if (sender.tag == 5) {
        isPossessionSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = YES;
        cellSortedBy.tblPossession.hidden = NO;
        
        if (cellSortedBy.tblPossession == nil) {

            cellSortedBy.tblPossession = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(sender.frame) - CGRectGetWidth(sender.frame)/2, CGRectGetMinY(sender.frame), 2 * CGRectGetWidth(sender.frame), 2 * CGRectGetHeight(sender.frame))];
            
            cellSortedBy.tblPossession.separatorColor = [UIColor clearColor];
            cellSortedBy.tblPossession.delegate = self;
            cellSortedBy.tblPossession.dataSource = self;
            [self.view addSubview:cellSortedBy.tblPossession];
            [cellSortedBy addSubview:cellSortedBy.tblPossession];
        }
    }
    
    if (sender.tag == 6) {
        isInfraSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
    }
    
    if (sender.tag == 7) {
        isNeedsSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
    }
    
    if (sender.tag == 8) {
        isLifeStyleSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
    }
    
    if (sender.tag == 9) {
        isReturnSelected = YES;
        cellSortedBy.tblPrice.hidden = cellSortedBy.tblPricePSF.hidden = cellSortedBy.tblPossession.hidden = YES;
    }
    
    
    
    
    for (UIView *temp in cellSortedBy.contentView.subviews) {
        
        if ([temp isKindOfClass:[UIButton class]]) {
            
            if (temp != sender) {
                
                UIButton *btn = (UIButton *)temp;
                [btn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
                [arrObservedValues addObject:btn];
                btn.selected = NO;
            }
            
            if (isReadyToMoveInSelected && temp == cellSortedBy.btnPossession) {
                cellSortedBy.btnPossession.selected = NO;
                cellSortedBy.btnPossession.enabled = NO;
                [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
                [cellSortedBy.btnPossession setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            }
        }
    }

}

- (void)didTappedOffers_Discounts:(UIButton *)sender {
    
    isDiscount_OffersSelected = cellDiscount_Offers.btnCheck.selected = !cellDiscount_Offers.btnCheck.selected;
   
    if (isDiscount_OffersSelected) {
        cellDiscount_Offers.imgCheck.image = [UIImage imageNamed:@"Checkbox_green"];//
    }
    else {
        cellDiscount_Offers.imgCheck.image = [UIImage imageNamed:@"Checkbox_gray"];
    }
    
}

- (void)didTappedPossessionIn:(UIButton *)sender {
    
    is6MSelected = NO;
    is1YRSelected = NO;
    is2YRSelected = NO;
    is3YRSeleted = NO;
    is4YRSelected = NO;
    is5PlusYRSelected = NO;
    
    [sender addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
    sender.selected = YES;
    [arrObservedValues addObject:sender];
    if (sender.tag == 1) {
        is6MSelected = YES;
    }
    else if (sender.tag == 2) {
        is1YRSelected = YES;
    }
    else if (sender.tag == 3) {
        is2YRSelected = YES;
    }
    else if (sender.tag == 4) {
        is3YRSeleted = YES;
    }
    else if (sender.tag == 5) {
        is4YRSelected = YES;
    }
    else if (sender.tag == 6) {
        is5PlusYRSelected = YES;
    }
    
    
    for (UIView *temp in cellPossessionIn.contentView.subviews) {
        
        if ([temp isKindOfClass:[UIView class]]) {
            
            for (UIButton *btn  in temp.subviews) {
                
                if (btn != sender) {
                    [btn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
                    btn.selected = NO;
                    [arrObservedValues addObject:btn];
                }
                
                
            }
        }
    }
}

- (void)setPropertyType:(UIButton *)btn {
    
    if (btn.tag == 1) {

        isShopSelected = NO;
        isOfficeSelected = NO;
        isServiceAppartmentSelected = NO;
        
        cellSelectPropertyType.btnResidential.selected = YES;
        cellSelectPropertyType.btnCommercial.selected = NO;
        
        isResidentialSelected = YES;
        isCommercialSelected = NO;
    }
    else if (btn.tag == 2) {
        
        isBHKSelected = NO;
        is1BHKSelected = NO;
        is2BHKSelected = NO;
        is3BHKSelected = NO;
        is4BHKSelected = NO;
        is5BHKSelected = NO;
        is5plusBHKSelected = NO;
        
        isFlatSelected = NO;
        isBuilderFloorSelected = NO;
        isPlotSelected = NO;
        isHouseVillaSelected = NO;
        
        cellSelectPropertyType.btnResidential.selected = NO;
        cellSelectPropertyType.btnCommercial.selected = YES;
        
        isResidentialSelected = NO;
        isCommercialSelected = YES;
        
        BHK *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
        
        for (UIView *temp in cell.contentView.subviews) {
            
            if ([temp isKindOfClass:[UIView class]]) {
                
                for (UIView *temp1 in temp.subviews) {
                    
                    for (UIView *temp2 in temp1.subviews) {
                        
                        if ([temp2 isKindOfClass:[UIImageView class]]) {
                            UIImageView *imgView = (UIImageView *)temp2;
                            imgView.image = [UIImage imageNamed:@"Checkbox_gray"];
                        }
                        else if ([temp2 isKindOfClass:[UIButton class]]) {
                            UIButton *btn = (UIButton *)temp2;
                            if (btn.selected) {
                                btn.selected = !btn.selected;
                            }
                        }
                    }
                }
            }
        }

    }
    
    [tblView reloadData];
    [cellSelectPropertyType.colView reloadData];
}




- (void)didSelectProperty:(UIButton *)sender {
    
}

- (void)didTappedBHKSelected:(UIButton *)sender  {
    
    if (sender.tag == 1) {
        is1BHKSelected = !is1BHKSelected;
        
        if (is1BHKSelected) {
            cellBHK.img1BHK.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellBHK.img1BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 2) {
        is2BHKSelected = !is2BHKSelected;
        if (is2BHKSelected) {
            cellBHK.img2BHK.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellBHK.img2BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 3) {
        is3BHKSelected = !is3BHKSelected;
        
        if (is3BHKSelected) {
            cellBHK.img3BHK.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellBHK.img3BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 4) {
        is4BHKSelected = !is4BHKSelected;
        if (is4BHKSelected) {
            cellBHK.img4BHK.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellBHK.img4BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 5) {
        is5BHKSelected = !is5BHKSelected;
        if (is5BHKSelected) {
            cellBHK.img5BHK.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellBHK.img5BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 6) {
        is5plusBHKSelected = !is5plusBHKSelected;
        if (is5plusBHKSelected) {
            cellBHK.img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellBHK.img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    
    isBHKSelected = is1BHKSelected | is2BHKSelected | is3BHKSelected | is4BHKSelected | is5BHKSelected | is5plusBHKSelected;
    
    //[tblView reloadData];
}

- (void)priceRangeDidChange:(MARKRangeSlider *)slider {
    
    int num = (int)slider.leftValue;
    if (num < 100) {
        cellPriceRange.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dL", num - (num % 25)];
        cellPriceRange.strLowerLimit = [NSString stringWithFormat:@"%d", (num - (num % 25)) * 100000];
    }
    else if (num >= 100) {
        num = num / 100;
        cellPriceRange.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", num];
        cellPriceRange.strLowerLimit = [NSString stringWithFormat:@"%d", (num) * 10000000];
    }
    
    num = (int)slider.rightValue;
    if (num < 100) {
        cellPriceRange.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dL", num - (num % 25)];
        cellPriceRange.strUpprerLimit = [NSString stringWithFormat:@"%d", (num - (num % 25)) * 100000];
    }
    else if (num >= 100) {
        num = num / 100;
        cellPriceRange.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", num];
        cellPriceRange.strUpprerLimit = [NSString stringWithFormat:@"%d", num * 10000000];
    }
    
    
    if (slider.leftValue > slider.minimumValue || slider.rightValue < slider.maximumValue) {
        isPriceRangeSelected = YES;
    }
    else {
        isPriceRangeSelected = NO;
    }
    
}

- (void)builtUpAreaDidChange:(MARKRangeSlider *)slider {
    
    int num = (int)slider.leftValue;
    
    if (num >= 250 && num < 500) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"250 sqft"];
        cellBuiltUp.strLowerLimit = @"250";
    }
    else if (num >= 500 && num < 750) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"500 sqft"];
        cellBuiltUp.strLowerLimit = @"500";
    }
    else if (num >= 750 && num < 1000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"750 sqft"];
        cellBuiltUp.strLowerLimit = @"750";
    }
    else if (num >= 1000 && num < 2000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"1000 sqft"];
        cellBuiltUp.strLowerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"2000 sqft"];
        cellBuiltUp.strLowerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"3000 sqft"];
        cellBuiltUp.strLowerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"4000 sqft"];
        cellBuiltUp.strLowerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"6000 sqft"];
        cellBuiltUp.strLowerLimit = @"6000";
    }
    else if (num >= 6000 && num < 7000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"7000 sqft"];
        cellBuiltUp.strLowerLimit = @"7000";
    }
    else if (num >= 7000 && num < 8000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"8000 sqft"];
        cellBuiltUp.strLowerLimit = @"8000";
    }else if (num == 8000) {
        cellBuiltUp.lblLowerBound.text = [NSString stringWithFormat:@"9000 sqft"];
        cellBuiltUp.strLowerLimit = @"9000";
    }
    
    num = (int)slider.rightValue;
    if (num >= 250 && num < 500) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"250 sqft"];
        cellBuiltUp.strUpprerLimit = @"250";
    }
    else if (num >= 500 && num < 750) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"500 sqft"];
        cellBuiltUp.strUpprerLimit = @"500";
    }
    else if (num >= 750 && num < 1000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"750 sqft"];
        cellBuiltUp.strUpprerLimit = @"750";
    }
    else if (num >= 1000 && num < 2000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"1000 sqft"];
        cellBuiltUp.strUpprerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"2000 sqft"];
        cellBuiltUp.strUpprerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"3000 sqft"];
        cellBuiltUp.strUpprerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"4000 sqft"];
        cellBuiltUp.strUpprerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"6000 sqft"];
        cellBuiltUp.strUpprerLimit = @"6000";
    }
    else if (num >= 6000 && num < 7000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"7000 sqft"];
        cellBuiltUp.strUpprerLimit = @"7000";
    }
    else if (num >= 7000 && num < 8000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"8000 sqft"];
        cellBuiltUp.strUpprerLimit = @"8000";
    }else if (num == 8000) {
        cellBuiltUp.lblUpperBound.text = [NSString stringWithFormat:@"9000 sqft"];
        cellBuiltUp.strUpprerLimit = @"9000";
    }
    
    
    if (slider.leftValue > slider.minimumValue || slider.rightValue < slider.maximumValue) {
        isBuildUpAreaSelected = YES;
    }
    else {
        isBuildUpAreaSelected = NO;
    }
}

- (void)pricePSFDidChange:(MARKRangeSlider *)slider {

    int num = (int)slider.leftValue;
    if (num >= 500 && num < 1000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 500"];
        cellPricePSF.strLowerLimit = @"500";
    }
    else if (num >= 1000 && num < 2000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 1000"];
        cellPricePSF.strLowerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 2000"];
        cellPricePSF.strLowerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 3000"];
        cellPricePSF.strLowerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 4000"];
        cellPricePSF.strLowerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 5000"];
        cellPricePSF.strLowerLimit = @"5000";
    }
    else if (num >= 6000 && num < 7000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 6000"];
        cellPricePSF.strLowerLimit = @"6000";
    }
    else if (num >= 7000 && num < 8000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 7000"];
        cellPricePSF.strLowerLimit = @"7000";
    }
    else if (num >= 8000 && num < 9000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 8000"];
        cellPricePSF.strLowerLimit = @"8000";
    }
    else if (num >= 9000 && num < 10000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 9000"];
        cellPricePSF.strLowerLimit = @"9000";
    }
    else if (num >= 10000 && num < 15000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 10000"];
        cellPricePSF.strLowerLimit = @"10000";
    }
    else if (num >= 15000 && num < 20000) {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 15000"];
        cellPricePSF.strLowerLimit = @"15000";
    }
    else {
        cellPricePSF.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
        cellPricePSF.strLowerLimit = @"20000";
    }
    
    
    num = (int)slider.rightValue;
    
    if (num >= 500 && num < 1000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 500"];
        cellPricePSF.strUpprerLimit = @"500";
    }
    else if (num >= 1000 && num < 2000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 1000"];
        cellPricePSF.strUpprerLimit = @"1000";
    }
    else if (num >= 2000 && num < 3000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 2000"];
        cellPricePSF.strUpprerLimit = @"2000";
    }
    else if (num >= 3000 && num < 4000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 3000"];
        cellPricePSF.strUpprerLimit = @"3000";
    }
    else if (num >= 4000 && num < 5000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 4000"];
        cellPricePSF.strUpprerLimit = @"4000";
    }
    else if (num >= 5000 && num < 6000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 5000"];
        cellPricePSF.strUpprerLimit = @"5000";
    }
    else if (num >= 6000 && num < 7000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 6000"];
        cellPricePSF.strUpprerLimit = @"6000";
    }
    else if (num >= 7000 && num < 8000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 7000"];
        cellPricePSF.strUpprerLimit = @"7000";
    }
    else if (num >= 8000 && num < 9000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 8000"];
        cellPricePSF.strUpprerLimit = @"8000";
    }
    else if (num >= 9000 && num < 10000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 9000"];
        cellPricePSF.strUpprerLimit = @"9000";
    }
    else if (num >= 10000 && num < 15000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 10000"];
        cellPricePSF.strUpprerLimit = @"10000";
    }
    else if (num >= 15000 && num < 20000) {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 15000"];
        cellPricePSF.strUpprerLimit = @"15000";
    }
    else {
        cellPricePSF.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
        cellPricePSF.strUpprerLimit = @"20000";
    }
    
    
    if (slider.leftValue > slider.minimumValue || slider.rightValue < slider.maximumValue) {
        isPricePSFRangeSelected = YES;
    }
    else {
        isPricePSFRangeSelected = NO;
    }
    
}

- (void)didTappedAminities:(UIButton *)sender {
    
    if (sender.tag == 1) {
        isGasSelected = !isGasSelected;
        if (isGasSelected) {
            cellAminities.imgGas.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellAminities.imgGas.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 2) {
        isLiftSelected = !isLiftSelected;
        
        if (isLiftSelected) {
            cellAminities.imgLift.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellAminities.imgLift.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 3) {
        isParkingSelected = !isParkingSelected;
        
        if (isParkingSelected) {
            cellAminities.imgParking.image = [UIImage imageNamed:@"Checkbox_green"];
        }
        else {
            cellAminities.imgParking.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 4) {
        isGymSelected = !isGymSelected;
        
        if (isGymSelected) {
            cellAminities.imgGym.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
           cellAminities.imgGym.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 5) {
        isSecuritySelected = !isSecuritySelected;
        
        if (isSecuritySelected) {
            cellAminities.imgSecurity.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgSecurity.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 6) {
        isTreatedWaterSupplySelected = !isTreatedWaterSupplySelected;
        
        if (isTreatedWaterSupplySelected) {
            cellAminities.imgWardrobe.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgWardrobe.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
        
    }
    else if (sender.tag == 7) {
        isWiFiSelected = !isWiFiSelected;
        
        if (isWiFiSelected) {
            cellAminities.imgWifi.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgWifi.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 8) {
        isPartyHallSelected = !isPartyHallSelected;
        
        if (isPartyHallSelected) {
            cellAminities.imgFurnished.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgFurnished.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
        
    }
    else if (sender.tag == 9) {
        isACSelected = !isACSelected;
        if (isACSelected) {
            cellAminities.imgAC.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgAC.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else if (sender.tag == 10) {
        isSwimmingPoolSelected = !isSwimmingPoolSelected;
        
        if (isSwimmingPoolSelected) {
            cellAminities.imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
        
    }
    else if (sender.tag == 11) {
        isHomeAutomationSelected = !isHomeAutomationSelected;
        
        if (isHomeAutomationSelected) {
            cellAminities.imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    else {
        isPowerBackupSelected = !isPowerBackupSelected;
        
        if (isPowerBackupSelected) {
            cellAminities.imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_green"];
            
        }
        else {
            cellAminities.imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_gray"];
        }
    }
    
    isAmenitiesSelected = isGasSelected | isLiftSelected | isParkingSelected | isGymSelected | isSecuritySelected | isTreatedWaterSupplySelected | isWiFiSelected | isPartyHallSelected| isACSelected | isSwimmingPoolSelected | isHomeAutomationSelected | isHomeAutomationSelected | isPowerBackupSelected;
    
}

#pragma mark - Table View Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == tblView) {
        return 11;
    }
    else if (tableView == cellSortedBy.tblPrice) {
        return 3;
    }
    else if (tableView == cellSortedBy.tblPricePSF) {
        return 3;
    }
    else {
        return 3;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        if (indexPath.row == 0) {
            return 112.0;
        }
        else if (indexPath.row == 1) {
            return 176.0;
        }
            else if (indexPath.row == 2) {
        
                //return 94.0;
                return 0.0;
            }
            else if (indexPath.row == 3) {
                return 98.0;
            }
            else if (indexPath.row == 4) {
                return 94.0;
            }
            else if (indexPath.row == 5) {
                return 229;
            }
            else if (indexPath.row == 6) {
        
                if (isCommercialSelected) {
                    return 0.0;
                }
                else {
                    return 176.0;
                }
                
            }
            else if (indexPath.row >= 7 && indexPath.row <= 9) {
                return 124.0;
            }
            else if (indexPath.row == 10) {
                return 298.0;
            }

        return 0.0;
    }
    else if (tableView == cellSortedBy.tblPrice || tableView == cellSortedBy.tblPricePSF || tableView == cellSortedBy.tblPossession) {
        return 25.0;
    }
    else {
        return 0;
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        if (indexPath.row == 0) {
            FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
            
            if (cell == nil) {
                cell = [FilterCell createCell];
                [cell.btnNewLaunch addTarget:self action:@selector(didTappedStatus:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnUnderConstruction addTarget:self action:@selector(didTappedStatus:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnReadyToMoveIn addTarget:self action:@selector(didTappedStatus:) forControlEvents:UIControlEventTouchUpInside];
                
                
  
            }
            
            [cell.btnNewLaunch addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
            [arrObservedValues addObject:cell.btnNewLaunch];
            [cell.btnUnderConstruction addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
            [arrObservedValues addObject:cell.btnUnderConstruction];
            [cell.btnReadyToMoveIn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
            [arrObservedValues addObject:cell.btnReadyToMoveIn];
            
            cell.btnNewLaunch.selected = isNewLaunchSelected;
            cell.btnUnderConstruction.selected = isUnderConstructionSelected;
            cell.btnReadyToMoveIn.selected = isReadyToMoveInSelected;
            
            
            cellFilter = cell;
            
            return cell;
        }
        else if (indexPath.row == 1) {
            
            SortedBy *cell  = [tableView dequeueReusableCellWithIdentifier:@"SortedBy"];
            
            if (cell == nil) {
                cell = [SortedBy createCell];
                
                for (UIView *temp in cell.contentView.subviews) {
                    
                    if ([temp isKindOfClass:[UIButton class]]) {
                        UIButton *btn = (UIButton *)temp;
                        [btn addTarget:self action:@selector(didTappedSortedBy:) forControlEvents:UIControlEventTouchUpInside];
                        
                    }
                }
             
                
            }
            
            for (UIView *temp in cell.contentView.subviews) {
                
                if ([temp isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)temp;
                    [btn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
                    [arrObservedValues addObject:btn];
                }
            }
            
            cell.btnNewest.selected = isNewestSelected;
            cell.btnPrice.selected = isPriceSelected;
            if (isPriceSelected) {
                
                if (isPriceLowToHighSlected && !isPriceHighToLowSlected) {
                    [cell.btnPrice setTitle:@"Low to high" forState:UIControlStateNormal];
                }
                else {
                    [cell.btnPrice setTitle:@"High to low" forState:UIControlStateNormal];
                }
            }
            cell.btnPricePSF.selected = isPricePSFSelected;
            
            if (isPricePSFSelected) {
                
                if (isPricePSFLowToHighSelected && !isPricePSFHighToLowSelected) {
                    [cell.btnPricePSF setTitle:@"Low to high" forState:UIControlStateNormal];
                }
                else {
                    [cell.btnPricePSF setTitle:@"High to low" forState:UIControlStateNormal];
                }
            }
            cell.btnRating.selected = isRatingSelected;
            
            if (isReadyToMoveInSelected) {
                cell.btnPossession.enabled = NO;
                //cell.btnPossession.selected = NO;
                [cell.btnPossession setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [cell.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
                
                if ([cell.tblPossession isHidden]) {
                    cell.tblPossession.hidden = YES;
                }
                
                
            }
            else {
               cell.btnPossession.selected = isPossessionSelected;
            }
            
            
            if (isPossessionSelected) {
                
                if (isPossessionLowToHighSelected && !isPossessionHighToLowSelected) {
                    [cell.btnPossession setTitle:@"Earliest to latest" forState:UIControlStateNormal];
                }
                else {
                    [cell.btnPossession setTitle:@"Latest to earliest" forState:UIControlStateNormal];
                }
            }
            cell.btnInfra.selected = isInfraSelected;
            cell.btnNeeds.selected = isNeedsSelected;
            cell.btnLifestyle.selected = isLifeStyleSelected;
            cell.btnReturns.selected = isReturnSelected;
            
            cellSortedBy = cell;
            
            return cell;
        }
        else if (indexPath.row == 2) {
            Discount_Offers *cell = [tableView dequeueReusableCellWithIdentifier:@"Discount_Offers"];
            if (cell == nil) {
                cell = [Discount_Offers createCell];
                [cell.btnCheck addTarget:self action:@selector(didTappedOffers_Discounts:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            cell.btnCheck.selected = isDiscount_OffersSelected;
            
            if (isDiscount_OffersSelected) {
                cell.imgCheck.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgCheck.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            cellDiscount_Offers = cell;
            return cell;
        }
        else if (indexPath.row == 3) {
            
            PossessionIn *cell = [tableView dequeueReusableCellWithIdentifier:@"PossessionIn"];
            if (cell == nil) {
                cell = [PossessionIn createCell];
                
                for (UIView *temp in cell.contentView.subviews) {
                    
                    if ([temp isKindOfClass:[UIView class]]) {
                        
                        for (UIButton *btn in temp.subviews) {
                            [btn addTarget:self action:@selector(didTappedPossessionIn:) forControlEvents:UIControlEventTouchUpInside];
                        }
                    }
                }
                
            }
            
            for (UIView *temp in cell.contentView.subviews) {
                
                if ([temp isKindOfClass:[UIView class]]) {
                    
                    for (UIButton *btn in temp.subviews) {
                        
                        [btn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
                        [arrObservedValues addObject:btn];
                    }
                }
            }
            
            cell.btn6m.selected = is6MSelected;
            cell.btn1yr.selected = is1YRSelected;
            cell.btn2yr.selected = is2YRSelected;
            cell.btn3yr.selected = is3YRSeleted;
            cell.btn4yr.selected = is4YRSelected;
            cell.btn5yr.selected = is5PlusYRSelected;
            
            cellPossessionIn = cell;
            return cell;
        }
        else if (indexPath.row == 4) {
            
            SelectBuilder *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectBuilder"];
            if (cell == nil) {
                cell = [SelectBuilder createCell];
                if (_arrBuilderList.count < 2) {
                    
                    for (UIView *temp in cell.contentView.subviews) {
                        
                        if ([temp isKindOfClass:[UIView class]]) {
                            
                            for (UIView *temp1 in temp.subviews) {
                                
                                if ([temp1 isKindOfClass:[UIImageView class]]) {
                                    UIImageView *imgView = (UIImageView *)temp1;
                                    imgView.hidden = YES;
                                    break;
                                    break;
                                }
                            }
                        }
                        
                    }
                }
                
                if (isBuilderListSelected) {
                    NSDictionary *dict = [pref objectForKey:strSaveMultiFilter];
                    
                    strBuilderList = [dict objectForKey:@"builders"];

                }
                else {
                    if (_arrBuilderList.count == 1) {
                        strBuilderList = [[_arrBuilderList firstObject] objectForKey:@"name"];
                    }
                    else {
                        strBuilderList = @"All";
                    }
                }
                
                
            }
            
            cell.lblAll.text = strBuilderList;
            cellSelectBuilder = cell;
            return cell;

        }
        else if (indexPath.row == 5) {
            SelectPropertyType *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectPropertyType"];
            
            if (cell == nil) {
                cell = [SelectPropertyType createCell];
                [cell.btnCommercial addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnResidential addTarget:self action:@selector(setPropertyType:) forControlEvents:UIControlEventTouchUpInside];
                
                if (_option == PropertyTypeResidentialOnly) {
                    
                    cell.btnResidential.selected = YES;
                    cell.btnCommercial.selected = NO;
                    
                    cell.btnCommercial.enabled = NO;
                    cell.btnResidential.enabled = YES;
                    
                    isShopSelected = NO;
                    isOfficeSelected = NO;
                    isServiceAppartmentSelected = NO;
                    
                    
                    cellSelectPropertyType.btnResidential.selected = YES;
                    cellSelectPropertyType.btnCommercial.selected = NO;
                    
                    isResidentialSelected = YES;
                    isCommercialSelected = NO;
                    
                }
                else if (_option == PropertyTypeCommenrcialOnly) {
                    
                    cell.btnResidential.selected = NO;
                    cell.btnCommercial.selected = YES;
                    
                    cell.btnCommercial.enabled = YES;
                    cell.btnResidential.enabled = NO;
                    
                    isShopSelected = NO;
                    isOfficeSelected = NO;
                    isServiceAppartmentSelected = NO;
                    
                    
                    cellSelectPropertyType.btnResidential.selected = YES;
                    cellSelectPropertyType.btnCommercial.selected = NO;
                    
                    isResidentialSelected = YES;
                    isCommercialSelected = NO;
                    
                }
                else {
                    
                    NSDictionary *dict = [pref objectForKey:strSaveMultiFilter];
                    NSString *str = [dict objectForKey:@"projectType"];
                    if (str == nil) {
                        cell.btnResidential.selected = YES;
                        cell.btnCommercial.selected = NO;
                        
                        cell.btnCommercial.enabled = YES;
                        cell.btnResidential.enabled = YES;
                        
                        isShopSelected = NO;
                        isOfficeSelected = NO;
                        isServiceAppartmentSelected = NO;
                        
                        
                        cellSelectPropertyType.btnResidential.selected = YES;
                        cellSelectPropertyType.btnCommercial.selected = NO;
                        
                        isResidentialSelected = YES;
                        isCommercialSelected = NO;
                        
                    }
                    else {
                        
                        if ([arrResidential containsObject:[[str componentsSeparatedByString:@","] firstObject]]) {
                            
                            cell.btnResidential.selected = YES;
                            cell.btnCommercial.selected = NO;
                            
                            cell.btnCommercial.enabled = YES;
                            cell.btnResidential.enabled = YES;
                            
                            isShopSelected = NO;
                            isOfficeSelected = NO;
                            isServiceAppartmentSelected = NO;
                            
                            
                            cellSelectPropertyType.btnResidential.selected = YES;
                            cellSelectPropertyType.btnCommercial.selected = NO;
                            
                            isResidentialSelected = YES;
                            isCommercialSelected = NO;
                            
                        }
                        else {
                            
                            cell.btnResidential.selected = NO;
                            cell.btnCommercial.selected = YES;
                            
                            cell.btnCommercial.enabled = YES;
                            cell.btnResidential.enabled = YES;
                            
                            isBHKSelected = NO;
                            is1BHKSelected = NO;
                            is2BHKSelected = NO;
                            is3BHKSelected = NO;
                            is4BHKSelected = NO;
                            is5BHKSelected = NO;
                            is5plusBHKSelected = NO;
                            
                            isFlatSelected = NO;
                            isBuilderFloorSelected = NO;
                            isPlotSelected = NO;
                            isHouseVillaSelected = NO;
                            
                            cellSelectPropertyType.btnResidential.selected = NO;
                            cellSelectPropertyType.btnCommercial.selected = YES;
                            
                            isResidentialSelected = NO;
                            isCommercialSelected = YES;
                            
                        }
                    }
                    
                }
                
                cell.colView.delegate = self;
                cell.colView.dataSource = self;
                
            }
            
            cellSelectPropertyType = cell;
            return cell;
        }
        else if (indexPath.row == 6) {
            BHK *cell = [tableView dequeueReusableCellWithIdentifier:@"BHK"];
            if (cell == nil) {
                cell = [BHK createCell];
                [cell.btn1BHK addTarget:self action:@selector(didTappedBHKSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn2BHK addTarget:self action:@selector(didTappedBHKSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn3BHK addTarget:self action:@selector(didTappedBHKSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn4BHK addTarget:self action:@selector(didTappedBHKSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn5BHK addTarget:self action:@selector(didTappedBHKSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn5PlusBHK addTarget:self action:@selector(didTappedBHKSelected:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if (!(isFlatSelected || isBuilderFloorSelected || isHouseVillaSelected || !isPlotSelected)) {
                
                cell.lbl5PlusBHK.textColor = cell.lbl5BHK.textColor = cell.lbl4BHK.textColor = cell.lbl3BHK.textColor = cell.lbl2BHK.textColor = cell.lbl1BHK.textColor = [UIColor grayColor];
    
                cell.btn5PlusBHK.enabled = cell.btn5BHK.enabled = cell.btn4BHK.enabled = cell.btn3BHK.enabled = cell.btn2BHK.enabled = cell.btn1BHK.enabled = NO;
                
                is1BHKSelected = is2BHKSelected = is3BHKSelected = is4BHKSelected = is5BHKSelected = is5plusBHKSelected = NO;
                
                
            }
            else {
                
                cell.lbl5PlusBHK.textColor = cell.lbl5BHK.textColor = cell.lbl4BHK.textColor = cell.lbl3BHK.textColor = cell.lbl2BHK.textColor = cell.lbl1BHK.textColor = [UIColor blackColor];
                
                cell.btn5PlusBHK.enabled = cell.btn5BHK.enabled = cell.btn4BHK.enabled = cell.btn3BHK.enabled = cell.btn2BHK.enabled = cell.btn1BHK.enabled = YES;
                
                
            }
            
            
            if (is1BHKSelected) {
                cell.img1BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.img1BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (is2BHKSelected) {
                cell.img2BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.img2BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (is3BHKSelected) {
                cell.img3BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.img3BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (is4BHKSelected) {
                cell.img4BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.img4BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (is5BHKSelected) {
                cell.img5BHK.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.img5BHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (is5plusBHKSelected) {
                cell.img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.img5PlusBHK.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            
            cellBHK = cell;
            return cell;

        }
        else if (indexPath.row >= 7 && indexPath.row <= 9) {
            
            PriceRange *cell = [tableView dequeueReusableCellWithIdentifier:@"PriceRange"];
            if (cell == nil) {
                cell = [PriceRange createCell];
                
                
  
            }
            
            NSDictionary *dict = [pref objectForKey:strSaveMultiFilter];
            
            if (indexPath.row == 7) {
                cell.lblTitle.text = @"Price Range";
                if ([dict objectForKey:@"max-p"] && [dict objectForKey:@"min-p"]) {
                    
                    CGFloat num_Max = [[dict objectForKey:@"max-p"] floatValue];
                    CGFloat num_Slide_Right_Value = num_Max / 100000.0;
                    
                    if (num_Slide_Right_Value < 100.0 && num_Slide_Right_Value > 25.0) {//For values in lakhs
                        cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dL", (int)num_Slide_Right_Value];
                        
                    }
                    else {
                        
                        cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", (int)(num_Slide_Right_Value/100)];
                    }
                    
                    cell.strUpprerLimit = [NSString stringWithFormat:@"%@", [dict objectForKey:@"max-p"]];
                    
                    CGFloat num_Min = [[dict objectForKey:@"min-p"] floatValue];
                    CGFloat num_Slide_Left_Value = num_Min / 100000.0;
                    
                     if (num_Slide_Left_Value < 100.0 && num_Slide_Left_Value >= 25.0) {
                        cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dL", (int)num_Slide_Left_Value];
                        
                    }
                    else {
                        
                        cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %dCr", (int)(num_Slide_Left_Value/100)];
                    }
                    cell.strLowerLimit = [NSString stringWithFormat:@"%@", [dict objectForKey:@"min-p"]];
                    
                    [cell.slider setMinValue:25.0 maxValue:500.0];
                    [cell.slider setLeftValue:num_Slide_Left_Value rightValue:num_Slide_Right_Value];
                }
                else {
                    
                    if (!isPriceRangeSelected) {
                        cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 25L"];
                        cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 5Cr"];
                        [cell.slider setMinValue:25.0 maxValue:500.0];
                        [cell.slider setLeftValue:25.0 rightValue:500.0];
                    }
                   
                }
                
                cellPriceRange = cell;
                [cell.slider addTarget:self action:@selector(priceRangeDidChange:) forControlEvents:UIControlEventValueChanged];
                
            }
            else if (indexPath.row == 8) {
                cell.lblTitle.text = @"Built Up Area (in sq. ft.)";
                if ([dict objectForKey:@"max_area_range"] && [dict objectForKey:@"min_area_range"]) {
                    
                    cell.lblUpperBound.text = [NSString stringWithFormat:@"%@ sqft", [dict objectForKey:@"max_area_range"]];
                    cell.strUpprerLimit = [NSString stringWithFormat:@"%@", [dict objectForKey:@"max_area_range"]];
                    
                    cell.lblLowerBound.text = [NSString stringWithFormat:@"%@ sqft", [dict objectForKey:@"min_area_range"]];
                    cell.strLowerLimit = [NSString stringWithFormat:@"%@", [dict objectForKey:@"min_area_range"]];
                    
                    [cell.slider setMinValue:250.0 maxValue:8000.0];
                    [cell.slider setLeftValue:[[dict objectForKey:@"min_area_range"] floatValue] rightValue:[[dict objectForKey:@"max_area_range"] floatValue]];
                }
                else {
                    
                    if (!isBuildUpAreaSelected) {
                        cell.lblLowerBound.text = [NSString stringWithFormat:@"250 sqft"];
                        cell.lblUpperBound.text = [NSString stringWithFormat:@"8000 sqft"];
                        [cell.slider setMinValue:250.0 maxValue:8000.0];
                        [cell.slider setLeftValue:250.0 rightValue:8000.0];
                    }
                }
                [cell.slider addTarget:self action:@selector(builtUpAreaDidChange:) forControlEvents:UIControlEventValueChanged];
                cellBuiltUp = cell;
                
            }
            else if (indexPath.row == 9) {
                
                cell.lblTitle.text = @"Price (psf)";
                if ([dict objectForKey:@"max_psf"] && [dict objectForKey:@"min_psf"]) {
                    cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"max_psf"]];
                    cell.strUpprerLimit = [NSString stringWithFormat:@"%@", [dict objectForKey:@"max_psf"]];
                    
                    cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 %@", [dict objectForKey:@"min_psf"]];
                    cell.strLowerLimit = [NSString stringWithFormat:@"%@", [dict objectForKey:@"min_psf"]];
                    
                    [cell.slider setMinValue:500.0 maxValue:20000.0];
                    [cell.slider setLeftValue:[[dict objectForKey:@"min_psf"] floatValue] rightValue:[[dict objectForKey:@"max_psf"] floatValue]];
                }
                else {
                    
                    if (!isPricePSFRangeSelected) {
                        cell.lblLowerBound.text = [NSString stringWithFormat:@"\u20B9 500"];
                        cell.lblUpperBound.text = [NSString stringWithFormat:@"\u20B9 20000"];
                        [cell.slider setMinValue:500.0 maxValue:20000.0];
                        [cell.slider setLeftValue:500.0 rightValue:20000.0];
                    }
                }
                
                [cell.slider addTarget:self action:@selector(pricePSFDidChange:) forControlEvents:UIControlEventValueChanged];
                cellPricePSF = cell;
            }

            
            
            return cell;
        }
        else if (indexPath.row == 10) {
            Aminities *cell = [tableView dequeueReusableCellWithIdentifier:@"Aminities"];
            
            if (cell == nil) {
                cell = [Aminities createCell];
                [cell.btnGas addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnLift addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnParking addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnGym addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnSecurity addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnWardrobe addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnWifi addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnFurnished addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnAC addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnSwimmingPool addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnHomeAutomation addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnPowerBackup addTarget:self action:@selector(didTappedAminities:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if (isGasSelected) {
                cell.imgGas.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgGas.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isLiftSelected) {
                cell.imgLift.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgLift.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isParkingSelected) {
                cell.imgParking.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgParking.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isGymSelected) {
                cell.imgGym.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgGym.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isSecuritySelected) {
                cell.imgSecurity.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgSecurity.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isTreatedWaterSupplySelected) {
                cell.imgWardrobe.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                 cell.imgWardrobe.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isWiFiSelected) {
                cell.imgWifi.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                 cell.imgWifi.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isPartyHallSelected) {
                cell.imgFurnished.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgFurnished.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isACSelected) {
                cell.imgAC.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgAC.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isSwimmingPoolSelected) {
                cell.imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgswimmingPool.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isHomeAutomationSelected) {
                cell.imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgHomeAutomation.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
            if (isPowerBackupSelected) {
                cell.imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_green"];
            }
            else {
                cell.imgPowerBackup.image = [UIImage imageNamed:@"Checkbox_gray"];
            }
            
                cellAminities = cell;
            
            return cell;
        }
        else {
            return nil;
        }

    }
    else if (tableView == cellSortedBy.tblPrice) {
        static NSString *str = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
            cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:15.0];
            
        }
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Low to high";
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = @"High to low";
        }
        else if (indexPath.row == 2) {
            cell.textLabel.text = @"Cancel";
        }
        
        cell.textLabel.layer.borderWidth = 1.0;
        cell.textLabel.layer.borderColor = [UIColor grayColor].CGColor;
        
        return cell;

    }
    else if (tableView == cellSortedBy.tblPricePSF) {
        static NSString *str = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
            cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:15.0];
            
        }
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Low to high";
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = @"High to low";
        }
        else if (indexPath.row == 2) {
            cell.textLabel.text = @"Cancel";
        }
        cell.textLabel.layer.borderWidth = 1.0;
        cell.textLabel.layer.borderColor = [UIColor grayColor].CGColor;
        
        return cell;

    }
    else {
        static NSString *str = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
            cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:15.0];
            
        }
        cell.textLabel.textAlignment = NSTextAlignmentCenter;

        if (indexPath.row == 0) {
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            
            cell.textLabel.text = @" Earliest to latest";
            [cell.textLabel sizeToFit];
        }
        else if (indexPath.row == 1) {
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.textLabel.text = @" Latest to earliest";
            [cell.textLabel sizeToFit];
            
        }
        else if (indexPath.row == 2) {
            cell.textLabel.text = @"Cancel";
        }
        cell.textLabel.layer.borderWidth = 1.0;
        cell.textLabel.layer.borderColor = [UIColor grayColor].CGColor;
        
        return cell;

    }

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        if (indexPath.row == 4) {
           // isBuilderSelected = YES;
            
            if (_arrBuilderList.count > 1) {
                SelectBuilderVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectBuilderVC"];
                obj.arrBuilderList = _arrBuilderList;
                [self presentViewController:obj animated:YES completion:nil];
            }
        }
        [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] animated:YES];
    }
    else if (tableView == cellSortedBy.tblPrice) {
        if (indexPath.row == 0) {
            [cellSortedBy.btnPrice setTitle:@"Low to high" forState:UIControlStateNormal];
            [self setButtonSelected:cellSortedBy.btnPrice];
            [cellSortedBy.btnPrice sizeToFit];
            isPriceLowToHighSlected = YES;
            isPriceHighToLowSlected = NO;
        }
        else if (indexPath.row == 1) {
            [cellSortedBy.btnPrice setTitle:@"High to low" forState:UIControlStateNormal];
            [self setButtonSelected:cellSortedBy.btnPrice];
            [cellSortedBy.btnPrice sizeToFit];
            isPriceLowToHighSlected = NO;
            isPriceHighToLowSlected = YES;
        }
        else if (indexPath.row == 2) {
            [cellSortedBy.btnPrice setTitle:@"Price" forState:UIControlStateNormal];
            [self setButtonUnselected:cellSortedBy.btnPrice];
            isPriceLowToHighSlected = NO;
            isPriceHighToLowSlected = NO;
        }
        
        cellSortedBy.tblPrice.hidden = YES;
    }
    else if (tableView == cellSortedBy.tblPricePSF) {
        
        if (indexPath.row == 0) {
            [cellSortedBy.btnPricePSF setTitle:@"Low to high" forState:UIControlStateNormal];
            [self setButtonSelected:cellSortedBy.btnPricePSF];
            [cellSortedBy.btnPricePSF sizeToFit];
            isPricePSFLowToHighSelected = YES;
            isPricePSFHighToLowSelected = NO;
        }
        else if (indexPath.row == 1) {
            [cellSortedBy.btnPricePSF setTitle:@"High to low" forState:UIControlStateNormal];
            [self setButtonSelected:cellSortedBy.btnPricePSF];
            [cellSortedBy.btnPricePSF sizeToFit];
            isPricePSFHighToLowSelected = YES;
            isPricePSFLowToHighSelected = NO;
        }
        else if (indexPath.row == 2) {
            [cellSortedBy.btnPricePSF setTitle:@"Price PSF" forState:UIControlStateNormal];
            [self setButtonUnselected:cellSortedBy.btnPricePSF];
            isPricePSFHighToLowSelected = NO;
            isPricePSFLowToHighSelected = NO;
            
        }
        cellSortedBy.tblPricePSF.hidden = YES;
        
    }
    else if (tableView == cellSortedBy.tblPossession) {
        if (indexPath.row == 0) {
            [cellSortedBy.btnPossession setTitle:@"Earliest to latest" forState:UIControlStateNormal];
            [self setButtonSelected:cellSortedBy.btnPossession];
            [cellSortedBy.btnPossession sizeToFit];
            isPossessionLowToHighSelected = YES;
            isPossessionHighToLowSelected = NO;

        }
        else if (indexPath.row == 1) {
            [cellSortedBy.btnPossession setTitle:@"Latest to earliest" forState:UIControlStateNormal];
            [self setButtonSelected:cellSortedBy.btnPossession];
            [cellSortedBy.btnPossession sizeToFit];
            isPossessionLowToHighSelected = NO;
            isPossessionHighToLowSelected = YES;
        }
        else if (indexPath.row == 2) {
            [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
            [self setButtonUnselected:cellSortedBy.btnPossession];
            
            isPossessionLowToHighSelected = NO;
            isPossessionHighToLowSelected = NO;
        }
        cellSortedBy.tblPossession.hidden = YES;
    }
}



#pragma mark - Collection View Delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(90.f, 90.f);
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (_option == PropertyTypeResidentialOnly) {
        return arrResidential.count;
    }
    else if (_option == PropertyTypeCommenrcialOnly) {
        return arrCommercial.count;
    }
    else {
        
        if (isResidentialSelected && !isCommercialSelected) {
            return arrResidential.count;
        }
        else {
            return arrCommercial.count;
        }
        
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PropertyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"propertyCell" forIndexPath:indexPath];
    
    if (_option == PropertyTypeResidentialOnly) {
        
        if (indexPath.row == 0) {
            
            if (isFlatSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
            }
        }
        else if (indexPath.row == 1) {
            
            if (isBuilderFloorSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
            }
        }
        else if (indexPath.row == 2) {
            
            if (isPlotSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
            }
        }
        else {
            
            if (isHouseVillaSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
            }
        }
        
        cell.lblTitle.text = [arrResidential objectAtIndex:indexPath.row];

    }
    else if (_option == PropertyTypeCommenrcialOnly) {
        
        if (indexPath.row == 0) {
            
            if (isShopSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconUnselected objectAtIndex:indexPath.row]];
            }
        }
        else if (indexPath.row == 1) {
            
            if (isOfficeSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconUnselected objectAtIndex:indexPath.row]];
            }
        }
        else {
            
            if (isServiceAppartmentSelected) {
                cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconSelected objectAtIndex:indexPath.row]];
            }
            else {
                cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconUnselected objectAtIndex:indexPath.row]];
            }
        }
        
        cell.lblTitle.text = [arrCommercial objectAtIndex:indexPath.row];
        
    }
    else {
        if (isResidentialSelected && !isCommercialSelected) {
            
            if (indexPath.row == 0) {
                
                if (isFlatSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
                }
            }
            else if (indexPath.row == 1) {
                
                if (isBuilderFloorSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
                }
            }
            else if (indexPath.row == 2) {
                
                if (isPlotSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
                }
            }
            else {
                
                if (isHouseVillaSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrResidentialIconUnSelected objectAtIndex:indexPath.row]];
                }
            }
            
            cell.lblTitle.text = [arrResidential objectAtIndex:indexPath.row];
        }
        else {
            
            if (indexPath.row == 0) {
                
                if (isShopSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconUnselected objectAtIndex:indexPath.row]];
                }
            }
            else if (indexPath.row == 1) {
                
                if (isOfficeSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconUnselected objectAtIndex:indexPath.row]];
                }
            }
            else {
                
                if (isServiceAppartmentSelected) {
                    cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconSelected objectAtIndex:indexPath.row]];
                }
                else {
                    cell.imgTitle.image = [UIImage imageNamed:[arrCommercialIconUnselected objectAtIndex:indexPath.row]];
                }
            }
            
            cell.lblTitle.text = [arrCommercial objectAtIndex:indexPath.row];
        }

    }
    
        return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_option == PropertyTypeResidentialOnly) {
        if (indexPath.row == 0) {
            isFlatSelected = !isFlatSelected;
        }
        else if (indexPath.row == 1) {
            isBuilderFloorSelected = !isBuilderFloorSelected;
        }
        else if (indexPath.row == 2) {
            isPlotSelected = !isPlotSelected;
        }
        else {
            isHouseVillaSelected = !isHouseVillaSelected;
        }
        
        isPropertyTypeSelected =  isFlatSelected | isBuilderFloorSelected | isHouseVillaSelected | isPlotSelected;
    }
    else if (_option == PropertyTypeCommenrcialOnly) {
        if (indexPath.row == 0) {
            isShopSelected = !isShopSelected;
        }
        else if (indexPath.row == 1) {
            isOfficeSelected = !isOfficeSelected;
        }
        else  {
            isServiceAppartmentSelected = !isServiceAppartmentSelected;
        }
        
        isPropertyTypeSelected =  isShopSelected | isOfficeSelected | isServiceAppartmentSelected;
    }
    else {
        if (isResidentialSelected && !isCommercialSelected) {
            
            if (indexPath.row == 0) {
                isFlatSelected = !isFlatSelected;
            }
            else if (indexPath.row == 1) {
                isBuilderFloorSelected = !isBuilderFloorSelected;
            }
            else if (indexPath.row == 2) {
                isPlotSelected = !isPlotSelected;
            }
            else {
                isHouseVillaSelected = !isHouseVillaSelected;
            }
            isPropertyTypeSelected =  isFlatSelected | isBuilderFloorSelected | isHouseVillaSelected | isPlotSelected;
        }
        else {
            
            if (indexPath.row == 0) {
                isShopSelected = !isShopSelected;
            }
            else if (indexPath.row == 1) {
                isOfficeSelected = !isOfficeSelected;
            }
            else  {
                isServiceAppartmentSelected = !isServiceAppartmentSelected;
            }
            isPropertyTypeSelected =  isShopSelected | isOfficeSelected | isServiceAppartmentSelected;
        }
    }
    
    [cellSelectPropertyType.colView reloadData];
    [tblView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:6 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Observer Mathods

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
   
    UIButton *btn = (UIButton *)object;
    [btn removeObserver:self forKeyPath:keyPath context:context];
    
    
    if ([arrObservedValues containsObject:btn]) {
        [arrObservedValues removeObject:btn];
    }
    
    if ([keyPath isEqualToString:@"selected"]) {
        
        if (btn.selected) {
            [self setButtonSelected:btn];
            
        }
        else {
            [self setButtonUnselected:btn];
        }
    }
}

#pragma mark - Auxillary Mathods
- (void)setButtonUnselected:(UIButton *)btn {
    
    if (btn != cellSortedBy.btnPossession || !isReadyToMoveInSelected) {
        btn.selected = NO;
        btn.layer.borderWidth = 1.0;
        btn.layer.borderColor = [UIColor whiteColor].CGColor;
        btn.titleLabel.textColor = [UIColor
                                    blackColor];
        
    }
    else {
        [cellSortedBy.btnPossession setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
        cellSortedBy.btnPossession.layer.borderColor = [UIColor whiteColor].CGColor;
    }
}

- (void)setButtonSelected:(UIButton *)btn {
    
    
    if (btn != cellSortedBy.btnPossession || !isReadyToMoveInSelected) {
        btn.selected = YES;
        btn.layer.borderWidth = 1.0;
        btn.layer.borderColor = [UIColor
                                 colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0].CGColor;
        
        btn.titleLabel.textColor = [UIColor
                                    colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
    }
    else {
        [cellSortedBy.btnPossession setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
        cellSortedBy.btnPossession.layer.borderColor = [UIColor whiteColor].CGColor;
    }
}

- (void)resetAllFlags:(BOOL)yesOrNo {
    
    isNewLaunchSelected = !yesOrNo;
    isUnderConstructionSelected = !yesOrNo;
    isReadyToMoveInSelected = !yesOrNo;
    
    
    isNewestSelected = !yesOrNo;
    isPriceSelected = !yesOrNo;
    isPriceHighToLowSlected = !yesOrNo;
    isPriceLowToHighSlected = !yesOrNo;
    isPricePSFSelected = !yesOrNo;
    isPricePSFHighToLowSelected = !yesOrNo;
    isPricePSFLowToHighSelected = !yesOrNo;
    isRatingSelected = !yesOrNo;
    isPossessionSelected = !yesOrNo;
    isPossessionLowToHighSelected = !yesOrNo;
    isPossessionHighToLowSelected = !yesOrNo;
    
    cellSortedBy.btnPossession.enabled = YES;
    [cellSortedBy.btnPossession setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    isInfraSelected = !yesOrNo;
    isNeedsSelected = !yesOrNo;
    isLifeStyleSelected = !yesOrNo;
    isReturnSelected = !yesOrNo;
    
    [cellSortedBy.btnPrice setTitle:@"Price" forState:UIControlStateNormal];
    [cellSortedBy.btnPossession setTitle:@"Possession" forState:UIControlStateNormal];
    [cellSortedBy.btnPricePSF setTitle:@"Price PSF" forState:UIControlStateNormal];
    
    
    isDiscount_OffersSelected = !yesOrNo;
    
    is6MSelected = !yesOrNo;
    is1YRSelected = !yesOrNo;
    is2YRSelected = !yesOrNo;
    is3YRSeleted = !yesOrNo;
    is4YRSelected = !yesOrNo;
    is5PlusYRSelected = !yesOrNo;
    
    isBuilderListSelected = !yesOrNo;
    strBuilderList = @"All";
    
    
//    isPropertyTypeSelected = !yesOrNo;
//    isResidentialSelected = !yesOrNo;
    isCommercialSelected = !yesOrNo;
    isFlatSelected = !yesOrNo;
    isBuilderFloorSelected = !yesOrNo;
    isPlotSelected = !yesOrNo;
    isHouseVillaSelected = !yesOrNo;
    isShopSelected = !yesOrNo;
    isOfficeSelected = !yesOrNo;
    isServiceAppartmentSelected = !yesOrNo;
    [cellSelectPropertyType.colView reloadData];
    
    
    
    isPriceRangeSelected = !yesOrNo;
    isBuildUpAreaSelected = !yesOrNo;
    isPricePSFRangeSelected = !yesOrNo;
    
    isBHKSelected = !yesOrNo;
    is1BHKSelected = !yesOrNo;
    is2BHKSelected = !yesOrNo;
    is3BHKSelected = !yesOrNo;
    is4BHKSelected = !yesOrNo;
    is5BHKSelected = !yesOrNo;
    is5plusBHKSelected = !yesOrNo;
    
    
    isAmenitiesSelected = !yesOrNo;
    isGasSelected = !yesOrNo;
    isLiftSelected = !yesOrNo;
    isParkingSelected = !yesOrNo;
    isGymSelected = !yesOrNo;
    isSecuritySelected = !yesOrNo;
    isTreatedWaterSupplySelected = !yesOrNo;
    isWiFiSelected = !yesOrNo;
    isPartyHallSelected = !yesOrNo;
    isACSelected = !yesOrNo;
    isSwimmingPoolSelected = !yesOrNo;
    isHomeAutomationSelected = !yesOrNo;
    isPowerBackupSelected = !yesOrNo;

}


- (void)getSelectedBuilder:(NSNotification *)info {
    
    NSMutableArray *arr = [NSMutableArray arrayWithArray:info.object];
    if (arr.count) {
        isBuilderListSelected = YES;
        strBuilderList = [[NSMutableArray arrayWithArray:[arr valueForKeyPath:@"@distinctUnionOfObjects.name"]] componentsJoinedByString:@","];
    }
    else {
        isBuilderListSelected = NO;
        strBuilderList = @"All";//[[_arrBuilderList firstObject] objectForKey:@"name"];
    }
    
    [tblView reloadData];
}

@end
