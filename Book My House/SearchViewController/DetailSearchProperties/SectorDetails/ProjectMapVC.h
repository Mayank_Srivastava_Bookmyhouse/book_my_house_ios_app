//
//  ProjectMapVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/16/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectorCell.h"
#import "FooterCrouselCell.h"
#import "DWBubbleMenuButton.h"
#import "Header.h"
#import "NotificationAlert.h"
#import "SectorDetailView.h"
#import "ProjectDetailVC.h"


typedef NS_ENUM(NSUInteger, SectorDetailsAccessOption) {
    SectorDetailsAccessOptionSearchDetailsForLocality,
    SectorDetailsAccessOptionSearchDetailsForSector,
    SectorDetailsAccessOptionBuilder,
    SectorDetailsAccessOptionCatagory,
    SectorDetailsAccessOptionPush,
    SectorDetailsAccessOptionMisc,
};


@interface ProjectMapVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, GMSMapViewDelegate>
@property (assign, nonatomic) SectorDetailsAccessOption option;
@property (strong, nonatomic) NSDictionary *dictSourse;
@property (strong, nonatomic) NSString *strTitle;
@property (strong, nonatomic) GMSPath *path;

@property (strong, nonatomic) NSArray *arrLegand;
@property (strong, nonatomic) NSArray *arrSourse;
@property (assign, nonatomic) NSUInteger index;
@property (copy, nonatomic) NSDictionary *dictInfo;
@property (strong, nonatomic) NSDictionary *dictFilter;
- (void)loadLocalityInfo;
@end
