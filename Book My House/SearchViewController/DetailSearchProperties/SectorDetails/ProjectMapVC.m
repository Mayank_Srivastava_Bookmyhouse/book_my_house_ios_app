//
//  ProjectMapVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/16/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ProjectMapVC.h"
#import "MultiFilterVC.h"
#import "ProjectListVC.h"

@import GoogleMaps;

static NSString * const strLoginRequest                    = @"Please login in to select your favorite unit";


@interface ProjectMapVC (){
    
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnFilter;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UISegmentedControl *segmentControl;
    __weak IBOutlet GMSMapView *mapView;
    __weak IBOutlet UIButton *btnZoomIn;
    __weak IBOutlet UIButton *btnZoomOut;
    __weak IBOutlet UICollectionView *colView;
    __weak IBOutlet UIButton *btnRight;
    __weak IBOutlet UIButton *btnLeft;
    
    
    NSMutableArray *arrResponseData, *arrMarkers;
    NSUInteger zoom;
    NSArray *arrSegmentControl, *arrColorCode, *arrMarkerImage;
    NSUserDefaults *pref;
    DWBubbleMenuButton *downMenuButton;
    NotificationAlert *notifAlert;
    GMSCameraPosition *camera;
    GMSCameraUpdate *updatedCamera;
    GMSMutablePath *path, *heatMapPath;
    GMSPolygon *heatMapRectangle, *polygonCurrentProject;
    CGFloat latitude, longitude;
    
    SectorDetailView *detailView;
    NSInteger indexFavorite;
    
}
- (IBAction)actionListeners:(id)sender;

@end

@implementation ProjectMapVC

- (void)loadView {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadLocalityInfo) name:kProjectMapMultifilter object:nil];
    arrMarkers = [NSMutableArray array];
    
    arrColorCode = @[[UIColor colorWithRed:100.0/255.0 green:198.0/255.0 blue:125.0/255.0 alpha:1.0], [UIColor colorWithRed:144.0/255.0 green:201.0/255.0 blue:122.0/255.0 alpha:1.0], [UIColor colorWithRed:254.0/255.0 green:229.0/255.0 blue:129.0/255.0 alpha:1.0], [UIColor colorWithRed:250.0/255.0 green:185.0/255.0 blue:127.0/255.0 alpha:1.0], [UIColor colorWithRed:239.0/255.0 green:103.0/255.0 blue:107.0/255.0 alpha:1.0]];
    

    arrMarkerImage = @[[UIImage imageNamed:@"marker_darkgreen"], [UIImage imageNamed:@"marker_lightgreen"], [UIImage imageNamed:@"marker_yellow"], [UIImage imageNamed:@"marker_orange"], [UIImage imageNamed:@"marker_red"]];
    
    zoom = 15;
    
    [super loadView];
    
    pref = [NSUserDefaults standardUserDefaults];
    
    arrResponseData = [NSMutableArray array];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
    [self loadLocalityInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [self updateScreenSettings];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [downMenuButton removeFromSuperview];
    downMenuButton = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Screen Settings
- (void)screenSettings {

    lblTitle.text = _strTitle;
    [colView registerClass:[SectorCell class] forCellWithReuseIdentifier:@"SectorCell"];
    [colView registerNib:[UINib nibWithNibName:@"SectorCell" bundle:nil] forCellWithReuseIdentifier:@"SectorCell"];
    
    [colView registerClass:[FooterCrouselCell class] forCellWithReuseIdentifier:@"NoProjectCell"];
    [colView registerNib:[UINib nibWithNibName:@"NoProjectCell" bundle:nil] forCellWithReuseIdentifier:@"NoProjectCell"];
    
    [self.view setNeedsDisplay];
    
    if (_option == SectorDetailsAccessOptionSearchDetailsForLocality || _option == SectorDetailsAccessOptionSearchDetailsForSector) {
        [self drawMicromarketPolygon];
    }
    else {
        [self drawMicromarketPolygon];
    }
    
}

- (void)updateScreenSettings {
    
    UIView *view = [self createHomeButtonView];
    
    CGRect frame = CGRectMake(CGRectGetMaxX(self.view.frame) - CGRectGetWidth(view.frame) - 10, CGRectGetMaxY(segmentControl.frame) + 10, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
    
    downMenuButton = [[DWBubbleMenuButton alloc] initWithFrame:frame
                                            expansionDirection:DirectionDown];
    
    downMenuButton.homeButtonView = view;
    downMenuButton.buttonSpacing = 0;
    [downMenuButton addButtons:[self createDemoButtonArray]];
    
    [self.view addSubview:downMenuButton];
    [self.view bringSubviewToFront:downMenuButton];
    
    [downMenuButton showButtons];

}

- (UIView *)createHomeButtonView {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 30.0)];
    view.backgroundColor = [UIColor whiteColor];
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 20, 20)];
    img.image = [UIImage imageNamed:@"Down-indicator"];
    
    [view addSubview:img];
    img.center = view.center;
    
    
    return view;
}

- (NSArray *)createDemoButtonArray {
    NSMutableArray *buttonsMutable = [NSMutableArray array];
    
    int i = 0;
    for (NSString *title in [arrSegmentControl objectAtIndex:segmentControl.selectedSegmentIndex]) {
        
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        button.frame = CGRectMake(0.f, 0.f, 60.f, 30.f);
        [button addSubview:[self addLabelWithText:title andFrame:button.frame]];
        button.backgroundColor = [arrColorCode objectAtIndex:i];
        button.clipsToBounds = YES;
        button.tag = i++;
        [buttonsMutable addObject:button];
    }
    
    
    return [buttonsMutable copy];
}

- (UILabel *)addLabelWithText:(NSString *)str andFrame:(CGRect)frame{
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.font = [UIFont fontWithName:@"PT Sans" size:10.0];
    titleLabel.text = str;
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    return titleLabel;
}


- (void)updateMapWithPlaceholder {
  
    int i = 0;
    for (NSDictionary *dict in arrResponseData) {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake([[[dict objectForKey:@"project_cord"] valueForKeyPath:@"@avg.lat"] doubleValue], [[[dict objectForKey:@"project_cord"] valueForKeyPath:@"@avg.long"] doubleValue]);
        
        
        
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.icon = [self setMarkerCodes:[NSIndexPath indexPathForItem:i++ inSection:0]];
        marker.title = [[dict objectForKey:@"display_name"] uppercaseString];
        [arrMarkers addObject:marker];
        
    }
    
    [arrMarkers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        GMSMarker *marker = (GMSMarker *)obj;
        marker.map = mapView;
        
    }];
    
    if (camera == nil) {
        camera = [GMSCameraPosition cameraWithLatitude:[[arrResponseData valueForKeyPath:@"@avg.latitude"] doubleValue]
                                             longitude:[[arrResponseData valueForKeyPath:@"@avg.longitude"] doubleValue]
                                                  zoom:zoom
                                               bearing:0
                                          viewingAngle:0];
        [mapView setCamera:camera];
    }
    else {
        updatedCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake([[arrResponseData valueForKeyPath:@"@avg.latitude"] doubleValue], [[arrResponseData valueForKeyPath:@"@avg.longitude"] doubleValue])
                                              zoom:zoom];
        
        [mapView animateWithCameraUpdate:updatedCamera];
    }
    
    colView.delegate = self;
    colView.dataSource = self;
    mapView.delegate = self;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"propertyList"]) {
        ProjectListVC *obj = segue.destinationViewController;
        obj.option = DetailFeaturedDeveloperAccessOptionSearchScreen;
        obj.arrSourse = [NSArray arrayWithArray:arrResponseData];
        obj.owner = self;
        obj.strCityId = [[_dictInfo objectForKey:@"city"] objectForKey:@"city_id"];
    }
}




#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnCancel) {
        
        if (_option == SectorDetailsAccessOptionSearchDetailsForLocality || _option ==
            SectorDetailsAccessOptionSearchDetailsForSector) {
            [self.navigationController popViewControllerAnimated:YES];
            [pref removeObjectForKey:strSaveMultiFilter];
        }
        else {
            for (UIViewController *viewController in self.navigationController.viewControllers) {
                if ([viewController isKindOfClass:[ProjectListVC class]]) {
                    ProjectListVC *obj = (ProjectListVC *)viewController;
                    [self.navigationController popViewControllerAnimated:YES];
                    [obj callingAPI];
                    break;
                }
                
            }
        }
        
        
        //
    }
    else if (sender == btnMenu) {
        
    }
    else if (sender == btnFilter) {
        MultiFilterVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"MultiFilterVC"];
        obj.accessOption = AccessOptionProjectMap;
        [self presentViewController:obj animated:YES completion:nil];
    }
    else if (sender == segmentControl) {
        [downMenuButton removeFromSuperview];
        downMenuButton = nil;
        [self updateScreenSettings];
        
        if (_option == TabularSearchOptionNormal) {
            
            [self updateAllMarkers];
            
        }
        else {
           
            [self updateAllMarkers];
        }
        
    }
    else if (sender == btnLeft) {
        
        NSArray *visibleItems = [colView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        
        if (prevItem.item >= 0) {
            [colView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        
    }
    else if (sender == btnRight) {
        NSArray *visibleItems = [colView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSInteger num = [colView numberOfItemsInSection:0];
        
        if (num > nextItem.item) {
            [colView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        
    }
    else if (sender == btnZoomIn  || sender == btnZoomOut) {
        
        if (zoom > 24) {
            zoom = 24;
            return;
        }
        
        if (zoom < 12) {
            zoom = 12;
            return;
        }
        
        if (sender == btnZoomOut) {
            [mapView animateToZoom:--zoom];
        }
        else if (sender == btnZoomIn) {
            [mapView animateToZoom:++zoom];
        }
    }
}


- (void)didClickedGetAlerts {
    
    NSString *strEmail = [pref objectForKey:strSaveUserEmail];
    if (strEmail == nil) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.view.window makeToast:strLoginForAlert duration:duration position:CSToastPositionCenter];
    }
    else {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:strEmail forKey:@"email"];
        [dict setObject:[[_dictInfo objectForKey:@"city"] objectForKey:@"city_id"] forKey:@"city"];
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI setAlertForCurrentLocation:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                
                notifAlert = [NotificationAlert createView];
                notifAlert.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                [notifAlert.btnClose addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:notifAlert];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButton)];
                [notifAlert addGestureRecognizer:tap];
                
                [UIView animateWithDuration:0.25
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     notifAlert.frame = self.view.frame;
                                 } completion:^(BOOL finished) {
                                     
                                     
                                     
                                 }];
            }
            else {
                [self.view makeToast:strAlertNotSaved duration:duration position:CSToastPositionCenter];
            }
        }];
    }
}

- (void)closeButton {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         notifAlert.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [notifAlert removeFromSuperview];
                         notifAlert = nil;
                     }];
    
}

- (void)didTapedOffers:(UIButton *)btn {
    
    if (detailView) {
        [detailView removeFromSuperview];
        detailView = nil;
    }
    detailView = [SectorDetailView createView];
    detailView.frame = self.view.frame;
    
    [self.view addSubview:detailView];
}

- (void)didTappedFavorite:(UIButton *)sender {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            indexFavorite = sender.tag;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId) {
            
            [dict setObject:strUserId forKey:@"user_id"];
            [dict setObject:[[arrResponseData objectAtIndex:indexFavorite] objectForKey:@"project_id"] forKey:@"id"];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kFavoriteProjectCarousel object:nil];
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI markAsFavorite:dict withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    [self loadLocalityInfo];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    
                }
            }];
            
        }
        else {
            [[[AppDelegate share] window] makeToast:strLoginRequest duration:duration position:CSToastPositionCenter];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTappedFavorite:) name:kFavoriteProjectCarousel object:nil];
            
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionFavoriteProjectCrousel;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didTappedFavorite:) fromClass:self];
    }
}


#pragma mark - API Actions
- (void)loadLocalityInfo {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]){
        [[AppDelegate share] disableNoInternetAlert];
    
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
//        for (NSString *key in _dictSourse) {
//            
//            [dict setObject:[_dictSourse objectForKey:key] forKey:key];
//            
//        }
//        
//        for (NSString *key in _dictInfo) {
//            [dict setObject:[_dictInfo objectForKey:key] forKey:key];
//        }
        
        
        if ([_dictSourse objectForKey:@"builder_id"]) {
            [dict setObject: [_dictSourse objectForKey:@"builder_id"] forKey:@"builder_id"];
        }
        
        if ([_dictSourse objectForKey:@"city_id"]) {
            [dict setObject: [_dictSourse objectForKey:@"city_id"] forKey:@"city_id"];
        }
        
        if ([_dictSourse objectForKey:@"type"]) {
            [dict setObject:[_dictSourse objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictSourse objectForKey:@"p_type"]) {
            [dict setObject:[_dictSourse objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictSourse  objectForKey:@"location"]) {
            [dict setObject:[_dictSourse  objectForKey:@"location"] forKey:@"location"];
        }
        
        if ([_dictInfo objectForKey:@"type"]) {
            [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictInfo objectForKey:@"p_type"]) {
            [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictInfo objectForKey:@"city_id"]) {
            [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
        }
        
        
        if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
            [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sublocation_id"];
            
        }
        
        if ([_dictSourse objectForKey:@"sublocation_id"]) {
            [dict setObject:[_dictSourse objectForKey:@"sub_location_id"] forKey:@"sub_location_id"];
        }
        
        if ([_dictSourse objectForKey:@"special_category"]) {
            [dict setObject:[_dictSourse objectForKey:@"special_category"] forKey:@"special_category"];
        }
        
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
        }
        
        NSDictionary *dictReq = [pref objectForKey:strSaveMultiFilter];
        if (dictReq) {
            [dict setObject:dictReq forKey:@"filter"];
        }
        
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getProjectInfoForMap:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            
            if (isSuccess) {
                
                if (arrResponseData.count) {
                    [arrResponseData removeAllObjects];
                }
                else {
                    
                }
                
                for (GMSMarker *temp in arrMarkers) {
                    temp.map = nil;
                }
                
                [arrMarkers removeAllObjects];
                [arrResponseData addObjectsFromArray:[data objectForKey:@"data"]];
                
                if (arrResponseData.count < 2) {
                    btnFilter.hidden = YES;
                }
                else {
                    btnFilter.hidden = NO;
                }
                
                
                [colView reloadData];
                arrSegmentControl = [_arrLegand copy];
                
                [downMenuButton removeFromSuperview];
                downMenuButton = nil;
                
                if (arrResponseData.count == 1) {
                    btnLeft.hidden = btnRight.hidden = YES;
                }
                else {
                    
                    if (colView.delegate == nil) {
                        btnLeft.hidden = YES;
                        btnRight.hidden = NO;
                    }
                    
                }
                
                [self updateScreenSettings];
                [self updateMapWithPlaceholder];
                
                
            }
            else {
                [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadLocalityInfo) fromClass:self];
    }
}

#pragma mark - CollectionView Mathods
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return (CGRectGetWidth(self.view.frame) - 300);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, (CGRectGetWidth(self.view.frame) - 300)/2, 0, (CGRectGetWidth(self.view.frame) - 300)/2);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(300, 150);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (section == 0) {
        return arrResponseData.count ? arrResponseData.count:1;
    }
    else if (section == 1) {
        return 1;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrResponseData.count) {
        
        NSDictionary *dict = [arrResponseData objectAtIndex:indexPath.row];
        static NSString *strId = @"SectorCell";
        SectorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strId forIndexPath:indexPath];
        
        cell.lblNeed.layer.cornerRadius = cell.lblInfra.layer.cornerRadius = cell.lblLifestyle.layer.cornerRadius = 5.0;
        cell.lblInfra.clipsToBounds = cell.lblNeed.clipsToBounds = cell.lblLifestyle.clipsToBounds = YES;
        

        [cell.imgBg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"banner_img"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        if (![[dict objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
            cell.lblBuilderName.text = [dict objectForKey:@"builder_name"];
        }
        else {
            cell.lblBuilderName.text = @"";
        }
        
        
        if (![[dict objectForKey:@"ratings_average"] isKindOfClass:[NSNull class]] && [dict objectForKey:@"ratings_average"]) {
            cell.lblRating.text = [dict objectForKey:@"ratings_average"];
            
        }
        else {
            cell.lblRating.text = @"0";
        }
        
        if (![[dict objectForKey:@"status"] isKindOfClass:[NSNull class]]) {
            cell.lblStatus.text = [dict objectForKey:@"status"];
            [cell.lblStatus sizeToFit];
        }
        else {
            cell.lblStatus.text = @"";
        }
        
        if (![[dict objectForKey:@"display_name"] isKindOfClass:[NSNull class]]) {
            cell.lblProjectName.text = [[dict objectForKey:@"display_name"] uppercaseString];
        }
        else {
            cell.lblProjectName.text = @"";
        }
        
        if (![[dict objectForKey:@"address"] isKindOfClass:[NSNull class]]) {
            cell.lblAddress.text = [dict objectForKey:@"address"];
        }
        else {
            cell.lblAddress.text = @"";
        }
        
        if (![[dict objectForKey:@"unit_type"] isKindOfClass:[NSNull class]]) {
            cell.lblUnitType.text = [dict objectForKey:@"unit_type"];
        }
        else {
            cell.lblUnitType.text = @"";
        }
        
        if (![[dict objectForKey:@"psf"] isKindOfClass:[NSNull class]]) {
            
            cell.lblPricePSF.text = [NSString stringWithFormat:@"\u20B9 %@ per sq.ft.", [dict objectForKey:@"psf"]];
        }
        else {
            cell.lblPricePSF.text = @"-- psf";
        }
        
        
        if (![[dict objectForKey:@"price_one_year"] isKindOfClass:[NSNull class]]) {
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"Increase"];
            attachment.bounds = CGRectMake(0, -1, 20, 0.8 * CGRectGetHeight(cell.lblAppriciation.frame));
            
            NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
            
            [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [dict objectForKey:@"price_one_year"]]]];
            
            cell.lblAppriciation.attributedText = strProgress;
        }
        else {
            cell.lblAppriciation.text = @"";
        }
        
        
        if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
            cell.lblNeed.text = [dict objectForKey:@"needs"];
        }
        else {
            cell.lblNeed.text = @"";
        }
        
        if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
            cell.lblInfra.text = [dict objectForKey:@"infra"];
        }
        else {
            cell.lblInfra.text = @"";
        }
        
        if (![[dict objectForKey:@"life_style"] isKindOfClass:[NSNull class]]) {
            cell.lblLifestyle.text = [dict objectForKey:@"life_style"];
        }
        else {
            cell.lblLifestyle.text = @"";
        }
        
        cell.btnFavorite.selected = [[dict objectForKey:@"favorite"] boolValue];
        cell.btnFavorite.tag = indexPath.item;
        
        if ([[cell.btnFavorite allTargets] count] == 0) {
            
            [cell.btnFavorite addTarget:self action:@selector(didTappedFavorite:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        
        return cell;
    }
    else {
        
        static NSString *str = @"NoProjectCell";
        FooterCrouselCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:str forIndexPath:indexPath];
        btnLeft.hidden = YES;
        btnRight.hidden = YES;
        return cell;
        
    }
    
    return nil;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
     if (arrResponseData.count) {
         ProjectDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailVC"];
         obj.option = FeaturedProjectAccessOptionSearchHeatMap;//
         obj.dictSourse = [arrResponseData objectAtIndex:indexPath.row];
        
         [self.navigationController pushViewController:obj animated:YES];
     }
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (arrResponseData.count) {
        
        [self drawProjectMapAndMarker:indexPath];
       
    }
    else {
        
        for (GMSMarker *markerTemp in arrMarkers) {
            markerTemp.map = nil;
        }
        [arrMarkers removeAllObjects];
        
        updatedCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(latitude, longitude)
                                              zoom:zoom];
        
        [mapView animateWithCameraUpdate:updatedCamera];
        
    }

    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSIndexPath *indexPath = [colView indexPathForCell:[[colView visibleCells] firstObject]];
    
    if (arrResponseData.count == 1) {
        btnLeft.hidden = YES;
        btnRight.hidden = YES;
    }
    else {
        if (indexPath.item == 0) {
            btnLeft.hidden = YES;
            btnRight.hidden = NO;
        }
        else if (indexPath.item == arrResponseData.count - 1) {
            btnRight.hidden = YES;
            btnLeft.hidden = NO;
        }
        else if (indexPath.item > 0 && indexPath.item < arrResponseData.count - 1) {
            btnLeft.hidden = NO;
            btnRight.hidden = NO;
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    NSIndexPath *indexPath = [colView indexPathForCell:[[colView visibleCells] firstObject]];
    
    if (arrResponseData.count == 1) {
        btnLeft.hidden = YES;
        btnRight.hidden = YES;
    }
    else {
        if (indexPath.item == 0) {
            btnLeft.hidden = YES;
            btnRight.hidden = NO;
        }
        else if (indexPath.item == arrResponseData.count - 1) {
            btnRight.hidden = YES;
            btnLeft.hidden = NO;
        }
        else if (indexPath.item > 0 && indexPath.item < arrResponseData.count - 1) {
            btnLeft.hidden = NO;
            btnRight.hidden = NO;
        }
    }
    
}


#pragma mark - Google maps
#pragma mark - Projects via Micromarket
- (void)drawMicromarketPolygon {
    
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:_path];
    polygon.strokeColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
    polygon.strokeWidth = 2.0;
    polygon.fillColor = [UIColor clearColor];
    polygon.map = mapView;
    
}

- (void)drawProjectMap:(NSArray *)arrProjectCords {
    
}

- (void)drawProjectMapAndMarker:(NSIndexPath *)indexPath {
    
    latitude = [[[[arrResponseData objectAtIndex:indexPath.row] objectForKey:@"project_cord"] valueForKeyPath:@"@avg.lat"] floatValue];
    longitude = [[[[arrResponseData objectAtIndex:indexPath.row] objectForKey:@"project_cord"] valueForKeyPath:@"@avg.long"] floatValue];
    
    GMSMarker *marker = [arrMarkers objectAtIndex:indexPath.row];
    marker.icon = [self setMarkerCodes:indexPath];
    mapView.selectedMarker = marker;
    
    updatedCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(latitude, longitude)
                                          zoom:zoom];
    
    [mapView animateWithCameraUpdate:updatedCamera];
}




- (void)updateAllMarkers {
    
    int i = 0;
    for (GMSMarker *marker in arrMarkers) {
        marker.icon = [self setMarkerCodes:[NSIndexPath indexPathForItem:i++ inSection:0]];
    }
}



#pragma mark - Auxillary Mathods
- (NSString *)setPriceRange:(NSString *)str {
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs+", result];
    }
    
    return strNum;
}

- (UIColor *)setColorCodes:(NSIndexPath *)indexPath {
    
    if (segmentControl.selectedSegmentIndex == 0) {
        return [self selectColorOnProject:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"infra_code"] floatValue]];
        
    }
    else if (segmentControl.selectedSegmentIndex == 1) {
        return [self selectColorOnProject:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"needs_code"] floatValue]];
    }
    else if (segmentControl.selectedSegmentIndex == 2) {
        return [self selectColorOnProject:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"psf_code"] floatValue]];
    }
    else if (segmentControl.selectedSegmentIndex == 3) {
        return [self selectColorOnProject:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"life_style_code"] floatValue]];
    }
    else //if (segmentControl.selectedSegmentIndex == 4)
    {
        return [self selectColorOnProject:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"returns_code"] floatValue]];
    }
}

- (UIColor *)selectColorOnProject:(CGFloat)value {
    
    return [arrColorCode objectAtIndex:(value - 1) < 0?1:(value - 1)];
}


- (UIImage *)setMarkerCodes:(NSIndexPath *)indexPath {
    
    if (segmentControl.selectedSegmentIndex == 0) {
        return [self setImageOnMarker:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"infra_code"] floatValue]];
        
    }
    else if (segmentControl.selectedSegmentIndex == 1) {
        return [self setImageOnMarker:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"needs_code"] floatValue]];
    }
    else if (segmentControl.selectedSegmentIndex == 2) {
        return [self setImageOnMarker:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"psf_code"] floatValue]];
    }
    else if (segmentControl.selectedSegmentIndex == 3) {
        return [self setImageOnMarker:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"life_style_code"] floatValue]];
    }
    else //if (segmentControl.selectedSegmentIndex == 4)
    {
        return [self setImageOnMarker:[[[arrResponseData objectAtIndex:indexPath.item] objectForKey:@"returns_code"] floatValue]];
    }
    
}

- (UIImage *)setImageOnMarker:(CGFloat)value {
    return [arrMarkerImage objectAtIndex:(value - 1) < 0?1:(value - 1)];
}

#pragma mark - GMSMapView Delegate
- (BOOL)mapView:(GMSMapView *)aMapView didTapMarker:(GMSMarker *)marker {
    
    if ([arrMarkers containsObject:marker]) {
        [colView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:[arrMarkers indexOfObject:marker] inSection:0]
                        atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                animated:YES];
    }
    else {
        
    }
    
    return YES;
}

@end
