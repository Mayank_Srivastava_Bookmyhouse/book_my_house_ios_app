//
//  SectorCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/29/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectorCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UIImageView *imgRating;
@property (weak, nonatomic) IBOutlet UILabel *lblAppriciation;

@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitType;
@property (weak, nonatomic) IBOutlet UILabel *lblPricePSF;



@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeed;
@property (weak, nonatomic) IBOutlet UILabel *lblLifestyle;

@end
