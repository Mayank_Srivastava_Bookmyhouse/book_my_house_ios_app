//
//  SectorDetailView.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectorDetailView : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnOffers;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblHomeLoanOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblHomeLoan;
+ (SectorDetailView *)createView;
- (IBAction)actionListeners:(id)sender;

@end
