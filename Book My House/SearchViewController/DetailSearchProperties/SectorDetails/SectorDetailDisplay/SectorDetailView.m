//
//  SectorDetailView.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SectorDetailView.h"

@implementation SectorDetailView

+ (SectorDetailView *)createView {
    
    SectorDetailView *view = [[[NSBundle mainBundle] loadNibNamed:@"SectorDetailView" owner:self options:nil] firstObject];
    
    view.lblDiscount.layer.cornerRadius = 2.0;
    view.lblDiscount.clipsToBounds = YES;
    
    view.lblOffer.layer.cornerRadius = 2.0;
    view.lblOffer.clipsToBounds = YES;
    
    return view;
}

- (IBAction)actionListeners:(id)sender {
    [self removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
