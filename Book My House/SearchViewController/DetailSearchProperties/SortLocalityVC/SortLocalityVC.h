//
//  SortLocalityVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface SortLocalityVC : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
