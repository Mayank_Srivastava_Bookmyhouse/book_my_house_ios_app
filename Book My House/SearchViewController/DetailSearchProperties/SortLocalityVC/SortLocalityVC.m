//
//  SortLocalityVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SortLocalityVC.h"

static NSString *strSelectFilter = @"Please select a filter";

@interface SortLocalityVC () {
    
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIButton *btnApplyFilters;
    
    
    NSArray *arrSourseData;
    NSMutableArray *arrSelectedData;
    NSUserDefaults *pref;
}
- (IBAction)didTapApplyFilters:(id)sender;
@end

@implementation SortLocalityVC


#pragma mark - View Controller Mathods
- (void)loadView {
    
    pref = [NSUserDefaults standardUserDefaults];
    [super loadView];
    
    arrSourseData = @[@"Price per sq. ft (low to high)",@"Price per sq. ft (high to low)", @"Rating (high to low)", @"Infra (high to low)", @"Needs (high to low)", @"Lifestyle (high to low)", @"Returns (high to low)"];
    
    arrSelectedData = [NSMutableArray array];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
}


#pragma mark - Screen Settings
- (void)screenSettings {
    
}


#pragma mark - Button Action
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnCancel) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender == btnApplyFilters && arrSelectedData.count) {
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kApplyFilters object:arrSelectedData];
    }
    
}


- (IBAction)didTapApplyFilters:(id)sender{
    NSLog(@"%s", __FUNCTION__);
    
    if (arrSelectedData.count) {
        
        NSInteger index = [arrSourseData indexOfObject:[arrSelectedData firstObject]];
        
        [pref setObject:[NSNumber numberWithDouble:index] forKey:strSaveSortLocalityIndex];
        [pref synchronize];
        
        if ([pref synchronize]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kApplyFilters object:arrSelectedData];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else {
        [self.view.window makeToast:strSelectFilter duration:duration position:CSToastPositionCenter];
    }
}

#pragma mark - API Actions
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - Table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrSourseData.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *str2 = @"SortDetails";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str2];
    UILabel *lbl = (UILabel *)[cell.contentView viewWithTag:1];
    
    
    UIImageView *imgview = (UIImageView *)[cell.contentView viewWithTag:2];
    NSNumber *num = [pref objectForKey:strSaveSortLocalityIndex];
    
    if (num && [num doubleValue] == indexPath.row) {
        imgview.image = [UIImage imageNamed:@"Checkbox_green"];
        [arrSelectedData addObject:[arrSourseData objectAtIndex:indexPath.row]];
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    else {
        imgview.image = [UIImage imageNamed:@"Checkbox_gray"];
        
    }

    lbl.text = [arrSourseData objectAtIndex:indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lbl = (UILabel *)[cell.contentView viewWithTag:1];
    lbl.font = [UIFont fontWithName:@"PT Sans" size:16.0];
    UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:2];
    imgView.image = [UIImage imageNamed:@"Checkbox_green"];
    [arrSelectedData addObject:[arrSourseData objectAtIndex:indexPath.row]];
    
    if (arrSelectedData.count) {
        
        NSInteger index = [arrSourseData indexOfObject:[arrSelectedData firstObject]];
        
        [pref setObject:[NSNumber numberWithDouble:index] forKey:strSaveSortLocalityIndex];
        [pref synchronize];
        
        if ([pref synchronize]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kApplyFilters object:arrSelectedData];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else {
        [self.view.window makeToast:strSelectFilter duration:duration position:CSToastPositionCenter];
    }
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lbl = (UILabel *)[cell.contentView viewWithTag:1];
    lbl.font = [UIFont fontWithName:@"PT Sans" size:16.0];
    UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:2];
    imgView.image = [UIImage imageNamed:@"Checkbox_gray"];
    [arrSelectedData removeObject:[arrSourseData objectAtIndex:indexPath.row]];
}
@end
