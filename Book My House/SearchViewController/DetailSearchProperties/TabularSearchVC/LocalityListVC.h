//
//  LocalityListVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "NotificationAlert.h"

typedef NS_ENUM(NSUInteger, TabularSearchOptions) {
    TabularSearchOptionNormal,
    TabularSearchOptionSectors,
};

@interface LocalityListVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
//@property (nonatomic, copy) NSArray *arrData;
@property (nonatomic, copy) NSDictionary *dictInfo;
@property (nonatomic, assign) TabularSearchOptions option;
@end
