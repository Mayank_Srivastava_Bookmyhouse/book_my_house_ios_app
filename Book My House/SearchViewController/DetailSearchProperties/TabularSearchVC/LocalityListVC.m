//
//  LocalityListVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 3/18/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "LocalityListVC.h"
#import "ProjectMapVC.h"
#import "ProjectListVC.h"
#import "CustomView.h"
#import "PrivacyViewController.h"

#define ALLOWED_CITY_ID 16

static NSString * const strNoMobilenumber                    = @"Please fill your mobile number";
static NSString * const strValidMobileNumber                 = @"Please give your correct mobile number.";
static NSString * const strNoName                            = @"Please enter your name";
static NSString * const strInvalidName                       = @"Please enter invalid name";
static NSString * const strNoEmail                           = @"Please give your email id";
static NSString * const strInvalidEmail                      = @"Please fill valid email id";
static NSString * const strPleaseCheckMarkT_C                = @"Please check mark T&C";
static NSString * const strEnquirySuccess                    = @"Enquiry successfully sent.";
static NSString * const strEnquiryFailed                     = @"Enquiry could not be sent";
static NSString * const strNoData                            = @"No result found as per your search criteria";
@interface LocalityListVC () {
    
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIButton *btnMap;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIButton *btnFilter;
    
    NSUserDefaults *pref;
    NotificationAlert *notifAlert;
    NSMutableArray *arrData;
    BOOL isGradientPresent;
    GetAlertView *viewAlert;
    int direction, shakes;
    
    
}
- (IBAction)actionListeners:(id)sender;

@end

@implementation LocalityListVC

#pragma mark - View Controller mathods
- (void)loadView {
    
    arrData = [NSMutableArray array];
    [super loadView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadFilteredSearchPrperty:) name:kApplyFilters object:nil];
    pref = [NSUserDefaults standardUserDefaults];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSDictionary *dict = [pref objectForKey:strSaveCityInfo];
    
    NSLog(@"Dictionary: %@", [dict objectForKey:@"city_id"]);
    
    if ([[dict objectForKey:@"city_id"] integerValue] == ALLOWED_CITY_ID) {
        btnMap.hidden = NO;
    }
    else {
        btnMap.hidden = YES;
    }
    
    
    if (_option == TabularSearchOptionNormal) {
        lblTitle.text = @"Select Locality";
    }
    else {
        lblTitle.text = @"Select Sector";
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self loadsearchedPropertyInformation];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kApplyFilters object:nil];

}



#pragma mark - Screen settings
- (void)screenSettings{
    
}

#pragma mark - API Actions
- (void)loadsearchedPropertyInformation {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
        [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
        
        if ([_dictInfo objectForKey:@"keyword"] != nil) {
            
            NSDictionary *dict1 = [_dictInfo objectForKey:@"keyword"];
            
            if ([[dict1 objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
                lblTitle.text = [[_dictInfo objectForKey:@"keyword"] objectForKey:@"title"];
                [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sub_location_id"];
                
            }
            else {
            
                lblTitle.text = [[_dictInfo objectForKey:@"keyword"] objectForKey:@"title"];
                [dict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"location_id"];
            }
            
        }
        
        NSString *strId = [pref objectForKey:strSaveUserId];
        if (strId) {
            [dict setObject:strId forKey:@"user_id"];
        }
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getSearchedPropertyInfoWithParams:dict fromCallingClass:self withSelector:@selector(getsearcheddetails:)];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadsearchedPropertyInformation) fromClass:self];
    }
}



- (void)loadFilteredSearchPrperty:(NSNotification *)info {
    
    
    if (_option == TabularSearchOptionNormal) {
        NSString *str = (NSString *)[info.object firstObject];
        if ([str isEqualToString:@"Price per sq. ft (low to high)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"avgPsfLocation"  ascending:YES];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Price per sq. ft (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"avgPsfLocation"  ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Rating (high to low)"]) {
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"avg_rating" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Infra (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"infra" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Needs (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"needs" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Lifestyle (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"lifeStyle" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Returns (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"returns" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
    }
    else if (_option == TabularSearchOptionSectors) {
        
        NSString *str = (NSString *)[info.object firstObject];
        if ([str isEqualToString:@"Price per sq. ft (low to high)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"avgPsfLocation"  ascending:YES];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Price per sq. ft (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"avgPSFLocation"  ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Rating (high to low)"]) {
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"avg_rating" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Infra (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"infra" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Needs (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"needs" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Lifestyle (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"lifeStyle" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        else if ([str isEqualToString:@"Returns (high to low)"]) {
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"returnval" ascending:NO];
            [arrData sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [tblView reloadData];
            [tblView setContentOffset:CGPointZero animated:YES];
        }
        
    }
    
    
    
}

- (void)getsearcheddetails:(id)response {
    
    [[AppDelegate share] enableUserInteraction];
    if (![response isKindOfClass:[NSNull class]]) {
        [arrData removeAllObjects];
        if ([[response objectForKey:@"success"] boolValue]) {
            
            if ([[response objectForKey:@"location_list"] count]) {
                [arrData addObjectsFromArray:[response objectForKey:@"location_list"]];
                
                if ([[response objectForKey:@"location_list"] count] < 2) {
                    btnFilter.hidden = YES;
                    btnMap.center = btnFilter.center;
                }
 
            }
            else if ([[response objectForKey:@"sector_lists"] count]) {
                [arrData addObjectsFromArray:[response objectForKey:@"sector_lists"]];
                
                if ([[response objectForKey:@"sector_lists"] count] < 2) {
                    btnFilter.hidden = YES;
                    btnMap.center = btnFilter.center;
                }
            }
            else {
                
                btnFilter.hidden = YES;
                btnMap.center = btnFilter.center;
                //[self.navigationController.view makeToast:strNoData duration:duration position:CSToastPositionCenter];
                //[self.navigationController popViewControllerAnimated:YES];
            }
            
        }
        else {
            [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
        }
        [tblView reloadData];
    }
    else {
        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnCancel) {
        [pref removeObjectForKey:strSaveSortLocalityIndex];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnMap) {
        
        LocalityMapVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalityMapVC"];
       
        obj.dictInfo = _dictInfo;
        
        if (_option == TabularSearchOptionNormal) {
            
            obj.option = LocalityMapVCAccessOptionLocality;
        }
        else {
            obj.option = LocalityMapVCAccessOptionSector;
        }
        
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
        
    }
}

- (void)cancelEnqiryForm {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewAlert.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [viewAlert removeFromSuperview];
                         viewAlert = nil;
                     }];
    
}



- (void)sendEnquiry {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        if ([viewAlert.btnSend isEnabled]) {
            viewAlert.btnSend.enabled = NO;
        }
        
        direction = 1;
        shakes = 0;
        
        viewAlert.txtName.text = [viewAlert.txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewAlert.txtName.text.length == 0) {
            [self.view makeToast:strNoName duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtName];
            viewAlert.btnSend.enabled = YES;
            return;
        }
        else if (![AuxillaryMathods isUserName:viewAlert.txtName.text]) {
            [self.view makeToast:strInvalidName duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtName];
             viewAlert.btnSend.enabled = YES;
            return;
        }
        else {
            
        }
        
        viewAlert.txtEmail.text = [viewAlert.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewAlert.txtEmail.text.length == 0) {
            [self.view makeToast:strNoEmail duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtEmail];
             viewAlert.btnSend.enabled = YES;
            return;
        }
        else if (![AuxillaryMathods isValidEmail:viewAlert.txtEmail.text]) {
            [self.view makeToast:strInvalidEmail duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtEmail];
             viewAlert.btnSend.enabled = YES;
            return;
        }
        else {
            
        }
        
        viewAlert.txtMobileNumber.text = [viewAlert.txtMobileNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewAlert.txtMobileNumber.text.length == 0) {
            [self.view makeToast:strNoMobilenumber duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtMobileNumber];
             viewAlert.btnSend.enabled = YES;
            return;
        }
        else if (![AuxillaryMathods isValidMobileNumber:viewAlert.txtMobileNumber.text]) {
            [self.view makeToast:strValidMobileNumber duration:duration position:CSToastPositionCenter];
            [self shake:viewAlert.txtMobileNumber];
             viewAlert.btnSend.enabled = YES;
            return;
        }
        else {
            
        }
        if (!viewAlert.btnCheck.selected) {
            [self.view makeToast:strPleaseCheckMarkT_C duration:duration position:CSToastPositionCenter];
             viewAlert.btnSend.enabled = YES;
            return;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];// projectName,name,contactno,email,enquiry
        [dict setObject:viewAlert.txtName.text forKey:@"name"];
        [dict setObject:viewAlert.txtMobileNumber.text forKey:@"contactno"];
        [dict setObject:viewAlert.txtEmail.text forKey:@"email"];
        
        NSMutableDictionary *subDict = [NSMutableDictionary dictionary];
        [subDict setObject:[_dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
        
        if ([_dictInfo objectForKey:@"p_type"]) {
            [subDict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictInfo objectForKey:@"type"]) {
            [subDict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Location"] && [[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"]) {
            [subDict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"location_id"];
        }
        
        
        if ([[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"] isEqualToString:@"Sublocation"] && [[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"]) {
            [subDict setObject:[[_dictInfo objectForKey:@"keyword"] objectForKey:@"id"] forKey:@"sublocation_id"];
        }
        
        [dict setObject:subDict forKey:@"search"];
        [ServiceAPI postAlert:dict andCalback:^(BOOL isSuccess, id data) {
            
            if (isSuccess) {
                [self cancelEnqiryForm];
                notifAlert = [NotificationAlert createView];
                notifAlert.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                [notifAlert.btnClose addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:notifAlert];
                
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButton)];
                [notifAlert addGestureRecognizer:tap];
                
                [UIView animateWithDuration:0.25
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     notifAlert.frame = self.view.frame;
                                 } completion:^(BOOL finished) {
                                     
                                 }];
            }
            else {
                [self.view.window makeToast:strAlertNotSaved duration:duration position:CSToastPositionCenter];
            }
            
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(sendEnquiry) fromClass:self];
    }
}

- (void)didTappedTermsAndConditions {
    
    PrivacyViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyViewController"];
    obj.option = PrivacyViewControllerAccessOptionTermsAndConditions;
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)didTappedGetAlerts {

    if (viewAlert == nil) {
        //btnEnquiry.selected = !btnEnquiry;
        
        viewAlert = [GetAlertView createView];
        viewAlert.frame = CGRectMake( -1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        //viewAlert.owner = self;
        
        [self setTextField:viewAlert.txtName withPlaceholder:@"Name"];
        [self setTextField:viewAlert.txtEmail withPlaceholder:@"Email"];
        [self setTextField:viewAlert.txtMobileNumber withPlaceholder:@"Mobile Number"];
        [viewAlert.btnCancel addTarget:self action:@selector(cancelEnqiryForm) forControlEvents:UIControlEventTouchUpInside];
        [viewAlert.btnSend addTarget:self action:@selector(sendEnquiry) forControlEvents:UIControlEventTouchUpInside];
        [viewAlert.btnTermsAndConditions addTarget:self action:@selector(didTappedTermsAndConditions) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:viewAlert];
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             viewAlert.frame = self.view.frame;
                         } completion:^(BOOL finished) {
                             
                             //if (![[dictInfo objectForKey:@"proj_name"] isKindOfClass:[NSNull class]]) {
                             //     viewAlert.txtProjectName.text = [[dictInfo objectForKey:@"proj_name"] uppercaseString];
                             //     }
                             //else {
                             //     viewAlert.txtProjectName.text = @"";
                             //      }
                             
                         }];
        
    }
}

- (void)closeButton {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         notifAlert.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [notifAlert removeFromSuperview];
                         notifAlert = nil;
                     }];
    
}

#pragma mark - API Actions
- (void)loadProjects {
    
}



#pragma mark - Table View Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (arrData.count) {
        
        if (_option == TabularSearchOptionNormal) {
            return 2;
        }
        else {
            return 3;
        }
    }
    else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (arrData.count) {
        
        if (_option == TabularSearchOptionNormal) {
            
            if (section == 0) {
                return arrData.count;
            }
            else {
                return 1;
            }
        }
        else {//_option == TabularSearchOptionSectors
            
            if (section == 0) {
                return 1;
            }
            else if (section == 1) {
                return arrData.count - 1;
            }
            else  { //if (section == 2)
                return 1;
            }
        }
    }
    else {
        return 2;
    }
   
}





- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrData.count) {
        
        if (_option == TabularSearchOptionNormal) {
            
            if (indexPath.section == 0) {
                return 180;
            }
            else {
                
                return 140;
            }
        }
        else { //TabularSearchOptionSectors
            
            if (indexPath.section == 0) {
                return 180.0;
            }
            else if (indexPath.section == 1) {
                return 180.0;
            }
            else {
                return 140.0;
            }
            
        }

    }
    else {
        if (indexPath.row == 0) {
            return CGRectGetHeight(self.view.frame) - 204.0;
        }
        else {
            return 140.0;
        }
        
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (_option == TabularSearchOptionSectors && section == 1) {
        return 35;
    }
    
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (_option == TabularSearchOptionSectors && section == 1) {
        UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 35)];
        aView.backgroundColor = [UIColor whiteColor];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:aView.frame];
        lbl.text = @" Nearby Sublocalities";
        lbl.font = [UIFont fontWithName:@"PT Sans" size:20.0];
        lbl.textColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
        
        [aView addSubview:lbl];
        return aView;
    }
    else {
        return nil;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrData.count) {
        if (_option == TabularSearchOptionNormal) {
            
            if (indexPath.section == 0) {
                return [self tableView:tableView settingLocalityInfoAtIndexPath:indexPath];
            }
            else {
                return [self tableView:tableView settingAlertInfoAtIndexPath:indexPath];
            }
        }
        else {//sectors
            
            if (indexPath.section == 0) {
                return [self tableView:tableView settingSectorInfoAtIndexPath:indexPath];
            }
            else if (indexPath.section == 1) {
                return [self tableView:tableView settingSectorInfoAtIndexPath:indexPath];
            }
            else {
                 return [self tableView:tableView settingAlertInfoAtIndexPath:indexPath];
            }
        }
    }
    else {
        if (indexPath.row == 0) {
            return [self tableView:tableView settingNoDataInfoAtIndexPath:indexPath];
        }
        else {
            return [self tableView:tableView settingAlertInfoAtIndexPath:indexPath];
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView settingLocalityInfoAtIndexPath:(NSIndexPath *)indexPath {
   
    static NSString *str = @"Summary";
    NSDictionary *dict = [arrData objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    UIImageView *imgview = (UIImageView *)[cell.contentView viewWithTag:1];
    
    
    
    
    UILabel *lblTitle1 = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *lblAvg = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *lblProjects = (UILabel *)[cell.contentView viewWithTag:4];
    UILabel *lblInfra = (UILabel *)[cell.contentView viewWithTag:5];
    UILabel *lblNeeds = (UILabel *)[cell.contentView viewWithTag:6];
    UILabel *lblLifestyle = (UILabel *)[cell.contentView viewWithTag:7];
    UILabel *lblRating = (UILabel *)[cell.contentView viewWithTag:8];
    UILabel *lblAppriciation = (UILabel *)[cell.contentView viewWithTag:9];
   
    
    lblInfra.layer.cornerRadius = lblNeeds.layer.cornerRadius = lblLifestyle.layer.cornerRadius = 5.0;
    lblInfra.clipsToBounds = lblNeeds.clipsToBounds = lblLifestyle.clipsToBounds = YES;
    
    [imgview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"location_img"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
    
    if (![[dict objectForKey:@"location_name"] isKindOfClass:[NSNull class]]) {
        lblTitle1.text = [[dict objectForKey:@"location_name"] uppercaseString];
    }
    else {
        lblTitle1.text = @"";
    }
    
    
    if (![[dict objectForKey:@"avgPsfLocation"] isKindOfClass:[NSNull class]]) {
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
        [formatter setGroupingSeparator:groupingSeparator];
        [formatter setGroupingSize:3];
        [formatter setAlwaysShowsDecimalSeparator:NO];
        [formatter setUsesGroupingSeparator:YES];
        
        
        NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[[dict objectForKey:@"avgPsfLocation"] doubleValue]]];
        lblAvg.text = str;
        lblAvg.text = [NSString stringWithFormat:@"\u20B9 %@ Avg Price/sq.ft.", str];
    }
    else {
        lblAvg.text = @"Avg:";
    }
    
    
    if (![[dict objectForKey:@"total_projects"] isKindOfClass:[NSNull class]]) {
        lblProjects.text = [NSString stringWithFormat:@"No. of Projects: %@", [dict objectForKey:@"total_projects"]];
    }
    else {
        lblProjects.text = @"";
    }
    
    
    if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
        lblInfra.text = [NSString stringWithFormat:@"Infra\n%@",[dict objectForKey:@"infra"]];
    }
    else {
        lblInfra.text = @"";
    }
    
    if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
        lblNeeds.text = [NSString stringWithFormat:@"Needs\n%@",[dict objectForKey:@"needs"]];
    }
    else {
        lblNeeds.text = @"";
    }
    
    if (![[dict objectForKey:@"lifeStyle"] isKindOfClass:[NSNull class]]) {
        lblLifestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@", [dict objectForKey:@"lifeStyle"]];
    }
    else {
        lblLifestyle.text = @"";
    }
    
    if ([dict objectForKey:@"avg_rating"] && ![[dict objectForKey:@"avg_rating"] isKindOfClass:[NSNull class]]) {
        lblRating.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"avg_rating"]];
    }
    else {
        lblRating.text = @"0";
    }
    
    if (![[dict objectForKey:@"returns"] isKindOfClass:[NSNull class]]) {
        lblAppriciation.textColor = [UIColor greenColor];
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"Increase"];
        attachment.bounds = CGRectMake(2, -4, 18, 18);
        
        NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
        
        [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@", [dict objectForKey:@"returns"]]]];
        
        lblAppriciation.attributedText = strProgress;
    }
    else {
        lblAppriciation.text = @"";
    }
    
    return cell;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView settingSectorInfoAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *str = @"Sector_summary";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    UIImageView *imgview = (UIImageView *)[cell.contentView viewWithTag:1];
    NSDictionary *dict;
    
    UILabel *lblTitle1 = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *lblAvg = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *lblProjects = (UILabel *)[cell.contentView viewWithTag:4];
    UILabel *lblInfra = (UILabel *)[cell.contentView viewWithTag:5];
    UILabel *lblNeeds = (UILabel *)[cell.contentView viewWithTag:6];
    UILabel *lblLifestyle = (UILabel *)[cell.contentView viewWithTag:7];
    UILabel *lblRating = (UILabel *)[cell.contentView viewWithTag:8];
    UILabel *lblAppriciation = (UILabel *)[cell.contentView viewWithTag:9];
    UILabel *lblLocality = (UILabel *)[cell.contentView viewWithTag:10];
    
    lblInfra.layer.cornerRadius = lblNeeds.layer.cornerRadius = lblLifestyle.layer.cornerRadius = 5.0;
    lblInfra.clipsToBounds = lblNeeds.clipsToBounds = lblLifestyle.clipsToBounds = YES;
    
    if (indexPath.section == 0) {
        dict = [arrData objectAtIndex:indexPath.section];
    }
    else {
        dict = [arrData objectAtIndex:indexPath.section + indexPath.row];
    }
    
    
       [imgview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"sector_image"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
    
    if (![[dict objectForKey:@"sector_name"] isKindOfClass:[NSNull class]]) {
        lblTitle1.text = [[dict objectForKey:@"sector_name"] uppercaseString];
    }
    else {
        lblTitle1.text = @"";
    }
    //[[_dictInfo objectForKey:@"keyword"] objectForKey:@"subtitle"];
    
    if ([[_dictInfo objectForKey:@"keyword"] objectForKey:@"location_name"]) {
        lblLocality.text = [[_dictInfo objectForKey:@"keyword"] objectForKey:@"location_name"];
    }
    else {
        lblLocality.text = @"";
    }
    
    if (![[dict objectForKey:@"avgPSFLocation"] isKindOfClass:[NSNull class]]) {
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
        [formatter setGroupingSeparator:groupingSeparator];
        [formatter setGroupingSize:3];
        [formatter setAlwaysShowsDecimalSeparator:NO];
        [formatter setUsesGroupingSeparator:YES];
        
        
        NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[[dict objectForKey:@"avgPSFLocation"] doubleValue]]];
        lblAvg.text = str;
        lblAvg.text = [NSString stringWithFormat:@"\u20B9 %@ Avg Price/sq.ft.", str];
    }
    else {
        lblAvg.text = @"Avg:";
    }
    
    
    if (![[dict objectForKey:@"total_project"] isKindOfClass:[NSNull class]]) {
        lblProjects.text = [NSString stringWithFormat:@"No. of Projects: %@", [dict objectForKey:@"total_project"]];
    }
    else {
        lblProjects.text = @"";
    }
    
    
    if (![[dict objectForKey:@"infra"] isKindOfClass:[NSNull class]]) {
        lblInfra.text = [NSString stringWithFormat:@"Infra\n%@",[dict objectForKey:@"infra"]];
    }
    else {
        lblInfra.text = @"";
    }
    
    if (![[dict objectForKey:@"needs"] isKindOfClass:[NSNull class]]) {
        lblNeeds.text = [NSString stringWithFormat:@"Needs\n%@",[dict objectForKey:@"needs"]];
    }
    else {
        lblNeeds.text = @"";
    }
    
    if (![[dict objectForKey:@"lifeStyle"] isKindOfClass:[NSNull class]]) {
        lblLifestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@", [dict objectForKey:@"lifeStyle"]];
    }
    else {
        lblLifestyle.text = @"";
    }
    
    if ([dict objectForKey:@"avg_rating"] && ![[dict objectForKey:@"avg_rating"] isKindOfClass:[NSNull class]]) {
        lblRating.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"avg_rating"]];
    }
    else {
        lblRating.text = @"0";
    }
    
    if (![[dict objectForKey:@"return"] isKindOfClass:[NSNull class]]) {
        lblAppriciation.textColor = [UIColor greenColor];
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"Increase"];
        attachment.bounds = CGRectMake(2, -4, 18, 18);
        
        NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
        
        [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@", [dict objectForKey:@"return"]]]];
        
        lblAppriciation.attributedText = strProgress;
    }
    else {
        lblAppriciation.text = @"";
    }
    return cell;

    
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView settingAlertInfoAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *strId = @"alerts";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strId];
    
    
    UIButton *btn = (UIButton *)[cell.contentView viewWithTag:1];
    btn.layer.borderWidth = 2.0;
    btn.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    btn.clipsToBounds = YES;
    
    [btn addTarget:self action:@selector(didTappedGetAlerts) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView settingNoDataInfoAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *strId = @"No data";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strId];
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (arrData.count) {
        
        if (_option == TabularSearchOptionNormal) {
            
            if (indexPath.section == 0) {
                
                ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectListVC"];
                obj.option = DetailFeaturedDeveloperAccessOptionSearchScreen;
                obj.dictSourse = [arrData objectAtIndex:indexPath.row];
                obj.dictInfo = _dictInfo;
                obj.strTitle = [[arrData objectAtIndex:indexPath.row] objectForKey:@"location_name"];//location_name
                [self.navigationController pushViewController:obj animated:YES];
                
            }
            else {
                
            }
        }
        else  {//Sector
            
            if (indexPath.section == 0) {
                ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectListVC"];
                obj.option = DetailFeaturedDeveloperAccessOptionSectors;
                obj.dictSourse = [arrData objectAtIndex:indexPath.section];
                obj.dictInfo = _dictInfo;
                obj.strTitle = [[arrData objectAtIndex:indexPath.section] objectForKey:@"sector_name"];//location_name
                [self.navigationController pushViewController:obj animated:YES];
            }
            else if (indexPath.section == 1) {
                ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectListVC"];
                obj.option = DetailFeaturedDeveloperAccessOptionSectors;
                obj.dictSourse = [arrData objectAtIndex:indexPath.section + indexPath.row];
                obj.dictInfo = _dictInfo;
                obj.strTitle = [[arrData objectAtIndex:indexPath.section + indexPath.row] objectForKey:@"sector_name"];//
                [self.navigationController pushViewController:obj animated:YES];
            }
            else {
                
            }
        }
    }
    else {
        
    }
}

#pragma mark - Auxillary View
- (void)setTextField:(UITextField *)txtField withPlaceholder:(NSString *)placeholder {
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtField.frame))];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer1.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [aView.layer addSublayer:layer1];
    
    aView.backgroundColor = [UIColor whiteColor];
    txtField.leftView = aView;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    
    txtField.placeholder = placeholder;
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtField.layer addSublayer:layer];
    
    txtField.delegate = self;
    
}

-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

#pragma mark - UITextField Delegates
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength <= 10;
    }
    else {
        return YES;
    }
}


@end
