//
//  AlbumViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageCell.h"
#import "Header.h"
#import "YouTubeCell.h"
#import "DownloadCell.h"

typedef NS_ENUM(NSUInteger, AlbumAccessOption) {
    AlbumAccessOptionFeaturedProject,
    AlbumAccessOptionUnitDescription,
    AlbumAccessOptionMisc,
};
@interface AlbumViewController : UICollectionViewController
@property (assign, nonatomic) AlbumAccessOption option;
@property (strong, nonatomic) NSArray *arrSourse;
@property (assign, nonatomic) NSUInteger index;
@end
