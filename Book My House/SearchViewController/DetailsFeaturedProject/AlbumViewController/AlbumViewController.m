//
//  AlbumViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "AlbumViewController.h"
#import "Header.h"

static NSString * const strSuccess                           = @"File Sucessfully downloaded";
static NSString * const strUnsucess                          = @"File could not be downloaded";

@interface AlbumViewController () {
    NSUInteger numberOfImages, numberOfVideos, documentContent;
    YTPlayerView *youTubeView;
    UIButton *btnBack;
    UILabel *lblCaption;
}
@property (strong, nonatomic) NSMutableArray *arrFileDownloadData;
@end

@implementation AlbumViewController

static NSString * const reuseIdentifier1 = @"Cell1";
static NSString * const reuseIdentifier2 = @"Cell2";
static NSString * const reuseIdentifier3 = @"Cell3";

#pragma mark - View Controller
- (void)viewDidLoad {
    [super viewDidLoad];
    
    btnBack = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 54)];
    [btnBack setImage:[UIImage imageNamed:@"window-close"] forState:UIControlStateNormal];
    btnBack.backgroundColor = [UIColor clearColor];
    
    [btnBack addTarget:self action:@selector(didTapedBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    lblCaption = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 21)];
    lblCaption.backgroundColor = [UIColor clearColor];
    lblCaption.textAlignment = NSTextAlignmentCenter;
    lblCaption.textColor = [UIColor whiteColor];
    
    if (_option == AlbumAccessOptionFeaturedProject) {
        [self.collectionView registerClass:[ImageCell class] forCellWithReuseIdentifier:reuseIdentifier1];
        [self.collectionView registerNib:[UINib nibWithNibName:@"ImageCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier1];
        
        [self.collectionView registerClass:[YouTubeCell class] forCellWithReuseIdentifier:reuseIdentifier2];
        [self.collectionView registerNib:[UINib nibWithNibName:@"YouTubeCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier2];
        
        [self.collectionView registerClass:[DownloadCell class] forCellWithReuseIdentifier:reuseIdentifier3];
        [self.collectionView registerNib:[UINib nibWithNibName:@"DownloadCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier3];

        [self screenSettingsForFeaturedProject];
    }
    else if (_option == AlbumAccessOptionUnitDescription) {
        
        [self.collectionView registerClass:[ImageCell class] forCellWithReuseIdentifier:reuseIdentifier1];
        [self.collectionView registerNib:[UINib nibWithNibName:@"ImageCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier1];
        
        [self screenSettingsForUnitdescriptions];
    }

}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [btnBack removeFromSuperview];
    [lblCaption removeFromSuperview];
}


#pragma mark - Screen settings
- (void)screenSettingsForFeaturedProject {
    
    numberOfImages = numberOfVideos = documentContent = 0;

    for (int i = 0; i < _arrSourse.count; i ++) {
        
        NSDictionary *dict = [_arrSourse objectAtIndex:i];
        if ([[dict objectForKey:@"type"] integerValue] == 1) {
            ++ numberOfImages;
        }
        else if ([[dict objectForKey:@"type"] integerValue] == 2) {
            ++ numberOfVideos;
        }
        else if ([[dict objectForKey:@"type"] integerValue] == 3) {
            ++ documentContent;
        }
    }
    
    [self.view layoutIfNeeded];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self.collectionView reloadData];
    
    
    
    [[[AppDelegate share] window] addSubview:btnBack];
    [[[AppDelegate share] window] addSubview:lblCaption];
}

- (void)screenSettingsForUnitdescriptions {
    
    [self.view layoutIfNeeded];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self.collectionView reloadData];
    
   
    [[[AppDelegate share] window] addSubview:btnBack];
    [[[AppDelegate share] window] addSubview:lblCaption];
}



#pragma mark - Collection View

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.collectionView.frame.size;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (_option == AlbumAccessOptionFeaturedProject) {
        return numberOfImages + numberOfVideos + documentContent;
    }
    else {
        return _arrSourse.count;
        //return 0;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_option == AlbumAccessOptionFeaturedProject) {

        if (indexPath.row < numberOfImages) {
            
            ImageCell *cell = (ImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier1 forIndexPath:indexPath];
            
            NSString *str = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 40, [[_arrSourse objectAtIndex:indexPath.row] objectForKey:@"url"]];
            
            [cell.imgMain setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
            
            if (indexPath.row % 2 == 0) {
                //cell.contentView.backgroundColor = [UIColor greenColor];
            }
            else {
                 //cell.contentView.backgroundColor = [UIColor redColor];
            }
            
            
            return cell;
        }
        else if (indexPath.row >= numberOfImages && indexPath.row < numberOfImages + numberOfVideos) {
            YouTubeCell *cell = (YouTubeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier2 forIndexPath:indexPath];
            
            if (youTubeView != nil) {
                [youTubeView removeFromSuperview];
                youTubeView = nil;
            }
            
            youTubeView = [[YTPlayerView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(collectionView.frame), 250)];
            youTubeView.center = collectionView.center;
            [youTubeView loadWithVideoId:[[_arrSourse objectAtIndex:indexPath.row] objectForKey:@"url"]];
            
            [cell addSubview:youTubeView];
            
            if (indexPath.row % 2 == 0) {
                cell.contentView.backgroundColor = [UIColor greenColor];
            }
            else {
                cell.contentView.backgroundColor = [UIColor redColor];
            }
            
            return cell;
        }
        else {
            DownloadCell *cell = (DownloadCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier3 forIndexPath:indexPath];
            
            cell.btnDownload.layer.cornerRadius = 5.0;
            cell.btnDownload.clipsToBounds = YES;
            
            [cell.btnDownload addTarget:self action:@selector(didPerformDownloading:) forControlEvents:UIControlEventTouchUpInside];
            
            if (indexPath.row % 2 == 0) {
                cell.contentView.backgroundColor = [UIColor greenColor];
            }
            else {
                cell.contentView.backgroundColor = [UIColor redColor];
            }
            
            return cell;
        }

    }
    else {
        
        ImageCell *cell = (ImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier1 forIndexPath:indexPath];
        [cell.imgMain setImageWithURL:[NSURL URLWithString:[[_arrSourse objectAtIndex:indexPath.row] objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        lblCaption.hidden = NO;
        CGRect rect = [cell convertRect:cell.imgMain.frame toView:self.view];
        lblCaption.frame = CGRectMake(0, CGRectGetMaxY(rect), CGRectGetWidth(rect), 21);
        
        [cell.btnBack addTarget:self action:@selector(didTapedBackButton) forControlEvents:UIControlEventTouchUpInside];
        return cell;        
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_option == AlbumAccessOptionFeaturedProject) {
        if (indexPath.row < numberOfImages) {
            
            ImageCell *cell1 = (ImageCell *)cell;
            lblCaption.hidden = NO;
            
            CGRect rect = [cell1 convertRect:cell1.imgMain.frame toView:self.view];
            lblCaption.frame = CGRectMake(0, CGRectGetMaxY(rect), CGRectGetWidth(rect), 21);
            btnBack.backgroundColor = [UIColor clearColor];

        }
        else if (indexPath.row >= numberOfImages && indexPath.row < numberOfImages + numberOfVideos) {
            
            YouTubeCell *cell1 = (YouTubeCell *)cell;
            lblCaption.hidden = NO;
            
            CGRect rect = [cell1 convertRect:youTubeView.frame toView:self.view];
            
            lblCaption.frame = CGRectMake(0, CGRectGetMaxY(rect), CGRectGetWidth(rect), 21);
            btnBack.backgroundColor = [UIColor clearColor];
            
        }
        else {
            
            lblCaption.hidden = YES;
            btnBack.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0  blue:200.0/255.0 alpha:0.4];
            
        }
        
    }
    else {
        
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    NSLog(@"Scrolling animations");
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    
    CGPoint translation = [self.collectionView.panGestureRecognizer translationInView:self.collectionView.superview];
    UICollectionViewCell *cell;
    NSLog(@"Number of cells: %d", (int)[[self.collectionView visibleCells] count]);
    
    if (translation.x <= 0) {
        NSLog(@"Translates towards Right");
        cell = [[self.collectionView visibleCells] firstObject];
    }
    else {
        NSLog(@"Translate towards Left");
        cell = [[self.collectionView visibleCells] lastObject];
    }
    
    
    
    if ([cell isKindOfClass:[ImageCell class]]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        NSLog(@"Indexpath %d", (int)indexPath.item + 1);
        lblCaption.text = [NSString stringWithFormat:@"Image %d of %d",(int)(indexPath.item + 1), (int)numberOfImages];
        
    }
    else if ([cell isKindOfClass:[YouTubeCell class]]) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        lblCaption.text = [NSString stringWithFormat:@"Video %d of %d", (int)(indexPath.item + 1 - numberOfImages), (int)numberOfVideos];
    }
    else if ([cell isKindOfClass:[DownloadCell class]]) {
        lblCaption.text = @"";

    }
    
    
}

#pragma mark - UICollectionViewDelegate

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/


#pragma mark - Button Actions
- (void)didTapedBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPerformDownloading:(UIButton *)sender {
    NSLog(@"Arr Sourse %@", _arrSourse);
    
    UIButton *btn = (UIButton *)sender;
    //NSArray *arr = [[_arrSourse lastObject] objectForKey:@"media_gallery"];
    _arrFileDownloadData = [NSMutableArray array];
    for (NSDictionary *dict in _arrSourse) {
        if ([[dict objectForKey:@"type"] integerValue] == 3) {
            NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"url"]];
            [_arrFileDownloadData addObject:[[FileDownloadInfo alloc] initWithFileTitle:[dict objectForKey:@"proj_name"]
                                                                      andDownloadSource:str]];
            
            break;
        }
    }
    
    [btn setTitle:@"Downloading" forState:UIControlStateNormal];
    BackgroundDownloadServices *service = [[BackgroundDownloadServices alloc] init];
    [self.navigationController.view makeToast:@"Downloading PDF" duration:duration position:CSToastPositionCenter];
    [service getPDFFromUrl:[_arrFileDownloadData firstObject] andCallBack:^(BOOL isSuccess) {
        if (isSuccess) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [btn setTitle:@"Download complete" forState:UIControlStateNormal];
                [self.view.window makeToast:strSuccess duration:duration position:CSToastPositionCenter];
            });
            
        }
        else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [btn setTitle:@"Download" forState:UIControlStateNormal];
                [self.view.window makeToast:strUnsucess duration:duration position:CSToastPositionCenter];
            });
            
        }
    }];
}
@end
