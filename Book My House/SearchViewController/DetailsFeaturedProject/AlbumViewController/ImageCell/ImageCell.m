//
//  ImageCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ImageCell.h"

@implementation ImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (ImageCell *)createCell {
    ImageCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"ImageCell" owner:self options:nil] firstObject];
    
    return cell;
}

@end
