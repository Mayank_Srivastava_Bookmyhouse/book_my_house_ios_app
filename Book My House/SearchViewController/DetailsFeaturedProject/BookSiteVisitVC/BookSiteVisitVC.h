//
//  BookSiteVisitVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/9/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MessageUI/MessageUI.h>

#import "Header.h"
#import "DVSwitch.h"
#import "BookSiteVisitCell.h"
#import "LocationHandler.h"

@interface BookSiteVisitVC : UIViewController<LocationHandlerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    LocationHandler *locationHandler;
}
@property (strong, nonatomic) NSDictionary *dictInfo;
@end
