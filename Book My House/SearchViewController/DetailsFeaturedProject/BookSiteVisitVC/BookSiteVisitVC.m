//
//  BookSiteVisitVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/9/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "BookSiteVisitVC.h"
#import "PrivacyViewController.h"
#import "SigninViewController.h"

typedef NS_ENUM(NSUInteger, TimeSlot) {
    TimeSlotBehind,
    TimeSlotAtPar,
    TimeSlotAhead,
};

static NSString * const strEnterProjectName             = @"Enter Project Name";
static NSString * const strName                         = @"Name";
static NSString * const strEmail                        = @"Email";
static NSString * const strMobilenumber                 = @"Mobile Number";
static NSString * const strEnterLocation                = @"Enter Location";
static NSString * const strMyPresentLocation            = @"My Present Location";

static NSString * const strPleaseEnterProjectLocation   = @"Please enter project name";
static NSString * const strPleaseEnterProjectName       = @"Please enter your name";
static NSString * const strPleaseProvideProperName      = @"Please provide your proper name";
static NSString * const strPleaseProvideYourMobNum      = @"Please provide your mobile number";
static NSString * const strPleaseProvideCorrectMobNum   = @"Please provide correct mobile number";
static NSString * const strPleaseProvideEmailId         = @"Please provide email id";
static NSString * const strPleaseProvideCorrectEmail    = @"Please provide correct email id";
static NSString * const strPleaseProvideYourLocation    = @"Please provide your location";

static NSString * const strEnterTime                    = @"Please enter time";
static NSString * const strEnterDate                    = @"Please enter date";
static NSString * const strPleaseCheckMarkT_C           = @"Please check mark T&C";
static NSString * const strCurrentLocationFailed        = @"Current location could not be fetched";
static NSString * const strBookSiteVisitSuccess         = @"Request for Booking Site visit sucessfully sent";
static NSString * const strBookSiteVisitFailed          = @"Request for Booking Site visit failed";
static NSString * const strLoginRequest                 = @"Please Login to book Site Visit";

@interface BookSiteVisitVC () {

    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UITableView *tblView;
    
    CLLocation *currentLocation;
    
    
    UITextField *txtSetDate, *txtSetTime;
    BookSiteVisitCell *cellSiteVisit;
    PickUpLocation *cellPickUpLocation;
    PickUpDateTime *cellPickUpDateTime;
    BookSiteVisit *cellBookSiteVisit;
    GMSGeocoder *geocoder;
    NSUserDefaults *pref;
    
    NSDate *selectedDate, *selectedTime;
    BOOL isTimeSet;
    TimeSlot timeSlot;
    
    BOOL didReceiveCoorinate;
    
}
- (IBAction)actionListener:(id)sender;

@end

@implementation BookSiteVisitVC
- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        locationHandler = [LocationHandler sharedManager];
        locationHandler.delegate = self;
        
        [locationHandler startUpdatingGPSLocation];

        pref = [NSUserDefaults standardUserDefaults];
    }
    return self;
}



#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"This is a project info\n\n %@", _dictInfo);
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [locationHandler stopUpdatingGPSLocation];
}



#pragma mark - Screen Settings
- (void)screenSettings{
    tblView.delegate = self;
    tblView.dataSource = self;
}

- (void)updatedScreenSettings {
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didTappedTermsAndConditions {
    
    PrivacyViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyViewController"];
    obj.option = PrivacyViewControllerAccessOptionTermsAndConditions;
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)setDateAndTime:(UIButton *)btn {
   
    if (btn.tag == 1) {
        [txtSetDate becomeFirstResponder];
    }
    else if (btn.tag == 2) {
        [txtSetTime becomeFirstResponder];
    }
    
}

- (void)setDATE:(UIDatePicker *)datePicker {

    NSComparisonResult result = [datePicker.date compare:[NSDate date]];
    
    if (result == NSOrderedDescending) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        txtSetDate.text = [dateFormatter stringFromDate:datePicker.date];
        selectedDate = datePicker.date;
    }
    else if (result == NSOrderedSame) {
        //datePicker.date = [NSDate date];
    }
    else {
        datePicker.date = [NSDate date];
        [self.view makeToast:@"Please select date at some future time." duration:duration position:CSToastPositionCenter];
    }
}

- (void)setTIME:(UIDatePicker *)datePicker {
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"hh:mm aa"];
    txtSetTime.text = [dateFormatter1 stringFromDate:datePicker.date];
    selectedTime = datePicker.date;
    
}

- (void)bookSiteVisit:(UIButton *)btn {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if (cellSiteVisit.txtProjectName.text.length == 0) {
            [self.view makeToast:strPleaseEnterProjectLocation duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (cellSiteVisit.txtName.text.length == 0) {
            [self.view makeToast:strPleaseEnterProjectName duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (cellSiteVisit.txtEmail.text.length == 0) {
            [self.view makeToast:strPleaseProvideEmailId duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (![AuxillaryMathods isValidEmail:cellSiteVisit.txtEmail.text]) {
            [self.view makeToast:strPleaseProvideCorrectEmail duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (![AuxillaryMathods isUserName:cellSiteVisit.txtName.text]) {
            [self.view makeToast:strPleaseProvideProperName duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (cellSiteVisit.txtMobileNumber.text.length == 0) {
            [self.view makeToast:strPleaseProvideYourMobNum duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (![AuxillaryMathods isValidMobileNumber:cellSiteVisit.txtMobileNumber.text]) {
            [self.view makeToast:strPleaseProvideCorrectMobNum duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (cellPickUpLocation.txtEnterLocation.text.length == 0 && cellPickUpLocation.txtMyPresentLocation.text.length == 0) {
            [self.view makeToast:strPleaseProvideYourLocation duration:duration position:CSToastPositionCenter];
            
            return;
        }
        
        //NSComparisonResult result = [selectedDate compare:[NSDate date]];
        
        if (cellPickUpDateTime.txtTime.text.length == 0) {
            [self.view makeToast:strEnterTime duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (cellPickUpDateTime.txtDate.text.length == 0) {
            [self.view makeToast:strEnterDate duration:duration position:CSToastPositionCenter];
            return;
        }
        
        
        if (!cellPickUpDateTime.btnCheck.selected) {
            [self.view makeToast:strPleaseCheckMarkT_C duration:duration position:CSToastPositionCenter];
            return;
        }
        
        NSString *strLocation;
        if (cellPickUpLocation.txtEnterLocation.text.length) {
            strLocation = cellPickUpLocation.txtEnterLocation.text;
        }
        else if (cellPickUpLocation.txtMyPresentLocation.text.length){
            strLocation = cellPickUpLocation.txtMyPresentLocation.text;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        if (![[_dictInfo objectForKey:@"id"] isKindOfClass:[NSNull class]]) {
            [dict setObject:[_dictInfo objectForKey:@"id"] forKey:@"project_id"];
        }
        
        [dict setObject:cellSiteVisit.txtProjectName.text forKey:@"project_name"];
        [dict setObject:cellSiteVisit.txtMobileNumber.text forKey:@"contact_no"];
        [dict setObject:[NSString stringWithFormat:@"%d", (int)cellSiteVisit.numberOfPerson] forKey:@"no_of_persons"];
        [dict setObject:cellSiteVisit.txtName.text forKey:@"name"];
        [dict setObject:strLocation forKey:@"location"];
        [dict setObject:cellPickUpDateTime.txtTime.text forKey:@"time"];
        [dict setObject:cellPickUpDateTime.txtDate.text forKey:@"date"];
        
        
        
        if (currentLocation != nil) {
            NSString *str = [NSString stringWithFormat:@"(%f, %f)", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
            [dict setObject:str forKey:@"coordinates"];
        }
        
        
        NSString *strUserID = [pref objectForKey:strSaveUserId];
        if (strUserID) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kBookSiteVisit object:nil];
            [dict setObject:strUserID forKey:@"user_id"];
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI bookSiteVisit:dict andCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    [self actionListener:btnClose];
                    [self.navigationController.view makeToast:strBookSiteVisitSuccess duration:duration position:CSToastPositionCenter];
                }
                else {
                    [self.view makeToast:strBookSiteVisitFailed duration:duration position:CSToastPositionCenter];
                }
            }];
            
        }
        else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookSiteVisit:) name:kBookSiteVisit object:nil];
            [self.view.window makeToast:strLoginRequest duration:duration position:CSToastPositionCenter];
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionBookSiteVisit;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(bookSiteVisit:) fromClass:self];
    }
    
}


#pragma mark - API Actions



#pragma mark - Tableview Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return 270;
    }
    else if (indexPath.row == 1) {
        return 486;
    }
    else if (indexPath.row == 2) {
        return 200.0;
    }
    else if (indexPath.row == 3) {
        return 44.0;
    }
    return 0.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        static NSString *strID = @"bookSiteVisit";
        BookSiteVisitCell *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [BookSiteVisitCell createCell];
            [self setBookMySiteTxtField:cell.txtProjectName withPlacehoder:strEnterProjectName];
            [self setBookMySiteTxtField:cell.txtName withPlacehoder:strName];
            [self setBookMySiteTxtField:cell.txtEmail withPlacehoder:strEmail];
            [self setBookMySiteTxtField:cell.txtMobileNumber withPlacehoder:strMobilenumber];
            
            
            NSInteger margin = 20;
            DVSwitch *secondSwitch = [DVSwitch switchWithStringsArray:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7"]];
            secondSwitch.frame = CGRectMake(11, 3, self.view.frame.size.width - margin * 2, 34);
            secondSwitch.backgroundColor = [UIColor whiteColor];
            secondSwitch.sliderColor = [UIColor colorWithRed:219.5/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0];
            secondSwitch.labelTextColorInsideSlider = [UIColor whiteColor];
            secondSwitch.labelTextColorOutsideSlider = [UIColor blackColor];
            secondSwitch.cornerRadius = 0;
            cell.segmentControl.frame = CGRectMake(CGRectGetMinX(cell.segmentControl.frame), CGRectGetMinY(cell.segmentControl.frame), CGRectGetWidth(secondSwitch.frame), 34);
            cell.segmentControl.layer.cornerRadius = 5.0;
            cell.segmentControl.clipsToBounds = YES;
            cell.segmentControl.layer.borderColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
            cell.segmentControl.layer.borderWidth = 2.0;
            
            [cell.segmentControl addSubview:secondSwitch];
            [secondSwitch forceSelectedIndex:0 animated:YES];
            [secondSwitch setPressedHandler:^(NSUInteger index) {
               
                cell.numberOfPerson = index + 1;
                cellSiteVisit.numberOfPerson = index + 1;
            }];
 
        }
        
        
        UIFont *font = cell.lblProjectName.font;
        NSString *str = [_dictInfo objectForKey:@"proj_name"];
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding]
                                                                        options:@{
                                                                                  NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                  }
                                                             documentAttributes:nil
                                                                          error:nil];
        cell.lblProjectName.font = font;

        if (attrStr.length) {
            cell.txtProjectName.attributedText = attrStr;
        }
        
        NSString *strName = [pref objectForKey:strSavedName];
        if (strName != nil) {
            cell.txtName.text = strName;
        }
        
        NSString *strEmail = [pref objectForKey:strSaveUserEmail];
        if (strEmail != nil) {
            cell.txtEmail.text = strEmail;
        }
        cellSiteVisit = cell;
        
        return cell;
    }
    else if (indexPath.row == 1) {
        static NSString * strId = @"pickUpLocation";
        PickUpLocation *cell = [tableView dequeueReusableCellWithIdentifier:strId];
        
        if (cell == nil) {
            cell = [PickUpLocation createCell];
            [self setPickUpLocation:cell.txtEnterLocation withPlaceHolder:strEnterLocation];
            [self setPickUpLocation:cell.txtMyPresentLocation withPlaceHolder:strMyPresentLocation];
        }
        
        if (currentLocation != nil) {
            
            GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)];
            marker.map = nil;
            marker.title = @"My location";
            marker.map = cell.mapView;
            
            GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                            longitude:currentLocation.coordinate.longitude
                                                                                 zoom:10.0];
            
            cell.mapView.camera = cameraPosition;
            
            [[GMSGeocoder geocoder] reverseGeocodeCoordinate:currentLocation.coordinate
                             completionHandler:^(GMSReverseGeocodeResponse * _Nullable response, NSError * _Nullable error) {
                                 
                                 if (!error) {
                                     cell.txtMyPresentLocation.text = [NSString stringWithFormat:@"%@ %@", [[[response firstResult] lines] firstObject], [[[response firstResult] lines] lastObject]];
                                     
                                     [locationHandler stopUpdatingGPSLocation];
                                 }
                                 else {
                                     NSLog(@"Error %@", error.localizedDescription);
                                 }
                             }];
        }
        else {
            
        }

        
        cellPickUpLocation = cell;
        return cell;
    }
    else if (indexPath.row == 2) {
        static NSString *strId = @"PickUpDateTime";
        PickUpDateTime *cell = [tableView dequeueReusableCellWithIdentifier:strId];
        
        if (cell == nil) {
            cell = [PickUpDateTime createCell];
            [self setPickUpDateAndTime:cell.txtDate];
            [self setPickUpDateAndTime:cell.txtTime];
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
    
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd-MM-yyyy"];
        
        if (!isTimeSet) {
            isTimeSet = YES;
            
            timeSlot = [self isTimeInSlot];
            
            if (timeSlot == TimeSlotAtPar) {
                txtSetDate.text = [dateFormatter1 stringFromDate:[NSDate date]];
                txtSetTime.text = [dateFormatter stringFromDate:[[NSDate date] dateByAddingTimeInterval:(30 * 60)]];
            }
            else if (timeSlot == TimeSlotAhead) {
                txtSetDate.text = [dateFormatter1 stringFromDate:[[NSDate date] dateByAddingTimeInterval:(60 * 60 * 24)]];
                txtSetTime.text = @"10:00 am";
            }
            else {
                txtSetDate.text = [dateFormatter1 stringFromDate:[NSDate date]];
                txtSetTime.text = @"10:00 am";
            }
        }
        else {
            
        }
        
        [cell.btnTC addTarget:self action:@selector(didTappedTermsAndConditions) forControlEvents:UIControlEventTouchUpInside];
        
        cellPickUpDateTime = cell;
        
        
        
        return cell;
    }
    else if (indexPath.row == 3) {
        static NSString *strId = @"BookSiteVisit";
        BookSiteVisit *cell = [tableView dequeueReusableCellWithIdentifier:strId];
        
        if (cell == nil) {
            cell = [BookSiteVisit createCell];
            [cell.btnBookSiteVisit addTarget:self action:@selector(bookSiteVisit:) forControlEvents:UIControlEventTouchUpInside];
        }
        cellBookSiteVisit = cell;
        return cell;
    }
    
    return nil;
    
}
#pragma mark  - Auxillary Mathods
- (void)setBookMySiteTxtField:(UITextField *)txtField withPlacehoder:(NSString *)strPlaceholder{
    
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtField.layer addSublayer:layer];
    
    [self setPlacehoder:strPlaceholder on:txtField];

    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtField.frame))];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer1.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [view.layer addSublayer:layer1];
    
    view.backgroundColor = [UIColor whiteColor];
    txtField.leftView = view;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    

    txtField.delegate = self;
}

- (void)setPickUpLocation:(UITextField *)txtField withPlaceHolder:(NSString *)strPlaceholder {
    
    [self setPlacehoder:strPlaceholder on:txtField];
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, CGRectGetHeight(txtField.frame))];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 20, 0.5 * CGRectGetHeight(txtField.frame))];
    
    imgView.center = aView.center;
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer1.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [imgView.layer addSublayer:layer1];
    
    [aView addSubview:imgView];
    
    if ([strPlaceholder isEqualToString:strEnterLocation]) {
        imgView.image = [UIImage imageNamed:@"finder"];
    }
    else {
        imgView.image = [UIImage imageNamed:@"cross-hair"];
    }
    txtField.leftView = aView;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtField.layer addSublayer:layer];

}


- (void)setPickUpDateAndTime:(UITextField *)txtField {
    
    if (txtField.tag == 1) {
        txtSetDate = txtField;
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
        lbl.font = [UIFont fontWithName:@"PT Sans" size:17.0];
        lbl.text = @"Date :";
        lbl.textAlignment = NSTextAlignmentLeft;
        
        txtField.leftView = lbl;
        txtField.leftViewMode = UITextFieldViewModeAlways;
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
        [btn setImage:[UIImage imageNamed:@"calendar"] forState:UIControlStateNormal];
        btn.tag = txtField.tag;
        [btn addTarget:self action:@selector(setDateAndTime:) forControlEvents:UIControlEventTouchUpInside];
        txtField.rightView = btn;
        txtField.rightViewMode = UITextFieldViewModeAlways;
        
        
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setDate:[NSDate date] animated:YES];
        [datePicker setMinimumDate:[NSDate date]];
        [datePicker addTarget:self action:@selector(setDATE:) forControlEvents:UIControlEventValueChanged];
        txtField.inputView = datePicker;

        
        
    }
    else {
        
        txtSetTime = txtField;
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
        lbl.font = [UIFont fontWithName:@"PT Sans" size:17.0];
        lbl.text = @"Time :";
        lbl.textAlignment = NSTextAlignmentLeft;
        txtField.leftView = lbl;
        txtField.leftViewMode = UITextFieldViewModeAlways;
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
        [btn setImage:[UIImage imageNamed:@"clock"] forState:UIControlStateNormal];
        btn.tag = txtField.tag;
        [btn addTarget:self action:@selector(setDateAndTime:) forControlEvents:UIControlEventTouchUpInside];
        txtField.rightView = btn;
        txtField.rightViewMode = UITextFieldViewModeAlways;
        
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePicker.datePickerMode = UIDatePickerModeTime;
        [datePicker setDate:[NSDate date] animated:YES];
        [datePicker setMinimumDate:[NSDate date]];
        [datePicker setMinuteInterval:5];
        [datePicker addTarget:self action:@selector(setTIME:) forControlEvents:UIControlEventValueChanged];
        txtField.inputView = datePicker;

    }
    
}



- (void)setPlacehoder:(NSString *)strPlaceholder on:(UITextField *)txtField {
    
    txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strPlaceholder attributes:@{
                                     NSForegroundColorAttributeName : [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1.0],
                                                                                                     }];

}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



#pragma mark - GPS Services
- (void)getCurrentLocation:(CLLocation *)location {
    //28.4238° N, 77.0399° E
    
    if (!didReceiveCoorinate && ![location isKindOfClass:[NSNull class]]) {
        
        currentLocation = location;
        if (!didReceiveCoorinate) {
            didReceiveCoorinate = YES;
            [locationHandler stopUpdatingGPSLocation];
            
        }
        else {
            
            didReceiveCoorinate = NO;
        }
        

    }
    else {
        
    }   
}

#pragma mark - Text Fields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength <= 10;
    }
    else {
        return YES;
    }
    
    
}

- (TimeSlot)isTimeInSlot {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm";
    
    NSString *strStartTime = @"10:00";
    NSString *strEndTime = @"16:30";
    NSString *strCurrentTime = [formatter stringFromDate:[NSDate date]];
    //NSString *strCurrentTime = @"01:00";
    
   // NSDate *currentTime = [NSDate date];
    
    NSComparisonResult result1 = [[formatter dateFromString:strCurrentTime] compare:[formatter dateFromString:strStartTime]];
    NSComparisonResult result2 = [[formatter dateFromString:strCurrentTime] compare:[formatter dateFromString:strEndTime]];
    
    if (result1 == NSOrderedDescending && result2 == NSOrderedAscending) {
        return TimeSlotAtPar;
    }
    else if (result1 == NSOrderedAscending && result2 == NSOrderedAscending) {
        return TimeSlotBehind;
    }
    else if (result1 == NSOrderedDescending && result2 == NSOrderedDescending) {
        return TimeSlotAhead;
    }
    else {
        return TimeSlotAtPar;
    }
}

//- (TimeSlot)checkTimeInSlotDate:(NSString *)strDate andTime:(NSString *)strTime {
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"hh:mm a"];
//    NSString *strCurrentDate = [dateFormatter stringFromDate:[NSDate date]];
//    
//    
//    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
//    [dateFormatter1 setDateFormat:@"dd-MM-yyyy"];
//    NSString *strCurrentTime = [dateFormatter1 stringFromDate:[NSDate date]];
//    
//    return 0;
//}

@end