//
//  BookSiteVisitCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/9/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DVSwitch.h"
@import GoogleMaps;

@interface BookSiteVisitCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UITextField *txtProjectName;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfPerson;
@property (weak, nonatomic) IBOutlet UIView *segmentControl;
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;

@property (assign, nonatomic) NSUInteger numberOfPerson;

+ (BookSiteVisitCell *)createCell;
@end


@interface PickUpLocation : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtEnterLocation;
@property (weak, nonatomic) IBOutlet UITextField *txtMyPresentLocation;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
+ (PickUpLocation *)createCell;
@end


@interface PickUpDateTime : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtTime;
@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnTC;
- (IBAction)actionListener:(id)sender;
+ (PickUpDateTime *)createCell;
@end

@interface BookSiteVisit : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnBookSiteVisit;
+ (BookSiteVisit *)createCell;
@end