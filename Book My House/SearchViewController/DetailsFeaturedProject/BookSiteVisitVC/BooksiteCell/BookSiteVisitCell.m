//
//  BookSiteVisitCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/9/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "BookSiteVisitCell.h"

@implementation BookSiteVisitCell

#pragma mark - Cell Creation
+ (BookSiteVisitCell *)createCell {
    BookSiteVisitCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"BookSiteVisitCell" owner:self options:nil] firstObject];
    
    return cell;
}

#pragma mark - UITextField Delegates
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if (textField == _txtProjectName) {
        return newLength <= 50 || returnKey;
    }
    else if (textField == _txtName) {
        return newLength <= 35 || returnKey;
    }
    else if (textField == _txtEmail) {
        if ([textField isFirstResponder])
        {
            if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
            {
                return NO;
            }
        }
        return newLength <= 40 || returnKey;
    }
    else if (textField == _txtMobileNumber) {
        return newLength <= 10 || returnKey;
    }
    return YES;
}
@end


@implementation PickUpLocation

+ (PickUpLocation *)createCell {
    PickUpLocation *cell = [[[NSBundle mainBundle] loadNibNamed:@"BookSiteVisitCell" owner:self options:nil] objectAtIndex:1];
    
    return cell;
}
@end

@implementation PickUpDateTime



+ (PickUpDateTime *)createCell {
    PickUpDateTime *cell = [[[NSBundle mainBundle] loadNibNamed:@"BookSiteVisitCell" owner:self options:nil] objectAtIndex:2];
    
    return cell;
}


- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnCheck) {
        _btnCheck.selected = !_btnCheck.selected;
    }
}
@end

@implementation  BookSiteVisit

+ (BookSiteVisit *)createCell {
    BookSiteVisit *cell = [[[NSBundle mainBundle] loadNibNamed:@"BookSiteVisitCell" owner:self options:nil] objectAtIndex:3];
    
    return cell;
}
@end