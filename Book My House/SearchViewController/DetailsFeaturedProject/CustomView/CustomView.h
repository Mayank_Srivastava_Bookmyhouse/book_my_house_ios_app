//
//  TopView.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/23/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "UIPlaceholderTextView.h"



@interface CustomView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgPdf;
@property (weak, nonatomic) IBOutlet UIButton *btnPDFDownload;

@end

@interface EnquiryView : UIView
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtProjectName;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UIPlaceholderTextView *txtMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (weak, nonatomic) UIViewController *owner;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsAndConditions;

@property (assign, nonatomic) BOOL isOpen;
- (IBAction)actionListener:(id)sender;
+ (EnquiryView *)createView;
@end


@interface GetAlertView : UIView
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsAndConditions;
+ (GetAlertView *)createView;
@end

@interface VarificationView : UIView

@end


@interface NoInternetView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnTryAgain;
@property (assign, nonatomic) SEL action;
@property (weak, nonatomic) UIViewController *parentClass;
- (IBAction)actionListener:(id)sender;
+ (NoInternetView *)createView;
@end

@interface ForgotPasswordView : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (assign, nonatomic) BOOL isSuccess;
+ (ForgotPasswordView *)createPassword;
- (IBAction)actionListerner:(id)sender;

@end

@interface NoUnitView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
+ (NoUnitView *)createView;
@end


