//
//  TopView.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/23/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

//- (void)awakeFromNib {
////    [super awakeFromNib];
//    _btnPDFDownload.layer.cornerRadius = 5.0;
//}

@end

@implementation EnquiryView

+ (EnquiryView *)createView {
    EnquiryView *view = [[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil] objectAtIndex:1];
    return view;
}


- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnCheck) {
        _btnCheck.selected = !_btnCheck.selected;
    }
}

@end

@implementation GetAlertView

+ (GetAlertView *)createView {
    GetAlertView *view = [[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil] objectAtIndex:2];
    
    return view;
}

- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnCheck) {
        _btnCheck.selected = !_btnCheck.selected;
    }
}

@end

@implementation VarificationView


@end

@implementation NoInternetView

+ (NoInternetView *)createView {
    
    
    NoInternetView *view = [[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil] objectAtIndex:4];
    
    return view;
}

- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnTryAgain) {
        
    }
}
@end


@implementation ForgotPasswordView

+ (ForgotPasswordView *)createPassword {
    ForgotPasswordView *view = [[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil] objectAtIndex:5];
    
    return view;
}

- (IBAction)actionListerner:(id)sender {
    
    if (sender == _btnCancel) {
        
    }
    else if (sender == _btnOK) {
        
    }
}
@end

@implementation NoUnitView

+ (NoUnitView *)createView {
    NoUnitView *view = [[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil] objectAtIndex:6];
    
    return view;
}

@end
