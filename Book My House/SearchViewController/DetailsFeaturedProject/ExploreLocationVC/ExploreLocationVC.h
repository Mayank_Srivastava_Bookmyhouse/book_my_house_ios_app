//
//  ExploreLocationVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/10/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

typedef NS_ENUM(NSUInteger, ELAccessOption) {
    ELAccessOptionFeatureProject,
    ELAccessOptionUnitSummary,
    ELAccessOptionMisc,
};

@interface ExploreLocationVC : UIViewController
@property(nonatomic, assign) ELAccessOption option;
@property (assign, nonatomic) NSDictionary *dictInfo;

@end
