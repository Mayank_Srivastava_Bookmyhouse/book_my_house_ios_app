//
//  ExploreLocationVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/10/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ExploreLocationVC.h"
#import "HMSegmentedControl.h"
@import GoogleMaps;

@interface ExploreLocationVC ()<GMSMapViewDelegate> {
    
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet GMSMapView *mapViewNearby;
    __weak IBOutlet UIButton *btnWay;
    __weak IBOutlet UIButton *btnZoomIn;
    __weak IBOutlet UIButton *btnZoomOut;
    
    
    __weak IBOutlet UIView *viewSegmented;
    HMSegmentedControl *segmentedControl;
    
    GMSPolyline *polyLine;
    
    NSInteger zoom;
    NSArray *arrDispalyData;
    GMSMarker *markerInfo, *myLocation;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation ExploreLocationVC

#pragma mark - View Controller Mathods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    zoom = 0;
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateScreenSettings];
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    if (_option == ELAccessOptionFeatureProject) {
        NSString *strProjectName = [NSString stringWithFormat:@"%@\n", [_dictInfo objectForKey:@"proj_name"]];
        NSString *strProjectAddress = [NSString stringWithFormat:@"%@, %@", [[_dictInfo objectForKey:@"address"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]], [_dictInfo objectForKey:@"city"]];
        
        
        NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:strProjectName
                                                                       attributes:@{
                                                                                    NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                    NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:19.0]
                                                                                                              }];
        
        NSMutableAttributedString *strAddress = [[NSMutableAttributedString alloc] initWithString:strProjectAddress
                                                                                       attributes:@{
                                                                        NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                        NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:14.0],
                                                                                                                                 }];
        [strTitle appendAttributedString:strAddress];
        lblTitle.attributedText = strTitle;
        
    }
    else if (_option == ELAccessOptionUnitSummary) {
        NSString *strProjectName = [NSString stringWithFormat:@"%@\n", [_dictInfo objectForKey:@"project_name"]];
        NSString *strProjectAddress = [NSString stringWithFormat:@"%@, %@", [[_dictInfo objectForKey:@"address"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]], [_dictInfo objectForKey:@"city_name"]];
        
        NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:strProjectName
                                                                                     attributes:@{
                                                                                                  NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                  NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:19.0],
                                                                                                  }];
        
        NSMutableAttributedString *strAddress = [[NSMutableAttributedString alloc] initWithString:strProjectAddress
                                                                                       attributes:@{
                                                                                                    NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                    NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:14.0],
                                                                                                    }];
        [strTitle appendAttributedString:strAddress];
        lblTitle.attributedText = strTitle;
    }
    
    NSArray *arr = [[_dictInfo objectForKey:@"lat_lng"] componentsSeparatedByString:@","];
    
    mapViewNearby.camera = [GMSCameraPosition cameraWithLatitude:[[arr firstObject] floatValue] longitude:[[arr lastObject] floatValue] zoom:10.0];
    
    myLocation = [[GMSMarker alloc] init];
    myLocation.position = CLLocationCoordinate2DMake([[arr firstObject] floatValue], [[arr lastObject] floatValue]);
    
    myLocation.title = [_dictInfo objectForKey:@"proj_name"];
    myLocation.snippet = [_dictInfo objectForKey:@"developer_name"];
    myLocation.map = mapViewNearby;
    myLocation.appearAnimation = kGMSMarkerAnimationPop;
    mapViewNearby.delegate = self;
}

- (void)updateScreenSettings {
    

    NSArray *arrGreen = [NSArray arrayWithObjects:[UIImage imageNamed:@"Landmarks-Green"], [UIImage imageNamed:@"Airport-Green"], [UIImage imageNamed:@"Railways-Green"], [UIImage imageNamed:@"Bus-stand-Green"],[UIImage imageNamed:@"Taxi-services-Green"], [UIImage imageNamed:@"Schools-Green"], [UIImage imageNamed:@"Hospitals-Green"], [UIImage imageNamed:@"Shopping-malls-Green"],[UIImage imageNamed:@"Departmental-stores-Green"],[UIImage imageNamed:@"Pharmacies-Green"],[UIImage imageNamed:@"Parks-Green"],[UIImage imageNamed:@"Banks-Green"],[UIImage imageNamed:@"Atms-Green"],[UIImage imageNamed:@"Restaurants-Green"],[UIImage imageNamed:@"Hotels-Green"],[UIImage imageNamed:@"Movie-theaters-Green"],[UIImage imageNamed:@"Night-clubs-Green"], nil];
    
    NSArray *arrGold = [NSArray arrayWithObjects:[UIImage imageNamed:@"Landmarks-Gold"], [UIImage imageNamed:@"Airport-Gold"], [UIImage imageNamed:@"Railways-Gold"], [UIImage imageNamed:@"Bus-stand-Gold"],[UIImage imageNamed:@"Taxi-services-Gold"], [UIImage imageNamed:@"Schools-Gold"], [UIImage imageNamed:@"Hospitals-Gold"], [UIImage imageNamed:@"Shopping-malls-Gold"],[UIImage imageNamed:@"Departmental-stores-Gold"],[UIImage imageNamed:@"Pharmacies-Gold"],[UIImage imageNamed:@"Parks-Gold"],[UIImage imageNamed:@"Banks-Gold"],[UIImage imageNamed:@"Atms-Gold"],[UIImage imageNamed:@"Restaurants-Gold"],[UIImage imageNamed:@"Hotels-Gold"],[UIImage imageNamed:@"Movie-theaters-Gold"],[UIImage imageNamed:@"Night-clubs-Gold"], nil];
    
//    NSArray *arrCaption = [NSArray arrayWithObjects:@"LANDMARKS", @"AIRPORT", @"AIRPORT", @"BUS STAND", @"TAXI SERVICES", @"SCHOOLS", @"HOSPITALS", @"SHOPPING MALL", @"DEPARTMENTAL STORE", @"PHARMACIES", @"PARKS", @"BANKS", @"ATM", @"RESTAURANT", @"HOTEL", @"MOVIE-THEATER", @"NIGHT CLUBS", nil];
    

    segmentedControl = [[HMSegmentedControl alloc] initWithSectionImages:arrGreen sectionSelectedImages:arrGold];
    segmentedControl.frame = CGRectMake(0, 0, CGRectGetWidth(viewSegmented.frame), CGRectGetHeight(viewSegmented.frame));
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;    
    [viewSegmented addSubview:segmentedControl];
    
    segmentedControl.selectedSegmentIndex = 0;
    [segmentedControl addTarget:self action:@selector(actionListener:) forControlEvents:UIControlEventValueChanged];
    [segmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
}


#pragma mark - Button Action
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnClose) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnWay) {
        
    }
    else if (sender == btnZoomIn || sender == btnZoomOut)  {
        
        if (zoom > 20) {
            zoom = 20;
            return;
        }
        
        if (zoom < 8) {
            zoom = 8;
            return;
        }
        
        if (sender == btnZoomOut) {
            [mapViewNearby animateToZoom:-- zoom];
        }
        else if (sender == btnZoomIn) {
            [mapViewNearby animateToZoom:++ zoom];
        }
    }
    else  {
        [self loadInfo];
    }
}

#pragma mark - API Action
- (void)loadInfo {

    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        polyLine.path = nil;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        if (![[_dictInfo objectForKey:@"lat_lng"] isKindOfClass:[NSNull class]]) {
            [dict setObject:[_dictInfo objectForKey:@"lat_lng"] forKey:@"location"];
        }
        [dict setObject:@"50000" forKey:@"radius"];
        
        
        if (segmentedControl.selectedSegmentIndex == 0) {//landmark
            [dict setObject:@"point_of_interest" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 1) {//Airport
            [dict setObject:@"airport" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 2) {//railway
            [dict setObject:@"train_station" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 3) {//Busstand
            [dict setObject:@"bus_station" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 4) {//Taxi service
            [dict setObject:@"taxi_stand" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 5) {//schools
            [dict setObject:@"school" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 6) {//Hospitals
            [dict setObject:@"hospital" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 7) {//Shopping mall
            [dict setObject:@"shopping_mall" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 8) {//departmental store
            [dict setObject:@"department_store" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 9) {//pharmacies
            [dict setObject:@"pharmacy" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 10) {//parks
            [dict setObject:@"park" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 11) {//banks
            [dict setObject:@"bank" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 12) {//atms
            [dict setObject:@"atm" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 13) {//Restaurant
            [dict setObject:@"restaurant" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 14) {//hotel
            [dict setObject:@"restaurant" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 16) {//movie theators
            [dict setObject:@"movie_theater" forKey:@"type"];
        }
        else if (segmentedControl.selectedSegmentIndex == 16) {//night clubs
            [dict setObject:@"night_club" forKey:@"type"];
        }
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getGooglePlaceAPIResponse:dict
                                 withCallback:^(BOOL isSuccess, id data) {
                                     
                                     [[AppDelegate share] enableUserInteraction];
                                     if (isSuccess) {
                                         
                                         [mapViewNearby clear];
                                         
                                         NSArray *arr = [[_dictInfo objectForKey:@"lat_lng"] componentsSeparatedByString:@","];
                                         
                                         mapViewNearby.camera = [GMSCameraPosition cameraWithLatitude:[[arr firstObject] floatValue] longitude:[[arr lastObject] floatValue] zoom:10.0];
                                         
                                         myLocation = [[GMSMarker alloc] init];
                                         myLocation.position = CLLocationCoordinate2DMake([[arr firstObject] floatValue], [[arr lastObject] floatValue]);
                                         
                                         myLocation.title = [_dictInfo objectForKey:@"proj_name"];
                                         myLocation.snippet = [_dictInfo objectForKey:@"developer_name"];
                                         myLocation.map = mapViewNearby;
                                         myLocation.iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UserLocation"]];
                                         myLocation.appearAnimation = kGMSMarkerAnimationPop;
                                         
                                         
                                         for (NSDictionary *dict in data) {
                                             
                                             if (segmentedControl.selectedSegmentIndex == 1) {
                                                 NSString *str = [dict objectForKey:@"name"];
                                                 
                                                 if (![str containsString:@"International Airport"]) {
                                                     continue;
                                                 }
                                             }
                                             
                                             markerInfo = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake([[dict valueForKeyPath:@"geometry.location.lat"] floatValue], [[dict valueForKeyPath:@"geometry.location.lng"] floatValue])];
                                             markerInfo.title = [dict objectForKey:@"name"];
                                             markerInfo.appearAnimation = YES;
                                             NSString *strImage;
                                             if (segmentedControl.selectedSegmentIndex == 0) {//landmark
                                                 strImage = @"MyLocation";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 1) {//Airport
                                                 strImage = @"airport";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 2) {//railway
                                                 strImage = @"railways";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 3) {//Busstand
                                                 strImage = @"bus-stand";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 4) {//Taxi service
                                                 strImage = @"taxi-services";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 5) {//schools
                                                 strImage = @"schools";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 6) {//Hospitals
                                                 strImage = @"hotels";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 7) {//Shopping mall
                                                 strImage = @"shopping-malls";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 8) {//departmental store
                                                 strImage = @"departmental-stores";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 9) {//pharmacies
                                                 strImage = @"pharmacies";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 10) {//parks
                                                 strImage = @"parks";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 11) {//banks
                                                 strImage = @"banks";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 12) {//atms
                                                 strImage = @"atms";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 13) {//Restaurant
                                                 strImage = @"restaurants";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 14) {//hotel
                                                 strImage = @"hotels";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 16) {//movie theators
                                                 strImage = @"movie-theaters";
                                             }
                                             else if (segmentedControl.selectedSegmentIndex == 16) {//night clubs
                                                 strImage = @"night-clubs";
                                             }
                                             
                                             markerInfo.iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:strImage]];
                                             markerInfo.map = mapViewNearby;
                                             
                                         }
                                     }
                                     else {
                                         [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                                     }
                                     
                                 }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadInfo) fromClass:self];
    }
    
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Auxillary Mathods



#pragma mark - Google maps Delegates
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    zoom = position.zoom;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
    
    
    if (marker != myLocation) {
        
        NSArray *arr = [[_dictInfo objectForKey:@"lat_lng"] componentsSeparatedByString:@","];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSMutableDictionary *dict1 = [NSMutableDictionary dictionary];
        [dict1 setObject:[arr firstObject] forKey:@"latitude"];
        [dict1 setObject:[arr lastObject] forKey:@"longitude"];
        [dict setObject:[dict1 copy] forKey:@"origin"];
        
        [dict1 setObject:[NSNumber numberWithFloat:marker.position.latitude] forKey:@"latitude"];
        [dict1 setObject:[NSNumber numberWithFloat:marker.position.longitude] forKey:@"longitude"];
        [dict setObject:[dict1 copy] forKey:@"destination"];
        
        polyLine.path = nil;
        [ServiceAPI getGoogleMapShortestRoute:dict withCallback:^(BOOL isSuccess, id data) {
           
            if (isSuccess) {
                NSArray *arrRoutes = [data objectForKey:@"routes"];
                
                if (arrRoutes.count) {
                    
                    if ([[arrRoutes firstObject] objectForKey:@"overview_polyline"]) {
                        NSString *points = [[[arrRoutes firstObject] objectForKey:@"overview_polyline"] objectForKey:@"points"];
                        
                        marker.snippet = [NSString stringWithFormat:@"Distance: %@", [[[[[arrRoutes firstObject] objectForKey:@"legs"] firstObject] objectForKey:@"distance"] objectForKey:@"text"]];
                        
                        GMSPath *path = [GMSPath pathFromEncodedPath:points];
                        polyLine = [GMSPolyline polylineWithPath:path];
                        polyLine.strokeColor = [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0];
                        polyLine.strokeWidth = 5.0;
                        
                        polyLine.map = mapView;
                        mapView.selectedMarker = marker;
                    }
                    else {
                        
                    }
                    
                }
                else {
                    
                }
            }
            else {
                
            }
        }];
        //Remove previous line then draw new line
        
        
        //Draw new line
        
        
    }
    return YES;
}


@end
