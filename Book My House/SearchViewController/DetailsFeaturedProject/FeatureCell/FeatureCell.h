//
//  FeatureCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/23/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderColCell.h"
#import "Header.h"
#import "CustomView.h"
#import "HeaderColCell.h"
#import "EDStarRating.h"
#import "UnitImageCollCell.h"
#import "LifestyleColCell.h"
#import "UIPlaceholderTextView.h"
#import "AlbumViewController.h"
@import GoogleMaps;



@protocol Header1Delegate <NSObject>
- (void)didSelectedClickForMore;
@end


@interface Header1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnClickForMore;
@property (weak, nonatomic) UIViewController *owner;
@property (weak, nonatomic) id<Header1Delegate> delegate;
@property (copy, nonatomic) NSArray *arrMedia;

+ (Header1 *)createCell;
@end




@interface FeatureCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPossession;
@property (weak, nonatomic) IBOutlet UILabel *lblNewLaunch;
@property (weak, nonatomic) IBOutlet UILabel *lblFeatures;
@property (weak, nonatomic) IBOutlet UILabel *lblBasePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblInfra;
@property (weak, nonatomic) IBOutlet UILabel *lblNeeds;
@property (weak, nonatomic) IBOutlet UILabel *lblLifeStyle;

@end


@interface OverviewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblOverview;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalArea;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfBlocks;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfFloors;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfUits;
@property (weak, nonatomic) IBOutlet UIWebView *webProjectSummary;
@property (weak, nonatomic) IBOutlet UIButton *btnReadMore;
@end

@interface Footer1 : UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblSpecialFeatures;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSArray *arrEntries;

- (IBAction)actionListener:(id)sender;
+ (Footer1 *)createCell;
@end


@interface Header2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUnits;
+ (Header2 *)createCell;
@end

@interface UnitImageCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnBookNow;

@property (copy, nonatomic) NSArray *arrInfo;
+ (UnitImageCell *)createCell;
@end


@interface UnitDescCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;

+ (UnitDescCell *)createCell;
@end


@interface SubUnitDescCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblUnits;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblAreaRange;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceRange;
+ (SubUnitDescCell *)createCell;
@end


@interface Header3 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblExploreLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;

+ (Header3 *)createCell;
@end


@interface LocationMap : UITableViewCell
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

+ (LocationMap *)createCell;
@end


@interface Footer3 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnExploreNeighbourhood;
@property (weak, nonatomic) IBOutlet UIButton *btnLandmark;
@property (weak, nonatomic) IBOutlet UIButton *btnNeeds;
@property (weak, nonatomic) IBOutlet UIButton *btnTransport;
+ (Footer3 *)createCell;
@end


@interface Header4 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
+ (Header4 *)createCell;
@end


@interface DetailLifeStyle : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) NSDictionary *dictAminities, *dictServices, *dictSafety, *dictRecreation;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (strong, nonatomic) NSArray *arrInfo;


@property (weak, nonatomic) IBOutlet UILabel *lblAminities;
@property (weak, nonatomic) IBOutlet UIButton *btnAminitiesLeft;
@property (weak, nonatomic) IBOutlet UICollectionView *colAminities;
@property (weak, nonatomic) IBOutlet UIButton *btnAminitiesRight;

@property (weak, nonatomic) IBOutlet UILabel *lblservices;
@property (weak, nonatomic) IBOutlet UIButton *btnServicesLeft;
@property (weak, nonatomic) IBOutlet UICollectionView *colServices;
@property (weak, nonatomic) IBOutlet UIButton *btnServicesRight;

@property (weak, nonatomic) IBOutlet UILabel *lblSafety;
@property (weak, nonatomic) IBOutlet UIButton *btnSafetyLeft;
@property (weak, nonatomic) IBOutlet UICollectionView *colSafety;
@property (weak, nonatomic) IBOutlet UIButton *btnSafetyRight;

@property (weak, nonatomic) IBOutlet UILabel *lblRecreation;
@property (weak, nonatomic) IBOutlet UIButton *btnRecreationLeft;
@property (weak, nonatomic) IBOutlet UICollectionView *colRecreation;
@property (weak, nonatomic) IBOutlet UIButton *btnRecreationRight;

- (IBAction)actionListener:(id)sender;
+ (DetailLifeStyle *)createCell;
@end

















@interface Header5 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUnitSpecifications;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;

+ (Header5 *)createCell;

@end


@interface DetailedUnitSpecification : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTop;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle1;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption1;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle2;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption2;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle3;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption3;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle4;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption4;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle5;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption5;

+ (DetailedUnitSpecification *)createCell;
@end

@interface Header6 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
+ (Header6 *)createCell;
@end


@interface PriceTrend : UITableViewCell
@property (weak, nonatomic) IBOutlet UIWebView *web;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

+ (PriceTrend *)createCell;
@end


@interface LocalityInsight : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *txtLocalityInsight;
@property (weak, nonatomic) IBOutlet UIButton *btnReadMore;

+ (LocalityInsight *)createCell;
@end


@interface DeveloperProfile : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblYearOfEstablishment;
@property (weak, nonatomic) IBOutlet UILabel *lblAreaDelivered;
@property (weak, nonatomic) IBOutlet UIWebView *web;
@property (weak, nonatomic) IBOutlet UIButton *btnReadMore;
+ (DeveloperProfile *)createCell;
@end

@interface ReviewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
+ (ReviewCell *)createCell;
@end


@interface Review  : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet EDStarRating *rating;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

+ (Review *)createCell;
@end


@interface Footer6 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnSeeAllReviews;
+ (Footer6 *)createCell;
@end

@interface PostComment : UITableViewCell<UITextFieldDelegate, UITextViewDelegate, EDStarRatingProtocol>
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UIView *viewTitle;

@property (weak, nonatomic) IBOutlet UIPlaceholderTextView *txtMessage;
@property (weak, nonatomic) IBOutlet UIView *viewMessage;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UIButton *btnPostAComment;

+ (PostComment *)createCell;
- (void)setUpStarRating;
@end
