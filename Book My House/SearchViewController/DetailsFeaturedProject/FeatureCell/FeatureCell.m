//
//  FeatureCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/23/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "FeatureCell.h"
#import "SpecialFeatureCell.h"




@implementation Header1

+ (Header1 *)createCell {
    
    Header1 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:2];
    [cell.collectionView registerClass:[HeaderColCell class] forCellWithReuseIdentifier:@"Header"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"HeaderColCell" bundle:nil] forCellWithReuseIdentifier:@"Header"];
    
    return cell;
}


#pragma mark - Collection View
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrMedia.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *strId = @"Header";
    HeaderColCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strId forIndexPath:indexPath];
    
    if ([[[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] == 1) {
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame))];
        
//        [imgView setImageWithURL:[NSURL URLWithString:[[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"url"]];
        [imgView setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        [cell addSubview:imgView];
        
        
    }
    else if ([[[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] == 2) {
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame))];
        [imgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/hqdefault.jpg", [[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"url"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        [cell addSubview:imgView];
        
        UIImageView *imgYoutubeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
        imgYoutubeIcon.image = [UIImage imageNamed:@"youtube-icon"];
        imgYoutubeIcon.center = imgView.center;
        [cell addSubview:imgYoutubeIcon];
        
        [cell bringSubviewToFront:imgYoutubeIcon];
//        YTPlayerView *youTubeView = [[YTPlayerView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame))];
//        [youTubeView loadWithVideoId:[[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"url"]];
//        //youTubeView.delegate = self;
//        [cell addSubview:youTubeView];
    }
    else if ([[[_arrMedia objectAtIndex:indexPath.row] objectForKey:@"type"] integerValue] == 3) {
        CustomView *pdfView = [[[NSBundle mainBundle] loadNibNamed:@"CustomView" owner:self options:nil] objectAtIndex:0];
        pdfView.frame = CGRectMake(0, 0, CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame));
        [pdfView.btnPDFDownload addTarget:_owner action:@selector(downLoadPDF:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:pdfView];
    }
    else {
        
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [_delegate didSelectedClickForMore];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}
@end



@implementation FeatureCell

- (void)awakeFromNib {
    
    _lblInfra.layer.cornerRadius = _lblLifeStyle.layer.cornerRadius = _lblNeeds.layer.cornerRadius = 1.0;
    _lblInfra.clipsToBounds = _lblLifeStyle.clipsToBounds = _lblNeeds.clipsToBounds = YES;
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


@implementation OverviewCell



@end



@implementation Footer1 {
    BOOL isCellVisible;
}



+ (Footer1 *)createCell {
    Footer1 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:3];
    [cell.collectionView registerClass:[SpecialFeatureCell class] forCellWithReuseIdentifier:@"specialFeatures"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"SpecialFeatureCell" bundle:nil] forCellWithReuseIdentifier:@"specialFeatures"];
    
    
    return cell;
}


#pragma mark - CollectionView Delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrEntries.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SpecialFeatureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"specialFeatures" forIndexPath:indexPath];
    cell.lblTitle.text = [_arrEntries objectAtIndex:indexPath.row];
    
    
    return cell;
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    if (!isCellVisible) {
//        isCellVisible = YES;
//        if (collectionView.contentSize.width > collectionView.frame.size.width) {
//            _btnRight.hidden = NO;
//            _btnLeft.hidden = YES;
//            
//        }
//        else {
//            _btnLeft.hidden = _btnRight.hidden = YES;
//            
//        }
//    }
//}

- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnLeft) {
        
    }
    else {
        
    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    
//    NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
//    if ([[visibleItems firstObject] row] == 0 && [[visibleItems lastObject] row] != _arrEntries.count - 1) {
//        _btnRight.hidden = NO;
//        _btnLeft.hidden = YES;
//    }
//    else if ([[visibleItems firstObject] row] != 0 && [[visibleItems lastObject] row] != _arrEntries.count - 1) {
//        _btnRight.hidden = NO;
//        _btnLeft.hidden = NO;
//    }
//    else if ([[visibleItems firstObject] row] != 0 && [[visibleItems lastObject] row] == _arrEntries.count - 1) {
//        _btnRight.hidden = YES;
//        _btnLeft.hidden = NO;
//    }
//    else {
//        _btnRight.hidden = YES;
//        _btnLeft.hidden = YES;
//    }
//}

@end


@implementation Header2

+ (Header2 *)createCell {
    Header2 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:4];
    
    return cell;
}


@end



@implementation UnitImageCell

+ (UnitImageCell *)createCell {
    UnitImageCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:5];
    cell.btnBookNow.layer.cornerRadius = 5.0;
    return cell;
}



#pragma mark - Collection View Delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame));
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrInfo.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UnitImageCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UnitImageCollCell" forIndexPath:indexPath];
    
    NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [[_arrInfo objectAtIndex:indexPath.row] objectForKey:@"url"]];
    [cell.imgBg setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
   
    return cell;
}

@end



@implementation UnitDescCell

+ (UnitDescCell *)createCell {
    UnitDescCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:6];
    
    return cell;
}

@end


@implementation SubUnitDescCell

+ (SubUnitDescCell *)createCell {
    SubUnitDescCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:7];
    return cell;
}

@end


@implementation Header3
+ (Header3 *)createCell {
    
    Header3 *cell =[[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:8];
    
    return cell;
}
@end


@implementation LocationMap

+ (LocationMap *)createCell {
    LocationMap *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:9];
    
    return cell;
}

@end


@implementation Footer3

+ (Footer3 *)createCell {
    Footer3 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:10];
    
    return cell;
}

@end


@implementation Header4

+ (Header4 *)createCell {
    Header4 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:11];
    return cell;
}

@end




@interface DetailLifeStyle () {
    NSMutableArray *arrAvailableAminities, *arrAvailableServices, *arrAvailableSafety, *arrAvailableRecreation;
}
@end
@implementation DetailLifeStyle
+ (DetailLifeStyle *)createCell {
    
    DetailLifeStyle *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:12];
    
    [cell.collectionView registerClass:[LifestyleColCell class] forCellWithReuseIdentifier:@"LifestyleColCell"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"LifestyleColCell" bundle:nil]  forCellWithReuseIdentifier:@"LifestyleColCell"];
    return cell;
}

#pragma mark - Collection View Delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _arrInfo.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *strId = @"LifestyleColCell";
    LifestyleColCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strId forIndexPath:indexPath];
    
    NSDictionary *dict;
    
    if (collectionView == _collectionView) {
        dict = [_arrInfo objectAtIndex:indexPath.row];
        cell.lblTitle.text = [dict objectForKey:@"title"];
        [cell.imgBg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
            
    }
    return cell;
}

- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnAminitiesLeft) {
        NSArray *visibleItems = [_colAminities indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_colAminities numberOfItemsInSection:0];
        if (num > nextItem.item) {
            [_colAminities scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    else if (sender == _btnAminitiesRight) {
        NSArray *visibleItems = [_colAminities indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)prevItem.item);
        if (prevItem.item > 0) {
           [_colAminities scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        }
    }
    else if (sender == _btnServicesLeft) {
        NSArray *visibleItems = [_colServices indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_colServices numberOfItemsInSection:0];
        if (num > nextItem.item) {
            [_colServices scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    else if (sender == _btnServicesRight) {
        NSArray *visibleItems = [_colServices indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)prevItem.item);
        if (prevItem.item > 0) {
            [_colServices scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        }
    }
    else if (sender == _btnSafetyLeft) {
        NSArray *visibleItems = [_colSafety indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_colSafety numberOfItemsInSection:0];
        if (num > nextItem.item) {
            [_colSafety scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    else if (sender == _btnSafetyRight) {
        NSArray *visibleItems = [_colSafety indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)prevItem.item);
        if (prevItem.item > 0) {
            [_colSafety scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        }
    }
    else if (sender == _btnRecreationLeft) {
        NSArray *visibleItems = [_colRecreation indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_colRecreation numberOfItemsInSection:0];
        if (num > nextItem.item) {
            [_colRecreation scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    else if (sender == _btnRecreationRight) {
        NSArray *visibleItems = [_colRecreation indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)prevItem.item);
        if (prevItem.item > 0) {
            [_colRecreation scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        }
    }
}

@end


@implementation Header5

+ (Header5 *)createCell {
    Header5 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:13];
    return cell;
}

@end


@implementation DetailedUnitSpecification

+ (DetailedUnitSpecification *)createCell {
    DetailedUnitSpecification *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:14];
    return cell;
}

@end


@implementation Header6

+ (Header6 *)createCell {
    Header6 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:15];
    return cell;
}

@end


@implementation PriceTrend

+ (PriceTrend *)createCell {
    PriceTrend *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:16];
    return cell;
}
@end


@implementation LocalityInsight

+ (LocalityInsight *)createCell {
    LocalityInsight *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:17];
    
    return cell;
}

@end


@implementation DeveloperProfile

+ (DeveloperProfile *)createCell {
    DeveloperProfile *cell =[[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:18];
    
    return cell;
}

@end

@implementation ReviewCell
+ (ReviewCell *)createCell {
    ReviewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:19];
    
    return cell;
}


@end

@implementation Review
+ (Review *)createCell {
    Review *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:20];
    
    return cell;
    
}




@end


@implementation Footer6

+ (Footer6 *)createCell {
    Footer6 *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:21];
    
    return cell;
}

@end


@implementation PostComment

+ (PostComment *)createCell {
    PostComment *cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:22];
    cell.viewTitle.backgroundColor = [UIColor clearColor];
    cell.viewMessage.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)setUpStarRating {
    
    _starRating.starImage = [UIImage imageNamed:@"star-template"];
    _starRating.starHighlightedImage = [UIImage imageNamed:@"star-highlighted-template"];
    _starRating.maxRating = 5.0;
    _starRating.delegate = self;
    _starRating.horizontalMargin = 0;
    _starRating.editable = YES;
    _starRating.rating = 0;
    _starRating.displayMode = EDStarRatingDisplayFull;
    
    [self starsSelectionChanged:_starRating rating:2.5];
    // This one will use the returnBlock instead of the delegate
    _starRating.returnBlock = ^(float rating )
    {
        NSLog(@"ReturnBlock: Star rating changed to %.1f", rating);
        //For the sample, Just reuse the other control's delegate method and call it
        [self starsSelectionChanged:_starRating rating:rating];
    };
}

#pragma mark - TextField Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _viewTitle.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0];
  
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _viewTitle.backgroundColor = [UIColor clearColor];
}



#pragma mark - TextView Delegates
- (void)textViewDidBeginEditing:(UITextView *)textView; {
    _viewMessage.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0];

}

- (void)textViewDidEndEditing:(UITextView *)textView {
    _viewMessage.backgroundColor = [UIColor clearColor];
}


-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
//    NSString *ratingString = [NSString stringWithFormat:@"Rating: %.1f", rating];
//    if( [control isEqual:_starRating] )
//        _starRating.text = ratingString;
//    else
//        _starRatingImageLabel.text = ratingString;
}
- (void)downLoadPDF:(id)sender {
    
}
@end
