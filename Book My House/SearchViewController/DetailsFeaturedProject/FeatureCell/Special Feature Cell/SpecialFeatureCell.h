//
//  SpecialFeatureCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 9/7/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialFeatureCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
