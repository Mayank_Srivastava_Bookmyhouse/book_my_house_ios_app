//
//  UnitCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 23/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblUnits;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblAreaRange;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceRange;
@end
