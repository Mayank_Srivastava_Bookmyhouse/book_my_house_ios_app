//
//  FileDownloadInfo.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/27/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "FileDownloadInfo.h"

@implementation FileDownloadInfo
-(id)initWithFileTitle:(NSString *)title andDownloadSource:(NSString *)source{
    self = [super init];
    if (self) {
        self.fileTitle = title;
        self.downloadSource = source;
        self.downloadProgress = 0.0;
        self.isDownloading = NO;
        self.downloadComplete = NO;
        self.taskIdentifier = -1;
    }
    
    return self;
}
@end
