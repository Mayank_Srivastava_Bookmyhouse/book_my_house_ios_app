//
//  LifestyleColCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 3/7/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LifestyleColCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
