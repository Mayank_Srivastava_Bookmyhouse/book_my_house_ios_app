//
//  ProjectDetailVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MessageUI/MessageUI.h>

#import "Header.h"
#import "CustomView.h"
#import "FeatureCell.h"
#import "UnitImageCollCell.h"
#import "ExploreLocationVC.h"
#import "BookSiteVisitVC.h"
#import "SelectUnitVC.h"
#import "FileDownloadInfo.h"
#import "CommentsVC.h"
#import "YTPlayerView.h"
#import "CustomTabBarController.h"


typedef NS_ENUM(NSUInteger, FeaturedProjectAccessOption) {
    FeaturedProjectAccessOptionSeeAllProjects,
    FeaturedProjectAccessOptionProjectList,
    FeaturedProjectAccessOptionFeaturedProjectList,
    FeaturedProjectAccessOptionSearchList,
    FeaturedProjectAccessOptionFavorite,
    FeaturedProjectAccessOptionSearchProperty,
    FeaturedProjectAccessOptionSearchHeatMap,
    FeaturedProjectAccessOptionMisc,
};
@interface ProjectDetailVC : UIViewController<UITableViewDelegate, UITableViewDataSource, YTPlayerViewDelegate,UITextFieldDelegate, UITextViewDelegate, EDStarRatingProtocol, UIWebViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, copy) NSDictionary *dictSourse;
@property (nonatomic, copy) NSDictionary *dictInfo;

@property (nonatomic, assign) FeaturedProjectAccessOption option;
@end
