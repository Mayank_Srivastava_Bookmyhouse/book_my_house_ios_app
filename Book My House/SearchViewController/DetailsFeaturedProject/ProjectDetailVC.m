//
//  ProjectDetailVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/17/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ProjectDetailVC.h"
#import "PrivacyViewController.H"
#import "CustomVC.h"
#import "UnitCell.h"

#define OFFSET_X 1.0
#define OFFSET_Y 0.0

static NSString * const strWrongMobileNumber                 = @"Mobile number not available";
static NSString * const strNoName                            = @"Please enter your name";
static NSString * const strInvalidName                       = @"Please enter a valid name";

static NSString * const strNoEmail                           = @"Please give your email id";
static NSString * const strInvalidEmail                      = @"Please fill valid email id";

static NSString * const strNoMobilenumber                    = @"Please fill your mobile number";
static NSString * const strValidMobileNumber                 = @"Please give your correct mobile number.";

static NSString * const strNoMessage                         = @"Please enter your message";

static NSString * const strSuccess                           = @"File Sucessfully downloaded";
static NSString * const strUnsucess                          = @"File could not be downloaded";
static NSString * const str_No_Review                        = @"No review available";
static NSString * const strLogin                             = @"Please login to add items in favorite list.";
static NSString * const strComment                           = @"Please login to post your comments";
static NSString * const strCommentTitle                      = @"Please enter your title.";
static NSString * const strCommentMassege                    = @"Please enter your message.";
static NSString * const strPleaseCheckMarkT_C                = @"Please check mark T&C";
static NSString * const strEnquirySuccess                    = @"Enquiry successfully sent.";
static NSString * const strEnquiryFailed                     = @"Enquiry could not be sent";

@interface ProjectDetailVC ()<Header1Delegate> {
    
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIButton *btnBookNow;
    __weak IBOutlet UIButton *btnSiteVisit;
    __weak IBOutlet UIButton *btnCall;
    __weak IBOutlet UIButton *btnEnquiry;
    __weak IBOutlet UIButton *btnShare;
    __weak IBOutlet UIButton *btnLike;
    __weak IBOutlet UIButton *btnUP;
    
    UIImageView *imgPreview;

    UnitImageCell *cellInfo;
    NSMutableDictionary *dictInfo;
    UIRefreshControl *refreshControl;
    PostComment *cellPostComment;
    PriceTrend *cellPriceTrends;
    
    //First Footet
    
    Footer1 *viewFooter;
    EnquiryView *viewEnquiry;
    
    int direction;
    int shakes;
    NSUInteger selectedIndex;
    BOOL isUnitSpecificationButtonSelected, isLocalityInsightSelected, isProjectPriceHistory, isLocalityInsights, isAboutBuilder, isReviewed, isInsightSelected;
    NSUserDefaults *pref;
    float starRating;
    DetailLifeStyle *cellDetailLifeStyle;
    NSString *strSelectedUnitIds, *strSelectedTitle;
    BOOL isFavoriteButtonSelected;
    
    OverviewCell *cellOverView;
    DeveloperProfile *cellDeveloperProfile;
    UnitDescCell *cellUnitDescription;
    NSIndexPath *indexPathForUnits;
}

@property (strong, nonatomic) NSMutableArray *arrFileDownloadData;

- (IBAction)actionListeners:(id)sender;

@end

@implementation ProjectDetailVC


#pragma maek - View controller Mathods
- (void)viewDidLoad {
    pref = [NSUserDefaults standardUserDefaults];
    dictInfo = [NSMutableDictionary dictionary];
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
    [self getProjectInformation];
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - screent Settings
- (void)screenSettings {
    
    btnShare.superview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];

    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSMutableAttributedString alloc] initWithString:loading];
    refreshControl.tintColor = [UIColor darkGrayColor];
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    [tblView addSubview:refreshControl];
}


- (void)pullToRefresh {
    [refreshControl beginRefreshing];
    [self getProjectInformation];
}

#pragma mark - API Actions
- (void)getProjectInformation {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        
        if (![refreshControl isRefreshing]) {
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
        }
        else {
            [refreshControl endRefreshing];
        }
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        if (_option == FeaturedProjectAccessOptionSeeAllProjects) {
            [dict setObject:[_dictSourse objectForKey:@"ID"] forKey:@"id"];
            [dict setObject:@"" forKey:@"type"];
            
        }
        else if (_option == FeaturedProjectAccessOptionFavorite || _option == FeaturedProjectAccessOptionProjectList || _option == FeaturedProjectAccessOptionSearchProperty || _option == FeaturedProjectAccessOptionSearchHeatMap){
            
            [dict setObject:[_dictSourse objectForKey:@"project_id"] forKey:@"id"];
            [dict setObject:@"" forKey:@"type"];
        }
        else if (_option == FeaturedProjectAccessOptionSearchList) {
            
            [self loadLocalityInfo];
            return;
        }
        else if (_option == FeaturedProjectAccessOptionSearchList) {
            
            [self loadLocalityInfo];
            return;
        }
        
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
            // [dict setObject:@"275" forKey:@"user_id"];
        }
        else {
            [dict setObject:@"" forKey:@"user_id"];
        }
        
        [ServiceAPI getProjectInformationWithParam:dict CallBack:^(BOOL isSuccess, id data) {
            
            if ([refreshControl isRefreshing]) {
                [refreshControl endRefreshing];
            }
            else {
                [[AppDelegate share] enableUserInteraction];
            }
            if (isSuccess) {
                if ([data isKindOfClass:[NSDictionary class]]) {
                    
                    [dictInfo removeAllObjects];
                    dictInfo = [data mutableCopy];
                    
                    if ([[dictInfo objectForKey:@"unit_type"] count]) {
                        strSelectedUnitIds = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:0] objectForKey:@"unit_ids"];
                        strSelectedTitle = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:0] objectForKey:@"wpcf_flat_typology"];
                    }
                    
                    if (![dictInfo count]) {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                    else {
                        btnLike.selected = [[dictInfo objectForKey:@"user_favourite"] boolValue];
                        
                        
                        
                        if ([[dictInfo objectForKey:@"is_static_unit"] boolValue]) {
                            btnBookNow.hidden = YES;
                            btnSiteVisit.frame = CGRectMake(0.5, 0, CGRectGetWidth(btnSiteVisit.superview.frame)/3 - 1, CGRectGetHeight(btnSiteVisit.superview.frame));
                            btnCall.frame = CGRectMake(CGRectGetMaxX(btnSiteVisit.frame) + 0.5, 0, CGRectGetWidth(btnSiteVisit.superview.frame)/3 - 1.0, CGRectGetHeight(btnSiteVisit.superview.frame));
                            btnEnquiry.frame = CGRectMake(CGRectGetMaxX(btnCall.frame) + 0.5, 0, CGRectGetWidth(btnSiteVisit.superview.frame)/3 - 1, CGRectGetHeight(btnSiteVisit.superview.frame));
                        }
                        else {
                            
                        }
                        
                        if (![[dictInfo objectForKey:@"total_units"] integerValue]) {
                            btnBookNow.hidden = YES;
                            btnSiteVisit.frame = CGRectMake(0.5, 0, CGRectGetWidth(btnSiteVisit.superview.frame)/3 - 1, CGRectGetHeight(btnSiteVisit.superview.frame));
                            btnCall.frame = CGRectMake(CGRectGetMaxX(btnSiteVisit.frame) + 0.5, 0, CGRectGetWidth(btnSiteVisit.superview.frame)/3 - 1.0, CGRectGetHeight(btnSiteVisit.superview.frame));
                            btnEnquiry.frame = CGRectMake(CGRectGetMaxX(btnCall.frame) + 0.5, 0, CGRectGetWidth(btnSiteVisit.superview.frame)/3 - 1, CGRectGetHeight(btnSiteVisit.superview.frame));
                        }
                        
                    }
                    
                    [tblView reloadData];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }
            else {
                [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
            }
            
        }];
        
        
    }
    else {
        
        if ([refreshControl isRefreshing]) {
            [refreshControl endRefreshing];
        }
        
        [[AppDelegate share] enableNoInternetAlert:@selector(getProjectInformation) fromClass:self];
    }
    
    

}

#pragma mark - API Actions
- (void)loadLocalityInfo {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        if ([_dictSourse  objectForKey:@"location_id"]) {
            [dict setObject:[_dictSourse  objectForKey:@"location_id"] forKey:@"location"];
        }
        
        if ([_dictInfo objectForKey:@"type"]) {
            [dict setObject:[_dictInfo objectForKey:@"type"] forKey:@"type"];
        }
        
        if ([_dictInfo objectForKey:@"p_type"]) {
            [dict setObject:[_dictInfo objectForKey:@"p_type"] forKey:@"p_type"];
        }
        
        if ([_dictInfo objectForKey:@"city"]) {
            [dict setObject:[_dictInfo objectForKey:@"city"] forKey:@"city"];
        }
        
       
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getProjectList:dict withCallBack:^(BOOL isSuccess, id data, id legands, id locationCordinates)  {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                if ([data isKindOfClass:[NSDictionary class]]) {
                    
                    [dictInfo removeAllObjects];
                    dictInfo = [data mutableCopy];
                    
                    if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                        strSelectedUnitIds = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:0] objectForKey:@"unit_ids"];
                        strSelectedTitle = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:0] objectForKey:@"wpcf_flat_typology"];
                    }
                    
                    if (![dictInfo count]) {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                    else {
                        btnLike.selected = [[dictInfo objectForKey:@"user_favourite"] boolValue];
                    }
                    
                    [tblView reloadData];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }
            else {
                [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
            }
        }];
        
        
    }
    else {
        
        [[AppDelegate share] enableNoInternetAlert:@selector(loadLocalityInfo) fromClass:self];
    }
}

#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnBookNow) {
        CGRect sectionRect = [tblView rectForSection:1];
        [tblView scrollRectToVisible:sectionRect animated:YES];
    }
    else if (sender == btnSiteVisit) {
        
    }
    else if (sender == btnCall){
        
        NSString *str = [dictInfo objectForKey:@"builder_mobile"];
        if (![str isKindOfClass:[NSNull class]]) {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:str];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        }
        else {
            [self.view makeToast:strWrongMobileNumber duration:duration position:CSToastPositionCenter];
        }
    }
    else if (sender == btnEnquiry) {
        
        if (viewEnquiry == nil) {
            btnEnquiry.selected = !btnEnquiry;
            
            viewEnquiry = [EnquiryView createView];
            viewEnquiry.frame = CGRectMake( -1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
            viewEnquiry.owner = self;
            [self setTextField:viewEnquiry.txtProjectName withPlaceholder:@"Project Name"];
            [self setTextField:viewEnquiry.txtName withPlaceholder:@"Name"];
            [self setTextField:viewEnquiry.txtEmail withPlaceholder:@"Email"];
            [self setTextField:viewEnquiry.txtMobileNumber withPlaceholder:@"Mobile Number"];
            [self setTextView:viewEnquiry.txtMessage];
            [viewEnquiry.btnCancel addTarget:self action:@selector(cancelEnqiryForm) forControlEvents:UIControlEventTouchUpInside];
            [viewEnquiry.btnSend addTarget:self action:@selector(sendEnquiry) forControlEvents:UIControlEventTouchUpInside];
            [viewEnquiry.btnTermsAndConditions addTarget:self action:@selector(didTappedTermsAndConditions) forControlEvents:UIControlEventTouchUpInside];
            
            [self.view addSubview:viewEnquiry];
            [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 viewEnquiry.frame = self.view.frame;
            } completion:^(BOOL finished) {
                
                if (![[dictInfo objectForKey:@"proj_name"] isKindOfClass:[NSNull class]]) {
                    
                    UIFont *font = viewEnquiry.txtProjectName.font;
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dictInfo objectForKey:@"proj_name"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                    options:@{
                                                                                              NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                              }
                                                                         documentAttributes:nil
                                                                                      error:nil];
                    
                    
                    viewEnquiry.txtProjectName.attributedText = attrStr;
                    viewEnquiry.txtProjectName.font = font;
                }
                else {
                    viewEnquiry.txtProjectName.text = @"";
                }
                
            }];
            
        }
    }
    else if (sender == btnShare) {
        
        NSMutableArray *arr = [NSMutableArray array];
        
        if ([dictInfo objectForKey:@"proj_name"]) {
            [arr addObject:[dictInfo objectForKey:@"proj_name"]];
        }
        
        if ([dictInfo objectForKey:@"project_url"]) {
            [arr addObject:[dictInfo objectForKey:@"project_url"]];
        }
        
        
        UIActivityViewController *obj = [[UIActivityViewController alloc] initWithActivityItems:arr applicationActivities:nil];
        
        [self presentViewController:obj animated:YES completion:nil];
        
        
    }
    else if (sender == btnLike) {
        [self didTappedFavoriteButton];
    }
    else if (sender == btnUP) {
        
        [tblView setContentOffset:CGPointZero animated:YES];
    }
    else{
        
//        if (isFavoriteButtonSelected) {
//            
//            
//        }
//        else {
//            [self.navigationController popViewControllerAnimated:YES];
//        }
        
        if (self.navigationController.viewControllers.count > 2) {
            UIViewController *controller = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2];
            if ([controller isKindOfClass:[ProjectListVC class]]) {
                
                ProjectListVC *obj = (ProjectListVC *)controller;
                [self.navigationController popToViewController:obj animated:YES];
                //[obj callingAPI];
                if ([pref objectForKey:strSaveMultiFilter]) {
                    [obj addFilters:nil];
                }
                else {
                    [obj callingAPI];
                }
                
                
            }
            else if ([controller isKindOfClass:[SearchViewController class]]) {
                
                SearchViewController *obj = (SearchViewController *)controller;
                [self.navigationController popToViewController:obj animated:YES];
                [obj getInfoFromApi];
            }
            else if ([controller isKindOfClass:[FavouriteVC class]]) {
                FavouriteVC *obj = (FavouriteVC *)controller;
                [self.navigationController popToViewController:obj animated:YES];
                [obj loadFavouriteList];
            }
            else if ([controller isKindOfClass:[ProjectMapVC class]]) {
                ProjectMapVC *obj = (ProjectMapVC *)controller;
                [self.navigationController popToViewController:obj animated:YES];
                [obj loadLocalityInfo];
            }
        }
        
        
    }
}


- (void)didTappedFavoriteButton {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict =[NSMutableDictionary dictionary];
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId != nil) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kFavoriteProjectDetail object:nil];
            
            [dict setObject:[dictInfo objectForKey:@"id"] forKey:@"id"];
            [dict setObject:strUserId forKey:@"user_id"];
            [dict setObject:@"project" forKey:@"type"];
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI markAsFavorite:dict withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
//                    isFavoriteButtonSelected = !isFavoriteButtonSelected;
                    if ([[data objectForKey:@"success"] boolValue]) {
                        [self getProjectInformation];
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }];
        }
        else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTappedFavoriteButton ) name:kFavoriteProjectDetail object:nil];
            
            [self.view makeToast:strLogin duration:duration position:CSToastPositionCenter];
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionProjectDetails;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didTappedFavoriteButton) fromClass:self];
    }
    
}


- (void)directionButtonAction:(UIButton *)sender {
    
    if (sender.tag == 1) {
        
//        if (viewFooter.scrlView.contentOffset.x - OFFSET_X - 135 > 0) {
//            [viewFooter.scrlView setContentOffset:CGPointMake(viewFooter.scrlView.contentOffset.x - OFFSET_X - 135, 0) animated:YES];
//        }
        
    }
    else if (sender.tag == 2) {
        
//        if (viewFooter.scrlView.contentOffset.x + OFFSET_X + 135 < viewFooter.scrlView.contentSize.width - 2 * 135) {
//            [viewFooter.scrlView setContentOffset:CGPointMake(viewFooter.scrlView.contentOffset.x + OFFSET_X + 135, 0) animated:YES];
//        }
        
    }
}

- (void)didTappedLeftRightButton:(UIButton *)sender {
    
    if (sender == cellUnitDescription.btnLeft) {
        NSArray *visibleItems = [cellUnitDescription.colView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        
        NSLog(@"Item number %d", (int)prevItem.item);
        
        if (prevItem.item >= 0) {
            [cellUnitDescription.colView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        else {
            cellUnitDescription.btnLeft.hidden = YES;
            cellUnitDescription.btnRight.hidden = NO;
        }
    }
    else {
        NSArray *visibleItems = [cellUnitDescription.colView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [cellUnitDescription.colView numberOfItemsInSection:0];
        
        if (num > nextItem.item) {
            [cellUnitDescription.colView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
//        else {
//            cellUnitDescription.btnLeft.hidden = NO;
//            cellUnitDescription.btnRight.hidden = YES;
//        }
    }
    
}


- (void)downLoadPDF:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    NSArray *arr = [dictInfo objectForKey:@"media_gallery"];
    _arrFileDownloadData = [NSMutableArray array];
    for (NSDictionary *dict in arr) {
        if ([[dict objectForKey:@"type"] integerValue] == 3) {
            NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"url"]];
            [_arrFileDownloadData addObject:[[FileDownloadInfo alloc] initWithFileTitle:[dict objectForKey:@"proj_name"]
                                                                      andDownloadSource:str]];
            
            break;
        }
    }
    
    [btn setTitle:@"Downloading" forState:UIControlStateNormal];
    BackgroundDownloadServices *service = [[BackgroundDownloadServices alloc] init];
    [self.navigationController.view makeToast:@"Downloading PDF" duration:duration position:CSToastPositionCenter];
    [service getPDFFromUrl:[_arrFileDownloadData firstObject] andCallBack:^(BOOL isSuccess) {
        if (isSuccess) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [btn setTitle:@"Download complete" forState:UIControlStateNormal];
                [self.view.window makeToast:strSuccess duration:duration position:CSToastPositionCenter];
            });
            
        }
        else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [btn setTitle:@"Download" forState:UIControlStateNormal];
                [self.view.window makeToast:strUnsucess duration:duration position:CSToastPositionCenter];
            });
            
        }
    }];
}

- (void)cancelEnqiryForm {
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewEnquiry.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [viewEnquiry removeFromSuperview];
                         viewEnquiry = nil;
                     }];
    
}

- (void)sendEnquiry {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        direction = 1;
        shakes = 0;
        
        viewEnquiry.txtName.text = [viewEnquiry.txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewEnquiry.txtName.text.length == 0) {
            [self.view makeToast:strNoName duration:duration position:CSToastPositionCenter];
            [self shake:viewEnquiry.txtName];
            return;
        }
        else if (![AuxillaryMathods isUserName:viewEnquiry.txtName.text]) {
            [self.view makeToast:strInvalidName duration:duration position:CSToastPositionCenter];
            [self shake:viewEnquiry.txtName];
            return;
        }
        else {
            
        }
        
        viewEnquiry.txtEmail.text = [viewEnquiry.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewEnquiry.txtEmail.text.length == 0) {
            [self.view makeToast:strNoEmail duration:duration position:CSToastPositionCenter];
            [self shake:viewEnquiry.txtEmail];
            return;
        }
        else if (![AuxillaryMathods isValidEmail:viewEnquiry.txtEmail.text]) {
            [self.view makeToast:strInvalidEmail duration:duration position:CSToastPositionCenter];
            [self shake:viewEnquiry.txtEmail];
            return;
        }
        else {
            
        }
        
        viewEnquiry.txtMobileNumber.text = [viewEnquiry.txtMobileNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewEnquiry.txtMobileNumber.text.length == 0) {
            [self.view makeToast:strNoMobilenumber duration:duration position:CSToastPositionCenter];
            [self shake:viewEnquiry.txtMobileNumber];
            return;
        }
        else if (![AuxillaryMathods isValidMobileNumber:viewEnquiry.txtMobileNumber.text]) {
            [self.view makeToast:strValidMobileNumber duration:duration position:CSToastPositionCenter];
            [self shake:viewEnquiry.txtMobileNumber];
            return;
        }
        else {
            
        }
        
        viewEnquiry.txtMessage.text = [viewEnquiry.txtMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (viewEnquiry.txtMessage.text.length == 0) {
            [self.view makeToast:strNoMessage duration:duration position:CSToastPositionCenter];
            [self shakeView:viewEnquiry.txtMessage];
            return;
        }
        else {
            
        }
        
        if (!viewEnquiry.btnCheck.selected) {
            [self.view makeToast:strPleaseCheckMarkT_C duration:duration position:CSToastPositionCenter];
            return;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];// previewsrojectName,name,contactno,email,enquiry
        [dict setObject:viewEnquiry.txtProjectName.text forKey:@"projectName"];
        [dict setObject:viewEnquiry.txtName.text forKey:@"name"];
        [dict setObject:viewEnquiry.txtMobileNumber.text forKey:@"contactno"];
        [dict setObject:viewEnquiry.txtEmail.text forKey:@"email"];
        [dict setObject:viewEnquiry.txtMessage.text forKey:@"enquiry"];
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI postEnquiry:dict andCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            [self cancelEnqiryForm];
            if (isSuccess) {
                [self.view makeToast:strEnquirySuccess duration:duration position:CSToastPositionCenter];
            }
            else {
                [self.view makeToast:strEnquiryFailed duration:duration position:CSToastPositionCenter];
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(sendEnquiry) fromClass:self];
    }
    
    
}

- (void)didTappedTermsAndConditions {
    
    PrivacyViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyViewController"];
    obj.option = PrivacyViewControllerAccessOptionTermsAndConditions;
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)didSelectedClickForMore {
    
    CustomTabBarController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomTabBarController"];
    if (![[dictInfo objectForKey:@"proj_name"] isKindOfClass:[NSNull class]]) {
        obj.strProjectName = [dictInfo objectForKey:@"proj_name"];
    }
    else {
        obj.strProjectName = @"";
    }
    
    if (![[dictInfo objectForKey:@"address"] isKindOfClass:[NSNull class]]) {
        obj.strAddress = [dictInfo objectForKey:@"address"];
    }
    else {
        obj.strAddress = @"";
    }
    
    if (![[dictInfo objectForKey:@"id"] isKindOfClass:[NSNull class]]) {
        obj.strID = [dictInfo objectForKey:@"id"];
    }
    else {
        obj.strID = @"";
    }
    
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)didSelectedBookNow:(UIButton *)sender {
    
    if ([[dictInfo objectForKey:@"is_static_unit"] boolValue]) {
        [self actionListeners:btnEnquiry];
    }
    else {
        SelectUnitVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectUnitVC"];
        obj.dictSourse = _dictSourse;
        obj.strSelectedUnitIDs = strSelectedUnitIds;
        obj.strSelectedTitle = strSelectedTitle;
        obj.strImagePlan =  [dictInfo objectForKey:@"project_plan_img"];
        obj.dictCord = [dictInfo objectForKey:@"cords"];
        [self.navigationController pushViewController:obj animated:YES];
    }
    
    
}

- (void)didSelectExploreNeighbourhood:(id)sender {
    
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *btn = (UIButton *)sender;
        ExploreLocationVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreLocationVC"];
        if (btn.tag == 1) {
            obj.option = ELAccessOptionFeatureProject;
            obj.dictInfo = dictInfo;
        }
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
    }
}

- (void)didSeeAllReviews:(UIButton *)sender {
    
    if ([dictInfo objectForKey:@"comments_detail"] != nil && [dictInfo objectForKey:@"id"] != nil) {
        NSArray *arr = [dictInfo objectForKey:@"comments_detail"];
        
        if (arr.count != 0) {
            CommentsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsVC"];
            obj.options = CommentsVCAccessOptionsFeaturedProjects;
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[dictInfo objectForKey:@"id"] forKey:@"id"];
            [dict setObject:@"project" forKey:@"comment_type"];
            
            obj.dictSourseInfo = dict;
            [self.navigationController pushViewController:obj animated:YES];
        }
        else {
            [self.view makeToast:str_No_Review duration:duration position:CSToastPositionCenter];
        }
    }
    else {
        [self.view makeToast:str_No_Review duration:duration position:CSToastPositionCenter];
    }
    
}

- (void)didTapedUnitSpecification:(UIButton *)sender {
    
    isUnitSpecificationButtonSelected = !isUnitSpecificationButtonSelected;
    
    [tblView reloadData];
}

- (void)didTappedInsights:(UIButton *)btn {
    isInsightSelected = !isInsightSelected;
    [tblView reloadData];
    
}

- (void)didTappedProjectHistory:(UIButton *)btn {
    isProjectPriceHistory = !isProjectPriceHistory;
    [tblView reloadData];
}

- (void)didTappedLocalityInsight:(UIButton *)btn {
    isLocalityInsightSelected = !isLocalityInsightSelected;
    
    [tblView reloadData];
    [tblView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:6] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void)didTappedAboutBuilder:(UIButton *)btn {
    isAboutBuilder = !isAboutBuilder;
    [tblView reloadData];
}

- (void)didTappedReviews:(UIButton *)sender {
    isReviewed = !isReviewed;
    [tblView reloadData];
}


- (void)didSelectPostAComment:(UIButton *)sender {

    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId != nil) {
            
            if (cellPostComment.txtMessage.text.length == 0) {
                [self.view makeToast:strCommentMassege duration:duration position:CSToastPositionCenter];
                return;
            }
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:strUserId forKey:@"user_id"];
            [dict setObject:[dictInfo objectForKey:@"id"] forKey:@"post_id"];
            [dict setObject:cellPostComment.txtMessage.text forKey:@"comment"];
            [dict setObject:@"" forKey:@"title"];
            [dict setObject:@"iOS" forKey:@"mobile_type"];
            [dict setObject:@"project" forKey:@"comment_type"];
            [dict setObject:[NSNumber numberWithFloat:starRating] forKey:@"rating"];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kPostCommentOnProjectDetails object:nil];
            [ServiceAPI postAComment:dict withCallback:^(BOOL isSuccess, id data) {
                
                if (isSuccess) {
                    [self.view makeToast:[data objectForKey:@"message"] duration:duration position:CSToastPositionCenter];
                    
                    cellPostComment.txtTitle.text = @"";
                    cellPostComment.txtMessage.text = @"";
                    cellPostComment.starRating.rating = 0;
                    [self getProjectInformation];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    
                }
                
            }];
            
        }
        else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectPostAComment:) name:kPostCommentOnProjectDetails object:nil];
            
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionPostCommentOnProject;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
            [self.view makeToast:strComment duration:duration position:CSToastPositionCenter];
        }
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didSelectPostAComment:) fromClass:self];
    }
    
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"bookSite"]) {
        BookSiteVisitVC *obj = segue.destinationViewController;
        obj.dictInfo = dictInfo;
    }
}
#pragma mark - Table View Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableView == tblView) {
        
        return 10;
    }
    else {
        return 1;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (tableView == tblView) {
        if (section == 0) {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                return 300;
            }
            else {
                return 400;
            }
            
        }
        else if (section == 1) {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                
                if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                    return 45;
                }
                else {
                    return 0;
                }
                
            }
            else {
                if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                    return 50;
                }
                else {
                    return 0;
                }
            }
        }
        else if (section == 1) {
            if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                return 40.0;
            }
            else {
                return 0.0;
            }
        }
        else if (section == 2) {
            return 45.0;
        }
        else if (section == 3) {
            return 45.0;
        }
        else if (section == 4) {
            if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                return 45;
            }
            else {
                return 0;
            }
        }
        else if (section == 5) {
            NSArray *arr = [dictInfo objectForKey:@"price_trends"];
            if (arr.count) {
                return 45.0;
            }
            else {
                return 0.0;
            }
        }
        else if (section == 6) {
            
            NSString *str  = [dictInfo objectForKey:@"locality_insights"];
            
            if (str && str.length) {
                return 45.0;
            }
            else {
                return 0.0;
            }
        }
        else if (section == 7) {
            
            return 45.0;
        }
        else if (section == 8) {
            
            NSArray *arr = [dictInfo objectForKey:@"comments_detail"];
            
            if (arr.count) {
                return 45.0;
            }
            else {
                return 0.0;
            }
            //return 45.0;
        }
        
    }
    else {
        return 0;
    }
    
    return 0.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView == tblView) {
        
        if (section == 0 && [dictInfo count]) {
            Header1 *headerCell = [tableView dequeueReusableCellWithIdentifier:@"Header1"];
            
            if (!headerCell) {
                headerCell = [Header1 createCell];
                headerCell.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGRectGetHeight(headerCell.frame));
                
            }
            
            headerCell.delegate = self;
            
            headerCell.arrMedia = [dictInfo objectForKey:@"media_gallery"];
            headerCell.owner = self;
//            headerCell.btnClickForMore.layer.cornerRadius = 5.0;//
//            [headerCell.btnClickForMore addTarget:self action:@selector(didSelectedClickForMore) forControlEvents:UIControlEventTouchUpInside];
            [headerCell.collectionView reloadData];
            
            
            return headerCell;
        }
        else if (section == 1) {
            
            Header2 *headerCell = [tableView dequeueReusableCellWithIdentifier:@"Header2"];
            
            if (!headerCell) {
                headerCell = [Header2 createCell];
                headerCell.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGRectGetHeight(headerCell.frame));
            }
            
            if (![[dictInfo objectForKey:@"availabe_unit"] isKindOfClass:[NSNull class]]) {
                headerCell.lblUnits.text = [NSString stringWithFormat:@" Units %@", [dictInfo objectForKey:@"availabe_unit"]];
            }
            else {
                headerCell.lblUnits.text = @" Units";
            }
            return headerCell;
        }
        else if (section == 2) {
            Header3 *headerCell = [tableView dequeueReusableCellWithIdentifier:@"Header3"];
            
            if (headerCell == nil) {
                headerCell = [Header3 createCell];
                
                
                [headerCell.btnPlus addTarget:self action:@selector(didTappedInsights:) forControlEvents:UIControlEventTouchUpInside];
            }
            headerCell.btnPlus.selected = !isInsightSelected;
            
            return headerCell;
        }
        else if (section == 3) {
            Header4 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header4"];
            
            if (cell == nil) {
                cell = [Header4 createCell];
            }
            
            return cell;
        }
        else if (section == 4) {
            Header5 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header5"];
            
            if (cell == nil) {
                cell = [Header5 createCell];
                cell.lblUnitSpecifications.text = @" Unit Specifications";
            }
            
            cell.btnPlus.selected = !isUnitSpecificationButtonSelected;
            [cell.btnPlus addTarget:self action:@selector(didTapedUnitSpecification:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else if (section == 5) {
            Header6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header6"];
            
            if (cell == nil) {
                cell = [Header6 createCell];
                [cell.btnPlus addTarget:self action:@selector(didTappedProjectHistory:) forControlEvents:UIControlEventTouchUpInside];
                cell.lblTitle.text = @" Project Price History";
            }
            cell.btnPlus.selected = !isProjectPriceHistory;
            
            return cell;
        }
        else if (section == 6) {
            Header6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header6"];
            
            if (cell == nil) {
                cell = [Header6 createCell];
                [cell.btnPlus addTarget:self action:@selector(didTappedLocalityInsight:) forControlEvents:UIControlEventTouchUpInside];
                cell.lblTitle.text = @" Locality Insights";
            }
            
            cell.btnPlus.selected = !isLocalityInsightSelected;
            return cell;
        }
        else if (section == 7) {
            Header6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header6"];
            
            if (cell == nil) {
                cell = [Header6 createCell];
                cell.lblTitle.text = @" About Builder";
                [cell.btnPlus addTarget:self action:@selector(didTappedAboutBuilder:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            
            cell.btnPlus.selected = !isAboutBuilder;
            return cell;
        }
        else if (section == 8) {
            
            Header6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header6"];
            
            if (cell == nil) {
                cell = [Header6 createCell];
                [cell.btnPlus addTarget:self action:@selector(didTappedReviews:) forControlEvents:UIControlEventTouchUpInside];
                cell.lblTitle.text = @" Reviews";
            }
            // cell.backgroundColor = [UIColor lightGrayColor];
            
            cell.btnPlus.selected = !isReviewed;
            return cell;
            
//            if ([[dictInfo objectForKey:@"comments_detail"] count]) {
//                Header6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header6"];
//                
//                if (cell == nil) {
//                    cell = [Header6 createCell];
//                    cell.backgroundColor = [UIColor greenColor];
//                }
//               // cell.backgroundColor = [UIColor lightGrayColor];
//                cell.lblTitle.text = @" Reviews";
//                
//                return cell;
//            }
//            else {
//                return nil;
//            }
        }
        
    }
    else {
        return nil;
    }

    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (tableView == tblView) {
        if (section == 0) {
            return 114;
            
        }
        else if (section == 2) {
            
            return 45.0;
        }
        else if (section == 8) {
            
            if ([[dictInfo objectForKey:@"is_comment_list_needed"] boolValue]) {
                return 30.0;
            }
            else {
                return 0.0;
            }
        }
    }
    else {
        return 0.0;
    }
    
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (tableView == tblView) {
        if (section == 0) {
            
            viewFooter = [tableView dequeueReusableCellWithIdentifier:@"Footer1"];
            
            if (!viewFooter) {
                viewFooter = [Footer1 createCell];
                NSArray *arr = [dictInfo objectForKey:@"wow"];
                
                if (arr.count) {
                    viewFooter.arrEntries = arr;
                    viewFooter.collectionView.backgroundColor = [UIColor whiteColor];
                    [viewFooter.collectionView reloadData];
                }
                
                
                
                [viewFooter.btnLeft addTarget:self action:@selector(directionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [viewFooter.btnRight addTarget:self action:@selector(directionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            return viewFooter;
        }
        else if (section == 2) {
            Footer3 *view = [tableView dequeueReusableCellWithIdentifier:@"Footer3"];
            
            if (view == nil) {
                view = [Footer3 createCell];
            }
            
            [view.btnExploreNeighbourhood addTarget:self action:@selector(didSelectExploreNeighbourhood:) forControlEvents:UIControlEventTouchUpInside];
            [view.btnLandmark addTarget:self action:@selector(didSelectExploreNeighbourhood:) forControlEvents:UIControlEventTouchUpInside];
            [view.btnNeeds addTarget:self action:@selector(didSelectExploreNeighbourhood:) forControlEvents:UIControlEventTouchUpInside];
            [view.btnTransport addTarget:self action:@selector(didSelectExploreNeighbourhood:) forControlEvents:UIControlEventTouchUpInside];
            
            return view;
        }
        else if (section == 8) {
            
            if ([[dictInfo objectForKey:@"comments_detail"] count]) {
                Footer6 *view = [tableView dequeueReusableCellWithIdentifier:@"Footer6"];
                
                if (view == nil) {
                    view = [Footer6 createCell];
                }
                
                view.btnSeeAllReviews.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                [view.btnSeeAllReviews addTarget:self action:@selector(didSeeAllReviews:) forControlEvents:UIControlEventTouchUpInside];
                return view;
            }
            else {
                return nil;
            }
            
        }
        else {
            return nil;
        }
    }
    else
    {
        return nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == tblView) {
        if (section == 0 && dictInfo.count) {
            return 2;
        }
        else if (section == 1 && dictInfo.count) {
            
            if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                return 2;
            }
            else {
                return 0;
            }
            
        }
        else if (section == 2 && dictInfo.count) {
            
            if (!isInsightSelected) {
                return 1;
            }
            else {
                return 0;
            }
            
        }
        else if (section == 3 && dictInfo.count) {
            int i = 0;
            
            if ([dictInfo objectForKey:@"Amenities"] && [[dictInfo objectForKey:@"Amenities"] count]) {
                i++;
            }
            
            if ([dictInfo objectForKey:@"recreation"] && [[dictInfo objectForKey:@"recreation"] count]) {
                i++;
            }
            
            if ([dictInfo objectForKey:@"safety"] && [[dictInfo objectForKey:@"safety"] count]) {
                i++;
            }
            
            if ([dictInfo objectForKey:@"safety"] && [[dictInfo objectForKey:@"safety"] count]) {
                i++;
            }
            
          return i;
        }
        else if (section == 4 && dictInfo.count) {
            
            if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                
                if (!isUnitSpecificationButtonSelected) {
                    return 3;
                }
                else {
                    return 0;
                }
                
            }
            else {
                return 0;
            }
            
            
        }
        else if (section == 5 && dictInfo.count) {
            
            if (isProjectPriceHistory) {
                return 0;
            }
            else {
                return 1;
            }
        }
        else if (section == 6 && dictInfo.count) {
            
            if (isLocalityInsightSelected) {
                return 0;
            }
            else {
                return 1;
            }
            
        }
        else if (section == 7 && dictInfo.count) {
            
            if (!isAboutBuilder) {
                return 1;
            }else {
                return 0;
            }
        }
        else if (section == 8 && dictInfo.count) {
            
            if (!isReviewed) {
                NSArray *arr = [dictInfo objectForKey:@"comments_detail"];
                return arr.count;
            }
            else
              return 0;
        }
        else if (section == 9 && dictInfo.count) {
            return 1;
        }
        else
            return 0;
    }
    else {
        if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
            NSLog(@"Total Units %d \n Action Unit %d", (int)[[dictInfo objectForKey:@"total_units"] integerValue], (int)[[dictInfo objectForKey:@"unit_type"] count]);
            return [[dictInfo objectForKey:@"unit_type"] count];
        }
        else {
            return 0;
        }
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                //return 217;
                return 170.0;
            }
            else {
                return 340;
            }
        }
        else if (indexPath.section == 1) {
            
            if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                
                if (indexPath.row == 0) {
                    return 300.0;
                }
                else {
                    
                    if ([[dictInfo objectForKey:@"unit_type"] count] == 1) {
                        return 104.0;
                    }
                    else if ([[dictInfo objectForKey:@"unit_type"] count] == 2) {
                        return 208.0;
                    }
                    else {
                        return 312.0;
                    }
                }
            }
            else {
                return 0.0;
            }
            //return 300.0;
        }
        else if (indexPath.section == 2) {
            return 300.0;
        }
        else if (indexPath.section == 3) {
            
            return 130;
        }
        else if (indexPath.section == 4) {
            
            return 290.0;
        }
        else if (indexPath.section == 5) {
            NSArray *arr = [dictInfo objectForKey:@"price_trends"];
            if (arr.count) {
                return 250.0;
            }
            else {
                return 0.0;
            }
            //return 250.0;
        }
        else if (indexPath.section == 6) {
            
            NSString *str = [dictInfo objectForKey:@"locality_insights"];
            
            if (str && str.length) {
                return 195.0;
            }
            else {
                return 0.0;
            }
        }
        else if (indexPath.section == 7) {
            return 284.0;
        }
        else if (indexPath.section == 8) {
            
            NSArray *arr = [dictInfo objectForKey:@"comments_detail"];
            if (arr.count) {
                return 85.0;
            }
            else {
                return 0;
            }
        }
        else if (indexPath.section == 9) {
            return 212.0;
        }
    }
    else {
        return 104;
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                //return 217;
                return UITableViewAutomaticDimension;
            }
            else {
                return 240;
            }
        }
        else if (indexPath.section == 1) {
            
            if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                
                if (indexPath.row == 0) {
                    return 300.0;
                }
                else {
                    
                    return 105;
//                    if ([[dictInfo objectForKey:@"unit_type"] count] == 1) {
//                        return 104.0;
//                    }
//                    else if ([[dictInfo objectForKey:@"unit_type"] count] == 2) {
//                        return 208.0;
//                    }
//                    else {
//                        return 312.0;
//                    }
                }
            }
            else {
                return 0.0;
            }
            //return 300.0;
        }
        else if (indexPath.section == 2) {
            return 300.0;
        }
        else if (indexPath.section == 3) {
            
            return 130;
        }
        else if (indexPath.section == 4) {
            
            return 290.0;
        }
        else if (indexPath.section == 5) {
            NSArray *arr = [dictInfo objectForKey:@"price_trends"];
            if (arr.count) {
                return 250.0;
            }
            else {
                return 0.0;
            }
            //return 250.0;
        }
        else if (indexPath.section == 6) {
            
            NSString *str = [dictInfo objectForKey:@"locality_insights"];
            
            if (str && str.length) {
                return 195.0;
            }
            else {
                return 0.0;
            }
        }
        else if (indexPath.section == 7) {
            return 188.0;
        }
        else if (indexPath.section == 8) {
            
            NSArray *arr = [dictInfo objectForKey:@"comments_detail"];
            if (arr.count) {
                return 85.0;
            }
            else {
                return 0;
            }
            
            
        }
        else if (indexPath.section == 9) {
            return 212.0;
        }
    }
    else {
        return 104;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        if (indexPath.section == 0) {
            FeatureCell *cell;
            if (indexPath.row == 0) {
                cell = (FeatureCell *)[tableView dequeueReusableCellWithIdentifier:@"FeatureCell"];
                
                if (!cell) {
                    cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] firstObject];
                }
               
                cell.lblInfra.layer.cornerRadius = cell.lblNeeds.layer.cornerRadius = cell.lblLifeStyle.layer.cornerRadius = 5.0;
                NSString *str = [dictInfo objectForKey:@"developer_name"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblBuilderName.text = str;
                }
                else {
                    cell.lblBuilderName.text = @"";
                }
                
                str = [dictInfo objectForKey:@"proj_name"];
                if (![str isKindOfClass:[NSNull class]]) {
                    
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                    options:@{
                                                                                              NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                              }
                                                                         documentAttributes:nil
                                                                                      error:nil];
                    
                    
                    cell.lblProjectName.attributedText = attrStr;
                    cell.lblProjectName.font = [UIFont fontWithName:@"PT Sans" size:19.0];
                    
                }
                else {
                    cell.lblProjectName.text = @"";
                }
                str = [dictInfo objectForKey:@"ratings_average"];
                
                if (![str isKindOfClass:[NSNull class]]) {
                    
                    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                    attachment.image = [UIImage imageNamed:@"Rating"];
                    attachment.bounds = CGRectMake(0, -8, 28, 28);
                    
                    NSMutableAttributedString *myString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
                    
                    [myString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@/5", str]]];
                    
                    cell.lblRating.attributedText = myString;
                }
                else {
                    cell.lblRating.text = @"";
                }
                
                str = [dictInfo objectForKey:@"price_one_year"];
                if (![str isKindOfClass:[NSNull class]]) {
                    
                    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                    attachment.image = [UIImage imageNamed:@"Increase"];
                    attachment.bounds = CGRectMake(0, -3, 18, 18);
                    
                    NSMutableAttributedString *strProgress = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
                    
                    [strProgress appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", str]]];

                    cell.lblProgress.attributedText = strProgress;
                }
                else {
                    cell.lblProgress.text = @"";
                }
                
                str = [dictInfo objectForKey:@"possession_dt"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblPossession.text = [NSString stringWithFormat:@"Possession: %@", str];
                }
                else {
                    cell.lblPossession.text = @"";
                }
                
                str = [dictInfo objectForKey:@"exactlocation"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblAddress.text = str;
                }
                else {
                    cell.lblAddress.text = @"";
                }
                
                str = [dictInfo objectForKey:@"status"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblNewLaunch.text = str;
                }
                else {
                    cell.lblNewLaunch.text = @"";
                }
                
                str = [dictInfo objectForKey:@"proj_unit_type"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblFeatures.text = str;
                }
                else {
                    cell.lblFeatures.text = @"";
                }
                
                str = [dictInfo objectForKey:@"price"];
                if (![str isKindOfClass:[NSNull class]]) {
                    //cell.lblBasePrice.text = [NSString stringWithFormat:@"\u20B9 %@", [self setPriceRange:str]];
                    cell.lblBasePrice.text = [NSString stringWithFormat:@"\u20B9 %@", str];
                }
                else {
                    cell.lblBasePrice.text = @"";
                }
                
                str = [dictInfo objectForKey:@"prop_price_persq"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblUnitPrice.text = [NSString stringWithFormat:@"\u20B9 %@", str];
                }
                else {
                    cell.lblUnitPrice.text = @"";
                }
                
                str = [dictInfo objectForKey:@"infra"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblInfra.text = str;
                }
                else {
                    cell.lblInfra.text = @"";
                }
                
                str = [dictInfo objectForKey:@"needs"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblNeeds.text = str;
                }
                else {
                    cell.lblNeeds.text = @"";
                }
                
                str = [dictInfo objectForKey:@"life_style"];
                if (![str isKindOfClass:[NSNull class]]) {
                    cell.lblLifeStyle.text = str;
                }
                else {
                    cell.lblLifeStyle.text = @"";
                }
                
                return cell;
            }
            else if (indexPath.row == 1) {
                
                OverviewCell *cell;
                cell = [tableView dequeueReusableCellWithIdentifier:@"OverviewCell"];
                
                if (!cell) {
                    cell = [[[NSBundle mainBundle] loadNibNamed:@"FeatureCell" owner:self options:nil] objectAtIndex:1];
                }
                
                if (![[dictInfo objectForKey:@"min_area_range"] isKindOfClass:[NSNull class]] && ![[dictInfo objectForKey:@"max_area_range"] isKindOfClass:[NSNull class]]) {
                    cell.lblTotalArea.text = [NSString stringWithFormat:@"Total Area: %@ - %@ sq ft", [dictInfo objectForKey:@"min_area_range"], [dictInfo objectForKey:@"max_area_range"]];
                }
                else {
                    cell.lblTotalArea.text =@"Total Area: ";
                }
                
                if (![[dictInfo objectForKey:@"unit_block_no"] isKindOfClass:[NSNull class]]) {
                    cell.lblNoOfBlocks.text = [NSString stringWithFormat:@"No of Blocks: %@", [dictInfo objectForKey:@"unit_block_no"]];
                }
                else {
                    cell.lblNoOfBlocks.text = [NSString stringWithFormat:@"No of Blocks: "];
                }
                
                if (![[dictInfo objectForKey:@"floor_no"] isKindOfClass:[NSNull class]]) {
                    cell.lblNoOfFloors.text = [NSString stringWithFormat:@"No. of Floors: %@", [dictInfo objectForKey:@"floor_no"]];
                }
                else {
                    cell.lblNoOfFloors.text = [NSString stringWithFormat:@"No. of Floors: "];
                }
                
                
                if (![[dictInfo objectForKey:@"total_units"] isKindOfClass:[NSNull class]]) {
                    cell.lblNoOfUits.text = [NSString stringWithFormat:@"Total No. of Units: %@", [dictInfo objectForKey:@"total_units"]];
                }
                else {
                    cell.lblNoOfUits.text = [NSString stringWithFormat:@"Total No. of Units: "];
                }
                
                
                if (![[dictInfo objectForKey:@"description"] isKindOfClass:[NSNull class]]) {
                    
                    [cell.webProjectSummary loadHTMLString:[dictInfo objectForKey:@"description"] baseURL:nil];
                    cell.webProjectSummary.scrollView.scrollEnabled = NO;
                    cell.webProjectSummary.delegate = self;
                   
                }
                else {
                    
                }

                cell.btnReadMore.tag = 1000;
                [cell.btnReadMore addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
                
                cellOverView = cell;
                return cell;
            }
            
            
            else {
                return nil;
            }
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0 && dictInfo.count) {
                UnitImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UnitImageCell"];
                
                if (cell == nil) {
                    cell = [UnitImageCell createCell];
                    [cell.collectionView registerClass:[UnitImageCollCell class] forCellWithReuseIdentifier:@"UnitImageCollCell"];
                    
                    [cell.collectionView registerNib:[UINib nibWithNibName:@"UnitImageCollCell" bundle:nil] forCellWithReuseIdentifier:@"UnitImageCollCell"];
                }
 
                cellInfo = cell;
                
                if ([[dictInfo objectForKey:@"total_units"] integerValue]) {
                    cell.arrInfo = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"flat_images"];
                }
                
                if ([[dictInfo objectForKey:@"is_static_unit"] boolValue]) {
                    [cell.btnBookNow setTitle:@"Enquiry" forState:UIControlStateNormal];
                }

                [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
                
                [cell.btnBookNow addTarget:self action:@selector(didSelectedBookNow:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
            }
            else if (indexPath.row == 1 && dictInfo.count) {
                UnitDescCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UnitDescCell"];
                
                if (cell == nil) {
                    cell = [UnitDescCell createCell];
                    
                    [cell.btnLeft addTarget:self action:@selector(didTappedLeftRightButton:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnRight addTarget:self action:@selector(didTappedLeftRightButton:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.colView registerClass:[UnitCell class] forCellWithReuseIdentifier:@"UnitCell"];
                    [cell.colView registerNib:[UINib nibWithNibName:@"UnitCell" bundle:nil] forCellWithReuseIdentifier:@"UnitCell"];
                    
                    cell.colView.delegate = self;
                    cell.colView.dataSource = self;
                    
                    [cell.colView reloadData];
                    cellUnitDescription = cell;
                }
                
                return cell;
            }
        }
        else if (indexPath.section == 2) {
            LocationMap *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationMap"];
            
            if (cell == nil) {
                cell = [LocationMap createCell];
            }
            NSArray *arr = [[dictInfo objectForKey:@"lat_lng"] componentsSeparatedByString:@","];
            cell.mapView.camera = [GMSCameraPosition cameraWithLatitude:[[arr firstObject] floatValue] longitude:[[arr lastObject] floatValue] zoom:13];
            cell.mapView.settings.scrollGestures = NO;
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake([[arr firstObject] floatValue], [[arr lastObject] floatValue]);
            marker.title = [dictInfo objectForKey:@"proj_name"];
            marker.snippet = [dictInfo objectForKey:@"developer_name"];
            marker.map = cell.mapView;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            
            return cell;
        }
        else if (indexPath.section == 3) {
            DetailLifeStyle *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailLifeStyle"];
            
            if (cell == nil) {
                cell = [DetailLifeStyle createCell];
                
            }
            cell.btnLeft.tag = cell.btnRight.tag = indexPath.row;
            NSArray *arr;
            if (indexPath.row == 0) {
                cell.lblTitle.text = @"Amenities";
                arr = [dictInfo objectForKey:@"Amenities"];
            }
            else if (indexPath.row == 1) {
                cell.lblTitle.text = @"Recreation";
                arr = [dictInfo objectForKey:@"recreation"];
            }
            else if (indexPath.row == 2) {
                cell.lblTitle.text = @"Safety";
                arr = [dictInfo objectForKey:@"safety"];
            }
            else if (indexPath.row == 3) {
                cell.lblTitle.text = @"Services";
                arr = [dictInfo objectForKey:@"services"];
            }
            
            NSMutableArray *array = [NSMutableArray array];
            array = [arr mutableCopy];
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
            [array sortUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            cell.arrInfo = [array copy];
            [cell.collectionView reloadData];
            
            return cell;
        }
        else if (indexPath.section == 4) {
            DetailedUnitSpecification *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailedUnitSpecification"];
            
            if (cell == nil) {
                cell = [DetailedUnitSpecification createCell];
            }
            
            NSArray *arr;
            if (indexPath.row == 0) {
                arr = [dictInfo objectForKey:@"walls"];
                
                
                cell.lblTop.text = @" Walls";
                
                cell.lblTitle1.text = @"LIVING/DINING";
                cell.lblTitle2.text = @"MASTER BEDROOM";
                cell.lblTitle3.text = @"OTHER BEDROOMS";
                cell.lblTitle4.text = @"KITCHEN";
                cell.lblTitle5.text = @"TOILETS";
                
                for (NSDictionary *dict in arr) {
                    
                    if ([[dict objectForKey:@"name"] isEqualToString:@"Living/Dining"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption1.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption1.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Master Bedroom"]){
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption2.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption2.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Other Bedrooms"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption3.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption3.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Kitchen"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption4.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption4.text = @"";
                        }
                        
                    }
                    else {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption5.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption5.text = @"";
                        }
                        
                    }
                    
                    
                }
            }
            else if (indexPath.row == 1) {
                
                arr = [dictInfo objectForKey:@"flooring"];
                
                cell.lblTop.text = @" Flooring";
                
                cell.lblTitle1.text = @"DINING/LIVING";
                cell.lblTitle2.text = @"MASTER BEDROOM";
                cell.lblTitle3.text = @"OTHER BEDROOMS";
                cell.lblTitle4.text = @"KITCHEN";
                cell.lblTitle5.text = @"TOILETS";
                
                
                for (NSDictionary *dict in arr) {
                    
                    if ([[dict objectForKey:@"name"] isEqualToString:@"Living/Dining"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption1.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption1.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Master Bedroom"]){
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption2.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption2.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Other Bedrooms"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption3.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption3.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Kitchen"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption4.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption4.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Toilets"]) {
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption5.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption5.text = @"";
                        }
                        
                    }
                    
                    
                }
                
            }
            else if (indexPath.row == 2) {
                arr = [dictInfo objectForKey:@"fitting"];
                
                cell.lblTop.text = @" Fittings";
                
                cell.lblTitle1.text = @"DOORS";
                cell.lblTitle2.text = @"WINDOWS";
                cell.lblTitle3.text = @"OTHER BEDROOMS";
                cell.lblTitle4.text = @"KITCHEN";
                cell.lblTitle5.text = @"TOILET";
                
                for (NSDictionary *dict in arr) {
                    
                    if ([[dict objectForKey:@"name"] isEqualToString:@"Doors"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption1.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption1.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Windows"]){
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption2.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption2.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Other Bedrooms"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption3.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption3.text = @"";
                        }
                        
                    }
                    else if ([[dict objectForKey:@"name"] isEqualToString:@"Kitchen"]) {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption4.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption4.text = @"";
                        }
                        
                    }
                    else {
                        
                        if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                            cell.lblCaption5.text = [dict objectForKey:@"type"];
                        }
                        else {
                            cell.lblCaption5.text = @"";
                        }
                        
                    }
                    
                    
                }
                
            }
            
            
            return cell;
    }
        else if (indexPath.section == 5) {
            PriceTrend *cell = [tableView dequeueReusableCellWithIdentifier:@"PriceTrend"];
            
            if (cell == nil) {
                cell = [PriceTrend createCell];
                
                NSArray *arr = [dictInfo objectForKey:@"price_trends"];
                
                if (arr.count) {
                    cell.web.delegate = self;
                    NSString *strPriceTrends = @"";
                    for (NSDictionary *dict in arr) {
                        NSString *str = [NSString stringWithFormat:@"{\"price_trend_date\":\"%@\",\"price_trends\":\"%@\"},", [dict objectForKey:@"price_trend_date"], [dict objectForKey:@"price_trend_value"]];
                        
                        strPriceTrends = [strPriceTrends stringByAppendingString:str];
                    }
                    
                    strPriceTrends = [strPriceTrends substringToIndex:strPriceTrends.length - 1];
                    
                    NSString* encodedText = [[NSString stringWithFormat:@"%@%@%@?price_trends=[%@]",baseUrl, url_line_chart, extension, strPriceTrends] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                    
                    NSURL *url = [NSURL URLWithString:encodedText];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    
                    [cell.web loadRequest:request];
                    
                }
            }
            
            cellPriceTrends = cell;
            return cell;
        }
        else if (indexPath.section == 6) {
            LocalityInsight *cell = [tableView dequeueReusableCellWithIdentifier:@"LocalityInsight"];
            
            if (cell == nil) {
                cell = [LocalityInsight createCell];
            }
            
            
            if (![[dictInfo objectForKey:@"locality_insights"] isKindOfClass:[NSNull class]]) {
                
                UIFont *font = cell.txtLocalityInsight.font;
                NSString *str = [dictInfo objectForKey:@"locality_insights"];
                if (![str isKindOfClass:[NSNull class]]) {
                    
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                    options:@{
                                                                                              NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                              }
                                                                         documentAttributes:nil
                                                                                      error:nil];
                    
                    
                    cell.txtLocalityInsight.attributedText = attrStr;
                    cell.txtLocalityInsight.font = font;
                
               // cell.txtLocalityInsight.text = [dictInfo objectForKey:@"locality_insights"];
            }
                else {
                    cell.txtLocalityInsight.text = @"No data available";
                }
            }
            
            return cell;
        }
        else if (indexPath.section == 7) {
            DeveloperProfile *cell = [tableView dequeueReusableCellWithIdentifier:@"DeveloperProfile"];
            
            if (cell == nil) {
                cell = [DeveloperProfile createCell];
            }
            
            [cell.imgIcon setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [dictInfo objectForKey:@"builder_logo"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];

            if (![[dictInfo objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
                 cell.lblName.text = [dictInfo objectForKey:@"builder_name"];
            }
            else {
                 cell.lblName.text = @"Name not available";
            }
            
            if (![[dictInfo objectForKey:@"establish_year"] isKindOfClass:[NSNull class]]) {
                cell.lblYearOfEstablishment.text = [NSString stringWithFormat:@"%@", [dictInfo objectForKey:@"establish_year"]];
            }
            else {
                cell.lblYearOfEstablishment.text = @"Unknown";
            }
            
            if (![[dictInfo objectForKey:@"area_delevered"] isKindOfClass:[NSNull class]]) {
                cell.lblAreaDelivered.text = [dictInfo objectForKey:@"area_delevered"];
            }
            else {
                cell.lblAreaDelivered.text = @"Unknown";
            }
            
            
            if (![[dictInfo objectForKey:@"builder_description"] isKindOfClass:[NSNull class]]) {
                [cell.web loadHTMLString:[dictInfo objectForKey:@"builder_description"] baseURL:nil];
            }
            else {
                [cell.web loadHTMLString:@"" baseURL:nil];
            }
            
            cell.web.delegate = self;
            cell.web.scrollView.scrollEnabled = NO;
            cell.btnReadMore.tag = 2000;
            [cell.btnReadMore addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
            
            cellDeveloperProfile = cell;
            return cell;
        }
        else if (indexPath.section == 8) {
            Review *cell = [tableView dequeueReusableCellWithIdentifier:@"Review"];
            
            if (cell == nil) {
                cell = [Review createCell];
            }
            
            NSDictionary *dict = [[dictInfo objectForKey:@"comments_detail"] objectAtIndex:indexPath.row];
            
            if (![[dict objectForKey:@"comment_user_name"] isKindOfClass:[NSNull class]]) {
                cell.lblUserName.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"comment_user_name"]];
            }
            else {
                cell.lblUserName.text = @"";
            }
            
            if (![[dict objectForKey:@"comment_title"] isKindOfClass:[NSNull class]]) {
                cell.lblDescription.text = [dict objectForKey:@"comment_description"];
            }
            else {
                cell.lblDescription.text = @"";
            }
            
            if (![[dict objectForKey:@"rating"] isKindOfClass:[NSNull class]]) {
                cell.rating.rating = [[dict objectForKey:@"rating"] floatValue];
                
                cell.rating.starImage = [UIImage imageNamed:@"star-template"];
                cell.rating.starHighlightedImage = [UIImage imageNamed:@"star-highlighted-template"];
                cell.rating.maxRating = 5.0;
                cell.rating.horizontalMargin = 12;
                cell.rating.editable = NO;
                cell.rating.displayMode = EDStarRatingDisplayHalf;
                cell.rating.tag = indexPath.row;
            }
            
            return cell;
        }
        else if (indexPath.section == 9) {
            PostComment *cell = [tableView dequeueReusableCellWithIdentifier:@"PostComment"];
            
            if (cell == nil) {
                cell = [PostComment createCell];
                
                
            }
            
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Title"];
            
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, 5)];
            [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PT Sans" size:17.0] range:NSMakeRange(0, 5)];
            
            cell.txtTitle.attributedPlaceholder = str;
            cell.txtMessage.placeholder = @"Enter your comment";
            
            [cell.btnPostAComment addTarget:self action:@selector(didSelectPostAComment:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell setUpStarRating];
            cell.starRating.delegate = self;
            cellPostComment = cell;
            return cell;
        }
    }
    else {
        SubUnitDescCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubUnitDescCell"];
        
//        if (indexPath.row == 0) {
//            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
//        }
        
        NSDictionary *dict = [[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row];
        
        
        if (cell == nil) {
            cell = [SubUnitDescCell createCell];
            
            UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame))];
            aView.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
            cell.selectedBackgroundView = aView;
        }

        if (![[dict objectForKey:@"wpcf_flat_typology"] isKindOfClass:[NSNull class]] && ![[dict objectForKey:@"total_types"] isKindOfClass:[NSNull class]]) {
            
            NSString *strFlatTopology = [dict objectForKey:@"wpcf_flat_typology"];
            
            NSString *strType = [NSString stringWithFormat:@"  - %@ Type", [dict objectForKey:@"total_types"]];
            
            NSMutableAttributedString *strAttributedTopology = [[NSMutableAttributedString alloc] initWithString:strFlatTopology];
            [strAttributedTopology addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PT Sans" size:18.0] range:NSMakeRange(0, strFlatTopology.length)];
            
            NSMutableAttributedString *strAttributedTypes = [[NSMutableAttributedString alloc] initWithString:strType];
            [strAttributedTypes addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, strType.length)];
            [strAttributedTypes addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PT Sans" size:18.0] range:NSMakeRange(0, strType.length)];
            
            
            [strAttributedTopology appendAttributedString:strAttributedTypes];
            
            
            cell.lblTitle.attributedText = strAttributedTopology;
        }
        else {
            cell.lblTitle.text = @"";
        }
        
        if (![[dict objectForKey:@"total_units"] isKindOfClass:[NSNull class]]) {
            
            cell.lblUnits.text = [NSString stringWithFormat:@"%@ Units", [dict objectForKey:@"total_units"]];
        }
        else {
            cell.lblUnits.text = @"Units";
        }
        
        
        if (![[dict objectForKey:@"area_range"] isKindOfClass:[NSNull class]]) {
            cell.lblAreaRange.text = [NSString stringWithFormat:@"Area Range:    %@ sqft", [dict objectForKey:@"area_range"]];
        }
        else {
            cell.lblAreaRange.text = @"Area Range:";
        }
        
        
        if (![[dict objectForKey:@"price_range"] isKindOfClass:[NSNull class]]) {
            cell.lblPriceRange.text = [NSString stringWithFormat:@"Price Range:     \u20B9 %@", [dict objectForKey:@"price_range"]];
        }
        else {
            cell.lblPriceRange.text = @"Price Range:";
        }
        
        
        if (![[dict objectForKey:@"per_square_price_range"] isKindOfClass:[NSNull class]]) {
            cell.lblUnitPrice.text = [NSString stringWithFormat:@"\u20B9 %@ per sqft",[dict objectForKey:@"per_square_price_range"]];
        }
        else {
            cell.lblUnitPrice.text = @"";
        }
        
        return cell;
        
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblView) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }
    else {
        cellInfo.arrInfo = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"flat_images"];
        strSelectedUnitIds = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"unit_ids"];
        strSelectedTitle = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"wpcf_flat_typology"];
        [cellInfo.collectionView reloadData];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    
}

#pragma mark - Colection View Delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame), CGRectGetHeight(collectionView.frame));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[dictInfo objectForKey:@"unit_type"] count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row];
    UnitCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UnitCell" forIndexPath:indexPath];
    
    if (![[dict objectForKey:@"wpcf_flat_typology"] isKindOfClass:[NSNull class]] && ![[dict objectForKey:@"total_types"] isKindOfClass:[NSNull class]]) {
        
        NSString *strFlatTopology = [dict objectForKey:@"wpcf_flat_typology"];
        
        NSString *strType = [NSString stringWithFormat:@"  - %@ Type", [dict objectForKey:@"total_types"]];
        
        NSMutableAttributedString *strAttributedTopology = [[NSMutableAttributedString alloc] initWithString:strFlatTopology];
        [strAttributedTopology addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PT Sans" size:18.0] range:NSMakeRange(0, strFlatTopology.length)];
        
        NSMutableAttributedString *strAttributedTypes = [[NSMutableAttributedString alloc] initWithString:strType];
        [strAttributedTypes addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:NSMakeRange(0, strType.length)];
        [strAttributedTypes addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PT Sans" size:18.0] range:NSMakeRange(0, strType.length)];
        
        
        [strAttributedTopology appendAttributedString:strAttributedTypes];
        
        
        cell.lblTitle.attributedText = strAttributedTopology;
    }
    else {
        cell.lblTitle.text = @"";
    }
    
    if (![[dict objectForKey:@"total_units"] isKindOfClass:[NSNull class]]) {
        
        cell.lblUnits.text = [NSString stringWithFormat:@"%@ Units", [dict objectForKey:@"total_units"]];
    }
    else {
        cell.lblUnits.text = @"Units";
    }
    
    
    if (![[dict objectForKey:@"area_range"] isKindOfClass:[NSNull class]]) {
        cell.lblAreaRange.text = [NSString stringWithFormat:@"Area Range:    %@ sqft", [dict objectForKey:@"area_range"]];
    }
    else {
        cell.lblAreaRange.text = @"Area Range:";
    }
    
    
    if (![[dict objectForKey:@"price_range"] isKindOfClass:[NSNull class]]) {
        cell.lblPriceRange.text = [NSString stringWithFormat:@"Price Range:     \u20B9 %@", [dict objectForKey:@"price_range"]];
    }
    else {
        cell.lblPriceRange.text = @"Price Range:";
    }
    
    
    if (![[dict objectForKey:@"per_square_price_range"] isKindOfClass:[NSNull class]]) {
        cell.lblUnitPrice.text = [NSString stringWithFormat:@"\u20B9 %@ per sqft",[dict objectForKey:@"per_square_price_range"]];
    }
    else {
        cell.lblUnitPrice.text = @"";
    }
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    cellInfo.arrInfo = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"flat_images"];
    strSelectedUnitIds = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"unit_ids"];
    strSelectedTitle = [[[dictInfo objectForKey:@"unit_type"] objectAtIndex:indexPath.row] objectForKey:@"wpcf_flat_typology"];
    [cellInfo.collectionView reloadData];
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self collectionView:collectionView didSelectItemAtIndexPath:indexPath];
    
    if ([[dictInfo objectForKey:@"unit_type"] count] == 1) {
        cellUnitDescription.btnLeft.hidden = YES;
        cellUnitDescription.btnRight.hidden = YES;
    }
    else {
//        NSLog(@"Index path %d", (int)indexPath.item);
        if (indexPath.item == 0) {
            cellUnitDescription.btnLeft.hidden = YES;
            cellUnitDescription.btnRight.hidden = NO;
        }
        else if (indexPath.item == [[dictInfo objectForKey:@"unit_type"] count] - 1) {
            cellUnitDescription.btnRight.hidden = YES;
            cellUnitDescription.btnLeft.hidden = NO;
        }
        else if (indexPath.item > 0 && indexPath.item < [[dictInfo objectForKey:@"unit_type"] count] - 1) {
            cellUnitDescription.btnLeft.hidden = NO;
            cellUnitDescription.btnRight.hidden = NO;
        }
    }
}

#pragma mark - ScrollView delegates
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    if ([scrollView isMemberOfClass:[cellUnitDescription.colView class]]) {
        
        NSIndexPath *indexPath = [cellUnitDescription.colView indexPathForCell:[[cellUnitDescription.colView visibleCells] firstObject]];

        if ([[dictInfo objectForKey:@"unit_type"] count] == 1) {
            cellUnitDescription.btnLeft.hidden = YES;
            cellUnitDescription.btnRight.hidden = YES;
        }
        else {
            NSLog(@"Index path %d", (int)indexPath.item);
            if (indexPath.item == 0) {
                cellUnitDescription.btnLeft.hidden = YES;
                cellUnitDescription.btnRight.hidden = NO;
            }
            else if (indexPath.item == [[dictInfo objectForKey:@"unit_type"] count] - 1) {
                cellUnitDescription.btnRight.hidden = YES;
                cellUnitDescription.btnLeft.hidden = NO;
            }
            else if (indexPath.item > 0 && indexPath.item < [[dictInfo objectForKey:@"unit_type"] count] - 1) {
                cellUnitDescription.btnLeft.hidden = NO;
                cellUnitDescription.btnRight.hidden = NO;
            }
        }
    }
    else {
        
    }
    
    
}

#pragma mark - Youtube player delegate
- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
   NSLog(@"%s, %ld", __FUNCTION__, (long)state);
    if (state == kYTPlayerStatePlaying) {
        
    }
    
    
}




#pragma mark - Touch delegates


#pragma mark - Auxillary Mathods
- (UILabel *)setLableWithValue:(NSString *)str andFrame:(CGRect)frame{
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectZero];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.text = str;
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.numberOfLines = 3;
    lbl.font = [UIFont fontWithName:@"PT Sans" size:15.0];
    [lbl sizeToFit];
    
    lbl.frame = CGRectMake(frame.origin.x, frame.origin.y, lbl.frame.size.width, frame.size.height);
    
    return lbl;
}

- (void)setTextField:(UITextField *)txtField withPlaceholder:(NSString *)placeholder {
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtField.frame))];
    
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer1.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [aView.layer addSublayer:layer1];
    
    aView.backgroundColor = [UIColor whiteColor];
    txtField.leftView = aView;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    
    txtField.placeholder = placeholder;
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtField.frame) - 2, CGRectGetWidth(txtField.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtField.layer addSublayer:layer];

    txtField.delegate = self;
    
}

- (void)setTextView:(UIPlaceholderTextView *)txtView {
    
    txtView.placeholder = @"Write Your Enquiry";
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtView.frame) - 2, CGRectGetWidth(txtView.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtView.layer addSublayer:layer];
    
    
}


-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

- (void)shakeView:(UITextView *)theOneYouWannaShake {
    
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shakeView:theOneYouWannaShake];
     }];
    
}


- (NSString *)setPriceRange:(NSString *)str {
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs+", result];
    }
    
    return strNum;
}


- (void)setTitleForFlats:(NSString *)strTitle {
    
}

#pragma mark - Rating Delegates
-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating {
    starRating = rating;
}

#pragma mark - Web view Delegates

- (void)webViewDidStartLoad:(UIWebView *)webView {
    if (webView == cellPriceTrends.web) {
        [cellPriceTrends.indicator startAnimating];
    }
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (webView == cellPriceTrends.web) {
        [cellPriceTrends.indicator stopAnimating];
    }
    else if (webView == cellOverView.webProjectSummary || webView == cellDeveloperProfile.web) {
        
        if (webView.scrollView.contentSize.height > webView.frame.size.height) {
             cellOverView.btnReadMore.hidden = NO;
        }
        else {
            cellOverView.btnReadMore.hidden = YES;
        }
    }
    
}

#pragma mark - Touch delegartes
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    CGPoint aPoint = [[touches anyObject] locationInView:self.view];
    
    if (CGRectContainsPoint(btnUP.frame, aPoint)) {
        NSLog(@"Touch Began");
    }
    
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint aPoint = [[touches anyObject] locationInView:self.view];
    
    if (CGRectContainsPoint(btnUP.frame, aPoint)) {
        NSLog(@"Touch Moved");
    }
   
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    CGPoint aPoint = [[touches anyObject] locationInView:self.view];
    
    if (CGRectContainsPoint(btnUP.frame, aPoint)) {
        NSLog(@"Touch cancelled");
    }
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    CGPoint aPoint = [[touches anyObject] locationInView:self.view];
    
    if (CGRectContainsPoint(btnUP.frame, aPoint)) {
        NSLog(@"Touch ended");
    }
}

#pragma mark - Scroll Delegates
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (scrollView == tblView) {
        CGPoint aPoint = scrollView.contentOffset;
        
        if (aPoint.y > 300) {
            
            [UIView animateWithDuration:2 animations:^{
                btnShare.superview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
                btnUP.hidden = NO;
            }];
            
        }
        else {
            
            [UIView animateWithDuration:2 animations:^{
                btnShare.superview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
                btnUP.hidden = YES;
            }];
        }
    }
    else {
        
    }
    
}


#pragma mark - UITextField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.keyboardType == UIKeyboardTypePhonePad) {
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return newLength <= 10;
    }
    else {
        return YES;
    }
}

- (void)readMore:(UIButton *)btn {
    
    CustomVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomVC"];
    if (btn.tag == 1000) {
        obj.strTitle = @"Overview";
        obj.strHTMLContent = [dictInfo objectForKey:@"description"];
    }
    else {
        obj.strTitle = @"Builder Description";
        obj.strHTMLContent = [dictInfo objectForKey:@"builder_description"];
    }
    [self presentViewController:obj animated:YES completion:nil];
}


@end
