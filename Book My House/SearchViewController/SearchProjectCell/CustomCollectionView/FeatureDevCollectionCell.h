//
//  FeatureDevCollectionCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/11/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface FeatureDevCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnSeeAllProjects;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblReadyToMove;
@property (weak, nonatomic) IBOutlet UILabel *lblPreLaunch;
@property (weak, nonatomic) IBOutlet UILabel *lblUnderConstruction;

- (IBAction)actionListeners:(id)sender;
@end
