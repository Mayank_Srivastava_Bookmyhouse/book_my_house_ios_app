//
//  FeatureProjectCollectionCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/11/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeatureProjectCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UILabel *lblProjectName;
@property (weak, nonatomic) IBOutlet UILabel *lblFlatType;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblMinPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnFeaturedProjects;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;

@end
