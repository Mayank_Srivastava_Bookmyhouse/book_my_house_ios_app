//
//  SearchProjectCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/8/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "SearchCell.h"
#import "FeatureProjectCollectionCell.h"
#import "FeatureDevCollectionCell.h"
#import "MARKRangeSlider.h"


#pragma mark - Search Project


@protocol SearchProjectCellDelegate <NSObject>
//- (void)didSearchProperty:(NSDictionary *)propInfo;
- (void)didSeeAllProjects:(NSDictionary *)propInfo;
- (void)didTappedSeeFeaturedProject:(NSDictionary *)propInfo;
- (void)didTappedFavoriteFeaturedProject:(UIButton *)sender;
- (void)didTappedSpecialProjects:(NSString *)strType;
@end

@interface SearchProjectCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtSelectCity;
@property (weak, nonatomic) IBOutlet UIButton *btnResidential;
@property (weak, nonatomic) IBOutlet UIButton *btnCommercial;
@property (weak, nonatomic) IBOutlet UITextField *txtEntry;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchProperties;
@property (weak, nonatomic) id<SearchProjectCellDelegate> delegate;

- (IBAction)actionListeners:(id)sender;

+ (SearchProjectCell *)createCell;
@end




#pragma mark - Featured Developers
@interface FeaturedDevelopers : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSArray *arrFeaturedDevelopers;
@property (weak, nonatomic) id<SearchProjectCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
+ (FeaturedDevelopers *)createCell;
- (IBAction)actionListners:(id)sender;
@end




#pragma mark - Featured Project
@interface FeaturedProject : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSArray *arrFeaturedProjects;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) id<SearchProjectCellDelegate> delegate;
+ (FeaturedProject *)createCell;
- (IBAction)actionListener:(id)sender;
@end

#pragma mark - Featured Catagories
@interface FeaturedCatagories: UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *arrCatagories;
@property (weak, nonatomic) id<SearchProjectCellDelegate> delegate;
+ (FeaturedCatagories *)createCell;
@end
