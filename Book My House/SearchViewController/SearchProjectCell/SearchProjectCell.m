//
//  SearchProjectCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/8/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//


#import "SearchProjectCell.h"
#import "Catagories.h"

#pragma mark - Search Project

@implementation SearchProjectCell

+ (SearchProjectCell *)createCell {
    
    SearchProjectCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"SearchProjectCell" owner:self options:nil] firstObject];
        
    [cell.collectionView registerClass:[SearchCell class] forCellWithReuseIdentifier:@"Search"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"SearchCell" bundle:nil] forCellWithReuseIdentifier:@"Search"];
    
    
    return cell;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    NSLog(@"%s", __FUNCTION__);
}
@end


#pragma mark -
#pragma mark - Featured Developers

@implementation FeaturedDevelopers
+ (FeaturedDevelopers *)createCell{
    
    FeaturedDevelopers *cell = [[[NSBundle mainBundle] loadNibNamed:@"SearchProjectCell" owner:self options:nil] objectAtIndex:1];
    [cell.collectionView registerClass:[FeatureDevCollectionCell class] forCellWithReuseIdentifier:@"FeaturedDevelopers"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"FeatureDevCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"FeaturedDevelopers"];

    cell.collectionView.layer.borderWidth = 1.0;
    cell.collectionView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.collectionView.layer.opacity = 0.9;
    
    
    return cell;
}


- (IBAction)actionListners:(id)sender {
    
    if (sender == _btnRight) {
        NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_collectionView numberOfItemsInSection:0];
        
        if (num > nextItem.item) {
            [_collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    else if (sender == _btnLeft) {
        NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        
        NSLog(@"Item number %d", (int)prevItem.item);
        if (prevItem.item >= 0) {
            [_collectionView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


#pragma mark - Collection View
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    //return UIEdgeInsetsMake(0, 5, 0, 5);
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrFeaturedDevelopers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"Index path %d", (int)indexPath.item);
    
    static NSString *strId = @"FeaturedDevelopers";
    FeatureDevCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strId forIndexPath:indexPath];
    cell.btnSeeAllProjects.tag = indexPath.item;

    if (_arrFeaturedDevelopers.count == 1) {
        _btnLeft.hidden = YES;
        _btnRight.hidden = YES;
    }
    else {
        if (indexPath.item == 0) {
            _btnLeft.hidden = YES;
            _btnRight.hidden = !_btnLeft.hidden;
        }
        else if (indexPath.row == _arrFeaturedDevelopers.count - 1) {
            _btnRight.hidden = YES;
            _btnLeft.hidden = !_btnRight.hidden;
        }
        else if (indexPath.item > 0 && indexPath.item < _arrFeaturedDevelopers.count - 1) {
            _btnLeft.hidden = NO;
            _btnRight.hidden = NO;

        }
    }
    
    [cell.btnSeeAllProjects addTarget:self action:@selector(actionListeners:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dict = [_arrFeaturedDevelopers objectAtIndex:indexPath.row];
    NSString *str = [dict objectForKey:@"company_name"];
    
    if (![str isKindOfClass:[NSNull class]]) {
        cell.lblBuilderName.text = str;
    }
    else {
        cell.lblBuilderName.text = @"";
    }
    
    str = [dict objectForKey:@"ready_to_move"];
    
    if (![str isKindOfClass:[NSNull class]]) {
        cell.lblReadyToMove.text = str;
    }
    else {
        cell.lblReadyToMove.text = @"";
    }
    //under_construction
    str = [dict objectForKey:@"under_construction"];
    
    if (![str isKindOfClass:[NSNull class]]) {
        cell.lblUnderConstruction.text = str;
    }
    else {
        cell.lblUnderConstruction.text = @"";
    }
    
    //new_launch
    str = [dict objectForKey:@"new_launch"];
    
    if (![str isKindOfClass:[NSNull class]]) {
        cell.lblPreLaunch.text = str;
    }
    else {
        cell.lblPreLaunch.text = @"";
    }

    str = [dict objectForKey:@"logo"];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(cell.imgLogo.frame), CGRectGetHeight(cell.imgLogo.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, str]];
    [cell.imgLogo setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
    
    
    
//    if (indexPath.row % 2 == 0) {
//        cell.contentView.backgroundColor = [UIColor greenColor];
//    }
//    else {
//        cell.contentView.backgroundColor = [UIColor redColor];
//    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //SearchCell *cell = (SearchCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    //cell.contentView.backgroundColor = [UIColor greenColor];
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    SearchCell *cell = (SearchCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor whiteColor];
}


#pragma mark - Button Actions
- (void)actionListeners:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    NSString *str = [[_arrFeaturedDevelopers objectAtIndex:btn.tag] objectForKey:@"builder_id"];
    NSString *strBuilderName = [[_arrFeaturedDevelopers objectAtIndex:btn.tag] objectForKey:@"company_name"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:str forKey:@"builder_id"];
    [dict setObject:strBuilderName forKey:@"builder_name"];
    
    NSString *strCity = [[[NSUserDefaults standardUserDefaults] objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
    
    if (strCity) {
        [dict setObject:strCity forKey:@"city_id"];
    }
    
    [_delegate didSeeAllProjects:dict];
}

@end

#pragma mark -
#pragma mark - Featured Project
@implementation FeaturedProject



+ (FeaturedProject *)createCell {
    
    FeaturedProject *cell = [[[NSBundle mainBundle] loadNibNamed:@"SearchProjectCell" owner:self options:nil] objectAtIndex:2];
    [cell.collectionView registerClass:[FeatureProjectCollectionCell class] forCellWithReuseIdentifier:@"FeatureProject"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"FeatureProjectCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"FeatureProject"];
    
    cell.collectionView.pagingEnabled = YES;
    
    return cell;
}

- (IBAction)actionListener:(id)sender {
    if (sender ==  _btnRight) {
        NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems firstObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
        NSLog(@"Item number %d", (int)nextItem.item);
        NSInteger num = [_collectionView numberOfItemsInSection:0];
        
        if (num > nextItem.item) {
            [_collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
    }
    else if (sender == _btnLeft) {
        NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
        
        NSLog(@"Item number %d", (int)prevItem.item);
        if (prevItem.item >= 0) {
            [_collectionView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        }
        
    }
}

- (void)didTappedFavorite:(UIButton *)sender {
    NSLog(@"Tag values %d", (int)sender.tag);
    [_delegate didTappedFavoriteFeaturedProject:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrFeaturedProjects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *identifier = @"FeatureProject";
    FeatureProjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *dict = [_arrFeaturedProjects objectAtIndex:indexPath.row];
    
    cell.btnFeaturedProjects.tag = indexPath.row;
    [cell.btnFeaturedProjects addTarget:self action:@selector(actionListeners:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnFavorite.tag = indexPath.row;
    [cell.btnFavorite addTarget:self action:@selector(didTappedFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    if (_arrFeaturedProjects.count == 1) {
        _btnLeft.hidden = YES;
        _btnRight.hidden = YES;
    }
    else {
        if (indexPath.row == 0) {
            _btnLeft.hidden = YES;
            _btnRight.hidden = !_btnLeft.hidden;
        }
        else if (indexPath.row == _arrFeaturedProjects.count - 1) {
            _btnRight.hidden = YES;
            _btnLeft.hidden = !_btnRight.hidden;
        }
        else if (indexPath.row > 0 && indexPath.row < _arrFeaturedProjects.count - 1) {
            _btnLeft.hidden = NO;
            _btnRight.hidden = NO;
            
        }
    }
    
    NSString *str = [dict objectForKey:@"banner"];
    if (![str isKindOfClass:[NSNull class]]) {

        NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(cell.imgBg.frame), CGRectGetHeight(cell.imgBg.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, str]];
        [cell.imgBg setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
    }
    else {
        
    }
    
    str = [dict objectForKey:@"name"];
    if (![str isKindOfClass:[NSNull class]]) {
        
        UIFont *font =  cell.lblProjectName.font;
        NSAttributedString *strProjectName = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding]
                                                                              options:@{
                                                                                        NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType
                                                                                        } documentAttributes:nil error:nil];
        
        
        
        cell.lblProjectName.attributedText = strProjectName;
        cell.lblProjectName.font = font;
    }
    else {
        
    }

    str = [dict objectForKey:@"unit_type"];
    if (![str isKindOfClass:[NSNull class]]) {
        cell.lblFlatType.text = [NSString stringWithFormat:@"%@", str];
    }
    else {
        
    }
    
    str = [dict objectForKey:@"address"];
    if (![str isKindOfClass:[NSNull class]]) {
        cell.lblAddress.text = str;
    }
    else {
       cell.lblAddress.text = @"--";
    }
    
    str = [dict objectForKey:@"price"];
    if (![str isKindOfClass:[NSNull class]]) {
        
        
        cell.lblMinPrice.text = [NSString stringWithFormat:@"\u20B9 %@", str];
    }
    else {
        cell.lblMinPrice.text = @"--";
    }
    
    cell.btnFavorite.selected = [[dict objectForKey:@"favorite"] boolValue];
    
    return cell;
}



#pragma mark - Button Actions
- (void)actionListeners:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    NSString *str = [[_arrFeaturedProjects objectAtIndex:btn.tag] objectForKey:@"id"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:str forKey:@"project_id"];
    [_delegate didTappedSeeFeaturedProject:dict];
}
@end


@implementation FeaturedCatagories

+ (FeaturedCatagories *)createCell {
    
    FeaturedCatagories *cell = [[[NSBundle mainBundle] loadNibNamed:@"SearchProjectCell" owner:self options:nil] objectAtIndex:3];
    
    [cell.collectionView registerClass:[Catagories class] forCellWithReuseIdentifier:@"Catagories"];
    [cell.collectionView registerNib:[UINib nibWithNibName:@"Catagories" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"Catagories"];
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //return CGSizeMake(150, 90);
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/1.5, 100);
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrCatagories.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Catagories *cell = (Catagories *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Catagories" forIndexPath:indexPath];
    
    NSString *str = [_arrCatagories objectAtIndex:indexPath.item];
    
    if (cell.lblTitle.layer.borderColor != [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:219.0/255.0 alpha:1.0].CGColor) {
        
        cell.lblTitle.layer.borderWidth = 2.0;
        cell.lblTitle.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    }
 
    if ([str isEqualToString:@"isHotProjectAvailable"]) {
        cell.lblTitle.text = @"Hot Projects";
        cell.img.image = [UIImage imageNamed:@"Hot"];
    }
    else if ([str isEqualToString:@"isLiveProjectAvailable"]) {
        cell.lblTitle.text = @"Live Projects";
        cell.img.image = [UIImage imageNamed:@"Live"];
    }
    else if ([str isEqualToString:@"isReadyToMoveInProjectAvailable"]) {
        cell.lblTitle.text = @"Ready to Move In Projects";
        cell.img.image = [UIImage imageNamed:@"Ready-to-Move"];
    }
    else if ([str isEqualToString:@"isLuxuryProjectAvailable"]) {
        cell.lblTitle.text = @"Luxury Projects";
        cell.img.image = [UIImage imageNamed:@"Luxury"];
    }
    else if ([str isEqualToString:@"isAffordableHomesProjectAvailable"]) {
        cell.lblTitle.text = @"Affordable Projects";
        cell.img.image = [UIImage imageNamed:@"Affordable"];
    }
 
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *str = [_arrCatagories objectAtIndex:indexPath.item];
    //hot/live/ready to move in/luxury/affordable
    if ([str isEqualToString:@"isHotProjectAvailable"]) {
        [_delegate didTappedSpecialProjects:@"hot"];
    }
    else if ([str isEqualToString:@"isLiveProjectAvailable"]) {
        [_delegate didTappedSpecialProjects:@"live"];
    }
    else if ([str isEqualToString:@"isReadyToMoveInProjectAvailable"]) {
        [_delegate didTappedSpecialProjects:@"ready to move in"];
    }
    else if ([str isEqualToString:@"isLuxuryProjectAvailable"]) {
        [_delegate didTappedSpecialProjects:@"luxury"];
    }
    else if ([str isEqualToString:@"isAffordableHomesProjectAvailable"]) {
        [_delegate didTappedSpecialProjects:@"affordable"];
    }
}

@end
