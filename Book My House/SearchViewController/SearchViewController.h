//
//  SearchViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/8/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "SearchProjectCell.h"
#import "FeatureProjectCollectionCell.h"
#import "LocalityMapVC.h"
#import "MenuView.h"
#import "ProjectListVC.h"
#import "ProjectDetailVC.h"
#import "SelectCityViewController.h"

typedef NS_ENUM(NSUInteger, SearchViewControllerAccessOption) {
    SearchViewControllerAccessOptionSkip,
    SearchViewControllerAccessOptionSignIn,
    SearchViewControllerAccessOptionSignUp,
    SearchViewControllerAccessOptionFacebook,
    SearchViewControllerAccessOptionGooglePlus,
    SearchViewControllerAccessOptionMisc,
};

@interface SearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SearchProjectCellDelegate, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (assign, nonatomic) SearchViewControllerAccessOption option;

- (void)getInfoFromApi;
@end
