//
//  SearchViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/8/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SearchViewController.h"
static NSString * const strLogin                             = @"Please login to add items in favorite list.";


#define TAG_BUTTON_MENU 1
#define TAG_BUTTON_BACK 2

@interface SearchViewController (){
    
    NSArray *arrByCity, *arrByBuilder, *arrByProject, *arrFeaturedDevelopers, *arrFeaturedProjects, *arrTypes, *arrImagesNormal, *arrImageInselected;
    UIRefreshControl *refreshControl;
    MenuView *viewMenu;
    NSDictionary *dictInfo;
    NSDictionary *dictCityInfo, *dictKeyword;
    NSUserDefaults *pref;
    SearchProjectCell *cellSearchProject;
    NSTimer *timerFeaturedDeveloper, *timerFeaturedProject;
    FeaturedDevelopers *cellFeaturedDeveloper;
    FeaturedProject *cellFeaturedProject;
    NSInteger numberOfCells;
    NSMutableDictionary *dictFavoriteReq;
    NSInteger indexSelected;
    
    UIButton *btnResidential, *btnCommercial;
    BOOL isResidenceSelected;
    SearchProjectCell *cellSearchProjectCell;
    FeaturedCatagories *cellFeaturedCatagory;
    NSMutableArray *arrSelectedIndeces;
    NSArray *arrCatagories;
    
    
}
@property (weak, nonatomic) IBOutlet UIView *viewStatusBar;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
- (IBAction)actionListeners:(id)sender;

@end

@implementation SearchViewController



#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)loadView {

    arrSelectedIndeces = [NSMutableArray array];
    isResidenceSelected = YES;
    numberOfCells = 1;
    arrTypes = [NSArray arrayWithObjects:@[@"FLAT", @"BUILDER FLOOR", @"PLOT", @"HOUSE/VILLA"], @[@"SHOP/SHOWROOM", @"OFFICE SPACE", @"SERVICE APARTMENT"],  nil];
    
    
    arrImagesNormal = [NSArray arrayWithObjects:@[@"Flat_Green", @"Builder_Floor_Green", @"Plot_Green", @"House_Villa_Green"], @[@"Shops_Green", @"Office_Green",@"Service_Appartment_Green" ], nil];
    
    arrImageInselected = [NSArray arrayWithObjects:@[@"Flat_Gold", @"Builder_Floor_Gold", @"Plot_Gold", @"House_Villa_Gold"], @[@"Shops_Gold", @"Office_Gold",@"Service_Appartment_Gold"], nil];
    
    pref = [NSUserDefaults standardUserDefaults];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getselectedCityInfo:) name:kSelectCity object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSuggestiveKeyword:) name:kSuggestion object:nil];
    [super loadView];
    
    
    if ([pref objectForKey:strSaveCityInfo] != nil) {
        [self getInfoFromApi];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self screenSettings];
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    viewMenu = [MenuView createView];
    viewMenu.frame = CGRectMake(- 1 * CGRectGetWidth(self.view.frame),  0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    viewMenu.viewController = self;
    [self.view addSubview:viewMenu];
    
    [self manageModalScreen];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];    
    [viewMenu closeMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)dealloc {
    [timerFeaturedDeveloper invalidate];
    [timerFeaturedProject invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSelectCity object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSuggestion object:nil];
}

#pragma mark -  Screen Settings 
- (void)screenSettings {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    
    _viewStatusBar.backgroundColor = _btnMenu.superview.backgroundColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:0.5];
    
    UIImageView *imgview = [[UIImageView alloc] initWithFrame:self.view.frame];
    imgview.image = [UIImage imageNamed:@"Home_Screen"];
    [self.view addSubview:imgview];
    [self.view sendSubviewToBack:imgview];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:loading];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:refreshControl];
    _tblView.backgroundColor = [UIColor clearColor];
}


- (void)refresh {
    [refreshControl beginRefreshing];
    [self getInfoFromApi];
    
}

- (void)getInfoFromApi {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring]; 
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict1 = [NSMutableDictionary dictionary];
        NSString *str1 = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
        [dict1 setObject:str1 forKey:@"city_id"];
        
        numberOfCells = 1;
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        
        [ServiceAPI getInfoForFeaturedBuilder:dict1 withCallBack:^(BOOL isSuccess, id featuredBuilders) {
            
            if (isSuccess) {
                arrFeaturedDevelopers = [NSArray arrayWithArray:featuredBuilders];
                
                
                numberOfCells ++;
                [_tblView reloadData];
                [ServiceAPI getCatagories:dict1 withCallback:^(BOOL isSuccess, id specialCatagories) {
                    
                    if (isSuccess) {
                        
                        //numberOfCells++;
                        
                        NSMutableArray *arr = [NSMutableArray array];

                        for (NSString *keys in [specialCatagories allKeys]) {
                            
                            if ([[specialCatagories objectForKey:keys] boolValue]) {
                                [arr addObject:keys];
                            }
                        }
                        
                        arrCatagories = [arr copy];
                        
                        //[_tblView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        [_tblView reloadData];
                        
                        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                        NSString *str = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
                        [dict setObject:str forKey:@"city_id"];
                        
                        NSString *strUserId = [pref objectForKey:strSaveUserId];
                        
                        if (strUserId) {
                            [dict setObject:strUserId forKey:strSaveUserId];
                        }
                        
                        
                        
                        ////////
                        [ServiceAPI getInfoForFeatureProjects:dict withCallBack:^(BOOL isSuccess, id featuredProject) {
                            [[AppDelegate share] enableUserInteraction];
                            if ([refreshControl isRefreshing]) {
                                [refreshControl endRefreshing];
                            }
                            if (isSuccess) {
                                arrFeaturedProjects = [NSArray arrayWithArray:featuredProject];
                                
                                if (arrFeaturedProjects.count) {
                                    if ([refreshControl isRefreshing]) {
                                        [refreshControl endRefreshing];
                                    }
                                    numberOfCells ++;
                                    //[_tblView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationNone];

                                    [_tblView reloadData];
                                }
                            }
                            else {
                                
                            }
                            
                            
                        }];
                    }
                }];
            }
            else {
                if ([refreshControl isRefreshing]) {
                    [refreshControl endRefreshing];
                }
                [[AppDelegate share] enableUserInteraction];
            }
            
        }];
    }
    else {
        
        if ([refreshControl isRefreshing]) {
            [refreshControl endRefreshing];
        }
        [[AppDelegate share] enableNoInternetAlert:@selector(getInfoFromApi) fromClass:self];
    }
}


#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if ([sender isKindOfClass:[UIButton class]]) {
        
        UIButton *btn = (UIButton *)sender;
        if (btn.tag == TAG_BUTTON_BACK) {
            
            [viewMenu closeMenu];
        }
        else if (btn.tag == TAG_BUTTON_MENU) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kResignKeyboard object:nil];
            
            if (viewMenu.isOpen) {
                
                [viewMenu closeMenu];
            }
            else {
                
                [viewMenu openMenu];
            }
        }
    }
    else {
        
    }
}

- (void)didSelectSearchProperty:(UIButton *)btn {
    
     NSString *str;
    if (![[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Builder"]) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        str = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
        [dict setObject:str forKey:@"city_id"];
        str = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"];
        [dict setObject:str forKey:@"city"];
        
        str = @"";
        NSMutableArray *arr = [NSMutableArray array];
        for (NSIndexPath *indexPath in arrSelectedIndeces) {
            
            NSString *strType = [[arrTypes objectAtIndex:isResidenceSelected? 0:1] objectAtIndex:indexPath.row];
            
            
            
            strType = [strType stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            
            if (![strType isEqualToString:@"SHOP/SHOWROOM"]) {
                strType = [strType stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
            }
            
            [arr addObject:strType];
            
        }
        
        if (arr.count) {
            str = [arr componentsJoinedByString:@","];
        }
        
        [dict setObject:str forKey:@"p_type"];
        [dict setObject:@"Buy" forKey:@"type"];
        
        str = [dictKeyword objectForKey:@"subtitle"];
        if (dictKeyword != nil && ([str isEqualToString:@"Location"] || [str isEqualToString:@"Sublocation"])) {
            
            [dict setObject:dictKeyword forKey:@"keyword"];
            str = [dictKeyword objectForKey:@"subtitle"];
            
            LocalityListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalityListVC"];
            obj.dictInfo = dict;
            obj.option = [str isEqualToString:@"Sublocation"] ? TabularSearchOptionSectors : TabularSearchOptionNormal;
            
            [self.navigationController pushViewController:obj animated:YES];
            
        }
        else {
            
            LocalityListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocalityListVC"];
            obj.dictInfo = dict;
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
    else {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[dictKeyword objectForKey:@"id"] forKey:@"builder_id"];
            [dict setObject:[dictKeyword objectForKey:@"title"] forKey:@"builder_name"];
    
            NSString *strCityId = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
    
            if (strCityId) {
                [dict setObject:strCityId forKey:@"city_id"];
            }
        
            str = @"";
            NSMutableArray *arr = [NSMutableArray array];
            for (NSIndexPath *indexPath in arrSelectedIndeces) {
            
                NSString *strType = [[arrTypes objectAtIndex:isResidenceSelected? 0:1] objectAtIndex:indexPath.row];
            
            
            
                strType = [strType stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            
                if (![strType isEqualToString:@"SHOP/SHOWROOM"]) {
                    strType = [strType stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
                }
            
            [arr addObject:strType];
            
        }
        
        if (arr.count) {
            str = [arr componentsJoinedByString:@","];
        }
        
        [dict setObject:str forKey:@"p_type"];
        [dict setObject:@"Buy" forKey:@"type"];
        
            ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectListVC"];
            obj.dictSourse = dict;
            obj.strTitle = [dict objectForKey:@"builder_name"];
            obj.option = DetailFeaturedDeveloperAccessOptionSearchProperty;
            [self.navigationController pushViewController:obj animated:YES];
    }
}


- (void)toggleResidence:(UIButton *)sender {
    
    if (sender == btnResidential && !isResidenceSelected) {
        
        btnResidential.backgroundColor = [UIColor colorWithRed:213.0/255.0 green:142.0/255.0 blue:0 alpha:1.0];
        [btnResidential setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        
        btnCommercial.backgroundColor = [UIColor clearColor];
        [btnCommercial setTitleColor:[UIColor colorWithRed:213.0/255.0 green:142.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        
        isResidenceSelected = YES;
        
        [arrSelectedIndeces removeAllObjects];
        [cellSearchProject.collectionView reloadData];
        
    }
    else if (sender == btnCommercial && isResidenceSelected) {
        
        btnResidential.backgroundColor = [UIColor clearColor];
        [btnResidential setTitleColor:[UIColor colorWithRed:213.0/255.0 green:142.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        
        btnCommercial.backgroundColor = [UIColor colorWithRed:213.0/255.0 green:142.0/255.0 blue:0 alpha:1.0];
        [btnCommercial setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        isResidenceSelected = NO;
        
        [arrSelectedIndeces removeAllObjects];
        [cellSearchProject.collectionView reloadData];
    }
    
}


#pragma mark - API Actions



#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return numberOfCells;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (section == 0) {
        return 2;
    }
    else if (section == 1) {
        return 1;
    }
    else {
        return 1;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }
    else if (section == 1) {
        
        
        return 50;
    }
    else {
        return 30;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *lbl;
    UIView *view;
    if (section == 0) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 30)];
        view.backgroundColor = [UIColor clearColor];
    }
    else if (section == 1) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50)];
        view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];

        lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(view.frame) - 10, CGRectGetHeight(view.frame))];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.font = [UIFont fontWithName:@"PT Sans" size:22.0];
        lbl.text = @"Featured Developers";
        [view addSubview:lbl];
    }
    else if (section == 2) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 30)];
        view.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(view.frame) - 10, CGRectGetHeight(view.frame))];
        lbl.textAlignment = NSTextAlignmentLeft;
        lbl.text = @"Featured Project";
        lbl.font = [UIFont fontWithName:@"PT Sans" size:22.0];
        [view addSubview:lbl];
    }
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 407;
        }
        else {
            
            return arrCatagories.count ? 120:0;
            //return arrCatagories.count ? CGRectGetWidth(tableView.frame) / 5 :0;
        }
    }
    else if (indexPath.section == 1) {
        return 340;
    }
    else if (indexPath.section == 2) {
        return 334;
    }
    else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            static NSString *strIdentifiers = @"SearchProjectCell";
            SearchProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:strIdentifiers];
            
            if (!cell) {
                cell = [SearchProjectCell createCell];
                
                cell.txtEntry.superview.layer.cornerRadius = 2.0;
                cell.txtEntry.superview.clipsToBounds = YES;
                
                CALayer *border = [CALayer layer];
                CGFloat borderWidth = 1;
                border.borderColor = [UIColor lightGrayColor].CGColor;
                border.frame = CGRectMake(0, cell.txtEntry.frame.size.height - borderWidth, cell.txtEntry.frame.size.width, cell.txtEntry.frame.size.height);
                border.borderWidth = borderWidth;
                [cell.txtEntry.layer addSublayer:border];
                cell.txtEntry.layer.masksToBounds = YES;
                cell.btnSearchProperties.layer.borderWidth = 1.5;
                cell.btnSearchProperties.layer.borderColor = [UIColor colorWithRed:213.0/255.0 green:142.0/255.0 blue:0 alpha:1.0].CGColor;
                
                btnCommercial = cell.btnCommercial;
                btnResidential = cell.btnResidential;
                
                cell.btnCommercial.superview.layer.borderWidth = 1.5;
                cell.btnCommercial.superview.layer.borderColor = [UIColor colorWithRed:213.0/255.0 green:142.0/255.0 blue:0 alpha:1.0].CGColor;
                
                [cell.btnCommercial addTarget:self action:@selector(toggleResidence:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnResidential addTarget:self action:@selector(toggleResidence:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnSearchProperties addTarget:self action:@selector(didSelectSearchProperty:) forControlEvents:UIControlEventTouchUpInside];
                
                cellSearchProject = cell;
            }
            
            cell.backgroundColor = [UIColor clearColor];
            
            cell.delegate = self;
            
            cell.collectionView.delegate = self;
            cell.collectionView.dataSource = self;
            cell.collectionView.allowsSelection = YES;
            cell.collectionView.allowsMultipleSelection = NO;
            
            cell.txtSelectCity.delegate = self;
            cell.txtEntry.delegate = self;
            
            UIView *viewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(cell.txtEntry.frame))];
            viewLeft.backgroundColor = [UIColor clearColor];
            cell.txtSelectCity.leftView = viewLeft;
            cell.txtSelectCity.leftViewMode = UITextFieldViewModeAlways;
            
            UIImageView *imgView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Expand_Arrow"]];
            imgView1.frame = CGRectMake(0, 0, 20, 20);
            cell.txtSelectCity.rightViewMode = UITextFieldViewModeAlways;
            cell.txtSelectCity.rightView = imgView1;
            
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, CGRectGetHeight(cell.txtEntry.frame))];
            view.backgroundColor = [UIColor clearColor];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Search_Sugestion"]];
            imgView.frame = CGRectMake(0, 0 , 35, CGRectGetHeight(view.frame) - 10);
            imgView.center = view.center;
            [view addSubview:imgView];
            
            cell.txtEntry.leftView = view;
            cell.txtEntry.leftViewMode = UITextFieldViewModeAlways;
            
            NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"ENTER LOCALITY, PROJECT OR BUILDER" attributes:@{
                                                                                                                                    NSForegroundColorAttributeName : [UIColor blackColor]
                                                                                                                                    }];
            cell.txtEntry.attributedPlaceholder = str;
            
            if ((dictKeyword && [[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Location"]) || (dictKeyword && [[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) || (dictKeyword && [[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Builder"])) {
                
                if ([dictKeyword objectForKey:@"title"]) {
                    cell.txtEntry.text = [dictKeyword objectForKey:@"title"];
                }
                else {
                    cell.txtEntry.text = @"";
                }
                
            }else {
                cell.txtEntry.text = @"";
            }
            
            
            
            
            cellSearchProject = cell;
            return cell;
        }
        else {
            static NSString * strIdentifiers = @"FeaturedCatagories";
            FeaturedCatagories *cell = [tableView dequeueReusableCellWithIdentifier:strIdentifiers];
            
            if (!cell) {
                cell = [FeaturedCatagories createCell];
                cell.collectionView.delegate = cell;
                cell.collectionView.dataSource = cell;
                cell.delegate = self;
                cellFeaturedCatagory = cell;
            }
            
            if (!cell.arrCatagories) {
                cell.arrCatagories = arrCatagories;
                [cell.collectionView reloadData];
            }
            
            
            
            
            return cell;
        }
        
        
        
    }
    else if (indexPath.section == 1) {
        static NSString *strIdentifiers = @"FeaturedDevelopers";
        FeaturedDevelopers *cell = (FeaturedDevelopers *)[tableView dequeueReusableCellWithIdentifier:strIdentifiers];
        if (!cell) {
            cell =[FeaturedDevelopers createCell];
            
            if (arrFeaturedDevelopers.count > 1) {
                
                timerFeaturedDeveloper = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollFeaturedDeveloper) userInfo:nil repeats:YES];
                
                [[NSRunLoop mainRunLoop] addTimer:timerFeaturedDeveloper forMode:NSRunLoopCommonModes];
            }
        }
        cell.delegate = self;
        cell.arrFeaturedDevelopers = arrFeaturedDevelopers;
        cell.collectionView.delegate = cell;
        cell.collectionView.dataSource = cell;
        [cell.collectionView reloadData];
        
        
//        if (arrFeaturedDevelopers.count && !cell.arrFeaturedDevelopers) {
//            cell.arrFeaturedDevelopers = arrFeaturedDevelopers;
//            cell.collectionView.delegate = cell;
//            cell.collectionView.dataSource = cell;
//            
//        }
        
        cellFeaturedDeveloper = cell;
        return cell;
        
    }
    else if (indexPath.section == 2) {
        
        static NSString *strIdentifiers = @"FeaturedProject";
        FeaturedProject *cell = (FeaturedProject *)[tableView dequeueReusableCellWithIdentifier:strIdentifiers];
        
        if (!cell) {
            cell = [FeaturedProject createCell];
            
            if (arrFeaturedProjects.count > 1) {
                if (timerFeaturedProject) {
                    [timerFeaturedProject invalidate];
                }
                
                timerFeaturedProject = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollFeaturedProjects) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:timerFeaturedProject forMode:NSRunLoopCommonModes];
            }
            
        }
        cell.delegate = self;
        cell.arrFeaturedProjects = arrFeaturedProjects;
        
        
        [cell.collectionView reloadData];
        cellFeaturedProject = cell;
        return cell;
    }
    else {
        UITableViewCell *cell;
        return cell;
    }
   
}

#pragma mark - Collection View Delegates

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80, 90);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (collectionView == cellFeaturedCatagory.collectionView) {
        return 1;
    }
    else {
        if (isResidenceSelected) {
            return [[arrTypes firstObject] count];
        }
        else {
            return [[arrTypes lastObject] count];
        }
    }
    
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView != cellFeaturedCatagory.collectionView) {
        static NSString *strId = @"Search";
        SearchCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strId forIndexPath:indexPath];
        
        NSInteger indexCatagory = isResidenceSelected ? 0:1;
        
        cell.lblTitle.text = [[arrTypes objectAtIndex:indexCatagory] objectAtIndex:indexPath.row];
        if ([arrSelectedIndeces containsObject:indexPath]) {
            cell.imgTitle.image = [UIImage imageNamed:[[arrImageInselected objectAtIndex:indexCatagory] objectAtIndex:indexPath.item]];
        }
        else {
            
            cell.imgTitle.image = [UIImage imageNamed:[[arrImagesNormal objectAtIndex:indexCatagory] objectAtIndex:indexPath.item]];
        }
        
        return cell;
    }
    else {
        return nil;
    }
  
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView != cellFeaturedCatagory.collectionView) {
        if (![arrSelectedIndeces containsObject:indexPath]) {
            [arrSelectedIndeces addObject:indexPath];
        }
        else {
            [arrSelectedIndeces removeObject:indexPath];
        }
        
        
        [collectionView reloadData];
    }
    else {
        
    }
    
    
}



#pragma mark - Search Property delegate
- (void)didSeeAllProjects:(NSDictionary *)propInfo {
    ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectListVC"];
    obj.dictSourse = propInfo;
    obj.strTitle = [propInfo objectForKey:@"builder_name"];
    [self.navigationController pushViewController:obj animated:YES];
}


- (void)didTappedSeeFeaturedProject:(NSDictionary *)propInfo {
    ProjectDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailVC"];
    obj.dictSourse = propInfo;
    obj.option = FeaturedProjectAccessOptionProjectList;
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)didTappedFavoriteFeaturedProject:(UIButton *)sender {
    indexSelected = sender.tag;
    dictFavoriteReq =[NSMutableDictionary dictionary];
    [dictFavoriteReq setObject:[[arrFeaturedProjects objectAtIndex:indexSelected] objectForKey:@"id"] forKey:@"id"];
    [dictFavoriteReq setObject:@"project" forKey:@"type"];
    
    [self favoriteFeaturedProjects];
    

}

- (void)didTappedSpecialProjects:(NSString *)strType {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:strType forKey:@"special_category"];
    NSString *str = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
    [dict setObject:str forKey:@"city_id"];
    
    ProjectListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectListVC"];
    obj.dictSourse = dict;
    
    if ([strType isEqualToString:@"hot"]) {
        obj.strTitle = @"Hot Projects";
    }
    else if ([strType isEqualToString:@"live"]) {
        obj.strTitle = @"Live Projects";
    }
    else if ([strType isEqualToString:@"ready to move in"]) {
        obj.strTitle = @"Ready to Move In Projects";
    }
    else if ([strType isEqualToString:@"luxury"]) {
        obj.strTitle = @"Luxury Projects";
    }
    else if ([strType isEqualToString:@"affordable"]) {
        obj.strTitle = @"Affordable Projects";
    }
    obj.option = DetailFeaturedDeveloperAccessOptionSpecialProject;
    [self.navigationController pushViewController:obj animated:YES];
    
    
}

- (void)favoriteFeaturedProjects {
    
    NSString *strUserId = [pref objectForKey:strSaveUserId];
    if (strUserId != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kFeaturedProjectFavorite object:nil];
        
        [dictFavoriteReq setObject:strUserId forKey:@"user_id"];
        
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI markAsFavorite:dictFavoriteReq withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                if ([[data objectForKey:@"success"] boolValue]) {
                    
                    NSMutableDictionary *dict = [[arrFeaturedProjects objectAtIndex:indexSelected] mutableCopy];
                    
                    if ([[dict objectForKey:@"favorite"] boolValue]) {
                        [dict setObject:@NO forKey:@"favorite"];
                    }
                    else {
                        [dict setObject:@YES forKey:@"favorite"];
                    }
                    
                    NSMutableArray *arr = [arrFeaturedProjects mutableCopy];
                    [arr replaceObjectAtIndex:indexSelected withObject:dict];
                    arrFeaturedProjects = [arr copy];
                    
                    [_tblView reloadData];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }
            else {
                [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
            }
        }];
    }
    else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteFeaturedProjects) name:kFeaturedProjectFavorite object:nil];
        
        [[[AppDelegate share] window] makeToast:strLogin duration:2 * duration position:CSToastPositionCenter];
        SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
        obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        obj.modalPresentationStyle = UIModalPresentationFormSheet;
        obj.option = SigninAccessOptionFavoriteFeaturedProject;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    
}

#pragma mark - Touch delegates
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [viewMenu closeMenu];
}


#pragma mark - select City Delegates
- (void)getselectedCityInfo:(NSNotification *)info {
    dictInfo = info.object;
    NSMutableDictionary *dictInfo1 = [NSMutableDictionary dictionary];
    [dictInfo1 setObject:[dictInfo objectForKey:@"city_id"] forKey:@"city_id"];
    [dictInfo1 setObject:[dictInfo objectForKey:@"city_name"] forKey:@"city_name"];
    [pref setObject:dictInfo1 forKey:strSaveCityInfo];
    ///
    
    dictKeyword = nil;
    [arrSelectedIndeces removeAllObjects];
    [cellSearchProject.collectionView reloadData];
    [cellSearchProject.btnResidential sendActionsForControlEvents:UIControlEventTouchUpInside];
    if ([pref synchronize]) {
        [self getInfoFromApi];
    }
}

- (void)getSuggestiveKeyword:(NSNotification *)info {

    dictKeyword = [info.object copy];
    
    if ([[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Location"]) {
        [_tblView reloadData];
    }
    else if ([[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
        [_tblView reloadData];
    }
    else if ([[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Builder"]) {
        [_tblView reloadData];

    }
    else if ([[dictKeyword objectForKey:@"subtitle"] isEqualToString:@"Project"]) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[dictKeyword objectForKey:@"id"] forKey:@"project_id"];
        
        ProjectDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectDetailVC"];
        obj.dictSourse = dict;
        obj.option = FeaturedProjectAccessOptionSearchProperty;
        [self.navigationController pushViewController:obj animated:YES];
    }

}

#pragma mark - UITextFieldDelegates
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    if (textField == cellSearchProject.txtSelectCity) {
        
        SelectCityViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCityViewController"];
        obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        obj.modalPresentationStyle = UIModalPresentationFormSheet;
        obj.options = SelectOpionsCity;
        [self presentViewController:obj animated:YES completion:nil];
    }
    else if (textField == cellSearchProject.txtEntry) {
        SelectCityViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCityViewController"];
        obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        obj.options = SelectOpionsLocality;
        [self presentViewController:obj animated:YES completion:nil];
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    textField.text = @"";
    dictKeyword = nil;
    return NO;
}

#pragma mark - Auxillary mathods
- (void)manageModalScreen {
    
    dictCityInfo = [pref objectForKey:strSaveCityInfo];
    
    if (dictCityInfo == nil) {
        SelectCityViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCityViewController"];
        obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        obj.options = SelectOpionsCity;
        [self presentViewController:obj animated:YES completion:nil];
    }
    else {
        cellSearchProject.txtSelectCity.text = [dictCityInfo objectForKey:@"city_name"];
        [_tblView reloadData];
    }
}


#pragma mark - Timer Operations
- (void)scrollFeaturedDeveloper {
    
    if (self.isViewLoaded && self.view.window) {
        NSArray *visibleItems = [cellFeaturedDeveloper.collectionView indexPathsForVisibleItems];
        
        if (visibleItems.count) {
            NSIndexPath *currentIndexPath = [visibleItems firstObject];
            NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentIndexPath.item + 1 inSection:0];
           
            NSInteger num = arrFeaturedDevelopers.count;
            if (nextItem.item < num) {
                
                [cellFeaturedDeveloper.collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            }
            else {
                [cellFeaturedDeveloper.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
            }
        }
    }
    
}

- (void)scrollFeaturedProjects {

    if (self.isViewLoaded && self.view.window) {
        NSArray *visibleItems = [cellFeaturedProject.collectionView indexPathsForVisibleItems];
        
        if (visibleItems.count) {
            NSIndexPath *currentIndexPath = [visibleItems firstObject];
            NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentIndexPath.item + 1 inSection:0];
            NSInteger num = [cellFeaturedProject.collectionView numberOfItemsInSection:0];
            
            if (nextItem.item < num) {
                [cellFeaturedProject.collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            }
            else {
                [cellFeaturedProject.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            }
        }
    }
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == _tblView) {
        
        if (scrollView.contentOffset.y > 0) {
            [UIView animateWithDuration:0.25 animations:^{
                _viewStatusBar.backgroundColor = _btnMenu.superview.backgroundColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
            }];
        }
        else {
            [UIView animateWithDuration:0.25 animations:^{
               _viewStatusBar.backgroundColor = _btnMenu.superview.backgroundColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:0.5];
            }];
        }
    }
}
@end
