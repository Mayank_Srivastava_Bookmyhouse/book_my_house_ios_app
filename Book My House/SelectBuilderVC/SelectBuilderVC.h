//
//  SelectBuilderVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 6/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface SelectBuilderVC : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSArray *arrBuilderList;
@end
