//
//  SelectBuilderVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 6/1/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SelectBuilderVC.h"

@interface SelectBuilderVC () {
    
    __weak IBOutlet UIButton *btnClearAll;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UIButton *btnDone;
    __weak IBOutlet UISearchBar *searchBar1;
    __weak IBOutlet UITableView *tblView;
    NSMutableArray *arrDisplayData, *arrSelectedData, *arrData;
    NSUserDefaults *pref;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation SelectBuilderVC

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    
    arrSelectedData = [NSMutableArray array];
    pref = [NSUserDefaults standardUserDefaults];
    
    NSArray *arr = [pref objectForKey:strSaveBuilderList];
    
    if (arr != nil) {
        
        for (NSDictionary *temp in arr) {
            [arrSelectedData addObject:temp];
        }
    }
    
    
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    //[self screenSettings];
    arrDisplayData = [NSMutableArray arrayWithArray:_arrBuilderList];
    arrData = [NSMutableArray arrayWithArray:_arrBuilderList];
    [tblView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}
#pragma mark - Screen Settings
- (void)screenSettings {
    
    [[AppDelegate share] disableUserInteractionwithLoader:YES];
    [ServiceAPI getInfoByBuilderWithCallBack:^(BOOL isSuccess, id builder) {
        
        [[AppDelegate share] enableUserInteraction];
        if (isSuccess) {
            NSArray *arr = (NSArray *)[builder objectForKey:@"data"];
            NSSortDescriptor *desciptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            arr = [arr sortedArrayUsingDescriptors:[NSArray arrayWithObjects:desciptor, nil]];
            arrDisplayData = [NSMutableArray arrayWithArray:arr];
            arrData = [NSMutableArray arrayWithArray:arr];
        }
        [tblView reloadData];
    }];

}

- (void)updateScreenSettings {
    
}
#pragma mark - API Actions
#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnDone) {
        
        if (arrSelectedData.count) {
            [pref setObject:[arrSelectedData copy] forKey:strSaveBuilderList];
        }
        else {
            [pref removeObjectForKey:strSaveBuilderList];
            
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kSelectBuilder object:arrSelectedData];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender == btnClearAll) {
        [arrSelectedData removeAllObjects];
        [tblView reloadData];
    }
    else {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
#pragma mark - Auxillary MAthods
#pragma mark - Table view delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDisplayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   static NSString * const strID = @"builderCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strID];
    
    UILabel *lbl = (UILabel *)[cell viewWithTag:1];
    UIImageView *img = (UIImageView *)[cell viewWithTag:2];
    NSDictionary *dict = [arrDisplayData objectAtIndex:indexPath.row];
    
    if (![[dict objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
        lbl.text = [[arrDisplayData objectAtIndex:indexPath.row] objectForKey:@"name"];
    }
    else {
        lbl.text = @"--";
    }
    
    if ([arrSelectedData containsObject:dict]) {
        img.image = [UIImage imageNamed:@"Checkbox_green"];
    }
    else {
        img.image = [UIImage imageNamed:@"Checkbox_gray"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *img = (UIImageView *)[cell viewWithTag:2];
    img.image = [UIImage imageNamed:@"Checkbox_green"];
   
    NSMutableDictionary *dict = [arrDisplayData objectAtIndex:indexPath.row];
    if (![arrSelectedData containsObject:dict]) {
        [arrSelectedData addObject:dict];
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *img = (UIImageView *)[cell viewWithTag:2];
    img.image = [UIImage imageNamed:@"Checkbox_gray"];
    
    NSMutableDictionary *dict = [arrDisplayData objectAtIndex:indexPath.row];
    if ([arrSelectedData containsObject:dict]) {
        [arrSelectedData removeObject:dict];
    }
    
}

#pragma mark - Search bar delegates
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [arrDisplayData removeAllObjects];
    arrSelectedData = [arrData mutableCopy];
    [tblView reloadData];
    [searchBar resignFirstResponder];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length == 0)
    {
        [arrDisplayData removeAllObjects];
        arrDisplayData = [arrData mutableCopy];
    }
    else
    {
        [arrDisplayData removeAllObjects];
        for (NSDictionary *dict in arrData)
        {
            NSRange nameRange = [[dict objectForKey:@"name"] rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            if(nameRange.location != NSNotFound)
            {
                [arrDisplayData addObject:dict];
            }
        }
    }
    [tblView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

@end
