//
//  SelectCityViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/19/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "SearchViewController.h"
typedef NS_ENUM(NSUInteger, SelectOpions) {
    SelectOpionsCity,
    SelectOpionsLocality,
    SelectOptionsMisc,
};


@interface SelectCityViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (assign, nonatomic) SelectOpions options;
@property (assign, nonatomic) BOOL isFirstTime;
@end
