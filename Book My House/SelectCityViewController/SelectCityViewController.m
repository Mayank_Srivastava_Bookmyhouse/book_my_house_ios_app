
//
//  SelectCityViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/19/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SelectCityViewController.h"

static NSString * const strNonSuggestive = @"non_suggestive";
static NSString * const strSuggestive = @"suggestive";
static NSString * const strRecents = @"recents";
static NSString * const strMicroMarket = @"micro_markets";
static NSString * const strProjects = @"projects";
static NSString * const strBuilders = @"builders";
static NSString * const strLocation = @"locations";

@interface SelectCityViewController () {
    NSArray *arrByCity, *arrSuggestions;
    NSMutableArray *arrDisplayData, *arrSearchData, *arrRercentData, *arrHeaderSuggestions, *arrHeaderRecent;
    NSString *strKeyword;
    UIRefreshControl *refreshControl;
    NSUserDefaults *pref;
    NSString *strCurrentCity;
    BOOL isSuggestionAvailable, isSuggestion, isNoSuggestionFound;
    NSMutableDictionary *dictDisplayData, *dictSearchData;
    
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)actionListener:(id)sender;


@end

@implementation SelectCityViewController


#pragma mark - View Controller
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)loadView {
    [super loadView];
    pref = [NSUserDefaults standardUserDefaults];
    if (_options == SelectOpionsLocality) {
        
        
        dictDisplayData = [NSMutableDictionary dictionary];
        dictSearchData = [NSMutableDictionary dictionary];
        
        arrHeaderSuggestions = [NSMutableArray array];
        arrHeaderRecent = [NSMutableArray array];
        
    }
    else {
       
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor darkGrayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:loading];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:refreshControl];
    
    
    
    [self screenSettings];
}

- (void)refresh {
    
    if ([refreshControl isRefreshing]) {
        [refreshControl endRefreshing];
    }

    _tblView.backgroundView = nil;
    
    if (_options == SelectOpionsCity) {
        [self loadCities];
        [_searchBar becomeFirstResponder];
    }
    else if (_options == SelectOpionsLocality) {
        
        if (dictSearchData.count) {
            
            [_tblView reloadData];
            
        }
        else {
            _searchBar.text = @"";
            [self loadLandmark];
        }
        
    }
    
}


#pragma mark - Screen Settings
- (void)screenSettings {

    if (_options == SelectOpionsCity) {
         _searchBar.placeholder = @"Select City";

    }
    else if (_options == SelectOpionsLocality) {
        _searchBar.placeholder = @"Enter keyword";
        
    }
    [self internetInteractivity];
}

- (void)internetInteractivity {
    
    if (_options == SelectOpionsCity) {
        [self loadCities];
    }
    else if (_options == SelectOpionsLocality) {
        
        arrDisplayData = [NSMutableArray array];
        [self loadLandmark];
    }
}

#pragma mark - API Actions
- (void)loadCities {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        [ServiceAPI getInfoByCityWithCallBack:^(BOOL isSuccess, id cities) {
            
            if (isSuccess) {
                _tblView.backgroundView = nil;
                arrByCity = [NSArray arrayWithArray:[cities objectForKey:@"city_list"]];
                
                NSDictionary *dict = [pref objectForKey:strSaveCityInfo];
                
                for (NSDictionary *temp in arrByCity) {
                    
                    if ([[temp objectForKey:@"city_id"] isEqualToString:[dict objectForKey:@"city_id"]]) {
                        NSMutableArray *arr = [arrByCity mutableCopy];
                        [arr removeObject:temp];
                        [arr insertObject:dict atIndex:0];
                        arrByCity = [arr copy];
                        break;
                    }
                }
                
                arrDisplayData = [NSMutableArray array];
                arrSearchData = [NSMutableArray array];
                for (NSDictionary *temp in arrByCity) {
                    
                    [arrSearchData addObject:[temp objectForKey:@"city_name"]];
                    [arrDisplayData addObject:[temp objectForKey:@"city_name"]];
                }
                
                [_searchBar becomeFirstResponder];
                
            }
            else {
                UILabel *lbl = [[UILabel alloc] initWithFrame:self.view.frame];
                lbl.text = @"Please enable Wifi and pull to refresh";
                lbl.textAlignment = NSTextAlignmentCenter;
                _tblView.backgroundView = lbl;
            }
            
            [_tblView reloadData];
        }];

        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadCities) fromClass:self];
    }
}

- (void)loadLandmark {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableArray *arr = [pref objectForKey:strSaveSuggestion];
        if (arr) {
            isSuggestionAvailable = YES;
            arrRercentData = [NSMutableArray array];
            NSMutableArray *arr1 = [NSMutableArray array];
            for (NSDictionary *dict in arr) {
                
                NSString *strCityId = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
                if ([[dict objectForKey:@"city_id"] isEqualToString:strCityId]) {
                    [arr1 addObject:[dict copy]];
                    
                }
            }
            for (NSInteger i = arr1.count - 1; i >= 0; i--) {
                [arrRercentData addObject:[arr1 objectAtIndex:i]];
                
            }
            
            [arrHeaderRecent addObject:@"Recent Searches"];
            NSDictionary *dict1 = [NSDictionary dictionaryWithObject:arrRercentData forKey:[arrHeaderRecent firstObject]];
            [dictSearchData setObject:dict1 forKey:strNonSuggestive];
            
            dictDisplayData = [dictSearchData mutableCopy];
            
            [_tblView reloadData];
        }
        
        
        NSDictionary *dict = [pref objectForKey:strSaveCityInfo];
        
        if (dict != nil) {
            NSMutableDictionary *dictReq = [NSMutableDictionary dictionary];
            [dictReq setObject:[dict objectForKey:@"city_id"] forKey:@"city_id"];
            [dictReq setObject:@"" forKey:@"searchtxt"];
            
            if (!isSuggestionAvailable) {
                [[AppDelegate share] disableUserInteractionwithLoader:YES];
            }
            
            [ServiceAPI getSuggestiveSearchResponse:dictReq andCallback:^(BOOL success, id data) {
                
                if (!isSuggestionAvailable) {
                    [[AppDelegate share] enableUserInteraction];
                }
                
                if (success) {
                    if ([data count]) {
                        
                        NSMutableArray *arrSublocation = [NSMutableArray array];
                        NSMutableArray *arrLocation = [NSMutableArray array];
                        NSMutableArray *arrBuilder = [NSMutableArray array];
                        NSMutableArray *arrProject = [NSMutableArray array];
                        
                        for (NSDictionary *temp in data) {
                            NSMutableDictionary *dictTemp = [temp mutableCopy];
                            NSString *strCityId = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
                            [dictTemp setObject:strCityId forKey:@"city_id"];
                            
                            if ([[temp objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
                                [arrSublocation addObject:temp];
                            }
                            else if ([[temp objectForKey:@"subtitle"] isEqualToString:@"Builder"]) {
                                [arrBuilder addObject:temp];
                            }
                            else if ([[temp objectForKey:@"subtitle"] isEqualToString:@"Location"]) {
                                [arrLocation addObject:temp];
                            }
                            else if ([[temp objectForKey:@"subtitle"] isEqualToString:@"Project"]) {
                                [arrProject addObject:temp];
                            }
                        }
                        
                        if (arrLocation.count) {
                            [arrHeaderRecent addObject:@"Localities"];
                            
                            if ([dictSearchData objectForKey:strNonSuggestive]) {
                                NSMutableDictionary *dict1 = [[dictSearchData objectForKey:strNonSuggestive] mutableCopy];
                                [dict1 setObject:arrLocation forKey:[arrHeaderRecent lastObject]];
                                [dictSearchData setObject:dict1 forKey:strNonSuggestive];
                            }
                            else {
                                NSDictionary *dict2 = [NSDictionary dictionaryWithObject:arrLocation forKey:[arrHeaderRecent lastObject]];
                                [dictSearchData setObject:dict2 forKey:strNonSuggestive];
                            }
                            
                        }
                        
                        
                        if (arrProject.count) {
                            [arrHeaderSuggestions addObject:@"Projects"];
                            
                            if ([dictSearchData objectForKey:strSuggestive]) {
                                NSMutableDictionary *dict1 = [[dictSearchData objectForKey:strSuggestive] mutableCopy];
                                [dict1 setObject:arrProject forKey:[arrHeaderSuggestions lastObject]];
                                [dictSearchData setObject:dict1 forKey:strSuggestive];
                            }
                            else {
                                NSDictionary *dict2 = [NSDictionary dictionaryWithObject:arrProject forKey:[arrHeaderSuggestions lastObject]];
                                [dictSearchData setObject:dict2 forKey:strSuggestive];
                            }
                        }
                        
                        if (arrBuilder.count) {
                            [arrHeaderSuggestions addObject:@"Builders"];
                            
                            if ([dictSearchData objectForKey:strSuggestive]) {
                                NSMutableDictionary *dict1 = [[dictSearchData objectForKey:strSuggestive] mutableCopy];
                                [dict1 setObject:arrBuilder forKey:[arrHeaderSuggestions lastObject]];
                                [dictSearchData setObject:dict1 forKey:strSuggestive];
                            }
                            else {
                                NSDictionary *dict2 = [NSDictionary dictionaryWithObject:arrBuilder forKey:[arrHeaderSuggestions lastObject]];
                                [dictSearchData setObject:dict2 forKey:strSuggestive];
                            }
                        }
                        
                        
                        NSArray *arr = [arrLocation arrayByAddingObjectsFromArray:arrSublocation];
                        if (arr.count) {
                            [arrHeaderSuggestions addObject:@"Locations"];
                            if ([dictSearchData objectForKey:strSuggestive]) {
                                NSMutableDictionary *dict1 = [[dictSearchData objectForKey:strSuggestive] mutableCopy];
                                [dict1 setObject:arr forKey:[arrHeaderSuggestions lastObject]];
                                [dictSearchData setObject:dict1 forKey:strSuggestive];
                            }
                            else {
                                NSDictionary *dict2 = [NSDictionary dictionaryWithObject:arr forKey:[arrHeaderSuggestions lastObject]];
                                [dictSearchData setObject:dict2 forKey:strSuggestive];
                            }
                        }
                        dictDisplayData = [dictSearchData mutableCopy];
                        [_tblView reloadData];
                        
                        [_searchBar becomeFirstResponder];
                    }
                    else {
                        [self.view makeToast:@"No suggestions available" duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    
                }
            }];
        }
        else {
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadLandmark) fromClass:self];
    }
}


#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == _btnBack) {
        
        NSString *strCity = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
        if (strCity != nil) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else {
            [self.view makeToast:@"Please select your city" duration:duration position:CSToastPositionCenter];
        }
    }
}
#pragma mark - Auxillary mathods




#pragma mark - Tableview Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (_options == SelectOpionsCity) {
        return 1;
    }
    else {
        
        if (isSuggestion) {
            NSInteger num = 0;
            for (int i = 0; i < arrHeaderSuggestions.count; i++) {
                num += [[[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:i]] count];
            }
            if (num > 0) {
                isNoSuggestionFound = NO;
                return [[[dictDisplayData objectForKey:strSuggestive] allKeys] count];
            }
            else {
                isNoSuggestionFound = YES;
                return 1;
            }
            
        }
        else
        return [[[dictDisplayData objectForKey:strNonSuggestive] allKeys] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_options == SelectOpionsCity) {
        return arrDisplayData.count;

    }
    else {
        if (isSuggestion) {
            
            if (isNoSuggestionFound) {
                return 1;
            }
            else {
                return [[[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:section]] count];
            }
        }
        else {
            
            return [[[dictDisplayData objectForKey:strNonSuggestive] objectForKey:[arrHeaderRecent objectAtIndex:section]] count];
        }
        return [[arrDisplayData objectAtIndex:section] count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (_options == SelectOpionsCity) {
        return 0;
    }
    else {
        
        if (isSuggestion) {
            
            if ([[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:section]]) {
                
                if ([[[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:section]] count]) {
                    return 35.0;
                }
                else
                    return 0.0;
            }
            else
                return 0.0;
            
        }
        else {
            
            if ([[[dictDisplayData objectForKey:strNonSuggestive] objectForKey:[arrHeaderRecent objectAtIndex:section]] count]) {
                return 35.0;
            }
            else
                return 0.0;
        }
        
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (_options == SelectOpionsCity) {
        return nil;
    }
    else {
        return [self createHeaderView:tableView withSection:section];
    }
    
}

- (UIView *)createHeaderView:(UITableView *)tableView withSection:(NSInteger)section{
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 33)];
    aView.backgroundColor = [UIColor whiteColor];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, CGRectGetWidth(aView.frame), 33.0)];
    lbl.font = [UIFont fontWithName:@"PT Sans" size:20.0];
    
    
    if (isSuggestion) {

        if (arrHeaderSuggestions.count > section) {
            lbl.text = [arrHeaderSuggestions objectAtIndex:section];
        }
        
    }
    else {
        if (arrHeaderRecent.count > section) {
            lbl.text = [arrHeaderRecent objectAtIndex:section];
        }
    }
    
    
    [aView addSubview:lbl];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(aView.frame), CGRectGetMaxY(lbl.frame), CGRectGetWidth(lbl.frame), 2.0)];
    line.backgroundColor = [UIColor colorWithRed:113.0/255.0 green:70.0/255.0 blue:23.0/255.0 alpha:1.0];
    [aView addSubview:line];
    
    
    return aView;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *strId = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strId];
    if (_options == SelectOpionsCity) {
        
        
        if (indexPath.row == 0) {
            cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:20];
        }
        else {
            cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:16];
        }
        cell.textLabel.text = [arrDisplayData objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = @"";
    }
    else {
        NSDictionary *dict;
        
        
        if (isSuggestion) {
            
            NSString *strOnSearchBar = [_searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if (strOnSearchBar.length) {
                
                if (!isNoSuggestionFound) {
                    
                    dict = [[[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
                    UIFont *font = cell.textLabel.font;
                    NSAttributedString * attrStr1 = [[NSAttributedString alloc] initWithData:[[[dict objectForKey:@"title"] uppercaseString] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                     options:@{
                                                                                               NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                               }
                                                                          documentAttributes:nil
                                                                                       error:nil];
                    
                    
                    cell.textLabel.attributedText = [self setSuggested:strOnSearchBar forString:attrStr1.string withFont:font];
                    cell.textLabel.font = font;
                    
                    UIFont *aFont = cell.detailTextLabel.font;
                    if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Project"]) {//
                        
                        NSRange range1 = [[[dict objectForKey:@"buildername"] uppercaseString] rangeOfString:[strOnSearchBar uppercaseString]];
                        NSRange range2 = [[[dict objectForKey:@"buildername"] uppercaseString] rangeOfString:[NSString stringWithFormat:@" %@", [strOnSearchBar uppercaseString]]];
                        if (range1.location == 0 || range2.location != NSNotFound) {
                            cell.detailTextLabel.attributedText = [self setSuggested:strOnSearchBar forString:[[dict objectForKey:@"buildername"] uppercaseString] withFont:aFont];
                        }
                        else {
                            cell.detailTextLabel.text = [[dict objectForKey:@"buildername"] uppercaseString];
                        }
                        cell.detailTextLabel.hidden = NO;
                    }
                    else if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Location"]) {
                        
                        NSRange range1 = [[[[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"] uppercaseString] rangeOfString:[strOnSearchBar uppercaseString]];
                        NSRange range2 = [[[[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"] uppercaseString] rangeOfString:[NSString stringWithFormat:@" %@", [strOnSearchBar uppercaseString]]];
                        
                        if (range1.location == 0 || range2.location != NSNotFound) {
                            cell.detailTextLabel.attributedText = [self setSuggested:strOnSearchBar
                                                                           forString:[[[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"] uppercaseString]
                                                                            withFont:aFont];
                        }
                        else {
                            cell.detailTextLabel.text = [[[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"] uppercaseString];
                        }
                        
                        
                        cell.detailTextLabel.hidden = NO;
                    }
                    else if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
                        
                        NSRange range1 = [[[dict objectForKey:@"location_name"] uppercaseString] rangeOfString:[strOnSearchBar uppercaseString]];
                        NSRange range2 = [[[dict objectForKey:@"location_name"] uppercaseString] rangeOfString:[NSString stringWithFormat:@" %@", [strOnSearchBar uppercaseString]]];
                        
                        if (range1.location == 0 || range2.location != NSNotFound) {
                            cell.detailTextLabel.attributedText = [self setSuggested:strOnSearchBar
                                                                           forString:[[dict objectForKey:@"location_name"] uppercaseString]
                                                                            withFont:aFont];
                        }
                        else {
                            cell.detailTextLabel.text = [[dict objectForKey:@"location_name"] uppercaseString];
                        }
                        
                        cell.detailTextLabel.hidden = NO;
                    }
                    else {
                        cell.detailTextLabel.hidden = YES;
                    }

                    
                }
                else {
                    
                    cell.textLabel.text = @"No search found as per your search criteria";
                    cell.detailTextLabel.text = @"";
                }
            }
            else {
                dict = [[[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
                
                UIFont *font = cell.textLabel.font;
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[[dict objectForKey:@"title"] uppercaseString] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                                options:@{
                                                                                          NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                          }
                                                                     documentAttributes:nil
                                                                                  error:nil];
                
                
                cell.textLabel.attributedText = attrStr;
                
                
                cell.textLabel.font = font;
                
                if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Project"]) {//
                    cell.detailTextLabel.text = [[dict objectForKey:@"buildername"] uppercaseString];
                    cell.detailTextLabel.hidden = NO;
                }
                else if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Location"]) {
                    cell.detailTextLabel.text = [[[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"] uppercaseString];
                    cell.detailTextLabel.hidden = NO;
                }
                else if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
                    cell.detailTextLabel.text = [[dict objectForKey:@"location_name"] uppercaseString];
                    cell.detailTextLabel.hidden = NO;
                }
                else {
                    cell.detailTextLabel.hidden = YES;
                }

            }
        }
        else {
            NSArray *arr = [[dictDisplayData objectForKey:strNonSuggestive] objectForKey:[arrHeaderRecent objectAtIndex:indexPath.section]];
            dict = [arr objectAtIndex:indexPath.row];
            
            UIFont *font = cell.textLabel.font;
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"title"] dataUsingEncoding:NSUnicodeStringEncoding]
                                                                            options:@{
                                                                                      NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                      }
                                                                 documentAttributes:nil
                                                                              error:nil];
            
            
            cell.textLabel.attributedText = attrStr;
            cell.textLabel.font = font;
            
            if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Sublocation"]) {
                cell.detailTextLabel.text = [dict objectForKey:@"location_name"];
            }
            else if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Location"]) {
                cell.detailTextLabel.text = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_name"];
            }
            else if ([[dict objectForKey:@"subtitle"] isEqualToString:@"Project"]) {
                cell.detailTextLabel.text = [dict objectForKey:@"buildername"];
            }
            else
            cell.detailTextLabel.text = [dict objectForKey:@"subtitle"];
            
            
        }
        
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_options == SelectOpionsCity) {
        _searchBar.text = [arrDisplayData objectAtIndex:indexPath.row];
        
        for (NSDictionary *dict in arrByCity) {
            if ([[dict objectForKey:@"city_name"] isEqualToString:[arrDisplayData objectAtIndex:indexPath.row]]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kSelectCity object:dict];
                break;
            }
        }
        
        

    }
    else {
        
        if (!isNoSuggestionFound) {
            
            NSDictionary *dict;
            if (isSuggestion) {
                
                dict = [[[dictDisplayData objectForKey:strSuggestive] objectForKey:[arrHeaderSuggestions objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
                
            }
            else {
                dict = [[[dictDisplayData objectForKey:strNonSuggestive] objectForKey:[arrHeaderRecent objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            }
            
            
            
            
            
            if (dict != nil) {
                NSString *strCity = [[pref objectForKey:strSaveCityInfo] objectForKey:@"city_id"];
                NSMutableDictionary *temp = [dict mutableCopy];
                [temp setObject:strCity forKey:@"city_id"];
                
                NSMutableArray *arr = [[pref objectForKey:strSaveSuggestion] mutableCopy];
                
                if (arr == nil) {
                    arr = [NSMutableArray array];
                }
                
                if (![[temp objectForKey:@"subtitle"] isEqualToString:@"Location"]) {
                    
                    if ([arr containsObject:temp]) {
                        [arr removeObject:temp];
                    }
                    
                    
                    [arr addObject:temp];
                    [pref setObject:[arr copy] forKey:strSaveSuggestion];
                    [pref synchronize];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kSuggestion object:dict];
            }
            
        }
        else {
            
        }
        
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Search bar delegates
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"%s", __FUNCTION__);
    if (_options == SelectOpionsCity) {
        
        [arrDisplayData removeAllObjects];
        [arrDisplayData addObjectsFromArray:arrSearchData];
        [_tblView reloadData];
        
        [searchBar resignFirstResponder];
    }
    else {
        
    }
    
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (_options == SelectOpionsCity) {
        if(searchText.length == 0)
        {
            [arrDisplayData removeAllObjects];
            [arrDisplayData addObjectsFromArray:arrSearchData];
        }
        else
        {
            [arrDisplayData removeAllObjects];
            
            for (NSString* string in arrSearchData)
            {
                NSRange nameRange = [string rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if(nameRange.location != NSNotFound)
                {
                    [arrDisplayData addObject:string];
                }
            }
        }
    }
    else {
        NSString *str = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        //searchBar.text = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (str.length < 2) {
            
            isSuggestion = NO;
            dictDisplayData = [dictSearchData mutableCopy];
        }
        else {
            isSuggestion = YES;
            
            NSString *str1 = [NSString stringWithFormat:@" %@", searchBar.text];
            NSString *str2 = [NSString stringWithFormat:@"-%@", searchBar.text];
            NSString *str3 = [NSString stringWithFormat:@" %@", searchBar.text];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.title BEGINSWITH [cd] %@ || self.buildername BEGINSWITH [cd] %@  || self.title CONTAINS ' ' && self.title CONTAINS [cd] %@ || self.title CONTAINS '-' && self.title CONTAINS [cd] %@ || self.subtitle == 'Sublocation' && self.location_name BEGINSWITH [cd] %@ || self.subtitle == 'Sublocation' && self.location_name BEGINSWITH ' ' && self.location_name CONTAINS [cd] %@", searchBar.text, searchBar.text, str1, str2, searchBar.text, str3];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            NSArray *arr;
            for (NSString *str  in arrHeaderSuggestions) {
                arr = [[[dictSearchData objectForKey:strSuggestive] objectForKey:str] filteredArrayUsingPredicate:predicate];
                [dict setObject:arr forKey:str];
            }
            
            [dictDisplayData setObject:dict forKey:strSuggestive];
        }
    }
    [_tblView reloadData];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_searchBar resignFirstResponder];
}


#pragma mark - Auxillary Mathods

- (NSMutableAttributedString *)setSuggested:(NSString *)strOnSearchBar forString:(NSString *)strInnitial withFont:(UIFont *)font {
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:strInnitial];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"(%@)", [strOnSearchBar uppercaseString]]
                                                                           options:kNilOptions
                                                                             error:nil];
    
    NSRange range = NSMakeRange(0, strInnitial.length);
    [regex enumerateMatchesInString:strInnitial options:kNilOptions range:range usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        
        NSRange subStringRange = [result rangeAtIndex:1];
        [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:254.0/255.0 green:229.0/255.0 blue:129.0/255.0 alpha:1.0] range:subStringRange];
        [attrStr addAttribute:NSFontAttributeName value:font range:subStringRange];
    }];
    
   // cell.textLabel.attributedText = attrStr;
    
    return attrStr;

}
@end
