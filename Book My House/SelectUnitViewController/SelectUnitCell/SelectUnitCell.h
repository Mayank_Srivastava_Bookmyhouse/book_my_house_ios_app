//
//  SelectUnitCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/21/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectUnitCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UIButton *btnOffers;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UIButton *btnFav;

@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitArea;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@property (weak, nonatomic) IBOutlet UILabel *lblPLC;
@property (weak, nonatomic) IBOutlet UILabel *lblPSF;
@property (weak, nonatomic) IBOutlet UILabel *lblLifestyle;
@end
