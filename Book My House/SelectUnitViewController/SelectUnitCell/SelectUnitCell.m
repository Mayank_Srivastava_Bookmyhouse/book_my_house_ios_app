//
//  SelectUnitCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/21/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SelectUnitCell.h"

@implementation SelectUnitCell


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.lblLifestyle.layer.cornerRadius = 5;
        
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
