//
//  SelectUnitVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/21/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"
#import "SelectUnitCell.h"
#import "Header.h"
#import "UnitSelectTabularVC.h"
#import "UnitDescriptionVC.h"

@interface SelectUnitVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (strong, nonatomic) NSDictionary *dictSourse;
@property (strong, nonatomic) NSString *strSelectedUnitIDs;
@property (strong, nonatomic) NSString *strImagePlan;
@property (strong, nonatomic) NSString *strSelectedTitle;
@property (strong, nonatomic) NSDictionary *dictCord;
- (void)loadSelectedUnitInfo;
- (void)receiveFilterParamenters:(NSNotification *)info;
@end
