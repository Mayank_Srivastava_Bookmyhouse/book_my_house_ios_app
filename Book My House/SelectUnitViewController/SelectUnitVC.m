//
//  SelectUnitVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/21/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SelectUnitVC.h"
#import "SelectUnitFilter.h"
@import GoogleMaps;


static NSString * const strIdentifier                      = @"cell";
static NSString * const strLoginRequest                    = @"Please login in to select your favorite unit";
@interface SelectUnitVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblselectUnit;
    __weak IBOutlet UIButton *btnFilter;
    __weak IBOutlet UIButton *btnTabular;
    __weak IBOutlet GMSMapView *mapView;
    __weak IBOutlet UIButton *btnZoomIn;
    __weak IBOutlet UIButton *btnZoomOut;
    __weak IBOutlet UICollectionView *colView;
    
    
    NSArray *arrResponse;
    GMSMutablePath *path;
    GMSPolygon *polygon;
    GMSCameraUpdate *updateCamera;
    BOOL isMapPlanLoaded;
    SectorDetailView *detailView;
    NSUserDefaults *pref;
    NSInteger indexFavorite;
    NSDictionary *dictFilterParam;
    NoUnitView *viewNoUnit;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation SelectUnitVC

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    
    pref = [NSUserDefaults standardUserDefaults];
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self screenSettings];
    [self loadSelectedUnitInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveFilterParamenters:) name:kUnitFilter object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUnitFilter object:nil];
    [pref removeObjectForKey:strSaveUnitFilter];
}



#pragma mark - Screen Settings
- (void)screenSettings {

    lblselectUnit.text = [NSString stringWithFormat:@"Select Unit (%@)", _strSelectedTitle];
    [colView registerClass:[SelectUnitCell class] forCellWithReuseIdentifier:strIdentifier];
    [colView registerNib:[UINib nibWithNibName:@"SelectUnitCell" bundle:nil] forCellWithReuseIdentifier:strIdentifier];
    
    NSString *strNorthEast = [_dictCord objectForKey:@"nw"];
    NSString *strSouthWest = [_dictCord objectForKey:@"se"];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:([[[strNorthEast componentsSeparatedByString:@","] firstObject] floatValue] + [[[strSouthWest componentsSeparatedByString:@","] firstObject] floatValue]) / 2
                                                            longitude:([[[strNorthEast componentsSeparatedByString:@","] lastObject] floatValue] + [[[strSouthWest componentsSeparatedByString:@","] lastObject] floatValue]) / 2
                                                                 zoom:17];
    mapView.camera = camera;
    
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake([[[strNorthEast componentsSeparatedByString:@","] firstObject] floatValue], [[[strNorthEast componentsSeparatedByString:@","] lastObject] floatValue]);
    
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake([[[strSouthWest componentsSeparatedByString:@","] firstObject] floatValue], [[[strSouthWest componentsSeparatedByString:@","] lastObject] floatValue]);
    
    GMSCoordinateBounds *overlayBounds = [[GMSCoordinateBounds alloc] initWithCoordinate:southWest
                                                                              coordinate:northEast];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%@&img=%@", baseImageUrl, extension, @"600", _strImagePlan];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            GMSGroundOverlay *overlay = [GMSGroundOverlay groundOverlayWithBounds:overlayBounds icon:image];
            overlay.bearing = 0;
            overlay.map = mapView;
            isMapPlanLoaded = YES;
            [colView reloadData];
        });
    });
}



#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnFilter) {
        
    }
    else if (sender == btnTabular) {
        
    }
    else if (sender == btnZoomIn || sender == btnZoomOut) {
        
    }
}

- (void)didTappedFavorite:(UIButton *)sender {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            indexFavorite = sender.tag;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
            [dict setObject:[[arrResponse objectAtIndex:indexFavorite] objectForKey:@"unit_id"] forKey:@"unit_id"];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kFavoriteUnitFromUnitSelect object:nil];
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI markAsFavorite:dict withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    [self loadSelectedUnitInfo];
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    
                }
            }];
            
        }
        else {
            [[[AppDelegate share] window] makeToast:strLoginRequest duration:duration position:CSToastPositionCenter];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTappedFavorite:) name:kFavoriteUnitFromUnitSelect object:nil];
            
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionFavoriteSelectUnit;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
            
        }

    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didTappedFavorite:) fromClass:self];
    }
}

- (void)didTapedOffers:(UIButton *)btn {
    NSLog(@"%s", __FUNCTION__);
    
    if (detailView) {
        [detailView removeFromSuperview];
        detailView = nil;
    }
    detailView = [SectorDetailView createView];
    detailView.frame = self.view.frame;
    
    [self.view addSubview:detailView];
}

- (void)closeButton {
    
    if (!viewNoUnit.btnClose.enabled) {
        viewNoUnit.btnClose.enabled = YES;
    }
    [pref removeObjectForKey:strSaveUnitFilter];
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         viewNoUnit.frame = CGRectMake(CGRectGetMaxX(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }
                     completion:^(BOOL finished) {
                         [viewNoUnit removeFromSuperview];
                         viewNoUnit = nil;
                         
                         
                     }];
    
}


#pragma mark - API Actions
- (void)loadSelectedUnitInfo {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[_dictSourse objectForKey:@"project_id"] forKey:@"project_id"];
        [dict setObject:_strSelectedUnitIDs forKey:@"unit_ids"];
        
        NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
        
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
        }
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getSelectUnitInfo:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                
                arrResponse = [data copy];
                
                if (!arrResponse.count) {

                    if (!viewNoUnit) {
                        
                        viewNoUnit = [NoUnitView createView];
                        viewNoUnit.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                        [viewNoUnit.btnClose addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
                        [self.view addSubview:viewNoUnit];
                        
                        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButton)];
                        [viewNoUnit addGestureRecognizer:tap];
                        
                        [UIView animateWithDuration:0.25
                                              delay:0.0
                                            options:UIViewAnimationOptionCurveEaseIn
                                         animations:^{
                                             viewNoUnit.frame = self.view.frame;
                                         } completion:^(BOOL finished) {
                                             
                                             
                                             
                                         }];
                    }
                    
                }
                else {
                    if (arrResponse.count <= 1) {
                        btnFilter.hidden = YES;
                    }
                    else {
                        btnFilter.hidden = NO;
                    }
                }
                [colView reloadData];
            }
            else {
                
            }
        }];
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadSelectedUnitInfo) fromClass:self];
    }
    
    
}


#pragma mark - Auxillary Mathods




#pragma mark - Collection view Delegates

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return (CGRectGetWidth(self.view.frame) - 300);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, (CGRectGetWidth(self.view.frame) - 300)/2, 0, (CGRectGetWidth(self.view.frame) - 300)/2);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(300, 150);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrResponse.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0) {
        
        NSDictionary *dict = [arrResponse objectAtIndex:indexPath.row];
        SelectUnitCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        cell.lblLifestyle.layer.cornerRadius = 5.0;
        cell.lblLifestyle.clipsToBounds = YES;
        cell.clipsToBounds = YES;
        
        
        NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"image_unit"]];
        [cell.imgBanner setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        if ([dict objectForKey:@"wpcf_flat_price"] != nil) {
            cell.lblPrice.text = [self setPriceRange:[dict objectForKey:@"wpcf_flat_price"]];
        }
        else {
            cell.lblPrice.text = [NSString stringWithFormat:@"1000"];
        }
        
        if ([dict objectForKey:@"wpcf_flat_size"] != nil) {
            cell.lblUnitArea.text = [NSString stringWithFormat:@"%@ sq ft", [dict objectForKey:@"wpcf_flat_size"]];
        }
        else {
            cell.lblUnitArea.text = [NSString stringWithFormat:@"-- sq ft"];
        }
        
        if ([dict objectForKey:@"wpcf_flat_unit_no"] != nil) {
            cell.lblUnitNumber.text = [NSString stringWithFormat:@"Unit No. %@", [dict objectForKey:@"wpcf_flat_unit_no"]];
        }
        else {
            cell.lblUnitNumber.text = [NSString stringWithFormat:@"Unit No. --"];
        }
        
        if ([dict objectForKey:@"wpcf_flat_floor"] != nil) {
            cell.lblFloor.text = [NSString stringWithFormat:@"Floor: %@", [dict objectForKey:@"wpcf_flat_floor"]];
        }
        else {
            cell.lblFloor.text = [NSString stringWithFormat:@"Floor: --"];
        }
        
        if ([dict objectForKey:@"wpcf_flat_price_plc"] != nil) {
            cell.lblPLC.text = [NSString stringWithFormat:@"PLC: \u20B9%@", [dict objectForKey:@"wpcf_flat_price_plc"]];
        }
        else {
            cell.lblPLC.text = [NSString stringWithFormat:@"PLC: --"];
        }

        if ([dict objectForKey:@"wpcf_flat_price_SqFt"] != nil) {
            cell.lblPSF.text = [NSString stringWithFormat:@"%@ psf", [dict objectForKey:@"wpcf_flat_price_SqFt"]];
        }
        else {
            cell.lblPSF.text = @"-- psf";
        }
        
        if ([dict objectForKey:@"lifestyle"] != nil) {
//            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Lifestyle " attributes:@{
//             NSForegroundColorAttributeName:[UIColor whiteColor],
//             NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:15.0],
//                                                                                                                          }
//                                              ];
//            
//            NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [dict objectForKey:@"lifestyle"]] attributes:@{
//                                                                                                                                                                   NSForegroundColorAttributeName:[UIColor whiteColor],
//                                                                                                                                                                   NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:25.0],
//                                                                                                                                                                   }];
//            [str appendAttributedString:str2];
//            
//            cell.lblLifestyle.attributedText = str;
            
            cell.lblLifestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@", [dict objectForKey:@"lifestyle"]];
        }
        else {
           cell.lblLifestyle.text = @"Lifestyle";
        }
        
        cell.btnFav.tag = indexPath.row;
        [cell.btnFav addTarget:self action:@selector(didTappedFavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnFav.selected = [[dict objectForKey:@"user_favourite"] boolValue];
    
        if (![[dict objectForKey:@"sold_status"] isEqualToString:@"not sold"]) {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame))];
            imgView.image = [UIImage imageNamed:@"Reserved_Image"];
            [cell addSubview:imgView];
            imgView.tag = 1001;
            imgView.backgroundColor = [UIColor colorWithRed:125.0/255.0 green:125.0/255.0 blue:125.0/255.0 alpha:0.6];
            
            [cell.contentView bringSubviewToFront:imgView];
        }
        else {
            UIImageView *imgView = [cell viewWithTag:1001];
            [imgView removeFromSuperview];
        }
        
        [cell.btnOffers addTarget:self action:@selector(didTapedOffers:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return nil;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [arrResponse objectAtIndex:indexPath.row];
    
    if ([[dict objectForKey:@"sold_status"] isEqualToString:@"not sold"]) {
        UnitDescriptionVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"UnitDescriptionVC"];
        obj.dictSourse = dict;
        obj.strPropertyCatagory = [NSString stringWithFormat:@"%@ (%@ sq ft)", _strSelectedTitle, [dict objectForKey:@"wpcf_flat_size"]];
        [self.navigationController pushViewController:obj animated:YES];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isMapPlanLoaded) {
        NSArray *arrLocation = [[arrResponse objectAtIndex:indexPath.row] objectForKey:@"unit_cords"];
        
        if (arrLocation.count >= 4) {
            
            if (![path isKindOfClass:[NSNull class]]) {
                [path removeAllCoordinates];
                polygon.map = nil;
                path = nil;
            }
            
            path = [GMSMutablePath path];
            
            for (NSDictionary *dict in arrLocation) {
                [path addLatitude:[[dict objectForKey:@"langitude"] floatValue] longitude:[[dict objectForKey:@"longitude"] floatValue]];
            }
            
            polygon = [GMSPolygon polygonWithPath:path];
            polygon.strokeColor = [UIColor colorWithRed:168.0/255.0 green:0 blue:0 alpha:1.0];
            polygon.strokeWidth = 2.0;
            polygon.fillColor = [UIColor colorWithRed:168.0/255.0 green:0 blue:0 alpha:0.5];
            polygon.map = mapView;
            
            CLLocationCoordinate2D cordinate = CLLocationCoordinate2DMake([[arrLocation valueForKeyPath:@"@avg.langitude"] floatValue], [[arrLocation valueForKeyPath:@"@avg.longitude"] floatValue]);

            updateCamera = [GMSCameraUpdate setTarget:cordinate zoom:20];
            [mapView animateWithCameraUpdate:updateCamera];
            
        }
    }
    else {
        [self.view makeToast:@"Fetching Plan Image..." duration:duration position:CSToastPositionCenter];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"table"]) {
        UnitSelectTabular_VC *obj  = segue.destinationViewController;
        obj.arrSourse = arrResponse;
        obj.owner = self;
        obj.strSelectedTitle = _strSelectedTitle;
        
    }
    else if ([segue.identifier isEqualToString:@"filter"]) {
        SelectUnitFilter *obj = segue.destinationViewController;
        obj.strTotalFloor = [[arrResponse firstObject] objectForKey:@"total_floor"];
    }
}

#pragma mark - Auxillary Mathods
- (NSString *)setPriceRange:(NSString *)str {
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"\u20B9 %0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"\u20B9 %0.2f Lacs+", result];
    }
    
    return strNum;
}

- (void)receiveFilterParamenters:(NSNotification *)info {
    
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        dictFilterParam = (NSDictionary *)info.object;
        
        if (!dictFilterParam) {
            dictFilterParam = [pref objectForKey:strSaveUnitFilter];
        }
        
        
        if ([[dictFilterParam objectForKey:@"filter_price"] isEqualToString:@"Price: High to low"] && !([dictFilterParam objectForKey:@"minfloor"] && [dictFilterParam objectForKey:@"maxfloor"])) {
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"wpcf_flat_price" ascending:NO];
            arrResponse = [arrResponse sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [colView reloadData];
            [colView setContentOffset:CGPointZero animated:YES];
            [pref setObject:[dictFilterParam copy] forKey:strSaveUnitFilter];
        }
        else if ([[dictFilterParam objectForKey:@"filter_price"] isEqualToString:@"Price: Low to high"] && !([dictFilterParam objectForKey:@"minfloor"] && [dictFilterParam objectForKey:@"maxfloor"])) {
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"wpcf_flat_price" ascending:YES];
             arrResponse = [arrResponse sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
            [colView reloadData];
            [colView setContentOffset:CGPointZero animated:YES];
            [pref setObject:[dictFilterParam copy] forKey:strSaveUnitFilter];
        }
        else if ([dictFilterParam objectForKey:@"minfloor"] && [dictFilterParam objectForKey:@"maxfloor"]) {
            
            NSMutableDictionary *dictReq = [NSMutableDictionary dictionary];
            [dictReq setObject:[_dictSourse objectForKey:@"project_id"] forKey:@"project_id"];
            [dictReq setObject:_strSelectedUnitIDs forKey:@"unit_ids"];
            [dictReq setObject:[dictFilterParam objectForKey:@"minfloor"]  forKey:@"minfloor"];
            [dictReq setObject:[dictFilterParam objectForKey:@"maxfloor"] forKey:@"maxfloor"];
            NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:strSaveUserId];
            
            if (strUserId) {
                [dictReq setObject:strUserId forKey:@"user_id"];
            }
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI getSelectUnitInfo:dictReq withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    
                    [pref setObject:[dictFilterParam copy] forKey:strSaveUnitFilter];
                    if ([data count]) {
                        arrResponse = [data copy];
//                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"wpcf_flat_floor_num" ascending:YES];
//                        arrResponse = [arrResponse sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
                        
                
                        
                        if ([[dictFilterParam objectForKey:@"filter_price"] isEqualToString:@"Price: High to low"]) {
                            
                            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"wpcf_flat_price" ascending:NO];
                            arrResponse = [arrResponse sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
                            [colView reloadData];
                            [colView setContentOffset:CGPointZero animated:YES];
                        }
                        
                        if ([[dictFilterParam objectForKey:@"filter_price"] isEqualToString:@"Price: Low to high"]) {
                            
                            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"wpcf_flat_price" ascending:YES];
                            arrResponse = [arrResponse sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor, nil]];
                            [colView reloadData];
                            [colView setContentOffset:CGPointZero animated:YES];
                        }
                        [colView reloadData];
                    }
                    else {
                        
                        if (!viewNoUnit) {
                            viewNoUnit = [NoUnitView createView];
                            viewNoUnit.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                            [viewNoUnit.btnClose addTarget:self action:@selector(closeButton) forControlEvents:UIControlEventTouchUpInside];
                            [self.view addSubview:viewNoUnit];
                            
                            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButton)];
                            [viewNoUnit addGestureRecognizer:tap];
                            
                            [UIView animateWithDuration:0.25
                                                  delay:0.0
                                                options:UIViewAnimationOptionCurveEaseIn
                                             animations:^{
                                                 viewNoUnit.frame = self.view.frame;
                                             } completion:^(BOOL finished) {
                                                 
                                                 
                                                 
                                             }];
                        }
                        
                    }
                    
                }
                else {
                    
                }
            }];
            
        }
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(receiveFilterParamenters:) fromClass:self];
    }
}
@end
