//
//  HeaderCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/28/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;

@end


