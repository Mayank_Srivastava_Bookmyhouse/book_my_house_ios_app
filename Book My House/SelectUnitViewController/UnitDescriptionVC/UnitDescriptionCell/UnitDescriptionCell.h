//
//  UnitDescriptionCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/28/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderCell.h"
#import "Header.h"
#import "UnitOverviewCell.h"
#import "UnitFecilitiesCell.h"
#import "AlbumViewController.h"


@interface Header : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (copy, nonatomic) NSArray *arrImage;
@property (strong, nonatomic) UIViewController *owner;
+ (Header *)createCell;
@end

@interface UnitSummary : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTotlaArea;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblBuilderName;
@property (weak, nonatomic) IBOutlet UILabel *lblPLC;
@property (weak, nonatomic) IBOutlet UILabel *lblPSF;
+ (UnitSummary *)createCell;
@end

@interface HeaderOverView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
+ (HeaderOverView *)createCell;
@end


@interface IconSet : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (copy, nonatomic) NSDictionary *dictOverview;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
+ (IconSet *)createCell;
@end

@interface FecilitiesCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) NSArray *arrData;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;

+ (FecilitiesCell *)createCell;
@end