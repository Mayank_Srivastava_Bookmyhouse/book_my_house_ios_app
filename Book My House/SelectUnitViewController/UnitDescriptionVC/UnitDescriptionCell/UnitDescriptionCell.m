//
//  UnitDescriptionCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/28/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "UnitDescriptionCell.h"
static NSString * const strId = @"cell";
@implementation Header

+ (Header *)createCell {
    Header *cell = [[[NSBundle mainBundle] loadNibNamed:@"UnitDescriptionCell" owner:self options:nil] firstObject];
    [cell.colView registerClass:[HeaderCell class] forCellWithReuseIdentifier:strId];
    [cell.colView registerNib:[UINib nibWithNibName:@"HeaderCell" bundle:nil] forCellWithReuseIdentifier:strId];
    
    
    return cell;
}



#pragma mark - Collection View cell
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.colView.frame.size;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrImage.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strId forIndexPath:indexPath];
    
    [cell.imgBg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", baseIconUrl, [[_arrImage objectAtIndex:indexPath.row] objectForKey:@"url"]]] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumViewController *obj = [_owner.storyboard instantiateViewControllerWithIdentifier:@"AlbumViewController"];
    obj.arrSourse = _arrImage;
    obj.index = indexPath.row;
    obj.option = AlbumAccessOptionUnitDescription;
    [_owner.navigationController pushViewController:obj animated:YES];
}

@end


@implementation UnitSummary

+ (UnitSummary *)createCell
{
    UnitSummary *cell = [[[NSBundle mainBundle] loadNibNamed:@"UnitDescriptionCell" owner:self options:nil] objectAtIndex:1];
    
    return cell;
}

@end


@implementation HeaderOverView
+ (HeaderOverView *)createCell {
    HeaderOverView *cell = [[[NSBundle mainBundle] loadNibNamed:@"UnitDescriptionCell" owner:self options:nil] objectAtIndex:2];
    
    return cell;
}
@end


@implementation IconSet

+ (IconSet *)createCell {
    IconSet *cell = [[[NSBundle mainBundle] loadNibNamed:@"UnitDescriptionCell" owner:self options:nil] objectAtIndex:3];
    [cell.colView registerClass:[UnitOverviewCell class] forCellWithReuseIdentifier:@"UnitOverviewCell"];
    [cell.colView registerNib:[UINib nibWithNibName:@"UnitOverviewCell" bundle:nil] forCellWithReuseIdentifier:@"UnitOverviewCell"];
    return cell;
}


#pragma mark - Collection View Delegates
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 75);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dictOverview.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UnitOverviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UnitOverviewCell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.lblCaption.text = @"Direction";
        if (![[_dictOverview objectForKey:@"direction"] isKindOfClass:[NSNull class]]) {
            cell.lbltitle.text = [NSString stringWithFormat:@"%@",[_dictOverview objectForKey:@"direction"]];
        }
        else {
            cell.lbltitle.text = @"";
        }
    }
    else if (indexPath.row == 1) {
        cell.lblCaption.text = @"Balconies";
        if (![[_dictOverview objectForKey:@"balconies"] isKindOfClass:[NSNull class]]) {
            cell.lbltitle.text = [NSString stringWithFormat:@"%@", [_dictOverview objectForKey:@"balconies"]];
        }
        else {
            cell.lbltitle.text = @"";
        }
    }
    else if (indexPath.row == 2) {
        cell.lblCaption.text =  @"Open Parking";
        if (![[_dictOverview objectForKey:@"openparking"] isKindOfClass:[NSNull class]]) {
            cell.lbltitle.text = [NSString stringWithFormat:@"%@", [_dictOverview objectForKey:@"openparking"]];
        }
        else {
            cell.lbltitle.text = @"";
        }
    }
    else if (indexPath.row == 3) {
        cell.lblCaption.text =  @"Covered Parking";
        if (![[_dictOverview objectForKey:@"covered_parking"] isKindOfClass:[NSNull class]]) {
            cell.lbltitle.text = [NSString stringWithFormat:@"%@", [_dictOverview objectForKey:@"covered_parking"]];
        }
        else {
            cell.lbltitle.text = @"";
        }
    }
    else {
        cell.lblCaption.text =  @"Toilets";
        if (![[_dictOverview objectForKey:@"toilet"] isKindOfClass:[NSNull class]]) {
            cell.lbltitle.text = [NSString stringWithFormat:@"%@", [_dictOverview objectForKey:@"toilet"]];
        }
        else {
            cell.lbltitle.text = @"";
        }
    }
    return cell;
}

@end


@implementation FecilitiesCell

+ (FecilitiesCell *)createCell {
    FecilitiesCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"UnitDescriptionCell" owner:self options:nil] objectAtIndex:4];
    [cell.colView registerClass:[UnitFecilitiesCell class] forCellWithReuseIdentifier:@"UnitFecilitiesCell"];
    [cell.colView registerNib:[UINib nibWithNibName:@"UnitFecilitiesCell" bundle:nil] forCellWithReuseIdentifier:@"UnitFecilitiesCell"];
    
    return cell;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *str = [_arrData objectAtIndex:indexPath.row];
    UnitFecilitiesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UnitFecilitiesCell" forIndexPath:indexPath];
    
    cell.lblTitle.text = str;
    UIImage *img = [UIImage imageNamed:str];
    cell.imgBg.image = img;
    
    return cell;
}

@end