//
//  UnitFecilitiesCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/29/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitFecilitiesCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
