//
//  UnitOverviewCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/29/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitOverviewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCaption;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@end
