//
//  UnitDescriptionVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/28/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "UnitDescriptionCell.h"
#import "FeatureCell.h"
#import "ExploreLocationVC.h"
#import "SigninViewController.h"
#import "PaymentVC.h"
#import "PaymentModal.h"
#import "EDStarRating.h"

@interface UnitDescriptionVC : UIViewController<UITableViewDelegate, UITableViewDataSource, EDStarRatingProtocol, UIWebViewDelegate>

@property (strong, nonatomic) NSDictionary *dictSourse;
@property (strong, nonatomic) NSString *strPropertyCatagory;
@end
