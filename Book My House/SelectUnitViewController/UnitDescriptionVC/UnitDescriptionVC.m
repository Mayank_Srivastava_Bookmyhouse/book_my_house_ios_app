//
//  UnitDescriptionVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/28/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "UnitDescriptionVC.h"
#import "CustomVC.h"

static NSString * const strLogin                             = @"Please login to add items in favorite list.";
static NSString * const strComment                           = @"Please login to post your comments";
static NSString * const strCommentTitle                      = @"Please enter your title.";
static NSString * const strCommentMassege                    = @"Please enter your message.";
static NSString * const str_No_Review                        = @"No review available";

@interface UnitDescriptionVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIButton *btnBookNow;
    __weak IBOutlet UIButton *btnFavorite;
    __weak IBOutlet UIButton *btnUp;
    __weak IBOutlet UILabel *lblTitle;
    
    NSUserDefaults *pref;
    NSDictionary *dictResponse;
    BOOL isAPILoaded, isUnitSpecificationButtonSelected, isReviewAvailable, isOverview, isFecilities, isProjectSpecification, isInsightSelected,isAboutBuilder, isReview, isPriceTrend;
    UnitSummary *cellUnitSummary;
    PostComment *cellPostComment;
    float starRating;
    PriceTrend *cellPriceTrends;
}

- (IBAction)actionListeners:(id)sender;

@end

@implementation UnitDescriptionVC


#pragma mark - View Controller Mathods
- (void)viewDidLoad {
    pref = [NSUserDefaults standardUserDefaults];
    [super viewDidLoad];
    [self screenSettings];
    [self loadUnitInfo];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - Screen settings
- (void)screenSettings {
    
    //btnBack.superview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
    
    [self setNeedsStatusBarAppearanceUpdate];
    tblView.contentInset = UIEdgeInsetsMake(0, 0, CGRectGetHeight(btnBookNow.frame), 0);
    
    lblTitle.text = _strPropertyCatagory;
    
    [btnFavorite addTarget:self action:@selector(didTappedAddFavorite:) forControlEvents:UIControlEventTouchUpInside];
    btnFavorite.selected = [[dictResponse objectForKey:@"user_favourite"] boolValue];
}
#pragma mark - API Actions
- (void)loadUnitInfo {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[_dictSourse objectForKey:@"unit_id"] forKey:@"unit_id"];
        
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        
        if (strUserId != nil) {
            [dict setObject:strUserId forKey:@"user_id"];
        }
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getUnitDetail:dict withCallback:^(BOOL isSuccess, id data) {
            [[AppDelegate share] enableUserInteraction];
            if (isSuccess) {
                dictResponse = data;
                isAPILoaded = YES;
                isReviewAvailable = [[dictResponse objectForKey:@"comments_detail"] count] == 0 ? NO:YES;
                [tblView reloadData];
            }
            else {
                [self.view makeToast:strSomeErrorOccured
                                                 duration:duration
                                                 position:CSToastPositionCenter];
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadUnitInfo) fromClass:self];
    }
}

#pragma mark - Button Actions
- (IBAction)actionListeners:(id)sender {
    
    if (sender == btnBack) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            if ([controller isKindOfClass:[SelectUnitVC class]]) {
                SelectUnitVC *obj = (SelectUnitVC *)controller;
                [self.navigationController popToViewController:obj animated:YES];
                
                if ([pref objectForKey:strSaveUnitFilter]) {
                    [obj receiveFilterParamenters:nil];
                }
                else {
                    [obj loadSelectedUnitInfo];
                }
                break;
            }
            else if ([controller isKindOfClass:[FavouriteVC class]]) {
                FavouriteVC *obj = (FavouriteVC *)controller;
                [self.navigationController popToViewController:obj animated:YES];
                [obj loadFavouriteList];
                break;
            }
        }
        
    }
    else if (sender == btnBookNow) {
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        
        PaymentModal *manager = [PaymentModal sharedManager];
        manager.dict = [dictResponse copy];
        if (strUserId != nil) {
            PaymentVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
            [self.navigationController pushViewController:obj animated:YES];
        }
        else {
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.option = SigninAccessOptionPayment;
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
    else if (sender == btnUp) {
        [tblView setContentOffset:CGPointZero animated:YES];
    }
}


- (void)didTappedAddFavorite:(UIButton *)btn {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict =[NSMutableDictionary dictionary];
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId != nil) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kFavoriteUnit object:nil];
            [dict setObject:[_dictSourse objectForKey:@"unit_id"] forKey:@"unit_id"];
            [dict setObject:strUserId forKey:@"user_id"];
            //[dict setObject:@"unit" forKey:@"type"];
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI markAsFavorite:dict withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    if ([[data objectForKey:@"success"] boolValue]) {
                        [self loadUnitInfo];
                    }
                    else {
                        [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    }
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
            }];
        }
        else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTappedAddFavorite:) name:kFavoriteUnit object:nil];
            [self.view makeToast:strLogin duration:duration position:CSToastPositionCenter];
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionProjectDetails;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
        }

        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didTappedAddFavorite:) fromClass:self];
    }
}


- (void)didTapedUnitSpecification:(UIButton *)sender {
    isUnitSpecificationButtonSelected = !isUnitSpecificationButtonSelected;
    
    [tblView reloadData];
}

- (void)didTappedProjectPriceHistory:(UIButton *)sender {
    
}

- (void)didTappedOverView:(UIButton *)btn {
    isOverview = !isOverview;
    [tblView reloadData];
}

- (void)didTappedFecilities:(UIButton *)btn {
    isFecilities = !isFecilities;
    [tblView reloadData];
}

- (void)didtappedProjectSpecification:(UIButton *)btn {
    isProjectSpecification = !isProjectSpecification;
    [tblView reloadData];
}

- (void)didTappedAboutBuilder:(UIButton *)sender {
    isAboutBuilder = !isAboutBuilder;
    [tblView reloadData];
}

- (void)didTappedReview:(UIButton *)sender {
    isReview = !isReview;
    [tblView reloadData];
}

- (void)didTappedPriceTrend:(UIButton *)sender {
    isPriceTrend = !isPriceTrend;
    [tblView reloadData];
}

- (void)didSelectExploreNeighbourhood:(id)sender {
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *btn = (UIButton *)sender;
        ExploreLocationVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreLocationVC"];
        if (btn.tag == 1) {
            obj.option = ELAccessOptionUnitSummary;
            obj.dictInfo = dictResponse;
        }
        [self.navigationController pushViewController:obj animated:YES];
    }
    else {
    }
}

- (void)didSelectPostAComment:(UIButton *)sender {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId != nil) {
            
            if (cellPostComment.txtMessage.text.length == 0) {
                [self.view makeToast:strCommentMassege duration:duration position:CSToastPositionCenter];
                return;
            }
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:strUserId forKey:@"user_id"];
            [dict setObject:[dictResponse objectForKey:@"id"] forKey:@"post_id"];
            [dict setObject:cellPostComment.txtMessage.text forKey:@"comment"];
            [dict setObject:@"" forKey:@"title"];
            [dict setObject:@"iOS" forKey:@"mobile_type"];
            [dict setObject:@"unit" forKey:@"comment_type"];
            [dict setObject:[NSNumber numberWithFloat:starRating] forKey:@"rating"];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kPostCommentOnUnitDetails object:nil];
            [ServiceAPI postAComment:dict withCallback:^(BOOL isSuccess, id data) {
                
                if (isSuccess) {
                    [self.view makeToast:[data objectForKey:@"message"] duration:duration position:CSToastPositionCenter];
                    cellPostComment.txtTitle.text = @"";
                    cellPostComment.txtMessage.text = @"";
                    cellPostComment.starRating.rating = 0;
                    [self loadUnitInfo];
                    
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                }
                
            }];
            
        }
        else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectPostAComment:) name:kPostCommentOnUnitDetails object:nil];
            
            [self.view makeToast:strComment duration:duration position:CSToastPositionCenter];
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didSelectPostAComment:) fromClass:self];
    }
}

- (void)didSeeAllReviews:(UIButton *)sender {
    
    if ([dictResponse objectForKey:@"comments_detail"] != nil && [dictResponse objectForKey:@"id"] != nil) {
        NSArray *arr = [dictResponse objectForKey:@"comments_detail"];
        
        if (arr.count != 0) {
            CommentsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsVC"];
            obj.options = CommentsVCAccessOptionsFeaturedProjects;
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[dictResponse objectForKey:@"id"] forKey:@"id"];
            [dict setObject:@"unit" forKey:@"comment_type"];
            obj.dictSourseInfo = dict;
            [self.navigationController pushViewController:obj animated:YES];
        }
        else {
            [self.view makeToast:str_No_Review duration:duration position:CSToastPositionCenter];
        }
    }
    else {
        [self.view makeToast:str_No_Review duration:duration position:CSToastPositionCenter];
    }
    
}

- (void)didTappedInsight:(UIButton *)sender {
    
    isInsightSelected = !isInsightSelected;
    [tblView reloadData];
}

#pragma mark - Table view Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 300.0;
    }
    else if (indexPath.row == 1) {
        return 215.0;
    }
    else if (indexPath.row == 2) {
        
        
        if ([[dictResponse objectForKey:@"balconies"] integerValue] || [[dictResponse objectForKey:@"toilet"] integerValue] || [[dictResponse objectForKey:@"direction"] integerValue] || [[dictResponse objectForKey:@"openparking"] integerValue] || [[dictResponse objectForKey:@"covered_parking"] integerValue]) {
            return 45.0;
        }
        else {
            return 0.0;
        }
        
        //return 45.0;
    }
    else if (indexPath.row == 3) {
        if (isOverview) {
            return 0.0;
        }
        else {
            
            if ([[dictResponse objectForKey:@"balconies"] integerValue] || [[dictResponse objectForKey:@"toilet"] integerValue] || [[dictResponse objectForKey:@"direction"] integerValue] || [[dictResponse objectForKey:@"openparking"] integerValue] || [[dictResponse objectForKey:@"covered_parking"] integerValue]) {
                return 156.0;
            }
            else {
                return 0.0;
            }
            
            //return 156.0;
        }
    }
    else if (indexPath.row == 4) {
        
        NSMutableArray *arr = [NSMutableArray array];
        NSDictionary *dict = [dictResponse objectForKey:@"facitlities"];
        
        if (![dict isKindOfClass:[NSNull class]]) {
            
            if ([[dict objectForKey:@"wardrobs"] boolValue]) {
                [arr addObject:@"Wardrobe"];
            }
            if ([[dict objectForKey:@"pooja_room"] boolValue]) {
                [arr addObject:@"Pooja Room"];
            }
            if ([[dict objectForKey:@"washing_area"] boolValue]) {
                [arr addObject:@"Washing Area"];
            }
            if ([[dict objectForKey:@"piped_gas"] boolValue]) {
                [arr addObject:@"Piped Gas"];
            }
            if ([[dict objectForKey:@"lifts"] boolValue]) {
                [arr addObject:@"Lift"];
            }
            if ([[dict objectForKey:@"stores"] boolValue]) {
                [arr addObject:@"Stores"];
            }
            if ([[dict objectForKey:@"furnished"] boolValue]) {
                [arr addObject:@"Furnished"];
            }
            if ([[dict objectForKey:@"automation"] boolValue]) {
                [arr addObject:@"Automation"];
            }
            if ([[dict objectForKey:@"centralwifi"] boolValue]) {
                [arr addObject:@"Central Wifi"];
            }
            if ([[dict objectForKey:@"highspeedinternet"] boolValue]) {
                [arr addObject:@"Hi-speed internet"];
            }
        }
        
        if (arr.count) {
            return 45.0;
        }
        else
            return 0.0;
        
        //return 45.0;
    }
    else if (indexPath.row == 5) {
        
        NSMutableArray *arr = [NSMutableArray array];
        NSDictionary *dict = [dictResponse objectForKey:@"facitlities"];
        
        if (![dict isKindOfClass:[NSNull class]]) {
            
            if ([[dict objectForKey:@"wardrobs"] boolValue]) {
                [arr addObject:@"Wardrobe"];
            }
            if ([[dict objectForKey:@"pooja_room"] boolValue]) {
                [arr addObject:@"Pooja Room"];
            }
            if ([[dict objectForKey:@"washing_area"] boolValue]) {
                [arr addObject:@"Washing Area"];
            }
            if ([[dict objectForKey:@"piped_gas"] boolValue]) {
                [arr addObject:@"Piped Gas"];
            }
            if ([[dict objectForKey:@"lifts"] boolValue]) {
                [arr addObject:@"Lift"];
            }
            if ([[dict objectForKey:@"stores"] boolValue]) {
                [arr addObject:@"Stores"];
            }
            if ([[dict objectForKey:@"furnished"] boolValue]) {
                [arr addObject:@"Furnished"];
            }
            if ([[dict objectForKey:@"automation"] boolValue]) {
                [arr addObject:@"Automation"];
            }
            if ([[dict objectForKey:@"centralwifi"] boolValue]) {
                [arr addObject:@"Central Wifi"];
            }
            if ([[dict objectForKey:@"highspeedinternet"] boolValue]) {
                [arr addObject:@"Hi-speed internet"];
            }
        }
        
        if (arr.count) {
            if (isFecilities) {
                return 0.0;
            }
            else {
                return 100;
            }
        }
        else
        return 0.0;
        
        
        
    }
    else if (indexPath.row == 6) {
        return 44.0;
    }
    else if (indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9) {
        
        if (isUnitSpecificationButtonSelected) {
            return 0;
        }
        else
        return 290.0;
    }
    else if (indexPath.row == 10) {
        return 45.0;
    }
    else if (indexPath.row >= 11 && indexPath.row <= 14) {
        
        if (isProjectSpecification) {
            return 0;
        }
        else {
            return 130;
        }
        
    }
    else if (indexPath.row == 11) {
        return 50.0;
    }
    else if (indexPath.row == 15) {
        return 45.0;
    }
    else if (indexPath.row == 16) {
        
        if (isInsightSelected) {
            return 0.0;
        }
        else {
            return 300.0;
        }
        
    }
    else if (indexPath.row == 17) {
        return 45.0;
    }
    else if (indexPath.row == 18) {
        NSArray *arr = [dictResponse objectForKey:@"price_trends"];
        if (arr.count) {
            return 48.0;
        }
        else {
            return 0.0;
        }
        
       // return 48.0;
    }
    else if (indexPath.row == 19) {
        
        
        NSArray *arr = [dictResponse objectForKey:@"price_trends"];
        if (arr.count) {
            
            if (isPriceTrend) {
                return 0.0;
            }
            else {
                return 250.0;
            }
            
            return 250.0;
        }
        else {
            return 0.0;
        }
        
        //return 250.0;
        
    }
    else if (indexPath.row == 20) {
        return 48.0;
    }
    else if (indexPath.row == 21) {
        if (!isAboutBuilder) {
            return 110.0;
        }
        else {
            return 0.0;
        }
        
    }
    else if (indexPath.row == 22) {
        
        NSArray *arr = [dictResponse objectForKey:@"comments_detail"];
        if (arr.count) {
            return 48.0;
        }
        else
        return 0.0;
    }
    else if (indexPath.row == 23 || indexPath.row == 24) {
        NSArray *arr = [dictResponse objectForKey:@"comments_detail"];
        if (arr.count) {
            if (isReviewAvailable) {
                
                if (!isReview) {
                    
                    if (arr.count >= 2) {
                        return 85.0;
                    }
                    else {
                        
                        if (indexPath.row == 23) {
                            return 85.0;
                        }
                        
                        if (indexPath.row == 24) {
                            return 0.0;
                        }
                        
                    }
                    return 85.0;
                }
                else {
                    return 0.0;
                }
            }
            else {
                return 0;
            }
        }
        else
            return 0.0;
    
        
        
    }
    else if (indexPath.row == 25) {
        
        
        
        if ([[dictResponse objectForKey:@"is_comment_list_needed"] boolValue]) {
            return 30.0;
        }
        else {
            return 0.0;
        }
//        NSArray *arr = [dictResponse objectForKey:@"comments_detail"];
//        
//        if (arr.count) {
//            return 30.0;
//        }
//        else {
//            return 0.0;
//        }
        
        
    }
    else if (indexPath.row == 26) {
        return 235.0;
    }
    return 0.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return isAPILoaded ? 27:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        static NSString * const strID = @"header";
        Header *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [Header createCell];
        }
        cell.arrImage = [dictResponse objectForKey:@"images"];
        cell.owner = self;
        [cell.colView reloadData];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        
        static NSString * const strID = @"UnitSummary";
        UnitSummary *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [UnitSummary createCell];
        }
        
        if (![[dictResponse objectForKey:@"original_bsp"] isKindOfClass:[NSNull class]]) {
            
            cell.lblPrice.text = [NSString stringWithFormat:@"\u20B9 %@", [NSString stringWithFormat:@"%@", [dictResponse objectForKey:@"original_bsp"]]];
        }
        else {
            cell.lblPrice.text = @"Price not available";
        }
        
        if (![[dictResponse objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
            cell.lblTotlaArea.text = [NSString stringWithFormat:@"%@ sq ft", [dictResponse objectForKey:@"size"]];
        }
        else {
            cell.lblTotlaArea.text = @"--";
        }
        
        if (![[dictResponse objectForKey:@"bedroom_no"] isKindOfClass:[NSNull class]]) {
            cell.lblType.text = [dictResponse objectForKey:@"bedroom_no"];
        }
        else {
            cell.lblType.text = @"--";
        }
        
        if (![[dictResponse objectForKey:@"unit_no"] isKindOfClass:[NSNull class]]) {
            
            cell.lblUnitNumber.text = [NSString stringWithFormat:@"Unit No.: %@", [dictResponse objectForKey:@"unit_no"]];

        }
        else {
            cell.lblUnitNumber.text = @"";
        }
        
        if (![[dictResponse objectForKey:@"UnitFloor"] isKindOfClass:[NSNull class]]) {
            
            cell.lblFloor.text = [NSString stringWithFormat:@"Floor: %@", [dictResponse objectForKey:@"UnitFloor"]];

        }
        else {
            cell.lblFloor.text = @"--";
        }
        
        if (![[dictResponse objectForKey:@"address"] isKindOfClass:[NSNull class]]) {
            cell.lblAddress.text = [dictResponse objectForKey:@"address"];
        }
        else {
            cell.lblAddress.text = @"--";
        }
        
        if (![[dictResponse objectForKey:@"builder_name"] isKindOfClass:[NSNull class]]) {
            cell.lblBuilderName.text = [dictResponse objectForKey:@"builder_name"];
        }
        else {
            cell.lblBuilderName.text = @"--";
        }
        
        if (![[dictResponse objectForKey:@"total_plc"] isKindOfClass:[NSNull class]]) {
            cell.lblPLC.text = [NSString stringWithFormat:@"PLC: \u20B9%@", [dictResponse objectForKey:@"total_plc"]];
        }
        else {
            cell.lblPLC.text = [NSString stringWithFormat:@"PLC: --"];
        }
        
        if (![[dictResponse objectForKey:@"price_SqFt"] isKindOfClass:[NSNull class]]) {
            cell.lblPSF.text = [NSString stringWithFormat:@"%@ psf", [dictResponse objectForKey:@"price_SqFt"]];
        }
        else {
            cell.lblPSF.text = @"-- psf";
        }
        
        if (![[dictResponse objectForKey:@"user_favourite"] isKindOfClass:[NSNull class]]) {
            cell.btnFavorite.selected = [[dictResponse objectForKey:@"user_favourite"] boolValue];
        }
        else {
            cell.btnFavorite.selected = NO;
        }
        
        [cell.btnFavorite addTarget:self action:@selector(didTappedAddFavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cellUnitSummary = cell;
        return cell;
        
    }
    else if (indexPath.row == 2) {
        static NSString * const strID = @"Header1";
        HeaderOverView *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [HeaderOverView createCell];
        }
        cell.lblTitle.text = @" Overview";
        cell.btnPlus.selected = !isOverview;

        [cell.btnPlus addTarget:self action:@selector(didTappedOverView:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (indexPath.row == 3) {
        static NSString * const strID = @"IconSet";
        IconSet *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        if (cell == nil) {
            cell = [IconSet createCell];
        }
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[dictResponse objectForKey:@"balconies"] forKey:@"balconies"];
        [dict setObject:[dictResponse objectForKey:@"toilet"] forKey:@"toilet"];
        [dict setObject:[dictResponse objectForKey:@"direction"] forKey:@"direction"];
        [dict setObject:[dictResponse objectForKey:@"openparking"] forKey:@"openparking"];
        [dict setObject:[dictResponse objectForKey:@"covered_parking"] forKey:@"covered_parking"];
        
        cell.dictOverview = dict;
        [cell.colView reloadData];
        
        return cell;
    }
    else if (indexPath.row == 4) {
        static NSString * const strID = @"Header1";
        HeaderOverView *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [HeaderOverView createCell];
        }
        cell.lblTitle.text = @" Facilities";
        cell.btnPlus.selected = !isFecilities;
        [cell.btnPlus addTarget:self action:@selector(didTappedFecilities:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (indexPath.row == 5) {
        FecilitiesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FecilitiesCell"];
        
        if (cell == nil) {
            cell = [FecilitiesCell createCell];
        }
        
        NSMutableArray *arr = [NSMutableArray array];
        NSDictionary *dict = [dictResponse objectForKey:@"facitlities"];
        
        if (![dict isKindOfClass:[NSNull class]]) {
            
            if ([[dict objectForKey:@"wardrobs"] boolValue]) {
                [arr addObject:@"Wardrobe"];
            }
            if ([[dict objectForKey:@"pooja_room"] boolValue]) {
                [arr addObject:@"Pooja Room"];
            }
            if ([[dict objectForKey:@"washing_area"] boolValue]) {
                [arr addObject:@"Washing Area"];
            }
            if ([[dict objectForKey:@"piped_gas"] boolValue]) {
                [arr addObject:@"Piped Gas"];
            }
            if ([[dict objectForKey:@"lifts"] boolValue]) {
                [arr addObject:@"Lift"];
            }
            if ([[dict objectForKey:@"stores"] boolValue]) {
                [arr addObject:@"Stores"];
            }
            if ([[dict objectForKey:@"furnished"] boolValue]) {
                [arr addObject:@"Furnished"];
            }
            if ([[dict objectForKey:@"automation"] boolValue]) {
                [arr addObject:@"Automation"];
            }
            if ([[dict objectForKey:@"centralwifi"] boolValue]) {
                [arr addObject:@"Central Wifi"];
            }
            if ([[dict objectForKey:@"highspeedinternet"] boolValue]) {
                [arr addObject:@"Hi-speed internet"];
            }
            
            cell.arrData = arr;
            [cell.colView reloadData];
        }
        
        return cell;
    }
    else if (indexPath.row == 6) {
        static NSString * const strID = @"Header5";
        Header5 *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        if (cell == nil) {
            cell = [Header5 createCell];
        }
        cell.lblUnitSpecifications.text = @" Unit Specification";
        cell.btnPlus.selected = !isUnitSpecificationButtonSelected;
        
        [cell.btnPlus addTarget:self action:@selector(didTapedUnitSpecification:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }
    else if (indexPath.row == 7) {
        
        DetailedUnitSpecification *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailedUnitSpecification"];
        
        NSArray *arr;
        if (cell == nil) {
            cell = [DetailedUnitSpecification createCell];
        }
        arr = [dictResponse objectForKey:@"walls"];
        
        
        cell.lblTop.text = @"   Walls";
        
        cell.lblTitle1.text = @"LIVING/DINING";
        cell.lblTitle2.text = @"MASTER BEDROOM";
        cell.lblTitle3.text = @"OTHER BEDROOMS";
        cell.lblTitle4.text = @"KITCHEN";
        cell.lblTitle5.text = @"TOILETS";
        
        for (NSDictionary *dict in arr) {
            
            if ([[dict objectForKey:@"name"] isEqualToString:@"Living/Dining"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption1.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption1.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Master Bedroom"]){
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption2.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption2.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Other Bedroom"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption3.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption3.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Toilets"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption4.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption4.text = @"";
                }
                
            }
            else {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption5.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption5.text = @"";
                }
            }
        }
        return cell;
    }
    
    else if (indexPath.row == 8) {
        DetailedUnitSpecification *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailedUnitSpecification"];
        
        NSArray *arr;
        if (cell == nil) {
            cell = [DetailedUnitSpecification createCell];
        }
        arr = [dictResponse objectForKey:@"flooring"];
        
        cell.lblTop.text = @" Flooring";
        
        cell.lblTitle1.text = @"LIVING/DINING";
        cell.lblTitle2.text = @"MASTER BEDROOM";
        cell.lblTitle3.text = @"OTHER BEDROOMS";
        cell.lblTitle4.text = @"KITCHEN";
        cell.lblTitle5.text = @"TOILET";
        
        for (NSDictionary *dict in arr) {
            
            if ([[dict objectForKey:@"name"] isEqualToString:@"Living/Dining"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption1.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption1.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Master Bedroom"]){
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption2.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption2.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Other Bedrooms"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption3.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption3.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Kitchen"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption4.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption4.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Toilets"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption5.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption5.text = @"";
                }
                
            }
            
            
        }
        
        return cell;
    }
    else if (indexPath.row == 9) {
        
        DetailedUnitSpecification *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailedUnitSpecification"];
        
        NSArray *arr;
        if (cell == nil) {
            cell = [DetailedUnitSpecification createCell];
        }
        arr = [dictResponse objectForKey:@"fittings"];
        
        cell.lblTop.text = @"   Fittings";
        
        cell.lblTitle1.text = @"DOORS";
        cell.lblTitle2.text = @"WINDOWS";
        cell.lblTitle3.text = @"OTHER BEDROOMS";
        cell.lblTitle4.text = @"KITCHEN";
        cell.lblTitle5.text = @"TOILETS";
        
        for (NSDictionary *dict in arr) {
            
            if ([[dict objectForKey:@"name"] isEqualToString:@"Doors"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption1.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption1.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Windows"]){
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption2.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption2.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Other Bedrooms"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption3.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption3.text = @"";
                }
                
            }
            else if ([[dict objectForKey:@"name"] isEqualToString:@"Kitchen"]) {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption4.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption4.text = @"";
                }
                
            }
            else {
                
                if (![[dict objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
                    cell.lblCaption5.text = [dict objectForKey:@"type"];
                }
                else {
                    cell.lblCaption5.text = @"";
                }
            }
        }
        
        return cell;
    }
    
    else if (indexPath.row == 10) {
        static NSString * const strID = @"Header1";
        HeaderOverView *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [HeaderOverView createCell];
            [cell.btnPlus addTarget:self action:@selector(didtappedProjectSpecification:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.lblTitle.text = @" Project Specification";
        cell.btnPlus.selected = !isProjectSpecification;
        return cell;
    }
    else if (indexPath.row >= 11 && indexPath.row <= 14) {
    
        DetailLifeStyle *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailLifeStyle"];
        
        if (cell == nil) {
            cell = [DetailLifeStyle createCell];
        }
        
        cell.btnLeft.tag = cell.btnRight.tag = indexPath.row;
        NSArray *arr;// = [NSArray array];
        if (indexPath.row == 11) {
            cell.lblTitle.text = @"Amenities";
            arr = [dictResponse objectForKey:@"Amenities"];
            
        }
        else if (indexPath.row == 12) {
            cell.lblTitle.text = @"Recreation";
            arr = [dictResponse objectForKey:@"recreation"];

        }
        else if (indexPath.row == 13) {
            cell.lblTitle.text = @"Safety";
            arr = [dictResponse objectForKey:@"safety"];
        }
        else if (indexPath.row == 14) {
            cell.lblTitle.text = @"Services";
            arr = [dictResponse objectForKey:@"services"];
        }
        NSMutableArray *array = [NSMutableArray array];
        array = [arr mutableCopy];
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
        [array sortUsingDescriptors:@[descriptor]];
        cell.arrInfo = [array copy];
        [cell.collectionView reloadData];
        
        return cell;
    }
    else if (indexPath.row == 15) {
        static NSString * const strID = @"Header1";
        HeaderOverView *cell = [tableView dequeueReusableCellWithIdentifier:strID];
        
        if (cell == nil) {
            cell = [HeaderOverView createCell];
        }
        cell.lblTitle.text = @" Insights";
        cell.btnPlus.selected = !isInsightSelected;
        [cell.btnPlus addTarget:self action:@selector(didTappedInsight:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (indexPath.row == 16) {
    
        LocationMap *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationMap"];
        if (cell == nil) {
            cell = [LocationMap createCell];
        }
        NSArray *arr = [[dictResponse objectForKey:@"lat_lng"] componentsSeparatedByString:@","];
        cell.mapView.camera = [GMSCameraPosition cameraWithLatitude:[[arr firstObject] floatValue] longitude:[[arr lastObject] floatValue] zoom:13];
        cell.mapView.settings.scrollGestures = NO;
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([[arr firstObject] floatValue], [[arr lastObject] floatValue]);
        marker.title = [dictResponse objectForKey:@"project_name"];
        marker.snippet = [dictResponse objectForKey:@"builder_name"];
        marker.map = cell.mapView;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        return cell;
    }
    else if (indexPath.row == 17) {
        
        Footer3 *cell = [tableView dequeueReusableCellWithIdentifier:@"Footer3"];
        
        if (cell == nil) {
            cell = [Footer3 createCell];
        }
        [cell.btnExploreNeighbourhood addTarget:self action:@selector(didSelectExploreNeighbourhood:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (indexPath.row == 18 || indexPath.row == 20 || indexPath.row == 22) {
        
        Header6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Header6"];
        
        if (cell == nil) {
            cell = [Header6 createCell];
        }
        if (indexPath.row == 18) {
            cell.lblTitle.text = @" Project Price History";
            cell.btnPlus.selected = !isPriceTrend;
            [cell.btnPlus addTarget:self action:@selector(didTappedPriceTrend:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if (indexPath.row == 20) {
            cell.lblTitle.text = @" About Builder";
            
            [cell.btnPlus addTarget:self action:@selector(didTappedAboutBuilder:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnPlus.selected = !isAboutBuilder;
        }
        else if (indexPath.row == 22) {
            cell.lblTitle.text = @" Reviews";
            [cell.btnPlus addTarget:self action:@selector(didTappedReview:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnPlus.selected = !isReview;
        }
        
        return cell;
    }
    else if (indexPath.row == 19) {
        PriceTrend *cell = [tableView dequeueReusableCellWithIdentifier:@"PriceTrend"];
        
        if (cell == nil) {
            cell = [PriceTrend createCell];
            NSArray *arr = [dictResponse objectForKey:@"price_trends"];
            
            if (arr.count) {
                cell.web.delegate = self;
                NSString *strPriceTrends = @"";
                for (NSDictionary *dict in arr) {
                    NSString *str = [NSString stringWithFormat:@"{\"price_trend_date\":\"%@\",\"price_trends\":\"%@\"},", [dict objectForKey:@"price_trend_date"], [dict objectForKey:@"price_trend_value"]];
                    
                    strPriceTrends = [strPriceTrends stringByAppendingString:str];
                }
                
                strPriceTrends = [strPriceTrends substringToIndex:strPriceTrends.length - 1];
                NSString* encodedText = [[NSString stringWithFormat:@"%@%@%@?price_trends=[%@]",baseUrl, url_line_chart, extension, strPriceTrends] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                
                NSURL *url = [NSURL URLWithString:encodedText];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                
                [cell.web loadRequest:request];
                
            }
        }
        
        cellPriceTrends = cell;
        return cell;
    }
    else if (indexPath.row == 21) {
        LocalityInsight *cell = [tableView dequeueReusableCellWithIdentifier:@"LocalityInsight"];
        
        if (cell == nil) {
            cell = [LocalityInsight createCell];
            [cell.btnReadMore addTarget:self action:@selector(readMore:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if (![[dictResponse objectForKey:@"builder_description"] isKindOfClass:[NSNull class]]) {
            cell.txtLocalityInsight.text = [dictResponse objectForKey:@"builder_description"];
        }
        else {
            cell.txtLocalityInsight.text = @"";
        }
        
        
        
        return cell;
    }
    else if (indexPath.row == 23 || indexPath.row == 24) {
        
        Review *cell = [tableView dequeueReusableCellWithIdentifier:@"Review"];
        
        if (cell == nil) {
            cell = [Review createCell];
        }
        
        NSDictionary *dict = [[dictResponse objectForKey:@"comments_detail"] firstObject];
        
        if (![[dict objectForKey:@"comment_user_name"] isKindOfClass:[NSNull class]]) {
            cell.lblUserName.text = [dict objectForKey:@"comment_user_name"];
        }
        
        if (![[dict objectForKey:@"comment_description"] isKindOfClass:[NSNull class]]) {
            cell.lblDescription.text = [dict objectForKey:@"comment_description"];
        }
        
        
        if (![[dict objectForKey:@"rating"] isKindOfClass:[NSNull class]]) {
            cell.rating.rating = [[dict objectForKey:@"rating"] floatValue];
            cell.rating.starImage = [UIImage imageNamed:@"star-template"];
            cell.rating.starHighlightedImage = [UIImage imageNamed:@"star-highlighted-template"];
            cell.rating.maxRating = 5.0;
            cell.rating.horizontalMargin = 12;
            cell.rating.editable = NO;
            cell.rating.displayMode = EDStarRatingDisplayHalf;
            cell.rating.tag = indexPath.row;
        }
        
        return cell;
    }
    else if (indexPath.row == 25) {
        Footer6 *cell = [tableView dequeueReusableCellWithIdentifier:@"Footer6"];
        
        if (cell == nil) {
            cell = [Footer6 createCell];
            cell.btnSeeAllReviews.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
        [cell.btnSeeAllReviews addTarget:self action:@selector(didSeeAllReviews:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if (indexPath.row == 26) {
        PostComment *cell = [tableView dequeueReusableCellWithIdentifier:@"PostComment"];
        
        if (cell == nil) {
            cell = [PostComment createCell];
        }
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Title"];
        
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, 5)];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PT Sans" size:17.0] range:NSMakeRange(0, 5)];
        
        cell.txtTitle.attributedPlaceholder = str;
        cell.txtMessage.placeholder = @"Enter your comment";
        
        [cell.btnPostAComment addTarget:self action:@selector(didSelectPostAComment:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell setUpStarRating];
        cell.starRating.delegate = self;
        
        cellPostComment = cell;
        return cell;
    }
    
    return nil;
}

#pragma mark - Star Rating delegates
-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating {
    starRating = rating;
}


#pragma mark - Auxillary Mathods
- (NSString *)setPriceRange:(NSString *)str {
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num/10000000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"%0.2f Lacs+", result];
    }
    
    return strNum;
}

#pragma mark - Web view Delegates
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [cellPriceTrends.indicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [cellPriceTrends.indicator stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Error %@", error.localizedDescription);
}

#pragma mark - Scrollview
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (scrollView == tblView) {
        
        if (scrollView.contentOffset.y < 300) {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                btnUp.hidden = YES;
            }];
            
        }
        else {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                btnUp.hidden = NO;
            }];
        }
        
    }
    
}

#pragma mark - Auxillary Mathods
- (void)readMore:(UIButton *)btn {
    
    CustomVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomVC"];
    obj.strTitle = @"Builder Description";
    obj.strHTMLContent = [dictResponse objectForKey:@"builder_description"];
    [self presentViewController:obj animated:YES completion:nil];
}

@end
