//
//  SelectUnitFilter.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/22/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
@interface SelectUnitFilter : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSString *strTotalFloor;

@end
