//
//  SelectUnitFilter.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/22/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SelectUnitFilter.h"
#import "MARKRangeslider.h"

static NSString * const strID = @"cell";
@interface SelectUnitFilter () {
    
    __weak IBOutlet UILabel *lblFilter;
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UIButton *btnReset;
    __weak IBOutlet UIButton *btnPrice;
    __weak IBOutlet UIButton *btnFloor;
    __weak IBOutlet UIButton *btnApplyFilters;
    __weak IBOutlet UIScrollView *scrlView;
    
    
    UITableView *tblPrice;
    //UITableView *tblFloor;
    
    NSMutableDictionary *dictFilterParameter;
    NSInteger indexSelected;
    NSMutableArray *arrTitle;
    NSUserDefaults *pref;
    NSString *strRange;

    BOOL isPriceSelected;
}

- (IBAction)actionListener:(id)sender;

@end

@implementation SelectUnitFilter

#pragma mark - View Controller Mathods

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    
    pref = [NSUserDefaults standardUserDefaults];
    [super viewDidLoad];
    dictFilterParameter = [NSMutableDictionary dictionary];
    
    [self screenSettings];
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateScreenSettings];
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    scrlView.backgroundColor = [UIColor clearColor];
    
    NSDictionary *dict = [pref objectForKey:strSaveUnitFilter];
    NSString *str = [dict objectForKey:@"filter_price"];
    
    strRange = [NSString stringWithFormat:@"%d - %d", (int)[[dict objectForKey:@"minfloor"] integerValue], (int)[[dict objectForKey:@"maxfloor"] integerValue]];
    
    if ([str isEqualToString:@"Price: High to low"]) {
        [btnPrice setTitle:@"Price: High to low" forState:UIControlStateNormal];
        [btnPrice setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
    }
    else if ([str isEqualToString:@"Price: Low to high"]) {
        [btnPrice setTitle:@"Price: Low to high" forState:UIControlStateNormal];
        [btnPrice setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
    }
}

- (void)updateScreenSettings {
    
    NSInteger numberOfFloors = [_strTotalFloor integerValue];
    arrTitle = [NSMutableArray array];
    NSInteger numberOfButtons;
    
    if (numberOfFloors < 100) {
        numberOfButtons = numberOfFloors / 10 + 1;
        for (int i = 0; i < numberOfButtons; i++) {
            [arrTitle addObject:[NSString stringWithFormat:@"%d - %d", i * 10 + 1, (i + 1) * 10]];
        }
        
        NSString *strLast = [arrTitle lastObject];
        NSMutableArray *arr = [[strLast componentsSeparatedByString:@" - "] mutableCopy];
        [arr removeObjectAtIndex:1];
        [arr addObject:_strTotalFloor];
        strLast = [arr componentsJoinedByString:@" - "];
        [arrTitle replaceObjectAtIndex:arrTitle.count - 1 withObject:strLast];
    }
    else {
        numberOfButtons = 10 + (numberOfFloors - 100) / 20;
        for (int i = 0; i < 10; i++) {
            [arrTitle addObject:[NSString stringWithFormat:@"%d - %d", i * 10 + 1, (i + 1) * 10]];
        }
        
        int a = 101, b= 120;
        for (int i = 10; i <= numberOfButtons; i ++) {
            [arrTitle addObject:[NSString stringWithFormat:@"%d - %d", a , b]];
            a = a + 20;
            b = b + 20;
        }
        
        NSString *strLast = [arrTitle lastObject];
        NSMutableArray *arr = [[strLast componentsSeparatedByString:@" - "] mutableCopy];
        [arr removeObjectAtIndex:1];
        [arr addObject:_strTotalFloor];
        strLast = [arr componentsJoinedByString:@" - "];
        [arrTitle replaceObjectAtIndex:arrTitle.count - 1 withObject:strLast];
    }
    
    NSInteger index = 0;
     CGSize aSize = CGSizeMake(CGRectGetWidth(scrlView.frame) / 4, 40);
    for (int i = 0; i <= numberOfButtons / 4; i++) {
        for (int j = 0; j < 4; j++) {
            CGRect frame = CGRectMake(j * aSize.width, i * aSize.height, aSize.width, aSize.height);
            
            if (index < arrTitle.count) {
                [scrlView addSubview:[self getFloorButtons:frame index:index andTitle:[arrTitle objectAtIndex:index]]];
                index ++;
            }
        }
    }
}

#pragma mark - API Action

#pragma mark - Button Action
- (IBAction)actionListener:(id)sender {
    if (sender == btnCancel) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender == btnReset) {
        [btnPrice setTitle:@"Price" forState:UIControlStateNormal];
        [btnPrice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnFloor setTitle:@"Floor" forState:UIControlStateNormal];
        [btnFloor setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [dictFilterParameter removeAllObjects];
        
        
        indexSelected = 0;
        for (UIView *temp in scrlView.subviews) {
            
            if ([temp isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)temp;
                btn.selected = NO;                
            }
            
        }
    }
    else if (sender == btnPrice) {
        
//        [btnFloor setTitle:@"Floor" forState:UIControlStateNormal];
//        [btnFloor setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//
//        
//        indexSelected = 0;
//        for (UIView *temp in scrlView.subviews) {
//            
//            if ([temp isKindOfClass:[UIButton class]]) {
//                UIButton *btn = (UIButton *)temp;
//                btn.enabled = NO;
//                btn.selected  = NO;
//            }
//            
//        }
        
//        if (tblFloor != nil) {
//            [tblFloor removeFromSuperview];
//            tblFloor = nil;
//        }
        
        
        if (tblPrice != nil) {
            [tblPrice removeFromSuperview];
            tblPrice = nil;

        }
        else {
            tblPrice = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(btnPrice.frame), CGRectGetMaxY(btnPrice.frame), 0.9 * CGRectGetWidth(btnPrice.frame), 3 * CGRectGetHeight(btnPrice.frame) / 2) style:UITableViewStylePlain];
            
            tblPrice.layer.borderWidth = 2.0;
            tblPrice.layer.borderColor = [UIColor darkGrayColor].CGColor;
            tblPrice.delegate = self;
            tblPrice.dataSource = self;
            [self.view addSubview:tblPrice];
        }
    }
    else if (sender == btnFloor) {
        
//        [btnPrice setTitle:@"Price" forState:UIControlStateNormal];
//        [btnPrice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        
//        if (tblPrice != nil) {
//            [tblPrice removeFromSuperview];
//            tblPrice = nil;
//
//        }
//        
//        indexSelected = 0;
//        for (UIView *temp in scrlView.subviews) {
//            
//            if ([temp isKindOfClass:[UIButton class]]) {
//                UIButton *btn = (UIButton *)temp;
//                btn.enabled = YES;
//                btn.selected  = NO;
//            }
//            
//        }
        
//        if (tblFloor != nil) {
//            [tblFloor removeFromSuperview];
//            tblFloor = nil;
//
//        }
//        else {
//            tblFloor = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(btnFloor.frame), CGRectGetMaxY(btnFloor.frame), 0.9 * CGRectGetWidth(btnFloor.frame), 3 * CGRectGetHeight(btnFloor.frame) / 2) style:UITableViewStylePlain];
//            
//            tblFloor.layer.borderWidth = 2.0;
//            tblFloor.layer.borderColor = [UIColor darkGrayColor].CGColor;
//            tblFloor.delegate = self;
//            tblFloor.dataSource = self;
//            [self.view addSubview:tblFloor];
//        }
    }
    else if (sender == btnApplyFilters) {
        
        if (![btnPrice.titleLabel.text isEqualToString:@"Price"]) {
            [dictFilterParameter setObject:btnPrice.titleLabel.text forKey:@"filter_price"];
        }
        
        if ([btnFloor.titleLabel.textColor isEqual:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0]]) {
            NSArray *arr = [[arrTitle objectAtIndex:indexSelected] componentsSeparatedByString:@" - "];
            [dictFilterParameter setObject:[arr firstObject] forKey:@"minfloor"];
            [dictFilterParameter setObject:[arr lastObject] forKey:@"maxfloor"];
        }
        
        if (dictFilterParameter.count) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kUnitFilter object:dictFilterParameter];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)didTappedFloorButton:(UIButton *)sender {
    
    [btnFloor setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
    indexSelected = sender.tag;
    for (UIView *temp in sender.superview.subviews) {
        
        if ([temp isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)temp;
            if (btn.tag == indexSelected) {
                btn.selected = YES;
            }
            else {
                btn.selected = NO;
            }
            
        }

    }
}

#pragma mark - Table View delegates 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strID];
        cell.textLabel.font = [UIFont fontWithName:@"PT Sans" size:12.0];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"High to low";
    }
    else if (indexPath.row == 1){
        cell.textLabel.text = @"Low to high";
    }
    else {
        cell.textLabel.text = @"Clear";
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblPrice) {
        if (indexPath.row == 0) {
            [btnPrice setTitle:@"Price: High to low" forState:UIControlStateNormal];
            [btnPrice setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 1) {
            [btnPrice setTitle:@"Price: Low to high" forState:UIControlStateNormal];
            [btnPrice setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        }
        else {
           [btnPrice setTitle:@"Price" forState:UIControlStateNormal];
            [btnPrice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
    else {
        if (indexPath.row == 0) {
           [btnFloor setTitle:@"Floor: High to low" forState:UIControlStateNormal];
           [btnFloor setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 1){
           [btnFloor setTitle:@"Floor: Low to high" forState:UIControlStateNormal];
           [btnFloor setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        }
        else {
           [btnFloor setTitle:@"Floor" forState:UIControlStateNormal];
           [btnFloor setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
    
//    if (tblFloor != nil) {
//        [tblFloor removeFromSuperview];
//        tblFloor = nil;
//    }
    
    
    if (tblPrice != nil) {
        [tblPrice removeFromSuperview];
        tblPrice = nil;
    }
}


#pragma mark - Touch delegates
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
//    if (tblFloor != nil) {
//        [tblFloor removeFromSuperview];
//        tblFloor = nil;
//    }
    
    
    if (tblPrice != nil) {
        [tblPrice removeFromSuperview];
        tblPrice = nil;
    }
}


- (nullable UIButton *)getFloorButtons:(CGRect)frame index:(NSUInteger)index andTitle:(NSString *)strTitle {
    
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    btn.tag = index;
    btn.layer.borderWidth = 0.5;
    btn.layer.borderColor = self.view.backgroundColor.CGColor;
    btn.clipsToBounds = YES;
    [btn setTitle:strTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    btn.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0];
    btn.titleLabel.font = [UIFont fontWithName:@"PT Sans" size:15.0];
    //didTappedFloorButton:
    [btn addTarget:self action:@selector(didTappedFloorButton:) forControlEvents:UIControlEventTouchUpInside];
    if ([strRange isEqualToString:strTitle]) {
        [btnFloor setTitleColor:[UIColor colorWithRed:0 green:128.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
        indexSelected = index;
        btn.selected = YES;
    }
    else {
        btn.selected = NO;
    }
    return btn;
}




@end
