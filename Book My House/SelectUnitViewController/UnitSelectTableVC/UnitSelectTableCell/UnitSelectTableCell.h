//
//  UnitSelectTableCell.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/22/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitSelectTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblArea;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPLC;
@property (weak, nonatomic) IBOutlet UILabel *lblPSF;
@property (weak, nonatomic) IBOutlet UILabel *lblLifestyle;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@end
