//
//  UnitSelectTableCell.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/22/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "UnitSelectTableCell.h"

@implementation UnitSelectTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
