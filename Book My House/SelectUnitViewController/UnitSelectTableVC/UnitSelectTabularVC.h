//
//  UnitSelectTabularVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 4/22/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnitSelectTableCell.h"
#import "Header.h"
#import "UnitDescriptionVC.h"

@interface UnitSelectTabular_VC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSArray *arrSourse;
@property (weak, nonatomic) UIViewController *owner;
@property (strong, nonatomic) NSString *strSelectedTitle;
@end