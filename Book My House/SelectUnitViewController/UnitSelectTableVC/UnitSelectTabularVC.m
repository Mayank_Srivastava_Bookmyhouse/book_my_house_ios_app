//
//  UnitSelectTabularVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 4/22/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "UnitSelectTabularVC.h"

static NSString * const strID = @"cell";
static NSString * const strLoginRequest                    = @"Please login in to select your favorite unit";


@interface UnitSelectTabular_VC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UITableView *tblView;
    NSUserDefaults *pref;
    NSInteger indexFavorite;
    
}
- (IBAction)actionListener:(id)sender;

@end

@implementation UnitSelectTabular_VC

#pragma mark - View Controller Mathods

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    pref = [NSUserDefaults standardUserDefaults];
    NSLog(@"Dict select %@", _arrSourse);
    [super viewDidLoad];
    [self screenSettings];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnBack) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didTappedFavorite:(UIButton *)sender {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if ([sender isKindOfClass:[UIButton class]]) {
            indexFavorite = sender.tag;
        }
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSString *strUserId = [pref objectForKey:strSaveUserId];
        if (strUserId) {
            [dict setObject:strUserId forKey:@"user_id"];
            [dict setObject:[[_arrSourse objectAtIndex:indexFavorite] objectForKey:@"unit_id"] forKey:@"unit_id"];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kFavoriteUnitFromUnitSelectTabular object:nil];
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI markAsFavorite:dict withCallback:^(BOOL isSuccess, id data) {
                [[AppDelegate share] enableUserInteraction];
                if (isSuccess) {
                    NSMutableArray *arr = [_arrSourse mutableCopy];
                    NSMutableDictionary *dict = [[_arrSourse objectAtIndex:indexFavorite] mutableCopy];
                    [dict setObject:@"1" forKey:@"user_favourite"];
                    
                    [arr replaceObjectAtIndex:indexFavorite withObject:dict];
                    
                    _arrSourse = [arr copy];
                    [tblView reloadData];
                    
                    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
                    for (UIViewController *controller in nav.viewControllers) {
                        
                        if ([controller isKindOfClass:[SelectUnitVC class]]) {
                            SelectUnitVC *obj = (SelectUnitVC *)controller;
                            //[obj loadSelectedUnitInfo];
                            [obj performSelector:@selector(loadSelectedUnitInfo) withObject:nil];
                            break;
                        }
                    }
                }
                else {
                    [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
                    
                }
            }];
            
        }
        else {
            [[[AppDelegate share] window] makeToast:strLoginRequest duration:duration position:CSToastPositionCenter];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTappedFavorite:) name:kFavoriteUnitFromUnitSelectTabular object:nil];
            
            SigninViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
            obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            obj.modalPresentationStyle = UIModalPresentationFormSheet;
            obj.option = SigninAccessOptionFavoriteSelectUnitTabular;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
            [self presentViewController:navigationController animated:YES completion:nil];
            
        }
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didTappedFavorite:) fromClass:self];
    }
}

#pragma mark - API Actions

#pragma mark - Table View Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrSourse.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 155.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UnitSelectTableCell *cell = [tableView dequeueReusableCellWithIdentifier:strID];
    NSDictionary *dict = [_arrSourse objectAtIndex:indexPath.row];
    
    NSString *str = [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"image_unit"]];
    [cell.imgBg setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
    
    if ([dict objectForKey:@"wpcf_flat_price"] != nil) {
        cell.lblTotal.text = [self setPriceRange:[dict objectForKey:@"wpcf_flat_price"]];
    }
    else {
        cell.lblTotal.text = [NSString stringWithFormat:@"1000"];
    }
    
    if ([dict objectForKey:@"wpcf_flat_size"] != nil) {
        cell.lblArea.text = [NSString stringWithFormat:@"%@ sq ft", [dict objectForKey:@"wpcf_flat_size"]];
    }
    else {
        cell.lblArea.text = [NSString stringWithFormat:@"-- sq ft"];
    }
    
    if ([dict objectForKey:@"wpcf_flat_unit_no"] != nil) {
        cell.lblUnitNumber.text = [NSString stringWithFormat:@"Unit no. %@", [dict objectForKey:@"wpcf_flat_unit_no"]];
    }
    else {
        cell.lblUnitNumber.text = [NSString stringWithFormat:@"Unit no. --"];
    }
    
    if ([dict objectForKey:@"wpcf_flat_floor"] != nil) {
        cell.lblFloor.text = [NSString stringWithFormat:@"Floor No. %@", [dict objectForKey:@"wpcf_flat_floor"]];
    }
    else {
        cell.lblFloor.text = [NSString stringWithFormat:@"Floor No. --"];
    }
    
    if ([dict objectForKey:@"wpcf_flat_price_plc"] != nil) {
        cell.lblPLC.text = [NSString stringWithFormat:@"PLC: \u20B9%@", [dict objectForKey:@"wpcf_flat_price_plc"]];
    }
    else {
        cell.lblPLC.text = [NSString stringWithFormat:@"PLC: --"];
    }
    
    if ([dict objectForKey:@"wpcf_flat_price_SqFt"] != nil) {
        cell.lblPSF.text = [NSString stringWithFormat:@"%@ psf", [dict objectForKey:@"wpcf_flat_price_SqFt"]];
    }
    else {
        cell.lblPSF.text = @"-- psf";
    }
    
    if ([dict objectForKey:@"lifestyle"] != nil) {

        cell.lblLifestyle.layer.cornerRadius = 5.0;
        cell.lblLifestyle.clipsToBounds = YES;
        cell.lblLifestyle.text = [NSString stringWithFormat:@"Lifestyle\n%@", [dict objectForKey:@"lifestyle"]];
    }
    else {
        cell.lblLifestyle.text = @"Lifestyle";
    }
    

    if (![[dict objectForKey:@"sold_status"] isEqualToString:@"not sold"]) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 155.0)];
        imgView.image = [UIImage imageNamed:@"Reserved_Image"];
        [cell addSubview:imgView];
        imgView.tag = 1001;
        imgView.backgroundColor = [UIColor colorWithRed:125.0/255.0 green:125.0/255.0 blue:125.0/255.0 alpha:0.6];
        
        [cell.contentView bringSubviewToFront:imgView];
    }
    else {
        UIImageView *imgView = [cell viewWithTag:1001];
        [imgView removeFromSuperview];
    }
    
    cell.btnFavourite.selected = [[dict objectForKey:@"user_favourite"] boolValue];
    cell.btnFavourite.tag = indexPath.row;
    [cell.btnFavourite addTarget:self action:@selector(didTappedFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lblLifestyle.layer.cornerRadius = 5.0;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dict = [_arrSourse objectAtIndex:indexPath.row];
    if ([[dict objectForKey:@"sold_status"] isEqualToString:@"not sold"]) {
        UnitDescriptionVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"UnitDescriptionVC"];
        obj.dictSourse = dict;
        obj.strPropertyCatagory = [NSString stringWithFormat:@"%@ (%@ sq ft)", _strSelectedTitle, [dict objectForKey:@"wpcf_flat_size"]];
        
        [_owner.navigationController pushViewController:obj animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
}


#pragma mark - Auxillary Mathods
- (NSString *)setPriceRange:(NSString *)str {
    CGFloat num = [str floatValue];
    NSString *strNum;
    CGFloat result;
    if (num >= 10000000.0) {
        result = (CGFloat)(num / 10000000.0);
        strNum = [NSString stringWithFormat:@"\u20B9 %0.2f Cr+", result];
    }
    else {
        result = (CGFloat)(num / 100000.0);
        strNum = [NSString stringWithFormat:@"\u20B9 %0.2f Lacs+", result];
    }
    
    return strNum;
}


- (void)formatLabel:(UILabel *)lbl withPrefix:(NSString *)strPrifix andString:(NSString *)str {
    
    
    
    NSMutableAttributedString *strPrefix = [[NSMutableAttributedString alloc] initWithString:strPrifix attributes:@{
                                                                                                                    
                                                                                                                    NSForegroundColorAttributeName : [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0],
                                                                                                                    NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:16.0]
                                                                                                                    }];
    NSAttributedString *strData = [[NSAttributedString alloc] initWithString:str attributes:@{
                                                                                            
                                                                                              NSForegroundColorAttributeName : [UIColor lightGrayColor],
                                                                                              NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:15.0]
                                                                                              }];
    
    [strPrefix appendAttributedString:strData];
    
    lbl.attributedText = strPrefix;    
}

@end
