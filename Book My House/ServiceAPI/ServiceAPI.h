//
//  ServiceAPI.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/2/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
#import "FileDownloadInfo.h"

typedef NS_ENUM(NSUInteger, GallaryOptions) {
    GallaryOptionPhoto,
    GallaryOptionVideo,
    GallaryOptionConstructionUpdate,
    GallaryOptionVirtualSiteVisit,
    GallaryOption360Tour,
};

typedef void(^GetPrivacyPolicy)(BOOL isSuccess, id data);
typedef void(^GetSocialLoginCredentials)(BOOL isSuccess, NSDictionary *responseData, NSError *error);
typedef void(^GetAllCities)(BOOL isSuccess, id cities);
typedef void(^GetAllBuilders)(BOOL isSuccess, id builder);
typedef void(^GetAllProjects)(BOOL isSuccess, id projects);
typedef void(^GetFeaturedBuilder)(BOOL isSuccess, id featuredBuilders);
typedef void(^GetFeaturedProjects)(BOOL isSuccess, id featuredProject);
typedef void(^GetCatagories)(BOOL isSuccess, id specialCatagories);

typedef void(^GetListOfProjects)(BOOL isSuccess, id projects);
typedef void(^GetAboutUsCallBack)(BOOL isSuccess, id data);
typedef void(^GetProjectDetails)(BOOL isSuccess, id data);
typedef void(^GetProjectPDF)(BOOL isSuccess);
typedef void(^GetSearchedPropertiesDetails)(BOOL isSuccess, id data);
typedef void(^GetLocalityMapInfo)(BOOL isSuccess, id data);
typedef void(^GetProjectMapInfo)(BOOL isSuccess, id data);
typedef void(^SetAlertsForProperty)(BOOL isSuccess, id data);

typedef void(^GetProjectList)(BOOL isSuccess, id data, id legands, id locationCordinates);
typedef void(^GetProjectsByLocation)(BOOL isSuccess, id data);
typedef void(^GetGooglePlaceAPIInfo)(BOOL isSuccess, id data);
typedef void(^GetSelectedUnitInfo)(BOOL isSuccess, id data);
typedef void(^GetUnitDetails)(BOOL isSuccess, id data);
typedef void(^MarkFavorite)(BOOL isSuccess, id data);
typedef void(^PostAComment)(BOOL isSuccess, id data);
typedef void(^GetAllReviews)(BOOL isSuccess, id data);
typedef void(^GetPriceTrends)(BOOL isSuccess, id data);
typedef void(^GetFavouriteList)(BOOL isSuccess, id data);
typedef void(^GetUnitMapImage)(UIImage *image);
typedef void(^GetPersonalInformation)(BOOL isSuccess, id data);
typedef void(^ShowPersonalInfo)(BOOL isSuccess, id data);
typedef void(^ShowPaymentForm)(BOOL isSuccess, id data);
typedef void(^ShowCCAvenueForm)(BOOL isSuccess, id data);
typedef void(^GetProjectAlbum)(BOOL isSuccess, id data);

typedef void(^GetSuggestiveSearchResponse)(BOOL success, id data);
typedef void(^GetPasswordReset)(BOOL success, id data);
typedef void(^GetEnquiryInfo)(BOOL isSuccess, id data);
typedef void(^BookASiteVisit)(BOOL isSuccess, id data);
typedef void(^ContactUsInfo)(BOOL isSuccess, id data);
typedef void(^GetAlertInfo)(BOOL isSuccess, id data);
typedef void(^GetContactBMHInfo)(BOOL isSucess, id data);
typedef void(^GetShortestRoute)(BOOL isSuccess, id data);
typedef void(^GetCatagoryProjects)(BOOL isSuccess, id data, id legands, id locationCordinates);

@interface ServiceAPI : NSObject
+ (void)getPrivacyPolicyWithUrl:(NSString *)strURL withCallBach:(GetPrivacyPolicy)info;
+ (void)signInUserWithParams:(NSDictionary *)param mathodName:(NSString *)strMathod baseClass:(id)callingClass andCallingClass:(SEL)callBack;
+ (void)signUpWithParams:(NSDictionary *)params mathodName:(NSString *)strMathod fromCallingClass:(id)callingClass andCallback:(SEL)callBack;
+ (void)loginWithSocialMedia:(NSDictionary *)params andCallback:(GetSocialLoginCredentials)info;

+ (void)getInfoByCityWithCallBack:(GetAllCities)info;
+ (void)getInfoByBuilderWithCallBack:(GetAllBuilders)info;
+ (void)getInfoByProjectWithCallBack:(GetAllProjects)info;
+ (void)getInfoForFeaturedBuilder:(NSDictionary *)params withCallBack:(GetFeaturedBuilder)info;
+ (void)getInfoForFeatureProjects:(NSDictionary *)params withCallBack:(GetFeaturedProjects)info;
+ (void)getCatagories:(NSDictionary *)params withCallback:(GetCatagories)info;


+ (void)getListOfProjectWithBuilderId:(NSString *)strId CallBack:(GetListOfProjects)info;
+ (void)getListOfProjectWith:(NSDictionary *)params callback:(GetListOfProjects)info;




+ (void)contactBookmyhouse:(NSDictionary *)params fromCallingclass:(id)callingClass andCallback:(SEL)callBack;
+ (void)getAboutUsWithCallBack:(GetAboutUsCallBack)info;


//+ (void)getProjectInformationWithProjectId:(NSString *)strId CallBack:(GetProjectDetails)info;
+ (void)getProjectInformationWithParam:(NSDictionary *)params CallBack:(GetProjectDetails)info;
+ (void)getSearchedPropertyInfoWithParams:(NSDictionary *)params fromCallingClass:(id)callingClass withSelector:(SEL)callback;
+ (void)getSuggestiveSearchResponse:(NSDictionary *)params andCallback:(GetSuggestiveSearchResponse)info;


+ (void)setAlertForCurrentLocation:(NSDictionary *)params withCallback:(SetAlertsForProperty)info;
+ (void)getProjectList:(NSDictionary *)params withCallBack:(GetProjectList)info;
+ (void)getProjectByLocationAndFilter:(NSDictionary *)params withCallback:(GetProjectsByLocation)info;
+ (void)getGooglePlaceAPIResponse:(NSDictionary *)params withCallback:(GetGooglePlaceAPIInfo)info;
+ (void)getSelectUnitInfo:(NSDictionary *)params withCallback:(GetSelectedUnitInfo)info;
+ (void)getUnitDetail:(NSDictionary *)params withCallback:(GetUnitDetails)info;
+ (void)markAsFavorite:(NSDictionary *)params withCallback:(MarkFavorite)info;
+ (void)postAComment:(NSDictionary *)params withCallback:(PostAComment)info;
+ (void)getAllReviewWith:(NSDictionary *)params withCallback:(GetAllReviews)info;
+ (void)getPriceTrends:(NSArray *)params withCallback:(GetPriceTrends)info;
+ (void)fetchFavouriteList:(NSDictionary *)params withCallback:(GetFavouriteList)info;
+ (void)getUnitMap:(NSDictionary *)params withCallback:(GetUnitMapImage)info;
+ (void)setPersonalInformation:(NSDictionary *)params withCallback:(GetPersonalInformation)info;
+ (void)showUserInfo:(NSDictionary *)params withCallback:(ShowPersonalInfo)info;
+ (void)postPaymentForm:(NSDictionary *)params withCallback:(ShowPaymentForm)info;
+ (void)postCCAvenueForm:(NSDictionary *)param withCallback:(ShowCCAvenueForm)info;
+ (void)getProjectAlbum:(NSDictionary *)params andCallback:(GetProjectAlbum)info;
+ (void)getForgetPasswordInformation:(NSDictionary *)params  andCallback:(GetPasswordReset)info;
+ (void)postEnquiry:(NSDictionary *)params andCallback:(GetEnquiryInfo)info;
+ (void)bookSiteVisit:(NSDictionary *)params andCallback:(BookASiteVisit)info;
+ (void)contactUs:(NSDictionary *)params andCallback:(ContactUsInfo)info;
+ (void)postAlert:(NSDictionary *)params andCalback:(GetAlertInfo)info;
+ (void)getBMHContactInfo:(GetContactBMHInfo)info;
+ (void)getGoogleMapShortestRoute:(NSDictionary *)params withCallback:(GetShortestRoute)info;
+ (void)getLocalityInfoForMap:(NSDictionary *)params withCallback:(GetLocalityMapInfo)info;
+ (void)getProjectInfoForMap:(NSDictionary *)params withCallback:(GetProjectMapInfo)info;

+ (void)getProjectByCatagories:(NSDictionary *)params withCallback:(GetCatagoryProjects)info;
@end

@interface BackgroundDownloadServices : NSObject<NSURLSessionDelegate>
@property (strong, nonatomic) NSURLSession *session;
@property (strong, nonatomic) NSURL *docDirectoryURL;
- (void)getPDFFromUrl:(FileDownloadInfo *)fdi andCallBack:(GetProjectPDF)info;
@end

