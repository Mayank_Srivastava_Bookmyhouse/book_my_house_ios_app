//
//  ServiceAPI.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/2/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ServiceAPI.h"



@implementation ServiceAPI

+ (void)getPrivacyPolicyWithUrl:(NSString *)strURL withCallBach:(GetPrivacyPolicy)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionDataTask *task = [manager POST:strURL
                                    parameters:nil
                                      progress:nil
                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                           NSLog(@"Success::: \n%@", responseObject);
                                           
                                           info(YES, [[responseObject objectForKey:@"data"] objectForKey:@"content"]);
                                           
                                           
                                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                           
                                           info(NO, nil);
                                           
                                       }];
    
    
    
    
    [task resume];

    
}

+ (void)signInUserWithParams:(NSDictionary *)param
                  mathodName:(NSString *)strMathod
                   baseClass:(id)callingClass
             andCallingClass:(SEL)callBack {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionDataTask *task = [manager POST:[NSString stringWithFormat:@"%@%@.php",baseUrl, strMathod]
                                    parameters:param
                                      progress:nil
                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                           NSLog(@"Success::: \n%@", responseObject);
                                           
                                           if ([callingClass respondsToSelector:callBack]) {
                                            
                                               [callingClass performSelector:callBack withObject:responseObject afterDelay:0.0];
                                           }
                                           
                                           
                                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                           NSLog(@"Failure::: \n%@", error.localizedDescription);
                                           
                                           
                                           
                                       }];
    
    
    

    [task resume];
    
    
}

+ (void)loginWithSocialMedia:(NSDictionary *)params andCallback:(GetSocialLoginCredentials)info {
   
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionDataTask *task = [manager POST:[NSString stringWithFormat:@"%@%@.php",baseUrl, url_Third_Party_Login]
                                    parameters:params
                                      progress:nil
                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                           NSLog(@"Success::: \n%@", responseObject);
                                           
                                           if (![responseObject isKindOfClass:[NSNull class]]) {
                                               
                                               if (![[responseObject objectForKey:@"success"] boolValue] && [[responseObject objectForKey:@"message"] isEqualToString:@"Either username or password is not correct"]) {
                                                   
                                                   
                                                   AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                                                   manager.responseSerializer = [AFJSONResponseSerializer serializer];
                                                   manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
                                                   
                                                   NSURLSessionDataTask *task = [manager POST:[NSString stringWithFormat:@"%@%@.php",baseUrl, urlRegister]
                                                                                   parameters:params
                                                                                     progress:nil
                                                                                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                                                          NSLog(@"Success::: \n%@", responseObject);
                                                                                          
                                                                                          [self loginWithSocialMedia:params andCallback:^(BOOL isSuccess, NSDictionary *responseData, NSError *error) {
                                                                                               info(YES, responseObject, nil);
                                                                                          }];
                                                                                         
                                                                                          
                                                                                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                                                          NSLog(@"Failure::: \n%@", error.localizedDescription);
                                                                                          
                                                                                          info(NO, nil, error);
                                                                                          
                                                                                      }];
                                                   
                                                   [task resume];
                                                   
                                               }
                                               else if ([[responseObject objectForKey:@"success"] boolValue] && [[responseObject objectForKey:@"message"] isEqualToString:@"Successfully logged in"]) {
                                                   info(YES, responseObject, nil);
                                               }
                                               
                                           }
                                           
                                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                           NSLog(@"Failure::: \n%@", error.localizedDescription);
                                           
                                           info(NO, nil, error);
                                           
                                       }];
    
    
    
    
    [task resume];
    
}





+ (void)signUpWithParams:(NSDictionary *)params
              mathodName:(NSString *)strMathod
        fromCallingClass:(id)callingClass
             andCallback:(SEL)callBack {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionDataTask *task = [manager POST:[NSString stringWithFormat:@"%@%@.php",baseUrl, strMathod]
                                    parameters:params
                                      progress:nil
                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                           NSLog(@"Success::: \n%@", responseObject);
                                           
                                           if ([callingClass respondsToSelector:callBack]) {
                                               
                                               [callingClass performSelector:callBack withObject:responseObject afterDelay:0.0];
                                           }
                                           
                                           
                                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                           NSLog(@"Failure::: \n%@", error.localizedDescription);
                                           
                                           
                                           
                                       }];
    
    
    
    
    [task resume];

    
}


+ (void)getInfoByCityWithCallBack:(GetAllCities)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@.php",baseUrl, urlGetCities]
                               parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      NSLog(@"Success::: \n%@", responseObject);
                                      info(YES, responseObject);
                                      
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      NSLog(@"Failure::: \n%@", error.localizedDescription);
                                      info (NO, nil);
                                  }];
    
    
    [task resume];
    
}


+ (void)getInfoByProjectWithCallBack:(GetAllProjects)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@.php",baseUrl, urlGet_All_projects]
                               parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      NSLog(@"Success::: \n%@", responseObject);
                                      info (YES, responseObject);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      NSLog(@"Failure::: \n%@", error.localizedDescription);
                                      info (NO, nil);
                                  }];
    
    
    [task resume];
    
}


+ (void)getInfoByBuilderWithCallBack:(GetAllBuilders)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@.php",baseUrl, urlGet_All_Builders]
                               parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      NSLog(@"Success::: \n%@", responseObject);
                                      info (YES, responseObject);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      NSLog(@"Failure::: \n%@", error.localizedDescription);
                                      info (NO, nil);
                                  }];
    
    
    [task resume];
}


+ (void)getInfoForFeaturedBuilder:(NSDictionary *)params withCallBack:(GetFeaturedBuilder)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@.php?city_id=%@",baseUrl, url_Featured_Developers, [params objectForKey:@"city_id"]]
                               parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      
                                      info (YES, [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      
                                      info (NO, nil);
                                  }];
    
    
    [task resume];
    
}

+ (void)getInfoForFeatureProjects:(NSDictionary *)params withCallBack:(GetFeaturedProjects)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@%@",baseUrl, url_Featured_Projects, extension]
                               parameters:params
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info (YES, [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info (NO, nil);
                                  }];
    
    
    [task resume];

}

+ (void)getCatagories:(NSDictionary *)params withCallback:(GetCatagories)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@",baseUrl, url_Special_Projects, extension]
                               parameters:params
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info (YES, [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info (NO, nil);
                                  }];
    
    
    [task resume];

}


+ (void)getListOfProjectWithBuilderId:(NSString *)strId CallBack:(GetListOfProjects)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@.php?builder_id=%@",baseUrl, urlSearchProject,strId]
                               parameters:nil progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      NSLog(@"Success::: \n%@", responseObject);
                                      info(YES, [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      NSLog(@"Failure::: \n%@", error.localizedDescription);
                                      info(NO, nil);
                                  }];
    
    
    
    [task resume];
}

+ (void)getListOfProjectWithBuilderParams:(NSDictionary *)params callBack:(GetListOfProjects)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *key in params) {
        
        if ([key isEqualToString:@"filter"]) {
            for (NSString *key in [[params objectForKey:@"filter"] allKeys]) {
                [dict setObject:[[params objectForKey:@"filter"] objectForKey:key] forKey:key];
            }
        }
        else {
            [dict setObject:[params objectForKey:key] forKey:key];
        }
    }
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@",baseUrl, urlSearchProject, extension]
                  parameters:dict
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         info(YES, [responseObject objectForKey:@"data"]);
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         info(NO, nil);
                     }];
    [task resume];
}

+ (void)getListOfProjectWith:(NSDictionary *)params callback:(GetListOfProjects)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task;
    
    if (![params objectForKey:@"filter"]) {
        
        task = [manager GET:[NSString stringWithFormat:@"%@%@.php?builder_id=%@&property_type=%@&type=%@",baseUrl, urlSearchProject,[params objectForKey:@"builder_id"], @"flat", [params objectForKey:@"type"]]
                 parameters:nil progress:nil
                    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        NSLog(@"Success::: \n%@", responseObject);
                        info(YES, [responseObject objectForKey:@"data"]);
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        NSLog(@"Failure::: \n%@", error.localizedDescription);
                        info(NO, nil);
                    }];
    }
    else {
        task = [manager POST:[NSString stringWithFormat:@"%@%@.php?builder_id=%@&property_type=%@&type=%@",baseUrl, urlSearchProject,[params objectForKey:@"builder_id"], @"flat", [params objectForKey:@"type"]]
                  parameters:[params objectForKey:@"filter"]
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         info(YES, [responseObject objectForKey:@"data"]);
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         info(NO, nil);
                     }];
    }

    [task resume];
}



+ (void)contactBookmyhouse:(NSDictionary *)params fromCallingclass:(id)callingClass andCallback:(SEL)callBack {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
//    NSURLSessionTask *task = [manager POST:<#(nonnull NSString *)#>
//                                parameters:<#(nullable id)#>
//                                  progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                                      
//                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                                      
//                                  }];
    
    
//    [task resume];
}


+ (void)getAboutUsWithCallBack:(GetAboutUsCallBack)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@.php",baseUrl, url_About_us]
                               parameters:nil progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      NSLog(@"Success::: \n%@", responseObject);
                                      info(YES, [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      NSLog(@"Failure::: \n%@", error.localizedDescription);
                                      info(NO, nil);
                                  }];
    
    
    [task resume];
}

+ (void)getProjectInformationWithProjectId:(NSString *)strId CallBack:(GetProjectDetails)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@.php?id=%@",baseUrl, url_Project_detail,strId]
                                parameters:nil
                 constructingBodyWithBlock:nil
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       NSLog(@"Success::: \n%@", responseObject);
                                       info(YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       NSLog(@"Failure::: \n%@", error.localizedDescription);
                                       info(NO, nil);
                                   }];
    
    
    [task resume];

}

+ (void)getProjectInformationWithParam:(NSDictionary *)params CallBack:(GetProjectDetails)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@",baseUrl, url_Project_detail,extension]
                                parameters:params
                 constructingBodyWithBlock:nil
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       NSLog(@"Success::: \n%@", responseObject);
                                       info(YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       NSLog(@"Error Domain:\t%@ \n Error Code:\t%d \n User info:\t%@\n Localise Sugestion\t%@\n Recovert Options:\t%@\n Help Anchor:\t%@\n", error.domain, (int)error.code, error.userInfo, error.localizedRecoverySuggestion, error.localizedRecoveryOptions, error.helpAnchor);
                                       info(NO, nil);
                                   }];
    
    
    [task resume];
}



+ (void)getSearchedPropertyInfoWithParams:(NSDictionary *)params fromCallingClass:(id)callingClass withSelector:(SEL)callback {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_get_locations, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                  NSLog(@"Sucess:::\n %@", responseObject);
        
                                if ([callingClass respondsToSelector:callback]) {
                                    [callingClass performSelector:callback withObject:responseObject afterDelay:0.0];
                                }
                                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                NSLog(@"Failiure:::\n %@", error.localizedDescription);
                                }];
   
    [task resume];
}

+ (void)getLocalityInfoForMap:(NSDictionary *)params withCallback:(GetLocalityMapInfo)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_heatmap, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, responseObject);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    [task resume];
    
}

+ (void)getProjectInfoForMap:(NSDictionary *)params withCallback:(GetProjectMapInfo)info {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *key in params) {
        
        if ([key isEqualToString:@"filter"]) {
            for (NSString *key1 in [params objectForKey:@"filter"]) {
                [dict setObject:[[params objectForKey:@"filter"] objectForKey:key1] forKey:key1];
            }
        }
        else {
           [dict setObject:[params objectForKey:key] forKey:key];
        }
    }
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, urlSearchProject, extension]
                                parameters:dict
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, responseObject);
                                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                    info(NO, nil);
                                }];
    
    [task resume];
    
}

+ (void)setAlertForCurrentLocation:(NSDictionary *)params withCallback:(SetAlertsForProperty)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_get_alerts, extension]
                                parameters:params
                 constructingBodyWithBlock:nil
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, responseObject);
                 }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    
    [task resume];
}

+ (void)getProjectList:(NSDictionary *)params withCallBack:(GetProjectList)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *key in params) {
        
        if ([key isEqualToString:@"filter"]) {
            for (NSString *key in [[params objectForKey:@"filter"] allKeys]) {
                [dict setObject:[[params objectForKey:@"filter"] objectForKey:key] forKey:key];
            }
        }
        else {
            [dict setObject:[params objectForKey:key] forKey:key];
        }
    }
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, urlSearchProject, extension ]
                                parameters:dict
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info (YES, [responseObject objectForKey:@"data"], [responseObject objectForKey:@"legend"], [responseObject objectForKey:@"city_location"]);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info (NO, nil, nil, nil);
                                   }];
    
    [task resume];
}



+ (void)getProjectByCatagories:(NSDictionary *)params withCallback:(GetCatagoryProjects)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *key in params) {
        
        if ([key isEqualToString:@"filter"]) {
            for (NSString *key in [[params objectForKey:@"filter"] allKeys]) {
                [dict setObject:[[params objectForKey:@"filter"] objectForKey:key] forKey:key];
            }
        }
        else {
            [dict setObject:[params objectForKey:key] forKey:key];
        }
    }
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, urlSearchProject, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info (YES, [responseObject objectForKey:@"data"], [responseObject objectForKey:@"legend"], [responseObject objectForKey:@"city_location"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info (NO, nil, nil, nil);
                                   }];
    
    [task resume];
    
//    if (![params objectForKey:@"filter"]) {
//        
//        NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, urlSearchProject, extension]
//                                    parameters:params
//                                      progress:nil
//                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                                           info (YES, [responseObject objectForKey:@"data"], [responseObject objectForKey:@"legend"], [responseObject objectForKey:@"city_location"]);
//                                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                                           info (NO, nil, nil, nil);
//                                       }];
//        
//        [task resume];
//    }
//    else {
//        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//        [dict setObject:[params objectForKey:@"city_id"] forKey:@"city_id"];
//        [dict setObject:[params objectForKey:@"special_category"] forKey:@"special_category"];
//        
//        for (NSString *key in [[params objectForKey:@"filter"] allKeys]) {
//            [dict setObject:[[params objectForKey:@"filter"] objectForKey:key] forKey:key];
//        }
//        
//        NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, urlSearchProject, extension]
//                                    parameters:dict
//                                      progress:nil
//                                       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                                           info (YES, [responseObject objectForKey:@"data"], [responseObject objectForKey:@"legend"], [responseObject objectForKey:@"city_location"]);
//                                       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                                           info (NO, nil, nil, nil);
//                                       }];
//        
//        [task resume];
//    }
    
}

+ (void)getProjectByLocationAndFilter:(NSDictionary *)params withCallback:(GetProjectsByLocation)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, urlSearchProject, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info (YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info (NO, nil);
                                   }];
    
    [task resume];
}

+ (void)getGooglePlaceAPIResponse:(NSDictionary *)params withCallback:(GetGooglePlaceAPIInfo)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@location=%@&type=%@&radius=%@&key=%@&sensor=false", strGooglePlaceAPIUrl, [params objectForKey:@"location"], [params objectForKey:@"type"], [params objectForKey:@"radius"], strGooglePlaceAPIKey ]
                               parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info (YES, [responseObject objectForKey:@"results"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info (NO, nil);
                                  }];
    [task resume];

}



+ (void)getSelectUnitInfo:(NSDictionary *)params withCallback:(GetSelectedUnitInfo)info {
    
    if (![params objectForKey:@"minfloor"]) {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSURLSessionTask *task =[manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_unit_list_by_proj_id, extension]
                                   parameters:params
                                     progress:nil
                                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                          info(YES, [responseObject objectForKey:@"data"]);
                                      }
                                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                          info(NO, nil);
                                      }];
        [task resume];
    }
    else {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[params objectForKey:@"project_id"] forKey:@"project_id"];
        [dict setObject:[params objectForKey:@"unit_ids"] forKey:@"unit_ids"];
        
        if ([params objectForKey:@"user_id"]) {
            [dict setObject:[params objectForKey:@"user_id"] forKey:@"user_id"];
        }
        
        
        NSURLSessionTask *task =[manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_unit_list_by_proj_id, extension]
                                   parameters:params
                                     progress:nil
                                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                          info(YES, [responseObject objectForKey:@"data"]);
                                      }
                                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                          info(NO, nil);
                                      }];
        [task resume];
    }
    
    
}



+ (void)getUnitDetail:(NSDictionary *)params withCallback:(GetUnitDetails)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSString *strUrl;
    if ([params objectForKey:@"user_id"] == nil) {
        strUrl = [NSString stringWithFormat:@"%@%@%@?id=%@", baseUrl, url_unit_detail, extension,[params objectForKey:@"unit_id"]];
    }
    else {
        strUrl = [NSString stringWithFormat:@"%@%@%@?id=%@&user_id=%@", baseUrl, url_unit_detail, extension,[params objectForKey:@"unit_id"], [params objectForKey:@"user_id"]];
    }
    NSURLSessionTask *task = [manager GET:strUrl
                               parameters:params
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info(YES, [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info(NO, nil);
                                  }];
    
    [task resume];
}


+ (void)getUnitMap:(NSDictionary *)params withCallback:(GetUnitMapImage)info {
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%@&img=%@", baseImageUrl, extension, [params objectForKey:@"width"], [params objectForKey:@"image_unit"]];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:strUrl]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *img = [[UIImage alloc] initWithData:data];
            info(img);
        });
    });
}





+ (void)markAsFavorite:(NSDictionary *)params withCallback:(MarkFavorite)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_add_favorite,extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info([[responseObject objectForKey:@"success"] boolValue], responseObject);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    
    [task resume];
}

+ (void)postAComment:(NSDictionary *)params withCallback:(PostAComment)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_add_comment,extension]
                                 parameters:params
                                   progress:nil
                                    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                        info(YES, responseObject);
                                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                        info(NO, nil);
                                    }];
                       
    [task resume];
    
}


+ (void)getAllReviewWith:(NSDictionary *)params withCallback:(GetAllReviews)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_get_all_comments,extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    [task resume];
}


+ (void)getPriceTrends:(NSArray *)params withCallback:(GetPriceTrends)info {
    
}


+ (void)fetchFavouriteList:(NSDictionary *)params withCallback:(GetFavouriteList)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_get_user_favorites,extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    [task resume];
}


+ (void)setPersonalInformation:(NSDictionary *)params withCallback:(GetPersonalInformation)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_Save_Info, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info(YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    [task resume];
    
}

+ (void)showUserInfo:(NSDictionary *)params withCallback:(ShowPersonalInfo)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_Show_User_Info, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, [responseObject objectForKey:@"data"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    [task resume];

    
}


+ (void)postPaymentForm:(NSDictionary *)params withCallback:(ShowPaymentForm)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@?action=payment_form", baseUrl, url_Payment_API, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, responseObject);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    [task resume];
    
}

+ (void)postCCAvenueForm:(NSDictionary *)params withCallback:(ShowCCAvenueForm)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@?action=payment_form_redirect", baseUrl, url_Payment_API, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, responseObject);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    [task resume];
}

+ (void)getProjectAlbum:(NSDictionary *)params andCallback:(GetProjectAlbum)info {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_project_album, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info([responseObject objectForKey:@"success"], [responseObject objectForKey:@"data"]);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO, nil);
                                   }];
    
    [task resume];
    
}


+ (void)getSuggestiveSearchResponse:(NSDictionary *)params andCallback:(GetSuggestiveSearchResponse)info {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_Suggestive_Search, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info(YES, [responseObject objectForKey:@"data"]);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO,nil);
                                   }];
    
    [task resume];
}

+ (void)getForgetPasswordInformation:(NSDictionary *)params  andCallback:(GetPasswordReset)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_forget_password, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info(YES, [responseObject objectForKey:@"message"]);
                                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO,nil);
                                   }];
    
    [task resume];
    
}


+ (void)postEnquiry:(NSDictionary *)params andCallback:(GetEnquiryInfo)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_Enquiry, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info([[responseObject objectForKey:@"success"] boolValue], [responseObject objectForKey:@"message"]);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO,nil);
                                   }];
    
    [task resume];
    
}

+ (void)bookSiteVisit:(NSDictionary *)params andCallback:(BookASiteVisit)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_site_visit, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info([[responseObject objectForKey:@"success"] boolValue], [responseObject objectForKey:@"message"]);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO,nil);
                                   }];
    
    
    [task resume];
}

+ (void)contactUs:(NSDictionary *)params andCallback:(ContactUsInfo)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_Contact_Us, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                       info([[responseObject objectForKey:@"success"] boolValue], [responseObject objectForKey:@"message"]);
                                   }
                                   failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                       info(NO,nil);
                                   }];
    
    
    [task resume];

    
}


+ (void)postAlert:(NSDictionary *)params andCalback:(GetAlertInfo)info {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager POST:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_get_alerts, extension]
                                parameters:params
                                  progress:nil
                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                    info([[responseObject objectForKey:@"success"] boolValue], [responseObject objectForKey:@"message"]);
                                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                    info(NO,nil);
                                }];
    
    [task resume];
}

+ (void)getBMHContactInfo:(GetContactBMHInfo)info {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@%@%@", baseUrl, url_BMH_Contact_Info, extension] parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      //url_BMH_Contact_Info
                                      info([[responseObject objectForKey:@"success"] boolValue], [responseObject objectForKey:@"data"]);
                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info(NO,nil);
                                  }];
    
    
    [task resume];
}

+ (void)getGoogleMapShortestRoute:(NSDictionary *)params withCallback:(GetShortestRoute)info {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    NSURLSessionTask *task = [manager GET:[NSString stringWithFormat:@"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@", strGoogleDirectionUrl, [[params valueForKeyPath:@"origin.latitude"] floatValue], [[params valueForKeyPath:@"origin.longitude"] floatValue], [[params valueForKeyPath:@"destination.latitude"] floatValue], [[params valueForKeyPath:@"destination.longitude"] floatValue], strGoogleDirectionAPIKey ]
                               parameters:nil
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                      info(YES, responseObject);
                                  }
                                  failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                      info(NO, nil);
                                  }];
    
    
    [task resume];
    
    
}

@end


@interface BackgroundDownloadServices () {
    GetProjectPDF localInfo;
}

@end
@implementation BackgroundDownloadServices

- (void)getPDFFromUrl:(FileDownloadInfo *)fdi andCallBack:(GetProjectPDF)info {
    
    localInfo = info;
    self.docDirectoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];

    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.BGTransferDemo"];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:nil];
    
    if (!fdi.isDownloading) {
        
        if (fdi.taskIdentifier == -1) {
            fdi.downloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:fdi.downloadSource]];
            fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
            [fdi.downloadTask resume];
        }
        else {
            
        }
    }
    else {
        [fdi.downloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
            if (resumeData != nil) {
                fdi.taskResumeData = [[NSData alloc] initWithData:resumeData];
            }
        }];
    }
    
    fdi.isDownloading = !fdi.isDownloading;

}

#pragma mark - NSURLSession Delegate method implementation

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {//2

    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *strDestinationFileName = downloadTask.originalRequest.URL.lastPathComponent;
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:strDestinationFileName];
    
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager removeItemAtURL:destinationURL error:nil];
    }
    
    [fileManager copyItemAtURL:location
                        toURL:destinationURL
                        error:&error];
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {//3
    if (error != nil) {
        localInfo(NO);
    }
    else{
        localInfo(YES);
    }
}


//-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
//    NSLog(@"Downloading on the way..........");
//    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) {
//        
//    }
//    else {
//        
//    }
//}


-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    AppDelegate *appDelegate = [AppDelegate share];
    // Check if all download tasks have been finished.
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        if ([downloadTasks count] == 0) {
            if (appDelegate.backgroundTransferCompletionHandler != nil) {
                // Copy locally the completion handler.
                void(^completionHandler)() = appDelegate.backgroundTransferCompletionHandler;
                
                // Make nil the backgroundTransferCompletionHandler.
                appDelegate.backgroundTransferCompletionHandler = nil;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Call the completion handler to tell the system that there are no other background transfers.
                    completionHandler();
                    
                    // Show a local notification when all downloads are over.
                    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                    localNotification.alertBody = @"All files have been downloaded!";
                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                }];
            }
        }
    }];
}

@end
