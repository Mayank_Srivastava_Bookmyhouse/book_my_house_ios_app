//
//  ResetVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 7/20/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "ResetVC.h"
#import "Header.h"
#import "CustomView.h"

@interface ResetVC () {
    
    __weak IBOutlet UILabel *lblCaption;
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UIButton *btnSubmit;
    __weak IBOutlet UIButton *btnBack;
    
    ForgotPasswordView *viewForgotPassword;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation ResetVC

#pragma mark - View Controller Mathods

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self screenSettings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - Screen Settings 
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    lblCaption.text = @"Please send us your registered e-mail id,\nWe will send you the reset-password link there.";
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtEmail.frame) - 2, CGRectGetWidth(txtEmail.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtEmail.layer addSublayer:layer];
    
    
    btnSubmit.layer.borderWidth = 2.0;
    btnSubmit.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    
}


#pragma mark - Action Listener
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnSubmit) {
        [self sendRestPasswod];
        
    }
}



- (void)didTappedOKButton:(UIButton *)btn {
    
    [UIView animateWithDuration:1.0 animations:^{
        viewForgotPassword.frame = CGRectMake(2 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    } completion:^(BOOL finished) {
       
        
        if (viewForgotPassword.isSuccess) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kPrefillEmail object:txtEmail.text];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            
        }
        [viewForgotPassword removeFromSuperview];
        viewForgotPassword = nil;
        
    }];
    
}

- (void)sendRestPasswod {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        if (txtEmail.text.length == 0) {
            [self.view makeToast:@"Please fill your email id" duration:duration position:CSToastPositionCenter];
            return;
        }
        
        if (![AuxillaryMathods isValidEmail:txtEmail.text]) {
            [self.view makeToast:@"Please fill your correct email id" duration:duration position:CSToastPositionCenter];
            return;
        }
        
        NSString *str = txtEmail.text;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:str forKey:@"email"];
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getForgetPasswordInformation:dict andCallback:^(BOOL success, id data) {
            [[AppDelegate share] enableUserInteraction];
            [txtEmail resignFirstResponder];
            if (success) {
                
                if (!viewForgotPassword) {
                    viewForgotPassword = [ForgotPasswordView createPassword];
                    
                    viewForgotPassword.frame = CGRectMake(-1 * CGRectGetWidth(self.view.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                    
                    [self.view addSubview:viewForgotPassword];
                    viewForgotPassword.lblInfo.text = data;
                  
                    if ([data isEqualToString:@"This E-Mail Id is not registered with us"]) {
                        viewForgotPassword.isSuccess = NO;
                                            }
                                            else if ([data isEqualToString:@"Reset Password Link has been sent on your Registered Email Id"]) {
                                                viewForgotPassword.isSuccess = YES;
                                            }
                    
                    [viewForgotPassword.btnOK addTarget:self action:@selector(didTappedOKButton:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [UIView animateWithDuration:1.0 animations:^{
                        viewForgotPassword.frame = self.view.frame;
                    }];
                }
            }
            else {
                
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(sendRestPasswod) fromClass:self];
    }
    
}
@end
