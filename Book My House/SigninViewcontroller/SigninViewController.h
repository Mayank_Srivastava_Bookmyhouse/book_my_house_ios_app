//
//  SigninViewController.h
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Header.h"
#import "UIView+Toast.h"
#import "SearchViewController.h"
#import "PaymentVC.h"
#import "RegisterViewController.h"

typedef NS_ENUM(NSUInteger, SigninAccessOption) {
    SigninAccessOptionNormal,
    SigninAccessOptionPayment,
    SigninAccessOptionFavorite,
    SigninAccessOptionFeaturedDeveloperGetAlert,
    SigninAccessOptionTabularSearchGetAlert,
    SigninAccessOptionBookSiteVisit,
    SigninAccessOptionProjectDetails,
    SigninAccessOptionUnitDescription,
    SigninAccessOptionPostCommentOnProject,
    SigninAccessOptionPostCommentOnUnit,
    SigninAccessOptionFavoriteSelectUnit,
    SigninAccessOptionFavoriteProjectCrousel,
    SigninAccessOptionProjectCarousel,
    SigninAccessOptionFavoriteSelectUnitTabular,
    SigninAccessOptionFavoriteFeaturedProject,
    SigninAccessOptionFavoriteDetailFeaturedDeveloper,
    SigninAccessOptionHomePage,
    SigninAccessOptionMisc,
};
@interface SigninViewController : UIViewController<UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate>
@property (assign, nonatomic) SigninAccessOption option;

- (void)loginForSocialMedia:(NSDictionary *)dict;
@end
