//
//  SigninViewController.m
//  Book My House
//
//  Created by Mayank Srivastava on 2/3/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "SigninViewController.h"
#import "FavouriteVC.h"


#define TAG_LOGIN 3

static NSString * const strEmailRequest                = @"Please enter email";
static NSString * const strCorrectEmailRequest         = @"Please enter email correctly";
static NSString * const strPasswordRequest             = @"Please enter password";
static NSString * const strCorrectPasswordRequest      = @"Password should not be less than 6 character";
static NSString * const strRedirectingPayment          = @"Redirecting to payment gateway.";

@interface SigninViewController () {
    
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UITextField *txtPassword;
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UIButton *btnForgotPassword;
    __weak IBOutlet UIButton *btnLogin;
    __weak IBOutlet UIButton *btnNewUser;
    __weak IBOutlet UIView *viewBottom;
    __weak IBOutlet UIButton *btnFacebook;
    __weak IBOutlet UIButton *btnGooglePlus;
    NSString *strEmailId;
    
    int direction;
    int shakes;
    
    NSUserDefaults *pref;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation SigninViewController


#pragma mark - UIViewController Mathods
- (void)viewDidLoad {
    strEmailId = @"";
    [super viewDidLoad];
    pref = [NSUserDefaults standardUserDefaults];
    [self screensettings];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPrefillEmail object:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Screen Settings
- (void)screensettings {
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self.navigationController setNavigationBarHidden:YES animated:YES];

    [self setTextFieldFormat:txtEmail];
    [self setTextFieldFormat:txtPassword];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    

    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:@"Forgot Password?" attributes:@{
                                        NSForegroundColorAttributeName : [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0],
                                        NSUnderlineStyleAttributeName : [NSNumber numberWithInteger:1.0]
                                                                                                              }];
    btnForgotPassword.titleLabel.attributedText = attrStr;
    
    
    //Customising New user button
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Not a member? " attributes:@{
                                                                                                                     
        NSForegroundColorAttributeName : [UIColor lightGrayColor],
        NSFontAttributeName :[UIFont fontWithName:@"PT Sans" size:16.0]
        }];
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Create Account" attributes:@{
                                                                                                         
    NSForegroundColorAttributeName : [UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0],
    
    NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:16.0]
    }];
    
    [str appendAttributedString:str1];
    
    [btnNewUser setAttributedTitle:str forState:UIControlStateNormal];
    
    btnNewUser.superview.layer.borderWidth = 0.5;
    btnNewUser.superview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    btnLogin.layer.borderWidth = 1.5;
    btnLogin.layer.borderColor = [UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}


#pragma mark - Action Listener
- (IBAction)actionListener:(id)sender {
    
    if ([sender isKindOfClass:[UIButton class]]) {
        
        if (sender == btnBack) {
            
            if (_option == SigninAccessOptionPayment) {
                [self.view makeToast:strRedirectingPayment
                                                                      duration:duration
                                                                      position:CSToastPositionCenter];
                 
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else if (sender == btnForgotPassword) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveEmailId:) name:kPrefillEmail object:nil];
        }
        else if (sender == btnLogin) {
            [self didClickedLogin];
        }
        else if (sender == btnNewUser) {
            BOOL isRegisterViewControllerExist = NO;
            for (RegisterViewController *viewController in self.navigationController.viewControllers) {
                
                if ([viewController isKindOfClass:[RegisterViewController class]]) {
                    isRegisterViewControllerExist = YES;
                    break;
                }
                else {
                    
                }
            }
            
            if (isRegisterViewControllerExist) {
                for (RegisterViewController *viewController in self.navigationController.viewControllers) {
                    
                    if ([viewController isKindOfClass:[RegisterViewController class]]) {
                        RegisterViewController *obj = (RegisterViewController *)viewController;
                        [self.navigationController popToViewController:obj animated:YES];
                        break;
                    }
                    else {
                        
                    }
                }
            }
            else {
                RegisterViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
                [self.navigationController pushViewController:obj animated:YES];
            }
        }
        else if (sender == btnFacebook) {
            [self signInWithFacebook];
        }
        else if (sender == btnGooglePlus) {
            [self signInWithGooglePlus];
        }
    }
}

- (void)didClickedLogin {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        direction = 1;
        shakes = 0;
        if (txtEmail.text.length == 0) {
            [self.view makeToast:strEmailRequest
                                             duration:duration
                                             position:CSToastPositionCenter];
            [self shake:txtEmail];
        }
        else if (![AuxillaryMathods isValidEmail:txtEmail.text]) {
            
            [self.view makeToast:strCorrectEmailRequest
                                             duration:duration
                                             position:CSToastPositionCenter];
            [self shake:txtEmail];
        }
        else if (txtPassword.text.length == 0) {
            [self.view makeToast:strPasswordRequest
                                             duration:duration
                                             position:CSToastPositionCenter];
            [self shake:txtPassword];
        }
        else if (![AuxillaryMathods isValidPassword:txtPassword.text]) {
            
            [self.view makeToast:strCorrectPasswordRequest
                                             duration:duration
                                             position:CSToastPositionCenter];
            [self shake:txtPassword];
        }
        else {
           [[AFNetworkReachabilityManager sharedManager] startMonitoring];
#if (TARGET_IPHONE_SIMULATOR)
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:txtEmail.text forKey:@"email"];
            [dict setObject:txtPassword.text forKey:@"password"];
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI signInUserWithParams:dict
                                  mathodName:urlSignIn
                                   baseClass:self
                             andCallingClass:@selector(didReceiveSignInResponse:)];
            
#else
            if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:txtEmail.text forKey:@"email"];
                [dict setObject:txtPassword.text forKey:@"password"];
                
                
                
                [ServiceAPI signInUserWithParams:dict
                                      mathodName:urlSignIn
                                       baseClass:self
                                 andCallingClass:@selector(didReceiveSignInResponse:)];
            }
            else {
                [self.view makeToast:strInternetNotAvailable
                                                 duration:duration
                                                 position:CSToastPositionCenter];
            }
#endif
            
            
            
        }

    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(didClickedLogin) fromClass:self];
    }
    
    
}

- (void)hideShowPassword:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    txtPassword.secureTextEntry = sender.selected;
    [txtPassword becomeFirstResponder];
}


#pragma mark - API Actions -
- (void)didReceiveSignInResponse:(id)response {
    
    [[AppDelegate share] enableUserInteraction];
    if (response && [response isKindOfClass:[NSDictionary class]]) {
        
        
        if ([[response objectForKey:@"success"] boolValue]) {

            [pref setObject:test_mode_Host forKey:test_mode];
            if (![[response objectForKey:@"user_id"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[NSString stringWithFormat:@"%@",[response objectForKey:@"user_id"]] forKey:strSaveUserId];
            }
            if (![[response objectForKey:@"user_login"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[response objectForKey:@"user_login"] forKey:strSaveUserLogin];
            }
            if (![[response objectForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[response objectForKey:@"first_name"] forKey:strSavedName];
            }
            if (![[response objectForKey:@"user_login"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[response objectForKey:@"user_login"] forKey:strSaveUserEmail];
            }
            if (![[response objectForKey:@"user_type"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[response objectForKey:@"user_type"] forKey:strSaveUserType];
            }
            if (![[response objectForKey:@"user_id"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[NSString stringWithFormat:@"%@",[response objectForKey:@"user_id"]] forKey:strSaveUserId];
            }
            if (![[response objectForKey:@"is_token_paid"] isKindOfClass:[NSNull class]]) {
                [pref setObject:[response objectForKey:@"is_token_paid"] forKey:strSaveUserIsTokenPaid];
            }
            
            if ([pref synchronize]) {
                
                if (_option == SigninAccessOptionNormal) {
                    SearchViewController *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
                    search.option = SearchViewControllerAccessOptionSignIn;
                    [self.navigationController pushViewController:search animated:YES];
                    
                }
                else if (_option == SigninAccessOptionPayment){
                    PaymentVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
                    [self.navigationController pushViewController:obj animated:YES];
                }
                else if (_option == SigninAccessOptionFavorite) {
                    
                    FavouriteVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteVC"];
                    [self.navigationController pushViewController:obj animated:YES];
                }
                else if (_option == SigninAccessOptionFeaturedDeveloperGetAlert) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGetAlertOnFeaturedDeveloper object:nil];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else if (_option == SigninAccessOptionTabularSearchGetAlert) {
                     [[NSNotificationCenter defaultCenter] postNotificationName:kGetAlertOnTabularScreen object:nil];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else if (_option == SigninAccessOptionBookSiteVisit) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kBookSiteVisit object:nil];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else if (_option == SigninAccessOptionProjectDetails) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteProjectDetail object:nil];
                }
                else if (_option == SigninAccessOptionUnitDescription) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteUnit object:nil];
                }
                else if (_option == SigninAccessOptionPostCommentOnProject) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPostCommentOnProjectDetails object:nil];
                }
                else if (_option == SigninAccessOptionPostCommentOnUnit) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPostCommentOnUnitDetails object:nil];
                }
                else if (_option == SigninAccessOptionFavoriteSelectUnit) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteUnitFromUnitSelect object:nil];
                }
                else if (_option == SigninAccessOptionFavoriteSelectUnitTabular) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteUnitFromUnitSelectTabular object:nil];
                }
                else if (_option == SigninAccessOptionFavoriteFeaturedProject) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFeaturedProjectFavorite object:nil];
                }
                else if (_option == SigninAccessOptionFavoriteDetailFeaturedDeveloper) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFeaturedDeveloperDetailsFav object:nil];
                }
                else if (_option == SigninAccessOptionFavoriteProjectCrousel) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteProjectCarousel object:nil];
                }
                else if (_option == SigninAccessOptionHomePage) {
                    
                    for (UIViewController *obj in self.navigationController.viewControllers) {
                        
                        if ([obj isKindOfClass:[SearchViewController class]]) {
                            
                            SearchViewController *objSearchVC = (SearchViewController *)obj;
                            [self.navigationController popToViewController:objSearchVC animated:NO];
                            
                        }
                    }
                    
                }
            }
            
        }
        else {
            if (![[response objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                
                [self.view makeToast:[response objectForKey:@"message"]
                                                 duration:duration
                                                 position:CSToastPositionCenter];
            }
        }
    }
    else {
       [self.view makeToast:strSomeErrorOccured duration:duration position:CSToastPositionCenter];
    }
}

#pragma mark - Auxillary mathods
- (void)setTextFieldFormat:(UITextField *)txtFields {
    
    txtFields.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(txtFields.frame))];
    txtFields.leftViewMode = UITextFieldViewModeAlways;
    txtFields.backgroundColor = [UIColor clearColor];
    
    if (txtFields == txtPassword) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, CGRectGetHeight(txtFields.frame))];
        view.backgroundColor = [UIColor clearColor];
        UIButton *btn = [[UIButton alloc] initWithFrame:view.frame];
        btn.titleLabel.font = [UIFont fontWithName:@"PT Sans" size:16.0];
        [btn setTitleColor:[UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [btn setTitle:@"Show" forState:UIControlStateSelected];//
        [btn setTitle:@"Hide" forState:UIControlStateNormal];
        btn.selected = YES;
        [btn addTarget:self action:@selector(hideShowPassword:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        
        txtFields.rightView = view;
        txtFields.rightViewMode = UITextFieldViewModeWhileEditing;
        
    }
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, CGRectGetHeight(txtFields.frame) - 2, CGRectGetWidth(txtFields.frame), 2);
    layer.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:243.0/255.0 blue:233.0/255.0 alpha:1.0].CGColor;
    [txtFields.layer addSublayer:layer];
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField == txtEmail) {
        return newLength <= 100;
    }
    else {
        return newLength <= 20;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == txtEmail) {
        [txtPassword becomeFirstResponder];
    }
    else if (textField == txtPassword) {
        [textField resignFirstResponder];
        
        UIButton *btn = [[UIButton alloc] init];
        btn.tag = TAG_LOGIN;
        [self actionListener:btn];
    }
    return YES;
}


#pragma mark - Touch Handling
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
}


#pragma mark - Shake text Field
-(void)shake:(UITextField *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5 * direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

#pragma mark - Facebook Login
- (void)signInWithFacebook {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        [[AppDelegate share] signInWithFacebook:self withCallBack:^(BOOL isSuccess, NSDictionary *dict) {
            
            [pref setObject:test_mode_facebook forKey:test_mode];
            [ServiceAPI loginWithSocialMedia:dict andCallback:^(BOOL isSuccess, NSDictionary *responseData, NSError *error) {
                [[AppDelegate share] enableUserInteraction];
                
                if (isSuccess) {
                    //[pref removeObjectForKey:strSaveCityInfo];
                }
                [self transitToSearchView:isSuccess andData:responseData];
                
            }];
        }];
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(signInWithFacebook) fromClass:self];
    }
}

- (void)loginForSocialMedia:(NSDictionary *)dict {
    [pref setObject:test_mode_facebook forKey:test_mode];
    [ServiceAPI loginWithSocialMedia:dict andCallback:^(BOOL isSuccess, NSDictionary *responseData, NSError *error) {
        [[AppDelegate share] enableUserInteraction];
        
        if (isSuccess) {
            //[pref removeObjectForKey:strSaveCityInfo];
        }
        [self transitToSearchView:isSuccess andData:responseData];
        
    }];
}

#pragma mark - Auxillary Mathods
- (void)transitToSearchView:(BOOL)isSuccess andData:(NSDictionary *)responseData {
    
    if (isSuccess) {
        
        if (![responseData isKindOfClass:[NSNull class]] && [responseData isKindOfClass:[NSDictionary class]]) {
            
            if ([[responseData objectForKey:@"success"] boolValue]) {
                
                if (![[responseData objectForKey:@"user_id"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"user_id"] forKey:strSaveUserId];
                }
                if (![[responseData objectForKey:@"user_login"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"user_login"] forKey:strSaveUserLogin];
                }
                if (![[responseData objectForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"first_name"] forKey:strSavedName];
                }
                if (![[responseData objectForKey:@"user_login"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"user_login"] forKey:strSaveUserEmail];
                }
                if (![[responseData objectForKey:@"user_type"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"user_type"] forKey:strSaveUserType];
                }
                if (![[responseData objectForKey:@"user_id"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"user_id"] forKey:strSaveUserId];
                }
                if (![[responseData objectForKey:@"is_token_paid"] isKindOfClass:[NSNull class]]) {
                    [pref setObject:[responseData objectForKey:@"is_token_paid"] forKey:strSaveUserIsTokenPaid];
                }
                
                if ([pref synchronize]) {
                    
                    
                    if (_option == SigninAccessOptionNormal) {
                        SearchViewController *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
                        search.option = SearchViewControllerAccessOptionSignIn;
                        [self.navigationController pushViewController:search animated:YES];
                    }
                    else if (_option == SigninAccessOptionPayment){
                        PaymentVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
                        [self.navigationController pushViewController:obj animated:YES];
                    }
                    else if (_option == SigninAccessOptionFavorite) {
//                        FavouriteVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteVC"];
//                        [self.navigationController pushViewController:obj animated:YES];
                        
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                    else if (_option == SigninAccessOptionFeaturedDeveloperGetAlert) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kGetAlertOnFeaturedDeveloper object:nil];
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                    else if (_option == SigninAccessOptionTabularSearchGetAlert) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kGetAlertOnTabularScreen object:nil];
                    }
                    else if (_option == SigninAccessOptionBookSiteVisit) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kBookSiteVisit object:nil];
                    }//
                    else if (_option == SigninAccessOptionProjectDetails) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteProjectDetail object:nil];
                    }
                    else if (_option == SigninAccessOptionUnitDescription) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteUnit object:nil];
                    }
                    else if (_option == SigninAccessOptionPostCommentOnProject) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kPostCommentOnProjectDetails object:nil];
                    }//
                    else if (_option == SigninAccessOptionPostCommentOnUnit) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kPostCommentOnUnitDetails object:nil];
                    }
                    else if (_option == SigninAccessOptionFavoriteSelectUnit) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteUnitFromUnitSelect object:nil];
                    }
                    else if (_option == SigninAccessOptionFavoriteSelectUnitTabular) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteUnitFromUnitSelectTabular object:nil];
                    }
                    else if (_option == SigninAccessOptionFavoriteFeaturedProject) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFeaturedProjectFavorite object:nil];
                    }
                    else if (_option == SigninAccessOptionFavoriteDetailFeaturedDeveloper) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFeaturedDeveloperDetailsFav object:nil];
                    }
                    else if (_option == SigninAccessOptionFavoriteProjectCrousel) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteProjectCarousel object:nil];
                    }
                    else if (_option == SigninAccessOptionHomePage) {
//                        [self dismissViewControllerAnimated:YES completion:nil];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:kFeaturedDeveloperDetailsFav object:nil];
                        
                        for (UIViewController *obj in self.navigationController.viewControllers) {
                            
                            if ([obj isKindOfClass:[SearchViewController class]]) {
                                
                                SearchViewController *objSearchVC = (SearchViewController *)obj;
                                [self.navigationController popToViewController:objSearchVC animated:NO];
                                
                            }


                        }
                        
                    }
                }
                
            }
            else {
                if (![[responseData objectForKey:@"message"] isKindOfClass:[NSNull class]]) {
                    
                    [self.view makeToast:[responseData objectForKey:@"message"]
                                                     duration:duration
                                                     position:CSToastPositionCenter];
                }
            }
            
        }
        else {
            [self.view makeToast:@"Fail to sign in via google plus" duration:duration position:CSToastPositionCenter];
        }
        
    }
    else {
        [self.view makeToast:@"Fail to sign in via google plus" duration:duration position:CSToastPositionCenter];
    }
    
}
#pragma mark - Google Plus Login
- (void)signInWithGooglePlus {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        NSError* configureError;
        [[GGLContext sharedInstance] configureWithError: &configureError];
        NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
        
        [[GIDSignIn sharedInstance] signIn];
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(signInWithGooglePlus) fromClass:self];
    }
    
    
}

#pragma mark - Google Plus Delegate mathods

// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [[AppDelegate share] enableUserInteraction];
    NSLog(@"Sign in %@", error.localizedDescription);
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    
    [[AppDelegate share] enableUserInteraction];
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [[AppDelegate share] disableUserInteractionwithLoader:YES];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    if (!error) {
        
        if (![user isKindOfClass:[NSNull class]]) {
            [pref setObject:test_mode_google forKey:test_mode];
            NSString *userId = user.userID;
            NSString *idToken = user.authentication.idToken;
            NSString *name = user.profile.name;
            NSString *email = user.profile.email;
            
            NSLog(@"UserId: %@\nidToken: %@\nname: %@\nemail: %@\n", userId, idToken, name, email);
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            
            if (![user.profile.name isKindOfClass:[NSNull class]]) {
                [dict setObject:user.profile.name forKey:@"username"];
            }
            if (![user.profile.email isKindOfClass:[NSNull class]]) {
                [dict setObject:user.profile.email forKey:@"email"];
            }
            if (![user.userID isKindOfClass:[NSNull class]]) {
                [dict setObject:user.userID forKey:@"gl_id"];
            }
            
            [[AppDelegate share] disableUserInteractionwithLoader:YES];
            [ServiceAPI loginWithSocialMedia:dict andCallback:^(BOOL isSuccess, NSDictionary *responseData, NSError *error) {
                
                
                [[AppDelegate share] enableUserInteraction];
                
                if (isSuccess) {
                    
                   // [pref removeObjectForKey:strSaveCityInfo];
                }
                [self transitToSearchView:isSuccess andData:responseData];
                
            }];
        }
        else {
            [[AppDelegate share] enableUserInteraction];
            [self.view makeToast:@"Some error occured"
                                             duration:duration position:CSToastPositionCenter];
        }
        
        
    }
    else {
        [[AppDelegate share] enableUserInteraction];
        [self.view makeToast:error.localizedDescription
                                         duration:duration position:CSToastPositionCenter];
    }
    
}

#pragma mark - Notification Mathods
- (void)receiveEmailId:(NSNotification *)info {
    txtEmail.text = info.object;
    
}
@end
