//
//  The360DegreeSiteVisit.h
//  Book My House
//
//  Created by Mayank Srivastava on 6/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface The360DegreeSiteVisit : UIViewController<UIWebViewDelegate>

@end
