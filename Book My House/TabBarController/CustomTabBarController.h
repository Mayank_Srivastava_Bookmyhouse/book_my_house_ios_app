//
//  CustomTabBarController.h
//  Book My House
//
//  Created by Mayank Srivastava on 6/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabBarController : UITabBarController
@property (strong, nonatomic) NSString *strProjectName;
@property (strong, nonatomic) NSString *strAddress;
@property (strong, nonatomic) NSString *strID;

@property (strong, nonatomic) NSArray *arrPhotos, *arrVideos, *arrconstruction_update, *arrvirtual_site_visit, *arr360_degree_tour;

@end
