//
//  CustomTabBarController.m
//  Book My House
//
//  Created by Mayank Srivastava on iss/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "CustomTabBarController.h"
#import "Header.h"
#import "PhotoVC.h"
#import "VideoVC.h"
#import "ConstructionUpdateVC.h"
#import "VirtualSiteVisitVC.h"
#import "The360DegreeSiteVisit.h"


@interface CustomTabBarController ()

@end

@implementation CustomTabBarController
- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
    }
    
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadProjectAlbum];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:219.0/255.0 green:157.0/255.0 blue:41.0/255.0 alpha:1.0]]; // for unselected items that are gray
    [[UITabBar appearance] setUnselectedItemTintColor:[UIColor colorWithRed:13.0/255.0 green:147.0/255.0 blue:124.0/255.0 alpha:1.0]];
}

- (void)loadProjectAlbum {
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
        [[AppDelegate share] disableNoInternetAlert];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:_strID forKey:@"project_id"];
        [[AppDelegate share] disableUserInteractionwithLoader:YES];
        [ServiceAPI getProjectAlbum:dict andCallback:^(BOOL isSuccess, id data) {
            
            if (isSuccess) {
                
                NSArray *arr = (NSArray *)data;
                NSMutableArray *arr1 = [NSMutableArray array];
                NSMutableArray *arr2 = [NSMutableArray array];
                NSMutableArray *arr3 = [NSMutableArray array];
                NSMutableArray *arr4 = [NSMutableArray array];
                
                NSMutableArray *arr5 = [NSMutableArray array];
                
                for (NSDictionary *dict in arr) {
                    
                    
                    if ([[dict objectForKey:@"gallary_type"] isEqualToString:@"Image"]) {
                        [arr1 addObject:dict];
                    }
                    else if ([[dict objectForKey:@"gallary_type"] isEqualToString:@"Video"]) {
                        [arr2 addObject:dict];
                    }
                    else if ([[dict objectForKey:@"gallary_type"] isEqualToString:@"Construction_Update"]) {
                        [arr3 addObject:dict];
                    }
                    else if ([[dict objectForKey:@"gallary_type"] isEqualToString:@"Virtual_Site_Visit"]) {
                        [arr4 addObject:dict];
                    }
                    else if ([[dict objectForKey:@"gallary_type"] isEqualToString:@"360_Degree_Tour"]) {
                        [arr5 addObject:dict];
                    }
                }
                
                _arrPhotos = [NSArray arrayWithArray:arr1];
                _arrVideos = [NSArray arrayWithArray:arr2];
                _arrconstruction_update = [NSArray arrayWithArray:arr3];
                _arrvirtual_site_visit = [NSArray arrayWithArray:arr4];
                _arr360_degree_tour = [NSArray arrayWithArray:arr5];
            
                PhotoVC *obj = (PhotoVC *)self.selectedViewController;
                [obj updatedScreenSettings];
                
                
                if (_arrVideos.count == 0) {
                    NSMutableArray *arrViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
                    for (UIViewController *obj in arrViewControllers) {
                        
                        if ([obj isKindOfClass:[VideoVC class]]) {
                            [arrViewControllers removeObject:obj];
                            break;
                        }
                    }
                    self.viewControllers = arrViewControllers;
                }
                
                if (_arrconstruction_update.count == 0) {
                    NSMutableArray *arrViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
                    
                    for (UIViewController *obj in arrViewControllers) {
                        
                        if ([obj isKindOfClass:[ConstructionUpdateVC class]]) {
                            [arrViewControllers removeObject:obj];
                            break;
                        }
                    }
                    
                    self.viewControllers = arrViewControllers;
                }
                
                if (_arrvirtual_site_visit.count == 0) {
                    NSMutableArray *arrViewcontrollers = [NSMutableArray arrayWithArray:self.viewControllers];
                    
                    for (UIViewController *obj in arrViewcontrollers) {
                        
                        if ([obj isKindOfClass:[VirtualSiteVisitVC class]]) {
                            [arrViewcontrollers removeObject:obj];
                            break;
                        }
                    }
                    self.viewControllers = arrViewcontrollers;
                }
                
                if (_arr360_degree_tour.count == 0) {
                    NSMutableArray *arrViewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
                    for (UIViewController *obj in arrViewControllers) {
                        
                        if ([obj isKindOfClass:[The360DegreeSiteVisit class]]) {
                            [arrViewControllers removeObject:obj];
                            break;
                        }
                    }
                    
                    self.viewControllers = arrViewControllers;
                }
                
                
                if (self.viewControllers.count == 0) {
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.view.window makeToast:@"No Images in the Galary" duration:duration position:CSToastPositionCenter];
                }
                
                [[AppDelegate share] enableUserInteraction];
            }
            else {
                
            }
        }];
        
    }
    else {
        [[AppDelegate share] enableNoInternetAlert:@selector(loadProjectAlbum) fromClass:self];
    }
    
    
    
}

@end
