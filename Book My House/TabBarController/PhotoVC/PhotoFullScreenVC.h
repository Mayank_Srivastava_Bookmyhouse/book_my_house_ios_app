//
//  PhotoFullScreenVC.h
//  Book My House
//
//  Created by Mayank Srivastava on 6/24/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoFullScreenVC : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) NSArray *arrImages;
@property (assign, nonatomic) NSInteger index;
@end
