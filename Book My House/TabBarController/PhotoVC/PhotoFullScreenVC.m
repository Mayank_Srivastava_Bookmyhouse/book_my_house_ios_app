//
//  PhotoFullScreenVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 6/24/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PhotoFullScreenVC.h"
#import "Header.h"

@interface PhotoFullScreenVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIButton *btnLeft;
    __weak IBOutlet UIButton *btnRight;
    __weak IBOutlet UICollectionView *colView;
}
- (IBAction)actionListener:(id)sender;

@end

@implementation PhotoFullScreenVC

#pragma mark - View Controller Mahods
- (BOOL)prefersStatusBarHidden {
    return YES;
}




- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self screenSettings];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    colView.delegate = self;
    colView.dataSource = self;
    [colView reloadData];
    [colView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings 
- (void)screenSettings {
    
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = btnBack.superview.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:43.0/255.0 green:42.0/255.0 blue:40.0/255.0 alpha:0.8] CGColor], (id)[[UIColor colorWithRed:43.0/255.0 green:42.0/255.0 blue:40.0/255.0 alpha:0.0] CGColor],  nil];
    
    [btnBack.superview.layer insertSublayer:gradient atIndex:0];
    
}

- (void)updatedScreenSettings {
    
}

#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnLeft) {
        NSIndexPath *currentIndexPath = [[colView indexPathsForVisibleItems] firstObject];
        if (currentIndexPath.item > 0) {
            NSIndexPath *prevIndexPath = [NSIndexPath indexPathForItem:currentIndexPath.item - 1 inSection:0];
            [colView scrollToItemAtIndexPath:prevIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnRight) {
        NSIndexPath *currentIndexPath = [[colView indexPathsForVisibleItems] firstObject];
        if (currentIndexPath.item < _arrImages.count - 1) {
            NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:currentIndexPath.item + 1 inSection:0];
            [colView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Collection view Delegates and DataSourse

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *img = (UIImageView *)[cell viewWithTag:4];
    
    
    
    NSDictionary *dict = [_arrImages objectAtIndex:indexPath.item];
    lblTitle.text = [NSString stringWithFormat:@"Photo %d of %d", (int)indexPath.item + 1, (int)_arrImages.count];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(img.frame), CGRectGetHeight(img.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"url"]]];
    
    [img setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
    
    if (_arrImages.count == 1) {
        btnLeft.hidden = YES;
        btnRight.hidden = YES;
    }
    else {
        if (indexPath.row == 0) {
            btnLeft.hidden = YES;
            btnRight.hidden = !btnLeft.hidden;
        }
        else if (indexPath.row == _arrImages.count - 1) {
            btnRight.hidden = YES;
            btnLeft.hidden = !btnRight.hidden;
        }
        else if (indexPath.row > 0 && indexPath.row < _arrImages.count - 1) {
            btnLeft.hidden = NO;
            btnRight.hidden = NO;
            
        }
    }
    
    return cell;
}


#pragma mark - Auxillary Mathods
@end
