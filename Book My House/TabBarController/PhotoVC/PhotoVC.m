//
//  PhotoVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 6/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "PhotoVC.h"
#import "CustomTabBarController.h"
#import "PhotoFullScreenVC.h"

@interface PhotoVC (){
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UIButton *btnTopLeft;
    __weak IBOutlet UIButton *btnTopRight;
    
    __weak IBOutlet UIButton *btnBottomLeft;
    __weak IBOutlet UIButton *btnBottomRight;
    
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UICollectionView *colMain;
    __weak IBOutlet UICollectionView *colNavig;
    __weak IBOutlet UILabel *lblCaption;
    
    CustomTabBarController *tabController;
    CGPoint imagePoint;
    
}
- (IBAction)actionListener:(id)sender;

@end

@implementation PhotoVC

#pragma mark - View Controller Mathods
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)loadView {
    [super loadView];
}

- (void)viewDidLoad {
    ;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self screenSettings];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}

#pragma mark - Screen Settings
- (void)screenSettings {
    
    [self setNeedsStatusBarAppearanceUpdate];
    tabController = (CustomTabBarController *)self.tabBarController;
    
    
    NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:tabController.strProjectName
                                                                                 attributes:@{
                                                                                              NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                              NSFontAttributeName : [UIFont fontWithName:@"PT Sans" size:20.0],
                                                                                              }];
    
    NSMutableAttributedString *strAddress = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", tabController.strAddress]
                                                                                   attributes:@{
                                                                                                NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                NSFontAttributeName :[UIFont fontWithName:@"PT Sans" size:15.0],
                                                                                                }];
    [strTitle appendAttributedString:strAddress];
    lblTitle.attributedText = strTitle;

    self.tabBarController.tabBarItem.title = [NSString stringWithFormat:@"Images (%d)", (int)tabController.arrPhotos.count];
    
}

- (void)updatedScreenSettings {
   
    [colMain reloadData];
    [colNavig reloadData];
}


#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    if (sender == btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if (sender == btnTopLeft) {
        NSIndexPath *selectedIndexPath = [[colMain indexPathsForVisibleItems] firstObject];
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item - 1 inSection:selectedIndexPath.section];
        if (previousIndexPath.item >= 0) {
            [colNavig scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnTopRight) {
        NSIndexPath *selectedIndexPath = [[colMain indexPathsForVisibleItems] firstObject];
        NSUInteger count = [colMain numberOfItemsInSection:0];
        NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item + 1 inSection:selectedIndexPath.section];
        
        if (nextIndexPath.item < count) {
            [colNavig scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnBottomLeft) {
        NSIndexPath *selectedIndexPath = [[colNavig indexPathsForSelectedItems] firstObject];
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item - 1 inSection:selectedIndexPath.section];
        if (previousIndexPath.item >= 0) {
            [colNavig scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnBottomRight) {
        NSIndexPath *selectedIndexPath = [[colNavig indexPathsForSelectedItems] firstObject];
        NSUInteger count = [colNavig numberOfItemsInSection:0];
        NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item + 1 inSection:selectedIndexPath.section];
        
        if (nextIndexPath.item < count) {
            [colNavig scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
}

#pragma mark - Collectionview Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == colMain) {
        return CGSizeMake(CGRectGetWidth(collectionView.frame) - 20, CGRectGetHeight(collectionView.frame));
    }
    else {
        return CGSizeMake(136, 72);
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (collectionView == colMain) {
        return tabController.arrPhotos.count;
    }
    else if (collectionView == colNavig) {
        return tabController.arrPhotos.count;
    }
    else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [tabController.arrPhotos objectAtIndex:indexPath.row];
    if (collectionView == colMain) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
        
        
        UIImageView *img = (UIImageView *)[cell viewWithTag:1];
        imagePoint = [img convertPoint:img.frame.origin toView:self.view];
        
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(img.frame), CGRectGetHeight(img.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"url"]]];
        [img setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        
        if (tabController.arrPhotos.count == 1) {
            btnTopLeft.hidden = YES;
            btnTopRight.hidden = YES;
        }
        else {
            if (indexPath.row == 0) {
                btnTopLeft.hidden = YES;
                btnTopRight.hidden = !btnTopLeft.hidden;
            }
            else if (indexPath.row == tabController.arrPhotos.count - 1) {
                btnTopRight.hidden = YES;
                btnTopLeft.hidden = !btnTopRight.hidden;
            }
            else if (indexPath.row > 0 && indexPath.row < tabController.arrPhotos.count - 1) {
                btnTopLeft.hidden = NO;
                btnTopRight.hidden = NO;
                
            }
        }
        
        return cell;
    }
    else if (collectionView == colNavig) {
        
        UICollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"cell2" forIndexPath:indexPath];
        UIImageView *img = (UIImageView *)[cell viewWithTag:1];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@?w=%f&h=%f&img=%@", baseImageUrl, extension, CGRectGetWidth(img.frame), CGRectGetHeight(img.frame), [NSString stringWithFormat:@"%@%@", baseIconUrl, [dict objectForKey:@"url"]]];
        [img setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        cell.selectedBackgroundView = [self setBackgroundView:cell];
        
        if (tabController.arrPhotos.count == 1) {
            btnBottomLeft.hidden = YES;
            btnBottomRight.hidden = YES;
        }
        else {
            if (indexPath.row == 0) {
                btnBottomLeft.hidden = YES;
                btnBottomRight.hidden = !btnBottomLeft.hidden;
            }
            else if (indexPath.row == tabController.arrPhotos.count - 1) {
                btnBottomRight.hidden = YES;
                btnBottomLeft.hidden = !btnBottomRight.hidden;
            }
            else if (indexPath.row > 0 && indexPath.row < tabController.arrPhotos.count - 1) {
                btnBottomLeft.hidden = NO;
                btnBottomRight.hidden = NO;
                
            }
        }
        
        return cell;
    }
    else {
        return nil;
    }
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == colMain) {
        PhotoFullScreenVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoFullScreenVC"];
        obj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        obj.modalPresentationStyle = UIModalPresentationFullScreen;
        obj.arrImages = tabController.arrPhotos;
        obj.index = indexPath.item;
        [self presentViewController:obj animated:YES completion:nil];
    }
    else {

        [colMain scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [colNavig scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == colMain) {

        [colNavig selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        [colNavig scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
        lblCaption.frame = CGRectMake(imagePoint.x, imagePoint.y, CGRectGetWidth(lblCaption.frame), CGRectGetHeight(lblCaption.frame));
        lblCaption.text = [NSString stringWithFormat:@"Photo %d of %d", (int)indexPath.row + 1, (int)tabController.arrPhotos.count];
    }
    else {
        
    }
}



#pragma mark - Auxillary Mathods
- (UIView *)setBackgroundView:(UICollectionViewCell *)cell {
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame))];
    aView.backgroundColor = [UIColor clearColor];
    aView.layer.borderWidth = 5.0;
    aView.layer.borderColor = [UIColor colorWithRed:26.0/255.0 green:152.0/255.0 blue:58.0/255.0 alpha:1.0].CGColor;
    return aView;
}
@end
