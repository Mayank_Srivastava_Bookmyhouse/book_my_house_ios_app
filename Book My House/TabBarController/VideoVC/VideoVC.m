//
//  VideoVC.m
//  Book My House
//
//  Created by Mayank Srivastava on 6/12/16.
//  Copyright © 2016 Mayank Srivastava. All rights reserved.
//

#import "VideoVC.h"
#import "CustomTabBarController.h"
#import "Header.h"
//#import "VideoFullScreenVC.h"

@interface VideoVC () {
    
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UICollectionView *colMain;
    __weak IBOutlet UIButton *btnTopLeft;
    __weak IBOutlet UIButton *btnTopRight;
    __weak IBOutlet UICollectionView *colNavig;
    __weak IBOutlet UIButton *btnBottomLeft;
    __weak IBOutlet UIButton *btnBottomRight;
    __weak IBOutlet UILabel *lblCaption;

    CustomTabBarController *tabController;

}
- (IBAction)actionListener:(id)sender;

@end

@implementation VideoVC

#pragma mark - View Controller Mathods
- (void)loadView {
    [super loadView];

}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self screenSettings];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    
}


#pragma mark - Screen Settings
- (void)screenSettings {
    [self setNeedsStatusBarAppearanceUpdate];
    
    tabController = (CustomTabBarController *)self.tabBarController;
    
    
    NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:tabController.strProjectName
                                                                                 attributes:@{
                                                                                              NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                              NSFontAttributeName :[UIFont fontWithName:@"PT Sans" size:20.0],
                                                                                              }];
    
    NSMutableAttributedString *strAddress = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", tabController.strAddress]
                                                                                   attributes:@{
                                                                                                NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                NSFontAttributeName :[UIFont fontWithName:@"PT Sans" size:15.0],
                                                                                                }];
    [strTitle appendAttributedString:strAddress];
    lblTitle.attributedText = strTitle;
    self.tabBarController.tabBarItem.title = [NSString stringWithFormat:@"Videos (%d)", (int)tabController.arrVideos.count];
}


#pragma mark - Button Actions
- (IBAction)actionListener:(id)sender {
    
    if (sender == btnBack) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender == btnTopLeft) {
        NSIndexPath *selectedIndexPath = [[colMain indexPathsForVisibleItems] firstObject];
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item - 1 inSection:selectedIndexPath.section];
        if (previousIndexPath.item >= 0) {
            [colNavig scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnTopRight) {
        NSIndexPath *selectedIndexPath = [[colMain indexPathsForVisibleItems] firstObject];
        NSUInteger count = [colMain numberOfItemsInSection:0];
        NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item + 1 inSection:selectedIndexPath.section];
        
        if (nextIndexPath.item < count) {
            [colNavig scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnBottomLeft) {
        NSIndexPath *selectedIndexPath = [[colNavig indexPathsForSelectedItems] firstObject];
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item - 1 inSection:selectedIndexPath.section];
        if (previousIndexPath.item >= 0) {
            [colNavig scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:previousIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
    else if (sender == btnBottomRight) {
        NSIndexPath *selectedIndexPath = [[colNavig indexPathsForSelectedItems] firstObject];
        NSUInteger count = [colNavig numberOfItemsInSection:0];
        NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:selectedIndexPath.item + 1 inSection:selectedIndexPath.section];
        
        if (nextIndexPath.item < count) {
            [colNavig scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            [colMain scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
}

#pragma mark - Collectionview Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == colMain) {
        return CGSizeMake(CGRectGetWidth(collectionView.frame) - 20, CGRectGetHeight(collectionView.frame));
    }
    else {
        return CGSizeMake(136, 72);
    }
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return tabController.arrVideos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [tabController.arrVideos objectAtIndex:indexPath.row];
    if (collectionView == colMain) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
        
        
        UIImageView *img = (UIImageView *)[cell viewWithTag:1];
        NSString *strUrl = [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/hqdefault.jpg", [dict objectForKey:@"url"]];
       
        [img setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        if (tabController.arrVideos.count == 1) {
            btnTopLeft.hidden = YES;
            btnTopRight.hidden = YES;
        }
        else {
            if (indexPath.row == 0) {
                btnTopLeft.hidden = YES;
                btnTopRight.hidden = !btnTopLeft.hidden;
            }
            else if (indexPath.row == tabController.arrVideos.count - 1) {
                btnTopRight.hidden = YES;
                btnTopLeft.hidden = !btnTopRight.hidden;
            }
            else if (indexPath.row > 0 && indexPath.row < tabController.arrVideos.count - 1) {
                btnTopLeft.hidden = NO;
                btnTopRight.hidden = NO;
                
            }
        }
        
        return cell;
    }
    else if (collectionView == colNavig) {

        UICollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"cell2" forIndexPath:indexPath];
        UIImageView *img = (UIImageView *)[cell viewWithTag:1];
        NSString *strString = [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/hqdefault.jpg", [dict objectForKey:@"url"]];
        [img setImageWithURL:[NSURL URLWithString:strString] placeholderImage:[UIImage imageNamed:@"Place_Holder"]];
        
        cell.selectedBackgroundView = [self setBackgroundView:cell];
        
        if (tabController.arrVideos.count == 1) {
            btnBottomLeft.hidden = YES;
            btnBottomRight.hidden = YES;
        }
        else {
            if (indexPath.row == 0) {
                btnBottomLeft.hidden = YES;
                btnBottomRight.hidden = !btnBottomLeft.hidden;
            }
            else if (indexPath.row == tabController.arrVideos.count - 1) {
                btnBottomRight.hidden = YES;
                btnBottomLeft.hidden = !btnBottomRight.hidden;
            }
            else if (indexPath.row > 0 && indexPath.row < tabController.arrVideos.count - 1) {
                btnBottomLeft.hidden = NO;
                btnBottomRight.hidden = NO;
                
            }
        }
        
        return cell;
    }
    else {
        return nil;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == colMain) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@", [[tabController.arrVideos objectAtIndex:indexPath.item] objectForKey:@"url"]]]];
        
    }
    else {
        [colMain scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [colNavig scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == colMain) {
        
        [colNavig selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        [colNavig scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
        lblCaption.text = [NSString stringWithFormat:@"Video %d of %d", (int)indexPath.row + 1, (int)tabController.arrVideos.count];
    }
    else {
        
    }
}


#pragma mark - Auxillary Mathods
- (UIView *)setBackgroundView:(UICollectionViewCell *)cell {
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame))];
    aView.backgroundColor = [UIColor clearColor];
    aView.layer.borderWidth = 5.0;
    aView.layer.borderColor = [UIColor colorWithRed:26.0/255.0 green:152.0/255.0 blue:58.0/255.0 alpha:1.0].CGColor;
    return aView;
}
@end
